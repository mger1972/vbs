﻿<%@ Page Title="Raw Data" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="RawData.aspx.cs" Inherits="VBS.SysAdmin.RawData" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Raw Data</h1>
				<h3>Use the screen below to view information posted to the MyVBC site.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script type="text/javascript">
		var colName;
		function OnItemClick(s, e) {
			if (e.item.name == 'HideColumn') {
				grid.PerformCallback(colName);
				colName = null;
			}
			else {
				if (grid.IsCustomizationWindowVisible())
					grid.HideCustomizationWindow();
				else
					grid.ShowCustomizationWindow();
			}
		}

		function OnContextMenu(s, e) {
			if (e.objectType == 'header') {
				colName = s.GetColumn(e.index).fieldName;
				headerMenu.GetItemByName('HideColumn').SetEnabled((colName == null ? false : true));
				headerMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
			}
		}
    </script>
	<dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" 
		Width="100%" SettingsEditing-Mode="Batch" OnDataBinding="grid_DataBinding" OnCustomCallback="grid_CustomCallback"
		SettingsBehavior-FilterRowMode="OnClick" SettingsPager-PageSize="30" OnCommandButtonInitialize="grid_CommandButtonInitialize"
		SettingsBehavior-AllowSort="false" Settings-UseFixedTableLayout="true">
		<Columns>
			<dx:GridViewDataColumn FieldName="SheetType" VisibleIndex="1" /> 
			<dx:GridViewDataColumn FieldName="ResponseID" VisibleIndex="2" /> 
			<dx:GridViewDataColumn FieldName="LastName" VisibleIndex="3" /> 
			<dx:GridViewDataColumn FieldName="FirstName" VisibleIndex="4" /> 
			<dx:GridViewDataColumn FieldName="ContactPhone" Visible="false" VisibleIndex="5" /> 

			<dx:GridViewDataColumn FieldName="DateSubmitted" Visible="false" VisibleIndex="103" /> 
			<dx:GridViewDataColumn FieldName="DateModified" Visible="false" VisibleIndex="104" /> 
			<dx:GridViewDataColumn FieldName="ConfirmationNum" Visible="false" VisibleIndex="106" /> 
			<dx:GridViewDataColumn FieldName="Street" Visible="false" VisibleIndex="111" /> 
			<dx:GridViewDataColumn FieldName="City" Visible="false" VisibleIndex="112" /> 
			<dx:GridViewDataColumn FieldName="State" Visible="false" VisibleIndex="113" /> 
			<dx:GridViewDataColumn FieldName="Zip" Visible="false" VisibleIndex="114" /> 
			<dx:GridViewDataColumn FieldName="MobilePhone" Visible="false" VisibleIndex="115" /> 

			<dx:GridViewDataColumn FieldName="HomePhone" Visible="false" VisibleIndex="116" /> 
			<dx:GridViewDataColumn FieldName="WorkPhone" Visible="false" VisibleIndex="117" /> 
			<dx:GridViewDataColumn FieldName="Gender" Visible="false" VisibleIndex="118" /> 
			<dx:GridViewDataColumn FieldName="ContactByEmail" Visible="false" VisibleIndex="119" /> 
			<dx:GridViewDataColumn FieldName="ContactByPhone" Visible="false" VisibleIndex="120" /> 
			<dx:GridViewDataColumn FieldName="HelpInAreas" Visible="false" VisibleIndex="121" /> 
			<dx:GridViewDataColumn FieldName="HelpDOW" Visible="false" VisibleIndex="122" /> 
			<dx:GridViewDataColumn FieldName="PaymentStatus" Visible="false" VisibleIndex="123" /> 
			<dx:GridViewDataColumn FieldName="PaymentType" Visible="false" VisibleIndex="124" /> 
			<dx:GridViewDataColumn FieldName="FormTotal" Visible="false" VisibleIndex="125" /> 
			<dx:GridViewDataColumn FieldName="GradeInFall" Visible="false" VisibleIndex="126" /> 
			<dx:GridViewDataColumn FieldName="GradeToHelpIn" Visible="false" VisibleIndex="127" /> 
			<dx:GridViewDataColumn FieldName="HelpAdultName" Visible="false" VisibleIndex="128" /> 
			<dx:GridViewDataColumn FieldName="RequestingChild" Visible="false" VisibleIndex="129" /> 
			<dx:GridViewDataColumn FieldName="JrHelperGrade" Visible="false" VisibleIndex="130" /> 
			<dx:GridViewDataColumn FieldName="JrHelperEmail" Visible="false" VisibleIndex="131" /> 
			<dx:GridViewDataColumn FieldName="NeedChildCare" Visible="false" VisibleIndex="132" /> 
			<dx:GridViewDataColumn FieldName="DateDOBChild" Visible="false" VisibleIndex="133" /> 
			<dx:GridViewDataColumn FieldName="ShirtSize" Visible="false" VisibleIndex="134" /> 
			<dx:GridViewDataColumn FieldName="School" Visible="false" VisibleIndex="135" /> 
			<dx:GridViewDataColumn FieldName="MedicalReleaseForm" Visible="false" VisibleIndex="136" /> 
			<dx:GridViewDataColumn FieldName="ParentFirst" Visible="false" VisibleIndex="137" /> 
			<dx:GridViewDataColumn FieldName="ParentLast" Visible="false" VisibleIndex="138" /> 
			<dx:GridViewDataColumn FieldName="ParentPhone" Visible="false" VisibleIndex="139" /> 
			<dx:GridViewDataColumn FieldName="ParentWorkPhone" Visible="false" VisibleIndex="140" /> 
			<dx:GridViewDataHyperLinkColumn FieldName="Email" VisibleIndex="141">
				<DataItemTemplate>
					<dx:ASPxHyperLink ForeColor="Blue" ID="ASPxHyperLinkTest" 
						runat="server" Text='<%#Eval("Email") %>' NavigateUrl='<%#String.Format("mailto:{0}",Eval("Email"))%>' />
				</DataItemTemplate>
			</dx:GridViewDataHyperLinkColumn>
			<dx:GridViewDataColumn FieldName="ParentAtSameAddress" Visible="false" VisibleIndex="142" /> 
			<dx:GridViewDataColumn FieldName="ParentStreet" Visible="false" VisibleIndex="143" /> 
			<dx:GridViewDataColumn FieldName="ParentCity" Visible="false" VisibleIndex="144" /> 
			<dx:GridViewDataColumn FieldName="ParentState" Visible="false" VisibleIndex="145" /> 
			<dx:GridViewDataColumn FieldName="ParentZip" Visible="false" VisibleIndex="146" /> 
			<dx:GridViewDataColumn FieldName="Friend1First" Visible="false" VisibleIndex="147" /> 
			<dx:GridViewDataColumn FieldName="Friend1Last" Visible="false" VisibleIndex="148" /> 
			<dx:GridViewDataColumn FieldName="CurrentBackgroundCheck" Visible="false" VisibleIndex="149" /> 
			<dx:GridViewDataColumn FieldName="ChurchAttended" Visible="false" VisibleIndex="150" /> 
			<dx:GridViewDataColumn FieldName="FoodAllergies" Visible="false" VisibleIndex="151" /> 
			<dx:GridViewDataColumn FieldName="SpecialNeeds" Visible="false" VisibleIndex="152" /> 
			<dx:GridViewDataColumn FieldName="WillingToVolunteer" Visible="false" VisibleIndex="153" /> 
			<dx:GridViewDataColumn FieldName="DonateMoneyToScholarship" Visible="false" VisibleIndex="154" /> 
			<dx:GridViewDataColumn FieldName="Comments" Visible="false" VisibleIndex="155" /> 
        </Columns>
		<Settings ShowFilterRow="true" ShowFilterRowMenu="true" />
		<SettingsCustomizationWindow Enabled="True" />
		<SettingsPopup>
			<CustomizationWindow Width="250" Height="400" />
		</SettingsPopup>
        <ClientSideEvents ContextMenu="OnContextMenu" />
	</dx:ASPxGridView>
	<dx:ASPxPopupMenu ID="headerMenu" runat="server" ClientInstanceName="headerMenu">
		<Items>
			<dx:MenuItem Text="Hide column" Name="HideColumn">
			</dx:MenuItem>
			<dx:MenuItem Text="Show/hide hidden field list" Name="ShowHideList">
			</dx:MenuItem>
		</Items>
		<ClientSideEvents ItemClick="OnItemClick"/>
	</dx:ASPxPopupMenu>
</asp:Content>