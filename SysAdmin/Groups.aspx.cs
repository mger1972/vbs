﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.SysAdmin
{
	public partial class Groups : System.Web.UI.Page
	{
		//protected DataTable _dt = null;
		protected Search_VBS_GroupCollection _groups = new Search_VBS_GroupCollection();

		protected bool _userCanEdit = true;

		private int _maxRecsOnPage = 1000;
		private int _extraRecs = 5;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Bind the grid
			GetData();
			gridGroup.KeyFieldName = Search_VBS_Group.FN_GroupGUID;
			gridGroup.DataSource = _groups;
			gridGroup.DataBind();

			ResetSelectionScript();

			//// Tell if the user saw something
			//if (!IsPostBack && !IsCallback)
			//{
			//	Logging.LogInformation("User edited group: " + cboFirstLetterLastName.SelectedItem.Text, null, string.Empty, HttpContext.Current.User.Identity.Name);
			//}

			// Set up the columns in the grid
			//foreach (GridViewColumn col in gridGroup.Columns)
			//{
			//	if (col.Caption.ToLower() == "Delete".ToLower())
			//	{
			//		ASPxCheckBox cb = (ASPxCheckBox)gridGroup.FindEditFormTemplateControl(
			//		break;
			//	}
			//}

			// Check to see if the user has the ability to edit.  If they don't, turn off the fields
			VBSUserCollection users = new VBSUserCollection("sUserName = '" + Session["UserName"].ToString().Replace("'", "''") + "'");
			_userCanEdit = true;
			if (users.Count > 0)
			{
				if (!users[0].CanSysAdmin) { _userCanEdit = false; }
			}

			if (!_userCanEdit)
			{
				btnSelectAll.Visible =
					btnSelectNone.Visible =
					btnSubmit.Visible =
					false;

				foreach (GridViewColumn col in gridGroup.Columns)
				{
					if (col is GridViewDataColumn)
					{
						((GridViewDataColumn)col).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
					}
				}
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			// Check the Session for a data table
			if ((_groups == null ||
				_groups.Count == 0) &&
				Session["GroupCollection"] != null)
			{
				_groups = Session["GroupCollection"] as Search_VBS_GroupCollection;
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			if (!IsPostBack || Session["GroupCollection"] == null)
			{
				// Get the groups
				_groups = new Search_VBS_GroupCollection(string.Empty);
				_groups.Sort(Search_VBS_Group.FN_GroupName);

				// Create _extraRecs extra groups at the bottom
				for (int i = 0; i < _extraRecs; i++)
				{
					Search_VBS_Group newGroup = new Search_VBS_Group();
					newGroup.GroupGUID = System.Guid.NewGuid().ToString();
					newGroup.GroupName = string.Empty;
					newGroup.Inactive = false;
					newGroup.Count = 0;
					_groups.Add(newGroup);
				}

				int count = 1;
				foreach (Search_VBS_Group a in _groups)
				{
					a.Numbering = count;
					count++;
				}

				// Store it off in the session
				Session["GroupCollection"] = _groups;
			}

			// Bring it back
			_groups = (Search_VBS_GroupCollection)Session["GroupCollection"];
		}

		private void ResetSelectionScript()
		{
			StringBuilder sb = new StringBuilder();
			int cellCount = 0;
			for (int i = 0; i < _groups.Count; i++)
			{
				if (!String.IsNullOrEmpty(_groups[i].GroupName))
				{
					cellCount++;
					sb.AppendLine(String.Format("gridGroup.batchEditApi.SetCellValue({0}, 'CheckedInGrid', true);", i));
				}
			}
			btnSelectAll.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";

			sb = new StringBuilder();
			for (int i = 0; i < _groups.Count; i++)
			{
				sb.AppendLine(String.Format("gridGroup.batchEditApi.SetCellValue({0}, 'CheckedInGrid', false);", i));
			}
			btnSelectNone.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";
		}

		protected void gridGroup_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
		{
			//VBSUserCollection users = new VBSUserCollection("sUserName = '" + Session["UserName"].ToString().Replace("'", "''") + "'");
			//_userCanEdit = true;
			//if (users.Count > 0)
			//{
			//	if (!users[0].CanSysAdmin) { _userCanEdit = false; }
			//}

			//if (!_userCanEdit)
			//{
			//	e.Editor.ReadOnly = true;
			//}
			//// When the cell editor initializes, handle it
			//if (e.Editor is ASPxComboBox &&
			//	cboFirstLetterLastName.SelectedItem != null)
			//{
			//	//// Get the listing of students
			//	//string left1LastName = cboFirstLetterLastName.SelectedItem.GetValue(cboFirstLetterLastName.Columns[0].FieldName).ToString();
			//	//Search_VBS_GroupCollection coll = new Search_VBS_GroupCollection("sLeft1LastName = '" + left1LastName + "'");
			//	//coll.Sort(Search_VBS_Group.FN_FullNameLastFirst);

			//	// Get the class listing
			//	Search_VBS_ClassCodeCollection classes = new Search_VBS_ClassCodeCollection(string.Empty);
			//	classes.Sort(Search_VBS_ClassCode.FN_Length + ", " + Search_VBS_ClassCode.FN_ClassCode);

			//	// Update the name to show where this person is registered
			//	foreach (Search_VBS_ClassCode item in classes)
			//	{
			//		if (!String.IsNullOrEmpty(item.ClassCode))
			//		{
			//			item.GridCustom_5 = String.Format("{0} ({1})", item.ClassCode, item.Grade);
			//		}
			//	}

			//	ASPxComboBox combo = ((ASPxComboBox)e.Editor);
			//	combo.Columns.Clear();
			//	combo.Columns.Add(Search_VBS_ClassCode.FN_ClassCode, "Class Code");
			//	combo.Columns.Add(Search_VBS_ClassCode.FN_Grade, "Grade");
			//	combo.Columns.Add(Search_VBS_ClassCode.FN_StudentCount, "# Students in Class");
			//	//combo.Columns.Add(Search_VBS_Group.FN_GradeInFall, "Grade");
			//	//combo.Columns.Add(Search_VBS_Group.FN_GridCustom_5, "Currently Assigned Class");

			//	combo.DataSource = classes;
			//	combo.TextField = Search_VBS_ClassCode.FN_GridCustom_5;
			//	combo.ValueField = Search_VBS_ClassCode.FN_ClassCode;
			//	combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
			//	combo.TextFormatString = "{0} ({1})";
			//	combo.DataBindItems();
			//	combo.ReadOnly = false;
			//}
		}

		protected void gridGroup_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			//if (e.Column.FieldName.ToLower() == "ResponseID".ToLower() &&
			//	e.GetFieldValue(Search_VBS_Group.FN_ClassCodeWithDesc) != null)
			//{
			//	e.DisplayText = e.GetFieldValue(Search_VBS_Group.FN_ClassCodeWithDesc).ToString();
			//}
		}

		protected void gridGroup_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			List<string> deletedGUIDs = new List<string>();
			VBSGroupCollection coll = new VBSGroupCollection();
			List<string> newGroupNames = new List<string>();

			// Go through the new values and see if we have something to save
			foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
			{
				OrderedDictionary keys = val.Keys;
				OrderedDictionary newVals = val.NewValues;
				OrderedDictionary oldVals = val.OldValues;

				string groupGUID = (keys["GroupGUID"] != null && !String.IsNullOrEmpty(keys["GroupGUID"].ToString()) ?
					keys["GroupGUID"].ToString().Trim() : string.Empty);
				string oldGroupName = oldVals["GroupName"].ToString().Trim();
				string newGroupName = newVals["GroupName"].ToString().Trim();
				bool checkedInGrid = ((bool)newVals["CheckedInGrid"]);

				if (!checkedInGrid)
				{
					// We're adding/editing a new one here
					VBSGroupCollection existingColl = new VBSGroupCollection("sGroupGUID = '" + groupGUID + "'");
					if (existingColl.Count > 0)
					{
						// This one already exists, just update it
						VBSGroupCollection existingName = new VBSGroupCollection("sGroupName = '" + newGroupName.Replace("'", "''") + "'");
						if (existingName.Count == 0)
						{
							foreach (VBSGroup g in existingColl)
							{
								g.GroupName = newGroupName;
								coll.Add(g);
							}
						}
						else
						{
							// It already exists with that name - don't create it
						}
					}
					else
					{
						// Create a new one - this one doesn't exist
						// If the name already exists, don't create it
						VBSGroupCollection existingName = new VBSGroupCollection("sGroupName = '" + newGroupName.Replace("'", "''") + "'");
						if (existingName.Count == 0)
						{
							VBSGroup item = new VBSGroup();
							item.GroupGUID = System.Guid.NewGuid().ToString();
							item.GroupName = newGroupName;
							coll.Add(item);		// Add the item to the collection
						}
						else
						{
							// It already exists with that name - don't create it
						}
					}
				}
				else
				{
					// We just need to delete this group and all of its inhabitants
					if (!deletedGUIDs.Contains(groupGUID)) { deletedGUIDs.Add(groupGUID); }
				}
			}

			// Delete any that need to be deleted first
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			if (deletedGUIDs.Count > 0)
			{
				// First, crush the assignments
				VBSResponseToGroupCollection existingGroupLink = new VBSResponseToGroupCollection("sGroupGUID IN ('" + Utils.JoinString(deletedGUIDs, "', '") + "')");
				foreach (VBSResponseToGroup link in existingGroupLink)
				{
					link.Delete();
					Logging.LogInformation("Deleting Response to Group: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				}

				// Then, crush the group
				VBSGroupCollection existingColl = new VBSGroupCollection("sGroupGUID IN ('" + Utils.JoinString(deletedGUIDs, "', '") + "')");
				foreach (VBSGroup link in existingColl)
				{
					link.Delete();
					Logging.LogInformation("Deleting Group: " + link.GroupGUID, null, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(existingGroupLink.AddUpdateAll());
				errors.AddRange(existingColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();
			}

			// Update the collection
			if (coll.Count > 0)
			{
				foreach (VBSGroup link in coll)
				{
					Logging.LogInformation("Adding/Modifying Group: " + link.GroupGUID, null, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(coll.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
			}

			// Go out and get the updated data from the collection
			if (_groups.Count > 0)
			{
				Search_VBS_GroupCollection newCollection = new Search_VBS_GroupCollection(string.Empty);

				for (int i = _groups.Count - 1; i >= 0; i--)
				{
					// Remove them all
					_groups.RemoveAt(i);
				}

				// Add them back in
				foreach (Search_VBS_Group assign in newCollection)
				{
					_groups.Add(assign);
				}

				_groups.Sort(Search_VBS_Group.FN_GroupName);

				for (int i = 0; i < _extraRecs; i++)
				{
					Search_VBS_Group newGroup = new Search_VBS_Group();
					newGroup.GroupGUID = System.Guid.NewGuid().ToString();
					newGroup.GroupName = string.Empty;
					newGroup.Inactive = false;
					newGroup.Count = 0;
					_groups.Add(newGroup);
				}

				// Handle the numbering
				int count = 1;
				foreach (Search_VBS_Group a in _groups)
				{
					a.Numbering = count;
					count++;
				}

				Session["GroupCollection"] = _groups;
			}

			gridGroup.DataBind();

			// Reset the javascript code on the buttons
			ResetSelectionScript();

			// Handle the event internally
			e.Handled = true;
		}

		protected void cbSelectAll_Callback(object sender, CallbackEventArgsBase e)
		{
			ResetSelectionScript();
		}

		protected void gridGroup_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			gridGroup.UpdateEdit();
			//int test = 1;
		}

		//protected void chkDelete_Init(object sender, EventArgs e)
		//{
		//	ASPxCheckBox chk = sender as ASPxCheckBox;
		//	GridViewDataItemTemplateContainer container = chk.NamingContainer as GridViewDataItemTemplateContainer;
		//	chk.ClientInstanceName = String.Format("chkDelete{0}", container.VisibleIndex);
		//	chk.ClientSideEvents.CheckedChanged =
		//		String.Format("function (s, e) {{ alert(gridGroup.batchEditApi.GetCellValue({0}, 'CheckedInGrid')) }}", container.VisibleIndex);
		//		//String.Format("function (s, e) {{ if(chkDelete{0}.GetChecked()) {{ gridGroup.batchEditApi.SetCellValue({0}, 'CheckedInGrid', true); alert('checked'); }} alert(gridGroup.batchEditApi.GetCellValue({0}, 'CheckedInGrid'); }}", container.VisibleIndex);
		//}
	}
}