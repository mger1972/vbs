﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.SysAdmin
{
	public partial class OldGroups : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Set the datasource on the grid
			if (!IsPostBack)
			{
				grid.KeyFieldName = VBSGroup.FN_GroupGUID;
				grid.DataBind();
			}
		}

		protected void grid_DataBinding(object sender, EventArgs e)
		{
			VBSGroupCollection coll = new VBSGroupCollection(string.Empty);
			coll.Sort("GroupName");
			grid.DataSource = coll;
		}

		protected void grid_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
		{
			// When they update, run the row back to the collection
			VBSGroupCollection coll = new VBSGroupCollection("sGroupGUID = '" + e.Keys[0].ToString() + "'");
			coll[0].GroupName = e.NewValues["GroupName"].ToString().ToUpper().Trim();
			//coll[0].Grade = (e.NewValues["Grade"].ToString().Substring(0, 1).ToUpper() +
			//	e.NewValues["Grade"].ToString().Substring(1).ToLower()).Trim();
			coll.AddUpdateAll();		// Update the collection

			grid.DataBind();

			// Handle the event internally
			e.Cancel = true;
		}

		protected void grid_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
		{
			// Kill off the row in the dataset
			VBSGroupCollection coll = new VBSGroupCollection("sGroupGUID = '" + e.Keys[0].ToString() + "'");
			foreach (VBSGroup group in coll)
			{
				group.Delete();		// Delete the row
			}
			coll.AddUpdateAll();		// Update the collection

			grid.DataBind();

			// Handle the event internally
			e.Cancel = true;
		}

		protected void grid_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
		{
			VBSGroupCollection groupColl = (VBSGroupCollection)grid.DataSource;
			bool exists = false;
			string errGroupName = string.Empty;
			List<string> existing = new List<string>();
			foreach (VBSGroup g in groupColl)
			{
				if (!existing.Contains(g.GroupName.ToLower()))
				{
					existing.Add(g.GroupName.ToLower());
				}
				else
				{
					//err = "There is already a group name entered as: " + g.GroupName;
					errGroupName = "There is already a group name entered as: " + g.GroupName;
					exists = true;
					break;
				}
			}

			if (!exists)
			{
				// Kill off the row in the dataset
				VBSGroup code = new VBSGroup();
				code.GroupGUID = System.Guid.NewGuid().ToString();
				code.GroupName = e.NewValues["GroupName"].ToString().ToUpper().Trim();
				//code.Grade = (e.NewValues["Grade"].ToString().Substring(0, 1).ToUpper() +
				//	e.NewValues["Grade"].ToString().Substring(1).ToLower()).Trim();
				code.AddUpdate();		// Update the collection

				grid.DataBind();
			}
			else
			{
				throw new Exception(errGroupName);
			}

			// Handle the event internally
			e.Cancel = true;
		}

		protected void grid_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
		{
			string err = string.Empty;
			//VBSGroupCollection groupColl = (VBSGroupCollection)grid.DataSource;
			//List<string> existing = new List<string>();
			//foreach (VBSGroup g in groupColl)
			//{
			//	if (!existing.Contains(g.GroupName.ToLower()))
			//	{
			//		existing.Add(g.GroupName.ToLower());
			//	}
			//	else
			//	{
			//		err = "There is already a group name entered as: " + g.GroupName;
			//		break;
			//	}
			//}

			// Validate the row
			if (e.Keys.Count > 0)
			{
				// Find out if it exists in the standard collection first
				VBSGroupCollection coll = new VBSGroupCollection("sGroupGUID <> '" + e.Keys[0].ToString() + "' AND " +
					"sGroupName = '" + e.NewValues["GroupName"].ToString().Replace("'", "''") + "'");
				if (coll.Count > 0)
				{
					err = "There is already a group name entered as: " + e.NewValues["GroupName"].ToString();
				}
			}

			if (!String.IsNullOrEmpty(err))
			{
				e.RowError = err;
			}
		}
	}
}