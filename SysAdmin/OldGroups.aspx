﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OldGroups.aspx.cs" Inherits="VBS.SysAdmin.OldGroups" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Groups</h1>
				<h3>Use the screen below to edit the group definitions.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" 
		Width="100%" SettingsEditing-Mode="Batch" OnDataBinding="grid_DataBinding"
		OnRowUpdating="grid_RowUpdating" OnRowDeleting="grid_RowDeleting" OnRowInserting="grid_RowInserting" 
		OnRowValidating="grid_RowValidating" SettingsBehavior-FilterRowMode="OnClick" SettingsPager-PageSize="15"
		Styles-Header-Font-Bold="true" Styles-Header-Font-Size="Medium" SettingsBehavior-AllowSort="false">
		<Columns>
            <dx:GridViewDataColumn FieldName="GroupName" VisibleIndex="1" /> 
			<dx:GridViewCommandColumn ShowClearFilterButton="true" ShowApplyFilterButton="true" 
				ShowNewButtonInHeader="true" ShowDeleteButton="true" VisibleIndex="2" />
        </Columns>
		<Settings ShowFilterRow="true" ShowFilterRowMenu="true" />
	</dx:ASPxGridView>
</asp:Content>
