﻿<%@ Page Title="Groups" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Groups.aspx.cs" Inherits="VBS.SysAdmin.Groups" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Groups</h1>
				<h3>Use the screen below to edit the group definitions.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script type="text/javascript">
		function OnClick(s, e) {
			// Check to see if they have any items checked
			var count = 0;
			var totalCount = gridGroup.GetVisibleRowsOnPage();
			for (var i = 0; i < totalCount; i++) {
				if (gridGroup.batchEditApi.GetCellValue(i, 'CheckedInGrid')) {
					count++;
				}
			}
			if (count > 0) {
				popupControlConfirm.Show();
			}
			else {
				gridGroup.UpdateEdit();
			}
		}
</script>   
	<table id="bottomPanel" width="100%" align="center" border="0">
		<tr>
			<td width="80%">&nbsp;</td>
			<td width="20%" align="right" nowrap>
				<dx:ASPxButton ID="btnSelectAll" ClientInstanceName="btnSelectAll" Text="Check All" runat="server" AutoPostBack="false" />	
				&nbsp;
				<dx:ASPxButton ID="btnSelectNone" ClientInstanceName="btnSelectNone" Text="Uncheck All" runat="server" AutoPostBack="false" />
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<dx:ASPxGridView ID="gridGroup" ClientInstanceName="gridGroup" runat="server" OnCommandButtonInitialize="gridGroup_CommandButtonInitialize"
					Width="100%" OnCellEditorInitialize="gridGroup_CellEditorInitialize" OnBatchUpdate="gridGroup_BatchUpdate"
					OnCustomColumnDisplayText="gridGroup_CustomColumnDisplayText" 
					Paddings-PaddingLeft="10px" Paddings-PaddingRight="10px" Paddings-PaddingTop="0px" Paddings-PaddingBottom="0px"
					EnableViewState="false">
					<Border BorderWidth="0px" />
					<ClientSideEvents BatchEditConfirmShowing="function(s, e) { e.cancel = true; }" 
						BeginCallback="function(s, e) {
							if (e.command == 'UPDATEEDIT') {
								pnlLoading.Text = 'Saving Changes';
						//alert('hi');
								pnlLoading.Show();
								isUpdateEdit = true;
							}
						}"
						EndCallback="function(s, e) {
							if (isUpdateEdit) {
								isUpdateEdit = false;
							}
							pnlLoading.Hide();
						}" />
					<Columns>
						<dx:GridViewDataColumn FieldName="NumberingAsString" Width="2px" Caption=" " CellStyle-HorizontalAlign="Right" VisibleIndex="1" ReadOnly="true">
							<EditItemTemplate>
								<dx:ASPxLabel ID="lbNumbering" runat="server" Value='<%# Bind("NumberingAsString") %>'>
								</dx:ASPxLabel>
							</EditItemTemplate>
						</dx:GridViewDataColumn>
						<dx:GridViewDataColumn FieldName="GroupName" Width="250px" Caption="Group Name" VisibleIndex="2">
						</dx:GridViewDataColumn>
						<dx:GridViewDataColumn FieldName="Count" Width="250px" Caption="Member Count" 
							VisibleIndex="3" ReadOnly="true">
							<EditFormSettings Visible="False" />
						</dx:GridViewDataColumn>
						<dx:GridViewDataCheckColumn FieldName="CheckedInGrid" Width="2px" Caption="Delete" 
							HeaderStyle-HorizontalAlign="Center" VisibleIndex="8">
						</dx:GridViewDataCheckColumn>
					</Columns>
					<Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowTitlePanel="false" />
					<SettingsBehavior FilterRowMode="OnClick" AllowSort="false" />
					<SettingsEditing Mode="Batch" />
					<SettingsPager PageSize="100" Visible="false" />
					<Styles>
						<InlineEditCell>
							<Border BorderStyle="None" />
						</InlineEditCell>
					</Styles>
				</dx:ASPxGridView>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right"><dx:ASPxButton ID="btnSubmit" ClientInstanceName="btnSubmit" runat="server" 
				Text="Save Changes" AutoPostBack="false">
				<ClientSideEvents Click="OnClick" />
			</dx:ASPxButton></td>
		</tr>
	</table>
	<dx:ASPxLoadingPanel ID="pnlLoading" runat="server" ContainerElementID="bottomPanel" ClientInstanceName="pnlLoading" Modal="true"></dx:ASPxLoadingPanel>
	<dx:ASPxPopupControl ID="popupControlConfirm" runat="server" Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
		ClientInstanceName="popupControlConfirm" ShowShadow="true" ShowCloseButton="false" ShowCollapseButton="false" AllowDragging="true"
		HeaderText="Confirm Group Deletion?"
		width="400px" Height="150px">
		<ContentCollection>
			<dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server" SupportsDisabledAttribute="True">
				<table width="80%" align="center">
					<tr>
						<td colspan="2">Deleting groups will cause all assigned individuals to be put into 
							an "unassigned" status...<br />&nbsp;<br />Are you sure you wish to continue?<br />&nbsp;</td>
					</tr>
					<tr>
						<td align="right"><dx:ASPxButton ID="btnYes" runat="server" Text="Yes" AutoPostBack="false" Width="80px" OnClick="btnSubmit_Click">
							<ClientSideEvents Click="function(s, e) { gridGroup.UpdateEdit();
								popupControlConfirm.Hide();
								 }" />
						    </dx:ASPxButton>&nbsp;&nbsp;</td>
						<td>&nbsp;&nbsp;<dx:ASPxButton ID="btnNo" runat="server" Text="No" Width="80px" AutoPostBack="False">
						<ClientSideEvents Click="function(s, e) {
							 popupControlConfirm.Hide();
						}" />
						</dx:ASPxButton></td>
					</tr>
				</table>
			</dx:PopupControlContentControl>
		</ContentCollection>
	</dx:ASPxPopupControl>
</asp:Content>
