﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.SysAdmin
{
	public partial class ClassDef : System.Web.UI.Page
	{
		//protected DataTable _dt = null;
		protected Search_VBS_ClassCodeCollection _classes = new Search_VBS_ClassCodeCollection();

		protected bool _userCanEdit = true;

		//private int _maxRecsOnPage = 200;
		private int _extraRecs = 5;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Bind the grid
			GetData();
			gridClass.KeyFieldName = Search_VBS_ClassCode.FN_ClassGUID;
			gridClass.DataSource = _classes;
			gridClass.DataBind();

			ResetSelectionScript();		// Reset the selection script

			// Tell if the user saw something
			//if (!IsPostBack && !IsCallback)
			//{
			//	Logging.LogInformation("User viewed students: " + cboFirstLetterLastName.SelectedItem.Text, null, string.Empty, HttpContext.Current.User.Identity.Name);
			//}

			// Check to see if the user has the ability to edit.  If they don't, turn off the fields
			VBSUserCollection users = new VBSUserCollection("sUserName = '" + Session["UserName"].ToString().Replace("'", "''") + "'");
			_userCanEdit = true;
			if (users.Count > 0)
			{
				if (!users[0].CanSysAdmin) { _userCanEdit = false; }
			}

			if (!_userCanEdit)
			{
				btnSelectAll.Visible =
					btnSelectNone.Visible =
					btnSubmit.Visible =
					btnSubmitHead.Visible =
					false;

				foreach (GridViewColumn col in gridClass.Columns)
				{
					if (col is GridViewDataColumn)
					{
						((GridViewDataColumn)col).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
					}
				}
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			// Check the Session for a data table
			if ((_classes == null ||
				_classes.Count == 0) &&
				Session["ClassDefAssignmentsCollection"] != null)
			{
				_classes = Session["ClassDefAssignmentsCollection"] as Search_VBS_ClassCodeCollection;
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			if (!IsPostBack || Session["ClassDefAssignmentsCollection"] == null)
			{
				_classes = new Search_VBS_ClassCodeCollection(string.Empty);
				_classes.Sort(Search_VBS_ClassCode.FN_Length + ", " + Search_VBS_ClassCode.FN_ClassCode);

				// Create _extraRecs extra groups at the bottom
				for (int i = 0; i < _extraRecs; i++)
				{
					Search_VBS_ClassCode newClass = new Search_VBS_ClassCode();
					newClass.ClassGUID = System.Guid.NewGuid().ToString();
					newClass.ClassCode = string.Empty;
					newClass.Grade = string.Empty;
					newClass.Rotation = string.Empty;
					newClass.ClassColor = string.Empty;
					newClass.Inactive = false;
					newClass.StudentCount = 0;
					newClass.TeacherCount = 0;
					newClass.JuniorHelperCount = 0;
					_classes.Add(newClass);
				}

				int count = 1;
				foreach (Search_VBS_ClassCode a in _classes)
				{
					a.Numbering = count;
					count++;
				}

				// Store it off in the session
				Session["ClassDefAssignmentsCollection"] = _classes;
			}

			// Bring it back
			_classes = (Search_VBS_ClassCodeCollection)Session["ClassDefAssignmentsCollection"];
		}

		private void ResetSelectionScript()
		{
			StringBuilder sb = new StringBuilder();
			int cellCount = 0;
			for (int i = 0; i < _classes.Count; i++)
			{
				if (!String.IsNullOrEmpty(_classes[i].ClassCode))
				{
					cellCount++;
					sb.AppendLine(String.Format("gridClass.batchEditApi.SetCellValue({0}, 'CheckedInGrid', true);", i));
				}
			}
			btnSelectAll.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";

			sb = new StringBuilder();
			for (int i = 0; i < _classes.Count; i++)
			{
				sb.AppendLine(String.Format("gridClass.batchEditApi.SetCellValue({0}, 'CheckedInGrid', false);", i));
			}
			btnSelectNone.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";
		}

		protected void gridClass_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
		{
			// When the cell editor initializes, handle it
			if (e.Editor is ASPxComboBox)
			{
				if (e.Column.FieldName.ToLower().Equals("Grade".ToLower()))
				{
					// Populate the grade
					DataTable dt = new DataTable();
					dt.Columns.Add("Grade", typeof(System.String));
					dt.Rows.Add(new object[] { "Kindergarten" });
					dt.Rows.Add(new object[] { "1st" });
					dt.Rows.Add(new object[] { "2nd" });
					dt.Rows.Add(new object[] { "3rd" });
					dt.Rows.Add(new object[] { "4th" });
					dt.Rows.Add(new object[] { "5th" });
					dt.Rows.Add(new object[] { "6th" });

					ASPxComboBox combo = ((ASPxComboBox)e.Editor);
					combo.Columns.Clear();
					combo.Columns.Add(Search_VBS_ClassCode.FN_Grade, "Grade");

					combo.DataSource = dt;
					combo.TextField = Search_VBS_ClassCode.FN_Grade;
					combo.ValueField = Search_VBS_ClassCode.FN_Grade;
					combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
					combo.TextFormatString = "{0}";
					combo.DataBindItems();
					combo.ReadOnly = false;
				}
				else if (e.Column.FieldName.ToLower().Equals("Rotation".ToLower()))
				{
					// Populate the grade
					DataTable dt = new DataTable();
					dt.Columns.Add("Rotation", typeof(System.String));
					dt.Rows.Add(new object[] { "1st" });
					dt.Rows.Add(new object[] { "2nd" });
					dt.Rows.Add(new object[] { "3rd" });
					
					ASPxComboBox combo = ((ASPxComboBox)e.Editor);
					combo.Columns.Clear();
					combo.Columns.Add(Search_VBS_ClassCode.FN_Rotation, "Rotation");

					combo.DataSource = dt;
					combo.TextField = Search_VBS_ClassCode.FN_Rotation;
					combo.ValueField = Search_VBS_ClassCode.FN_Rotation;
					combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
					combo.TextFormatString = "{0}";
					combo.DataBindItems();
					combo.ReadOnly = false;
				}
				else if (e.Column.FieldName.ToLower().Equals("ClassColor".ToLower()))
				{
					// Populate the grade
					DataTable dt = new DataTable();
					dt.Columns.Add("ClassColor", typeof(System.String));
					dt.Rows.Add(new object[] { "Blue" });
					dt.Rows.Add(new object[] { "Red" });
					
					ASPxComboBox combo = ((ASPxComboBox)e.Editor);
					combo.Columns.Clear();
					combo.Columns.Add(Search_VBS_ClassCode.FN_ClassColor, "ClassColor");

					combo.DataSource = dt;
					combo.TextField = Search_VBS_ClassCode.FN_ClassColor;
					combo.ValueField = Search_VBS_ClassCode.FN_ClassColor;
					combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
					combo.TextFormatString = "{0}";
					combo.DataBindItems();
					combo.ReadOnly = false;
				}
			}
		}

		protected void gridClass_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			//if (e.Column.FieldName.ToLower() == "ResponseID".ToLower() &&
			//	e.GetFieldValue(Search_VBS_ClassCode.FN_ClassCodeWithDesc) != null)
			//{
			//	e.DisplayText = e.GetFieldValue(Search_VBS_ClassCode.FN_ClassCodeWithDesc).ToString();
			//}
		}

		protected void gridClass_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			List<string> deletedClassCodes = new List<string>();
			VBSClassCodeCollection coll = new VBSClassCodeCollection();
			List<string> newClassCodes = new List<string>();

			// Go through the new values and see if we have something to save
			foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
			{
				OrderedDictionary keys = val.Keys;
				OrderedDictionary newVals = val.NewValues;
				OrderedDictionary oldVals = val.OldValues;

				string classGUID = (keys["ClassGUID"] != null && !String.IsNullOrEmpty(keys["ClassGUID"].ToString()) ?
					keys["ClassGUID"].ToString().Trim() : string.Empty);
				string oldClassCode = oldVals["ClassCode"].ToString().Trim();
				string newClassCode = newVals["ClassCode"].ToString().Trim();
				string newRotation = (newVals["Rotation"] != null ? newVals["Rotation"].ToString().Trim() : string.Empty);
				string newGrade = newVals["Grade"].ToString().Trim();
				string newColor = newVals["ClassColor"].ToString().Trim();
				bool checkedInGrid = ((bool)newVals["CheckedInGrid"]);

				if (!checkedInGrid)
				{
					// We're adding/editing a new one here
					VBSClassCodeCollection existingColl = new VBSClassCodeCollection("sClassGUID = '" + classGUID + "'");
					if (existingColl.Count > 0)
					{
						// This one already exists, just update it
						VBSClassCodeCollection existingName = new VBSClassCodeCollection("sClassCode = '" + newClassCode.Replace("'", "''") + "' AND sClassGUID <> '" + classGUID + "'");
						if (existingName.Count == 0)
						{
							foreach (VBSClassCode g in existingColl)
							{
								g.ClassCode = newClassCode;
								g.Rotation = newRotation;
								g.Grade = newGrade;
								g.ClassColor = newColor;
								coll.Add(g);
							}
						}
						else
						{
							// It already exists with that name - don't create it
						}
					}
					else
					{
						// Create a new one - this one doesn't exist
						// If the name already exists, don't create it
						VBSClassCodeCollection existingName = new VBSClassCodeCollection("sClassCode = '" + newClassCode.Replace("'", "''") + "'");
						if (existingName.Count == 0)
						{
							VBSClassCode item = new VBSClassCode();
							item.ClassGUID = System.Guid.NewGuid().ToString();
							item.ClassCode = newClassCode;
							item.Rotation = newRotation;
							item.Grade = newGrade;
							item.ClassColor = newColor;
							coll.Add(item);		// Add the item to the collection
						}
						else
						{
							// It already exists with that name - don't create it
						}
					}
				}
				else
				{
					// We just need to delete this class and all of its inhabitants
					if (!deletedClassCodes.Contains(oldClassCode)) { deletedClassCodes.Add(oldClassCode); }
				}
			}

			// Delete any that need to be deleted first
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			if (deletedClassCodes.Count > 0)
			{
				// First, crush the assignments
				VBSClassLinkCollection existingClassLink = new VBSClassLinkCollection("sClassCode IN ('" + Utils.JoinString(deletedClassCodes, "', '") + "')");
				foreach (VBSClassLink link in existingClassLink)
				{
					link.Delete();
					Logging.LogInformation("Deleting Class Link: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				}

				// Then, crush the class
				VBSClassCodeCollection existingColl = new VBSClassCodeCollection("sClassCode IN ('" + Utils.JoinString(deletedClassCodes, "', '") + "')");
				foreach (VBSClassCode link in existingColl)
				{
					link.Delete();
					Logging.LogInformation("Deleting Class: " + link.ClassCode, null, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(existingClassLink.AddUpdateAll());
				errors.AddRange(existingColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();
			}

			// Update the collection
			if (coll.Count > 0)
			{
				foreach (VBSClassCode link in coll)
				{
					Logging.LogInformation("Adding/Modifying Class: " + link.ClassCode, null, link.GetAsXML(false), Session["UserName"].ToString());
					link.Rowversion = DAL.GetTimeStampFromTable(VBSClassCode.DB_TableName, 
						VBSClassCode.GetDBFieldName(VBSClassCodeField.Rowversion), 
						"sClassGUID = '" + link.ClassGUID + "'");
				}
				errors.AddRange(coll.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
			}

			// Go out and get the updated data from the collection
			if (_classes.Count > 0)
			{
				Search_VBS_ClassCodeCollection newCollection = new Search_VBS_ClassCodeCollection(string.Empty);

				for (int i = _classes.Count - 1; i >= 0; i--)
				{
					// Remove them all
					_classes.RemoveAt(i);
				}

				// Add them back in
				foreach (Search_VBS_ClassCode assign in newCollection)
				{
					_classes.Add(assign);
				}

				_classes.Sort(Search_VBS_ClassCode.FN_Length + ", " + Search_VBS_ClassCode.FN_ClassCode);

				for (int i = 0; i < _extraRecs; i++)
				{
					Search_VBS_ClassCode newClass = new Search_VBS_ClassCode();
					newClass.ClassGUID = System.Guid.NewGuid().ToString();
					newClass.ClassCode = string.Empty;
					newClass.Grade = string.Empty;
					newClass.Rotation = string.Empty;
					newClass.Inactive = false;
					newClass.StudentCount = 0;
					newClass.TeacherCount = 0;
					newClass.JuniorHelperCount = 0;
					_classes.Add(newClass);
				}

				// Handle the numbering
				int count = 1;
				foreach (Search_VBS_ClassCode a in _classes)
				{
					a.Numbering = count;
					count++;
				}

				Session["ClassDefAssignmentsCollection"] = _classes;
			}

			gridClass.DataBind();

			// Reset the javascript code on the buttons
			ResetSelectionScript();

			// Handle the event internally
			e.Handled = true;
		}

		protected void cbSelectAll_Callback(object sender, CallbackEventArgsBase e)
		{
			ResetSelectionScript();
		}

		protected void gridClass_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			gridClass.UpdateEdit();
		}

		protected void btnSubmitHead_Click(object sender, EventArgs e)
		{
			gridClass.UpdateEdit();
		}
	}
}