﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS
{
	public partial class testComboBox : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (cboMain.SelectedIndex == -1)
			{
				cboMain.SelectedIndex = 0;
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			cboMain.DataBind();
		}

		protected void cboMain_SelectedIndexChanged(object sender, EventArgs e)
		{
			int test = 1;
		}

		protected void cboMain_DataBinding(object sender, EventArgs e)
		{
			// This works - firing the Selected Index Changed event above
			// But I can't do multi-column
			//cboMain.Items.Clear();
			//cboMain.Items.Add("Item 1");
			//cboMain.Items.Add("Item 2");
			//cboMain.Items.Add("Item 3");
			//cboMain.Items.Add("Item 4");
			//cboMain.SelectedIndex = 2;

			// But this doesn't fire the event
			DataTable dt = new DataTable();
			dt.Columns.Add("Field1", typeof(System.String));
			dt.Columns.Add("Field2", typeof(System.String));

			for (int i = 0; i < 5; i++)
			{
				dt.Rows.Add(new object[] {
					"Item " + i.ToString(),
					"Item " + (i * 2).ToString(),
				});
			}

			cboMain.DataSource = dt;

			//cboMain.SelectedIndex = 2;
		}
	}
}