﻿<%@ Page Title="Assignments" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Assign.aspx.cs" Inherits="VBS.Assign" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>



<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Assignments</h1>
				<h3>Use the screen below to edit the different class/team leader assignments.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<ol class="round">
        <li class="one">
            <h5>By Class</h5>
            Use this screen to assign team leaders, helpers and students to a single class.
            <a href="~/Assignments/ByClass.aspx" id="link1" runat="server">Edit By Class</a>
        </li>
		<li class="two">
            <h5>By Student</h5>
            Assign students to classes by looking at a list of students and their currently associated classes.
            <a href="~/Assignments/ByStudent.aspx" id="link2" runat="server">Edit By Student</a>
        </li>
		<li class="three">
            <h5>By Team</h5>
            Assign team leaders/volunteers (who is in Rec, Crafts, etc.)
            <a href="~/Assignments/ByTeam.aspx" id="link3" runat="server">Assign Team Leaders/Helpers to Groups</a>
        </li>
		<li class="four">
            <h5>By Team Leader</h5>
            Assign team leaders to multiple classes/teams at the same time.
            <a href="~/Assignments/ByTeamLeader.aspx" id="link4" runat="server">Edit By Team Leader/Helper</a>
        </li>
		<li class="five">
            <h5>EMail Class Rosters</h5>
			EMail Class Rosters to the Team Leaders for each class.
			<a href="~/Assignments/SendClassRosters.aspx" id="A10" runat="server">EMail Class Rosters</a>
        </li>
		<li class="six">
            <h5>EMail Team Rosters</h5>
			EMail Team Rosters to the Team Coordinators.
			<a href="~/Assignments/SendTeamRosters.aspx" id="A11" runat="server">EMail Team Rosters</a>
        </li>
		<li class="seven">
            <h5>Nursery Assignments</h5>
			Assign children to the nursery.
			<a href="~/Assignments/NurseryAssignments.aspx" id="A1" runat="server">Nursery Assignments</a>
        </li>
    </ol>
</asp:Content>