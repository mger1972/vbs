﻿<%@ Page Title="Test Send E-Mail" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TestSendEMail.aspx.cs" Inherits="VBS.TestSendEMail" EnableSessionState="False" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
	<asp:PlaceHolder id="MetaPlaceHolder" runat="server" />
</asp:Content>
<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Sonrise Island VBS</h1>
                <h3>Click the button to send an email from jen@gerlach5.com to mark@gerlach5.com and jen@gerlach5.com...</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<table width="100%" align="center">
		<tr>
            <td>
                <dx:ASPxCallback ID="cbProcess" runat="server" ClientInstanceName="cbProcess" OnCallback="cbProcess_Callback">
                    <ClientSideEvents CallbackComplete="function(s, e) {
                        btnProcessDownload.SetEnabled(true);
                        downloadTimer.SetEnabled(false);
                        //cbGetStatus.PerformCallback();
                        lblStatus.SetText('Test EMail Sent.<br />' + lblStatus.GetText());
                     }" />
                </dx:ASPxCallback>
                <dx:ASPxButton ID="btnProcessDownload" runat="server" Text="Send Test EMail" AutoPostBack="false" ClientInstanceName="btnProcessDownload">
                    <ClientSideEvents Click="function(s, e) {
                        s.SetEnabled(false);
	                    lblStatus.SetText('');
	                    //lblStatus.SetText('Process completion: 0% ');
	                    lblStatus.SetClientVisible(true);
	                    downloadTimer.SetEnabled(true);
                        cbProcess.PerformCallback();
                    }" />
                </dx:ASPxButton>
                <dx:ASPxCallback ID="cbGetStatus" runat="server" ClientInstanceName="cbGetStatus" OnCallback="cbGetStatus_Callback">
                    <ClientSideEvents CallbackComplete="function(s, e) {
                        var labelText = lblStatus.GetText();
                        //var alreadyComplete = (labelText.substring(0, 19) == 'EMail Sent');
                        //alert(alreadyComplete);
                        //if (!alreadyComplete) {
	                        lblStatus.SetText(e.result);
	                    //}
                    }" />
                </dx:ASPxCallback>
                <dx:ASPxTimer ID="downloadTimer" runat="server" ClientInstanceName="downloadTimer" Enabled="false" Interval="1000">
                    <ClientSideEvents Tick="function(s, e) {
                        //alert('hi');
	                    cbGetStatus.PerformCallback();
                    }" />
                </dx:ASPxTimer>
            </td>
		</tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="lblStatus" runat="server" Text="" ClientVisible="false" ClientInstanceName="lblStatus"></dx:ASPxLabel>
            </td>
        </tr>
	</table>
</asp:Content>

