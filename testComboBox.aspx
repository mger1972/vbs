﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testComboBox.aspx.cs" Inherits="VBS.testComboBox" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<dx:ASPxComboBox ID="cboMain" ClientInstanceName="cboMain" runat="server" ValueType="System.String"
			DropDownStyle="DropDownList" width="170px" DropDownHeight="400" OnSelectedIndexChanged="cboMain_SelectedIndexChanged"
			OnDataBinding="cboMain_DataBinding" AutoPostBack="true" ValueField="Field1" TextField="Field1">
			<Columns>
				<dx:ListBoxColumn FieldName="Field1" Width="150px" />
				<dx:ListBoxColumn FieldName="Field2" Width="250px" />
			</Columns>
		</dx:ASPxComboBox>
    </div>
    </form>
</body>
</html>
