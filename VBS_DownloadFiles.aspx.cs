﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using VBS;

namespace SmarterASPNetTestProject
{
	public partial class VBS_DownloadFiles : System.Web.UI.Page
	{
		protected StringBuilder _sb = new StringBuilder();
		protected StringBuilder _sbLog = new StringBuilder();

		protected void Page_Load(object sender, EventArgs e)
		{
			// Set the meta refresh tag
			HtmlMeta meta = new HtmlMeta();
			meta.Name = "refreshTag";
			meta.HttpEquiv = "refresh";

			// Randomize the code so it goes between 8 minutes and 12 minutes (every 5 seconds)
			Random r = new Random();
			int diff = (60 * 8) + (r.Next(0, 48) * 5);
			meta.Content = diff.ToString();		
			MetaPlaceHolder.Controls.Add(meta);

			// Download the files
			// Login to the system
			string loginAddress = "https://voyagers.ccbchurch.com/login.php";
			StringBuilder sb = new StringBuilder();
			sb.Append("ax=" + HttpUtility.UrlEncode("login"));
			sb.Append("&" + "form[login]=" + HttpUtility.UrlEncode("jen@efgtech.com"));
			sb.Append("&" + "form[password]=" + HttpUtility.UrlEncode("stretch1"));

			VBS_CookieAwareWebClient client = new VBS_CookieAwareWebClient();
			client.Login(loginAddress, sb.ToString());

			// Download the files
			Dictionary<string, int> fileIDs = new Dictionary<string, int>();
			//fileIDs.Add("Base", 331);
			//fileIDs.Add("AltPay", 342);
			//fileIDs.Add("Volunteer", 332);

			//fileIDs.Add("VolunteerUnder18", 423);
			//fileIDs.Add("VolunteerAdult", 422);
			//fileIDs.Add("Base", 418);
			//fileIDs.Add("AltPay", 419);

			//fileIDs.Add("VolunteerUnder18", 520);		// 2016 Values
			//fileIDs.Add("VolunteerAdult", 519);
			//fileIDs.Add("Base", 455);
			//fileIDs.Add("AltPay", 518);
			//fileIDs.Add("MedicalReleaseForm", 503);

			fileIDs.Add("Base", 604);           // 2017 Values
			fileIDs.Add("VolunteerUnder18", 607);
			fileIDs.Add("VolunteerAdult", 606);
			fileIDs.Add("MedicalReleaseForm1", 589);     // 2017-2018 Medical form
			fileIDs.Add("MedicalReleaseForm2", 503);     // 2016-2017 Medical form
			
			// Download the first file
			bool firstPass = true;
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			foreach (KeyValuePair<string, int> kvp in fileIDs)
			{
				string remoteUri = "https://voyagers.ccbchurch.com/output/form_responses.php?form_id=" + kvp.Value.ToString();
				string serverPath = System.AppDomain.CurrentDomain.BaseDirectory;
				string fileName = Path.Combine(serverPath, @"vbs_dl\dl_" + kvp.Key + "_" + kvp.Value.ToString() + ".xlsx");
				//Response.Write(fileName + "<BR>");
				if (File.Exists(fileName)) { File.Delete(fileName); }
				client.DownloadFile(remoteUri, fileName);

				// Check to see if the new file exists and parse it if it does
				if (File.Exists(fileName))
				{
					ImportType type = ImportType.Base;
					//if (kvp.Key.ToLower().Contains("alt")) { type = ImportType.AltPay; }
					//else 
					if (kvp.Key.ToLower().Contains("under18")) { type = ImportType.VolunteerUnder18; }
					else if (kvp.Key.ToLower().Contains("adult")) { type = ImportType.VolunteerAdult; }
                    else if (kvp.Key.ToLower().Contains("medicalrelease")) { type = ImportType.MedicalReleaseForm; }
                    errors.AddRange(ImportFiles.Import(type, fileName));
					
					VBSLog log = new VBSLog();
					log.LogGUID = System.Guid.NewGuid().ToString();
					log.DateLogged = DateTime.Now;
					log.Information = true;
					log.Description = "Start import: " + type.ToString();
					log.AddedByUserID =
						log.UpdatedByUserID =
						"<SystemImport>";
					errors.AddRange(log.AddUpdate());

					_sb.AppendLine("<b>" + type.ToString() + "</b><br />");
					int errorCount = 0;
					VBSLogCollection logCollectionUpdate = new VBSLogCollection();
					foreach (ClassGenException ex in errors)
					{
						VBSLog item = new VBSLog();
						item.LogGUID = System.Guid.NewGuid().ToString();
						item.DateLogged = DateTime.Now;
						if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Warning) { item.Warning = true; }
						if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Critical) { item.Error = true; }
						if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Information) { item.Information = true; }
						item.Description = ex.Message;
						item.AddedByUserID =
							item.UpdatedByUserID =
							"<SystemImport>";
						logCollectionUpdate.Add(item);

						//if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Information ||
						//	ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Warning ||
						//	ex.ClassGenExceptionIconType == ClassGenExceptionIconType.System) 
						//{
						//	// Do nothing
						//}
						//else
						//{
							//_sb.AppendLine(ex.ClassGenExceptionIconType.ToString() + "<br />");
							_sb.AppendLine("&nbsp; &nbsp;" + ex.DescriptionWithException + "<br />");
							errorCount++;
						//}
						//_sb.AppendLine("&nbsp; &nbsp;" + ex.StackTrace + "<br />");
					}
					if (logCollectionUpdate.Count > 0)
					{
						errors.AddRange(logCollectionUpdate.AddUpdateAll());
					}
					//if (errorCount == 0) { _sb.AppendLine("&nbsp; &nbsp;No errors...<br />"); }
					errors.Clear();
				}
				//if (!firstPass) { _sb.AppendLine("<br>"); }
				_sb.AppendLine("<br>");
				firstPass = false;
			}

			_sb.AppendLine("<br>&nbsp;");

			// Go out and pull the last 100 messages from the log so we can see what happened
			_sbLog = new StringBuilder();
			VBSLogCollection logCollection = new VBSLogCollection("sAddedByUserID = '<SystemImport>' AND dtLogged > DATEADD(dd, -14, GETDATE())");
			logCollection.Sort(VBSLog.FN_DateLogged + " DESC");
			int logCount = 0;
			foreach (VBSLog item in logCollection)
			{
				_sbLog.AppendLine(String.Format("{0}: {1}<br>", item.DateLogged.ToString("MM/dd/yyyy hh:mm:ss tt"), item.Description));
				logCount++;
				if (logCount > 250) { break; }
			}
			_sbLog.AppendLine("<br>&nbsp;");
		}
	}
}