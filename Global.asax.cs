﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using VBS;

namespace VBS
{
	public class Global : HttpApplication
	{
		//void Application_PreRequestHandlerExecute(object sender, EventArgs e)
		//{
		//	if (Context.Request.UserAgent.ToLower().Contains("mobile"))
		//	{
		//		DevExpressHelper.Theme = "iOS";
		//	}
		//	else
		//	{
		//		DevExpressHelper.Theme = "Office2010Black";
		//	}
		//}

		void Application_Start(object sender, EventArgs e)
		{
			// Code that runs on application startup
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			//AuthConfig.RegisterOpenAuth();
		}

		void Application_End(object sender, EventArgs e)
		{
			//  Code that runs on application shutdown

		}

		void Application_Error(object sender, EventArgs e)
		{
			// Code that runs when an unhandled error occurs

		}
	}
}
