﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using VBS.Reports;

namespace VBS.Assignments
{
	public partial class ByTeam : System.Web.UI.Page
	{
		protected VBSGroupCollection _groups = new VBSGroupCollection();
		protected Search_VBS_ResponseToGroupCollection _assignments = new Search_VBS_ResponseToGroupCollection();

		private int _maxRecsOnPage = 175;

		protected bool _userCanEdit = true;

		protected StringBuilder _sbErrors = new StringBuilder();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			try
			{
				if (cboGroup.SelectedIndex == -1)
				{
					cboGroup.SelectedIndex = 0;
					cboGroup_SelectedIndexChanged(this, null);
				}

				// Bind the grid
				GetData();
				gridGroup.KeyFieldName = Search_VBS_ResponseToGroup.FN_LinkGUID;
				gridGroup.DataSource = _assignments;
				gridGroup.DataBind();

				// Tell if the user saw something
				if (!IsPostBack && !IsCallback)
				{
					Logging.LogInformation("User viewed group: " + cboGroup.SelectedItem.Text, null, string.Empty, Session["UserName"].ToString());

					// Reset the message sent
					//MessageText.Text = string.Empty;
					_sbErrors = new StringBuilder();
				}

				// Check to see if the user has the ability to edit.  If they don't, turn off the fields
				VBSUserCollection users = new VBSUserCollection("sUserName = '" + Session["UserName"].ToString().Replace("'", "''") + "'");
				_userCanEdit = true;
				if (users.Count > 0)
				{
					if (!users[0].CanEditTeams) { _userCanEdit = false; }
				}

				if (!_userCanEdit)
				{
					foreach (GridViewDataColumn col in gridGroup.Columns)
					{
						col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
						if (col.FieldName == "CheckedInGrid")
						{
							col.Visible = false;
						}
					}

					btnSelectAll.Visible =
						btnSelectNone.Visible =
						btnSubmit.Visible =
						btnSubmitHead.Visible =
						false;
				}
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			try
			{
				cboGroup.DataBind();		// Bind the class type

				// Check the Session for a data table
				if ((_assignments == null ||
					_assignments.Count == 0) &&
					Session["GroupAssignmentCollection"] != null)
				{
					_assignments = Session["GroupAssignmentCollection"] as Search_VBS_ResponseToGroupCollection;
				}
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			try
			{
				if (!IsPostBack || Session["GroupAssignmentCollection"] == null)
				{
					string groupName = cboGroup.SelectedItem.GetValue(VBSGroup.FN_GroupName).ToString();
					string groupGUID = cboGroup.SelectedItem.GetValue(VBSGroup.FN_GroupGUID).ToString();

					// Get the group assignments
					_assignments = new Search_VBS_ResponseToGroupCollection("sGroupGUID = '" + groupGUID + "'");
					_assignments.Sort("TeamCoordinator DESC, LastName, FirstName");

					// If this is a team leader or junior helper then add the list of juniors in there as well
					if (groupName.ToLower().Replace(" ", "").Equals("teamleader") ||
						groupName.ToLower().Replace(" ", "").Replace(".", "").Equals("jrteamleader"))
					{
						// Get the sub-data
						Search_VBS_VolunteerCollection localColl = new Search_VBS_VolunteerCollection("bTeacher <> 0");

						if (groupName.ToLower().Replace(" ", "").Replace(".", "").Equals("jrteamleader"))
						{
							_assignments = new Search_VBS_ResponseToGroupCollection("sGroupGUID IN (SELECT sGroupGUID FROM tVBSGroup WHERE sGroupName LIKE '%jr%')");
							_assignments.Sort("TeamCoordinator DESC, LastName, FirstName");
							localColl = new Search_VBS_VolunteerCollection("bJuniorHelper <> 0");
						}

						// Get a listing of individuals who are team leaders and/or jr. helpers
						localColl.Sort(Search_VBS_Volunteer.FN_Length + ", " +
							Search_VBS_Volunteer.FN_ClassCode + ", " +
							Search_VBS_Volunteer.FN_Teacher + " DESC, " +
							Search_VBS_Volunteer.FN_JuniorHelper + " DESC, " +
							Search_VBS_Volunteer.FN_FullNameLastFirst + "");
						foreach (Search_VBS_Volunteer item in localColl)
						{
							Search_VBS_ResponseToGroup resp = new Search_VBS_ResponseToGroup();
							resp.LinkGUID = System.Guid.NewGuid().ToString();
							resp.TeamCoordinator = false;
							resp.FirstName = item.FirstName;
							resp.LastName = item.LastName;
							resp.Email = item.Email;
							resp.JrHelperEmail = item.JrHelperEmail;
							resp.ShirtSize = item.ShirtSize;
							_assignments.Add(resp);
						}
					}

					// Build out the assignment collection to _maxRecsOnPage
					VBSGroupCollection groups = new VBSGroupCollection("sGroupGUID = '" + groupGUID + "'");
					if (groups.Count > 0)
					{
						while (_assignments.Count < _maxRecsOnPage)
						{
							Search_VBS_ResponseToGroup assign = new Search_VBS_ResponseToGroup();
							assign.LinkGUID = System.Guid.NewGuid().ToString();
							assign.GroupGUID = groups[0].GroupGUID;
							assign.GroupName = groups[0].GroupName;
							assign.TeamCoordinator = false;
							_assignments.Add(assign);
						}
					}

					// Update for the sort
					foreach (Search_VBS_ResponseToGroup assign in _assignments)
					{
						if (assign.TeamCoordinator)
						{
							assign.GridCustom_6 = "aaaaaaaaa";
						}
						else if (!String.IsNullOrEmpty(assign.LastName) &&
							!String.IsNullOrEmpty(assign.FirstName))
						{
							assign.GridCustom_6 = "bbbbbbbbb";
						}
						else
						{
							assign.GridCustom_6 = "zzzzzzzzzz";
						}
						//assign.GridCustom_6 = (String.IsNullOrEmpty(assign.LastName) &&
						//	String.IsNullOrEmpty(assign.FirstName) ? "zzzzzzzzzz" : "aaaaaaaaa");
					}

					int count = 1;
					foreach (Search_VBS_ResponseToGroup a in _assignments)
					{
						a.Numbering = count;
						count++;
					}

					// Store it off in the session
					Session["GroupAssignmentCollection"] = _assignments;
				}

				// Bring it back
				_assignments = (Search_VBS_ResponseToGroupCollection)Session["GroupAssignmentCollection"];
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void cboGroup_DataBinding(object sender, EventArgs e)
		{
			try
			{
				_groups = new VBSGroupCollection(string.Empty);
				_groups.Sort(VBSGroup.FN_GroupName);
				cboGroup.DataSource = _groups;
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void cboGroup_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				// When the index changes, bind the grid
				Session["GroupAssignmentCollection"] = null;

				// Get the dataset from the database
				GetData();
				gridGroup.KeyFieldName = Search_VBS_ResponseToGroup.FN_LinkGUID;
				gridGroup.DataSource = _assignments;
				gridGroup.DataBind();

				// Log the information
				Logging.LogInformation("User viewed group: " + cboGroup.SelectedItem.Text, null, string.Empty, Session["UserName"].ToString());

				ResetSelectionScript();
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		private void ResetSelectionScript()
		{
			try
			{
				StringBuilder sb = new StringBuilder();
				int cellCount = 0;
				for (int i = 0; i < _assignments.Count; i++)
				{
					if (_assignments[i].ResponseID != null)
					{
						cellCount++;
						sb.AppendLine(String.Format("gridGroup.batchEditApi.SetCellValue({0}, 'CheckedInGrid', true);", i));
					}
				}
				btnSelectAll.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";

				sb = new StringBuilder();
				for (int i = 0; i < _assignments.Count; i++)
				{
					sb.AppendLine(String.Format("gridGroup.batchEditApi.SetCellValue({0}, 'CheckedInGrid', false);", i));
				}
				btnSelectNone.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void gridGroup_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
		{
			try
			{
				// When the cell editor initializes, handle it
				if (e.Editor is ASPxComboBox &&
					cboGroup.SelectedItem != null)
				{
					// Get the listing of volunteers
					string groupName = cboGroup.SelectedItem.GetValue(VBSGroup.FN_GroupName).ToString();
					string groupGUID = cboGroup.SelectedItem.GetValue(VBSGroup.FN_GroupGUID).ToString();
					Search_VBS_VolunteerCollection coll = new Search_VBS_VolunteerCollection(string.Empty);
					coll.Sort(Search_VBS_Volunteer.FN_FullNameLastFirstUpper);

					// Update the name to show where this person is registered
					//foreach (Search_VBS_Volunteer s in coll)
					//{
					//	if (!String.IsNullOrEmpty(s.GroupGUID))
					//	{
					//		//s.GridCustom_5 = String.Format("{0}{1}", s.GroupName,
					//		//	(s.TeamCoordinator.HasValue && s.TeamCoordinator.Value ? " (Team Coord.)" : string.Empty)).Trim();
					//		//s.GridCustom_6 = (!String.IsNullOrEmpty(s.GridCustom_5) ? " - " + s.GridCustom_5 : string.Empty);

					//	}
					//}

					ASPxComboBox combo = ((ASPxComboBox)e.Editor);
					combo.Columns.Clear();
					combo.Columns.Add(Search_VBS_Volunteer.FN_LastName, "Last Name");
					combo.Columns.Add(Search_VBS_Volunteer.FN_FirstName, "First Name");
					//combo.Columns.Add(Search_VBS_Volunteer.FN_GradeInFall, "Grade");
					//combo.Columns.Add(Search_VBS_Volunteer.FN_GridCustom_5, "Currently Assigned Team");
					//combo.Columns.Add(Search_VBS_Volunteer.FN_GridCustom_6, "");
					combo.Columns.Add(Search_VBS_Volunteer.FN_AssignedTeamName, "Currently Assigned Team");
					combo.Columns.Add(Search_VBS_Volunteer.FN_IsAdultString, "Adult (Over 18)?");
					combo.Columns.Add(Search_VBS_Volunteer.FN_VolunteerNameFormattedWithTeamName, "");
					combo.Columns[combo.Columns.Count - 1].Width = 0;

					combo.DataSource = coll;
					combo.TextField = Search_VBS_Volunteer.FN_VolunteerNameFormattedWithTeamName;
					combo.ValueField = Search_VBS_Volunteer.FN_ResponseID;
					combo.ValueType = typeof(System.Int32);
					combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
					combo.TextFormatString = "{3}";
					combo.DataBindItems();
					combo.ReadOnly = false;
				}
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void gridGroup_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			try
			{
				if (e.Column.FieldName.ToLower() == "ResponseID".ToLower() &&
					e.GetFieldValue(Search_VBS_ResponseToGroup.FN_VolunteerNameFormatted) != null)
				{
					e.DisplayText = e.GetFieldValue(Search_VBS_ResponseToGroup.FN_VolunteerNameFormatted).ToString();
				}
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void gridGroup_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			try
			{
				VBSResponseToGroupCollection existingColl = new VBSResponseToGroupCollection();
				VBSClassLinkCollection classLinkColl = new VBSClassLinkCollection();
				List<string> existingResponseIDs = new List<string>();
				//string teamCoordID = string.Empty;

				//List<string> deletedGUIDs = new List<string>();
				//List<string> classLinkDeletedGUIDs = new List<string>();
				//VBSResponseToGroupCollection coll = new VBSResponseToGroupCollection();
				//List<string> newResponseIDs = new List<string>();

				// Now take care of the volunteers
				foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
				{
					OrderedDictionary keys = val.Keys;
					OrderedDictionary newVals = val.NewValues;
					OrderedDictionary oldVals = val.OldValues;

					//int respID = int.Parse(ASPxGridView.GetDetailRowValues(gridGroup, "iResponseID").ToString());
					//string valNew = ASPxGridView.GetDetailRowKeyValue(gridGroup).ToString();

					//ASPxGridView gv = ((ASPxGridView)sender).FindEditFormTemplateControl("gridGroup") as ASPxGridView;
					//ASPxCheckBox chk = gv.FindControl("TeamCoordinator") as ASPxCheckBox;
					//e.NewValues["CategoryName"] = txt1.Text;

					// Get the responseID from the record
					string linkGUID = (keys["LinkGUID"] != null && !String.IsNullOrEmpty(keys["LinkGUID"].ToString()) ?
						keys["LinkGUID"].ToString().Trim() : string.Empty);
					VBSResponseToGroupCollection resp = new VBSResponseToGroupCollection("sLinkGUID = '" + linkGUID + "'");
					int respID = (resp.Count > 0 ? resp[0].ResponseID : -1);
					
					//string oldResponseID = (oldVals["ResponseID"] != null && !String.IsNullOrEmpty(oldVals["ResponseID"].ToString()) ?
					//	oldVals["ResponseID"].ToString().Trim() : string.Empty);
					//string newResponseID = (newVals["ResponseID"] != null && !String.IsNullOrEmpty(newVals["ResponseID"].ToString()) ? 
					//	newVals["ResponseID"].ToString().Trim() : string.Empty);
					bool teamCoord = (bool)newVals["TeamCoordinator"];
					//if (!String.IsNullOrEmpty(teamCoordID) && 
					//	teamCoord)
					//{
					//	teamCoord = false;
					//}
					//if (teamCoord) { teamCoordID = respID.ToString(); }
					bool checkedInGrid = ((bool)newVals["CheckedInGrid"]);

					// Delete the old and new ones from the class listing first
					if (!checkedInGrid)
					{
						// We're going to make an update here
						existingColl.AddRange(new VBSResponseToGroupCollection("iResponseID = " + respID.ToString()));
						foreach (VBSResponseToGroup vbr in existingColl)
						{
							if (vbr.ResponseID == respID &&
								!existingResponseIDs.Contains(respID.ToString()))
							{
								vbr.TeamCoordinator = teamCoord;
							}
						}
						classLinkColl.AddRange(new VBSClassLinkCollection("iResponseID = " + respID.ToString()));
						foreach (VBSClassLink vcl in classLinkColl)
						{
							if (!existingResponseIDs.Contains(respID.ToString()))
							{
								vcl.Delete();
							}
						}
						existingResponseIDs.Add(respID.ToString());		// Add to the existing
						//sb.Append(newResponseID);
						//if (!String.IsNullOrEmpty(oldResponseID)) { sb.Append((sb.ToString().Length > 0 ? ", " : "") + oldResponseID); }
						//if (sb.ToString().Length > 0)
						//{
						//	VBSResponseToGroupCollection existingColl = new VBSResponseToGroupCollection("iResponseID IN (" + sb.ToString() + ")");
						//	foreach (VBSResponseToGroup link in existingColl)
						//	{
						//		if (!deletedGUIDs.Contains(link.LinkGUID))
						//		{
						//			deletedGUIDs.Add(link.LinkGUID);
						//		}
						//	}
						//	VBSClassLinkCollection classLinkColl = new VBSClassLinkCollection("iResponseID IN (" + sb.ToString() + ")");
						//	foreach (VBSClassLink link in classLinkColl)
						//	{
						//		if (!classLinkDeletedGUIDs.Contains(link.LinkGUID))
						//		{
						//			classLinkDeletedGUIDs.Add(link.LinkGUID);
						//		}
						//	}
						//}

						// Add the new row in
						//if (!newResponseIDs.Contains(newResponseID))
						//{
						//	VBSResponseToGroup linkNew = new VBSResponseToGroup();
						//	linkNew.LinkGUID = System.Guid.NewGuid().ToString();
						//	linkNew.ResponseID = int.Parse(newResponseID);
						//	newResponseIDs.Add(newResponseID);
						//	linkNew.GroupGUID = cboGroup.SelectedItem.Value.ToString();
						//	linkNew.TeamCoordinator = teamCoord;
						//	linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
						//	linkNew.UpdatedByUserID = linkNew.AddedByUserID;
						//	coll.Add(linkNew);
						//}
					}
					else
					{
						// Just delete the row
						existingColl.AddRange(new VBSResponseToGroupCollection("iResponseID = " + respID));
						foreach (VBSResponseToGroup link in existingColl)
						{
							link.Delete();		// Delete the record
							//if (!deletedGUIDs.Contains(link.LinkGUID))
							//{
							//	deletedGUIDs.Add(link.LinkGUID);
							//}
						}
					}
				}

				// Delete any that need to be deleted first
				ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
				errors.AddRange(classLinkColl.AddUpdateAll());
				errors.AddRange(existingColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();

				//if (deletedGUIDs.Count > 0)
				//{
				//	VBSResponseToGroupCollection existingColl = new VBSResponseToGroupCollection("sLinkGUID IN ('" + Utils.JoinString(deletedGUIDs, "', '") + "')");
				//	foreach (VBSResponseToGroup link in existingColl)
				//	{
				//		link.Delete();
				//		Logging.LogInformation("Deleting Volunteer from Group: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				//	}
				//	errors.AddRange(existingColl.AddUpdateAll());
				//	if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				//	errors.Clear();
				//}
				//if (classLinkDeletedGUIDs.Count > 0)
				//{
				//	VBSClassLinkCollection existingClassLinkColl = new VBSClassLinkCollection("sLinkGUID IN ('" + Utils.JoinString(classLinkDeletedGUIDs, "', '") + "')");
				//	foreach (VBSClassLink link in existingClassLinkColl)
				//	{
				//		link.Delete();
				//		Logging.LogInformation("Deleting Volunteer from Team Assignment: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				//	}
				//	errors.AddRange(existingClassLinkColl.AddUpdateAll());
				//	if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				//	errors.Clear();
				//}

				// Update the collection
				//if (coll.Count > 0)
				//{
				//	foreach (VBSResponseToGroup link in coll)
				//	{
				//		Logging.LogInformation("Adding Volunteer To Group: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				//	}
				//	errors.AddRange(coll.AddUpdateAll());
				//	if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				//}

				// Go out and get the updated data from the collection
				if (_assignments.Count > 0)
				{
					Search_VBS_ResponseToGroupCollection newCollection =
						new Search_VBS_ResponseToGroupCollection("sGroupGUID = '" + _assignments[0].GroupGUID + "'");

					_assignments.Clear();		// Clear out the collection
					//for (int i = _assignments.Count - 1; i >= 0; i--)
					//{
					//	// Remove them all
					//	_assignments.RemoveAt(i);
					//}

					// Add them back in
					foreach (Search_VBS_ResponseToGroup assign in newCollection)
					{
						_assignments.Add(assign);
					}

					// Fill out the list
					while (_assignments.Count > _maxRecsOnPage)
					{
						for (int i = _assignments.Count - 1; i >= 0; i--)
						{
							if (!String.IsNullOrEmpty(_assignments[i].GridCustom_6) &&
								_assignments[i].GridCustom_6.ToLower().StartsWith("zz"))
							{
								_assignments.RemoveAt(i);
								break;
							}
						}
					}

					VBSGroupCollection groups = new VBSGroupCollection("sGroupGUID = '" +
						cboGroup.SelectedItem.Value.ToString() + "'");
					if (groups.Count > 0)
					{
						while (_assignments.Count < _maxRecsOnPage)
						{
							Search_VBS_ResponseToGroup assign = new Search_VBS_ResponseToGroup();
							assign.LinkGUID = System.Guid.NewGuid().ToString();
							assign.GroupGUID = groups[0].GroupGUID;
							assign.GroupName = groups[0].GroupName;
							assign.TeamCoordinator = false;
							_assignments.Add(assign);
						}
					}

					// Update for the sort
					foreach (Search_VBS_ResponseToGroup assign in _assignments)
					{
						if (assign.TeamCoordinator)
						{
							assign.GridCustom_6 = "aaaaaaaaa";
						}
						else if (!String.IsNullOrEmpty(assign.LastName) &&
							!String.IsNullOrEmpty(assign.FirstName))
						{
							assign.GridCustom_6 = "bbbbbbbbb";
						}
						else
						{
							assign.GridCustom_6 = "zzzzzzzzzz";
						}
						//assign.GridCustom_6 = (String.IsNullOrEmpty(assign.LastName) &&
						//	String.IsNullOrEmpty(assign.FirstName) ? "zzzzzzzzzz" : "aaaaaaaaa");
					}

					_assignments.Sort(Search_VBS_ResponseToGroup.FN_GridCustom_6 + ", " +
						Search_VBS_ResponseToGroup.FN_FullNameLastFirstUpper);

					// Handle the numbering
					int count = 1;
					foreach (Search_VBS_ResponseToGroup a in _assignments)
					{
						a.Numbering = count;
						count++;
					}

					Session["GroupAssignmentCollection"] = _assignments;
				}

				gridGroup.DataBind();

				// Reset the javascript code on the buttons
				ResetSelectionScript();

				// Handle the event internally
				e.Handled = true;
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		//protected void gridGroup_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
		//{
		//	ASPxGridView gv = (ASPxGridView)sender;
		//	gv.JSProperties.Add("cpIsCustomCallback", null);
		//	gv.JSProperties["cpIsCustomCallback"] = e.CallbackName == "CUSTOMCALLBACK";
		//}

		protected void cbSelectAll_Callback(object sender, CallbackEventArgsBase e)
		{
			try
			{
				ResetSelectionScript();
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void gridGroup_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			try
			{
				// Hide the buttons
				if (e.ButtonType == ColumnCommandButtonType.Update)
				{
					e.Visible = false;
				}
				else if (e.ButtonType == ColumnCommandButtonType.Cancel)
				{
					e.Visible = false;
				}
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				gridGroup.UpdateEdit();
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void btnSubmitHead_Click(object sender, EventArgs e)
		{
			try
			{
				gridGroup.UpdateEdit();
			}
			catch (Exception ex)
			{
				Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, Session["UserName"].ToString());
			}
		}

		protected void btnSendCoordinatorEmail_Click(object sender, EventArgs e)
		{
			// When they click on the send email button, generate the report and send it
			string groupName = cboGroup.SelectedItem.GetValue(VBSGroup.FN_GroupName).ToString();
			string groupGUID = cboGroup.SelectedItem.GetValue(VBSGroup.FN_GroupGUID).ToString();

			// Get the collections we'll be using to build both reports
			Search_VBS_VolunteerCollection groupCollection = new Search_VBS_VolunteerCollection("sGroupGUID IS NOT NULL AND sGroupName = '" + 
				groupName.Replace("'", "''") + "'");
			if (groupName.ToLower().Replace(" ", "").Replace(".", "").Equals("jrteamleader"))
			{
				groupCollection = new Search_VBS_VolunteerCollection("sGroupGUID IS NOT NULL AND sGroupName LIKE '%jr%'");
			}

			// Add all the teachers and junior helpers
			if (groupName.ToLower().Replace(" ", "").Equals("teamleader"))
			{
				Search_VBS_VolunteerCollection localColl = new Search_VBS_VolunteerCollection("bTeacher <> 0");
				localColl.Sort(Search_VBS_Volunteer.FN_Length + ", " +
					Search_VBS_Volunteer.FN_ClassCode + ", " +
					Search_VBS_Volunteer.FN_Teacher + " DESC, " +
					Search_VBS_Volunteer.FN_JuniorHelper + " DESC, " +
					Search_VBS_Volunteer.FN_FullNameLastFirst + "");
				foreach (Search_VBS_Volunteer item in localColl)
				{
					item.GroupGUID = groupGUID;
					item.GroupName = groupName;
					groupCollection.Add(item);
				}
			}
			else if (groupName.ToLower().Replace(" ", "").Replace(".", "").Equals("jrteamleader"))
			{
				Search_VBS_VolunteerCollection localColl = new Search_VBS_VolunteerCollection("bJuniorHelper <> 0");
				localColl.Sort(Search_VBS_Volunteer.FN_Length + ", " +
					Search_VBS_Volunteer.FN_ClassCode + ", " +
					Search_VBS_Volunteer.FN_Teacher + " DESC, " +
					Search_VBS_Volunteer.FN_JuniorHelper + " DESC, " +
					Search_VBS_Volunteer.FN_FullNameLastFirst + "");
				foreach (Search_VBS_Volunteer item in localColl)
				{
					item.GroupGUID = groupGUID;
					item.GroupName = groupName;
					groupCollection.Add(item);
				}
			}
			else
			{
				groupCollection.Sort(Search_VBS_Volunteer.FN_GroupName + ", " +
					Search_VBS_Volunteer.FN_TeamCoordinator + " DESC, " +
					Search_VBS_Volunteer.FN_FullNameLastFirstUpper);
			}
			Dictionary<string, int> _adultCount = new Dictionary<string, int>();
			Dictionary<string, int> _juniorCount = new Dictionary<string, int>();
			foreach (Search_VBS_Volunteer item in groupCollection)
			{
				item.GridCustom_1 = "Team: " + item.GroupName;
				if (!_adultCount.ContainsKey(item.GroupName)) { _adultCount.Add(item.GroupName, 0); }
				if (!_juniorCount.ContainsKey(item.GroupName)) { _juniorCount.Add(item.GroupName, 0); }
				if (item.IsAdult.HasValue && item.IsAdult.Value)
				{
					_adultCount[item.GroupName]++;
				}
				else
				{
					_juniorCount[item.GroupName]++;
				}
			}
			foreach (Search_VBS_Volunteer item in groupCollection)
			{
				item.GridCustom_2 = _adultCount[item.GroupName].ToString();
				item.GridCustom_3 = _juniorCount[item.GroupName].ToString();
			}

			// Next run the group listing
			rptGroupRoster rptgrouproster = new rptGroupRoster();
			rptgrouproster.DataSource = groupCollection;
			rptgrouproster.DisplayName = "Groups";
			rptgrouproster.CreateDocument();

			// Reset all the page numbers if they're set
			rptgrouproster.PrintingSystem.ContinuousPageNumbering = true;

			// Send the file
			MemoryStream strm = new MemoryStream();
			rptgrouproster.ExportToPdf(strm);
			strm.Seek(0, System.IO.SeekOrigin.Begin);

			// Get the people who are the leaders for this team
			List<MailAddress> leaders = new List<MailAddress>();
			List<string> existingLeaders = new List<string>();
			foreach (Search_VBS_Volunteer v in groupCollection)
			{
				if (v.TeamCoordinator.HasValue && v.TeamCoordinator.Value && !existingLeaders.Contains(v.Email.ToLower()))
				{
					existingLeaders.Add(v.Email.ToLower());
					leaders.Add(new MailAddress(v.Email.ToLower(), v.FullNameFirstLast));
				}
				//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.Email.ToLower())) { leaders.Add(v.Email.ToLower()); }
				//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.JrHelperEmail.ToLower())) { leaders.Add(v.JrHelperEmail.ToLower()); }
			}

			//string body = "Attached is a copy of the latest " + group + " roster.";
			string body = "Attached is a copy of the latest " + groupName + " roster.\r\n\r\n" +
				"Please contact any new volunteers that have been assigned to the team.\r\n\r\n" +
				"If you have any questions, please feel free to contact me at jen@gerlach5.com.";
			if (leaders.Count == 0)
			{
				body += "\r\n\r\n" + "THERE ARE NO LEADERS FOR THIS GROUP/TEAM!!";
			}

			List<MailAddress> ccList = new List<MailAddress>();
			ccList.Add(new MailAddress(Session["UserName"].ToString()));

			List<MailAddress> bccList = new List<MailAddress>();

			// Send the email
			System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(strm, groupName.Replace(" ", "") + "Roster.pdf", "application/pdf");
			ClassGenExceptionCollection errors = Utils.SendEMailAsJen(leaders,
				ccList,
				bccList,
				groupName + " Roster", body, string.Empty,
			new List<System.Net.Mail.Attachment>(new System.Net.Mail.Attachment[] {
				attachment,
			}), Session["UserName"].ToString());

			// Kill off the memory string
			strm.Close();

			_sbErrors = new StringBuilder();
			if (errors.Count > 0)
			{
				foreach (ClassGenException ex in errors)
				{
					if (_sbErrors.ToString().Length > 0) { _sbErrors.Append("<br />"); }
					_sbErrors.Append(ex.Message + " | " + ex.StackTrace);
				}
				_sbErrors.Append("&nbsp; &nbsp;");
				//MessageText.Text = sbErrors.ToString();
			}
			else
			{
				//MessageText.Text = "Message sent.";
				_sbErrors.Append("Message sent.&nbsp; &nbsp;");
			}

			// Clear the label
			ClientScript.RegisterStartupScript(this.GetType(), "clrLabel", "ClearLabel();", true);
		}
	}
}