﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Assignments
{
	public partial class ByStudent : System.Web.UI.Page
	{
		protected DataTable _dt = null;
		protected Search_VBS_StudentsCollection _assignments = new Search_VBS_StudentsCollection();

		protected bool _userCanEdit = true;

		private int _maxRecsOnPage = 1000;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			if (cboFirstLetterLastName.SelectedIndex == -1)
			{
				cboFirstLetterLastName.SelectedIndex = 2;		// Hit the "A"
				cboFirstLetterLastName_SelectedIndexChanged(this, null);
			}

			// Bind the grid
			GetData();
			gridStudent.KeyFieldName = Search_VBS_Students.FN_RawGUID;
			gridStudent.DataSource = _assignments;
			gridStudent.DataBind();

			// Tell if the user saw something
			if (!IsPostBack && !IsCallback)
			{
				Logging.LogInformation("User viewed students: " + cboFirstLetterLastName.SelectedItem.Text, null, string.Empty, HttpContext.Current.User.Identity.Name);
			}

			// Check to see if the user has the ability to edit.  If they don't, turn off the fields
			VBSUserCollection users = new VBSUserCollection("sUserName = '" + Session["UserName"].ToString().Replace("'", "''") + "'");
			_userCanEdit = true;
			if (users.Count > 0)
			{
				if (!users[0].CanEditClasses) { _userCanEdit = false; }
			}

			if (!_userCanEdit)
			{
				btnSelectAll.Visible =
					btnSelectNone.Visible =
					btnSubmit.Visible =
					btnSubmitHead.Visible = 
					false;

				foreach (GridViewColumn col in gridStudent.Columns)
				{
					if (col is GridViewDataColumn)
					{
						((GridViewDataColumn)col).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
					}
				}
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			cboFirstLetterLastName.DataBind();		// Bind the class type

			// Check the Session for a data table
			if ((_assignments == null ||
				_assignments.Count == 0) &&
				Session["StudentAssignmentsCollection"] != null)
			{
				_assignments = Session["StudentAssignmentsCollection"] as Search_VBS_StudentsCollection;
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			if (!IsPostBack || Session["StudentAssignmentsCollection"] == null)
			{
				string firstLetter = cboFirstLetterLastName.SelectedItem.GetValue(Search_VBS_Students.FN_Left1LastName).ToString();

				// Get the class assignments
				if (firstLetter.ToLower().StartsWith("Last Name ".ToLower()))
				{
					_assignments = new Search_VBS_StudentsCollection("sLeft1LastName = '" + firstLetter.Substring(firstLetter.IndexOf(":") + 1).Trim() + "'");
				}
				else if (firstLetter.ToLower().StartsWith("Grade:".ToLower()))
				{
					_assignments = new Search_VBS_StudentsCollection("sGradeInFall = '" + firstLetter.Substring(firstLetter.IndexOf(":") + 1).Trim() + "'");
				}
				else if (firstLetter.ToLower().Contains("all"))
				{
					_assignments = new Search_VBS_StudentsCollection(string.Empty);
				}
				else if (firstLetter.ToLower().Contains("unassign"))
				{
					_assignments = new Search_VBS_StudentsCollection("sLinkGUID IS NULL");
				}
				//else
				//{
				//	_assignments = new Search_VBS_StudentsCollection("sLeft1LastName = '" + firstLetter + "'");
				//}
				_assignments.Sort(Search_VBS_Students.FN_FullNameLastFirstUpper);

				// Build out the assignment collection to 50
				//VBSClassCodeCollection classCollection = new VBSClassCodeCollection("sClassCode = '" + classCode + "'");
				//if (classCollection.Count > 0)
				//{
				//	while (_assignments.Count < _maxRecsOnPage)
				//	{
				//		Search_VBS_ClassAssignment assign = new Search_VBS_ClassAssignment();
				//		assign.LinkGUID = System.Guid.NewGuid().ToString();
				//		assign.RawGUID = System.Guid.NewGuid().ToString();
				//		assign.ClassCode = classCollection[0].ClassCode;
				//		assign.Grade = classCollection[0].Grade;
				//		assign.Length = classCollection[0].Length;
				//		_assignments.Add(assign);
				//	}
				//}

				// Update for the sort
				//foreach (Search_VBS_ClassAssignment assign in _assignments)
				//{
				//	assign.GridCustom_6 = (String.IsNullOrEmpty(assign.LastName) &&
				//		String.IsNullOrEmpty(assign.FirstName) ? "zzzzzzzzzz" : "aaaaaaaaa");
				//}

				int count = 1;
				foreach (Search_VBS_Students a in _assignments)
				{
					a.Numbering = count;
					count++;
				}

				// Store it off in the session
				Session["StudentAssignmentsCollection"] = _assignments;
			}

			// Bring it back
			_assignments = (Search_VBS_StudentsCollection)Session["StudentAssignmentsCollection"];
		}

		protected void cboFirstLetterLastName_DataBinding(object sender, EventArgs e)
		{
			List<string> list = Search_VBS_Students.GetDistinctFromDB(Search_VBS_StudentsField.Left1LastName);
			list.Sort();
			_dt = new DataTable();
			_dt.Columns.Add("Left1LastName", typeof(System.String));
			_dt.Rows.Add(new object[] { "< ALL >" });
			_dt.Rows.Add(new object[] { "< Unassigned >" });
			foreach (string s in list)
			{
				_dt.Rows.Add(new object[] { "Last Name Starts With: " + s });
			}

			// Add the grade in there as well
			List<string> listGrades = Search_VBS_Students.GetDistinctFromDB(Search_VBS_StudentsField.GradeInFall);
			listGrades.Sort();
			foreach (string s in listGrades)
			{
				if (s.ToUpper().StartsWith("K")) { _dt.Rows.Add(new object[] { "Grade: " + s }); }
			}
			foreach (string s in listGrades)
			{
				if (!s.ToUpper().StartsWith("K")) { _dt.Rows.Add(new object[] { "Grade: " + s }); }
			}

			// Set the collection
			cboFirstLetterLastName.DataSource = _dt;
		}

		protected void cboFirstLetterLastName_SelectedIndexChanged(object sender, EventArgs e)
		{
			// When the index changes, bind the grid
			Session["StudentAssignmentsCollection"] = null;

			// Get the dataset from the database
			GetData();
			gridStudent.KeyFieldName = Search_VBS_Students.FN_RawGUID;
			gridStudent.DataSource = _assignments;
			gridStudent.DataBind();

			// Log the information
			Logging.LogInformation("User viewed students: " + cboFirstLetterLastName.SelectedItem.Text, null, string.Empty, Session["UserName"].ToString());

			ResetSelectionScript();
		}

		private void ResetSelectionScript()
		{
			StringBuilder sb = new StringBuilder();
			int cellCount = 0;
			for (int i = 0; i < _assignments.Count; i++)
			{
				if (_assignments[i].ResponseID != null)
				{
					cellCount++;
					sb.AppendLine(String.Format("gridStudent.batchEditApi.SetCellValue({0}, 'CheckedInGrid', true);", i));
				}
			}
			btnSelectAll.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";

			sb = new StringBuilder();
			for (int i = 0; i < _assignments.Count; i++)
			{
				sb.AppendLine(String.Format("gridStudent.batchEditApi.SetCellValue({0}, 'CheckedInGrid', false);", i));
			}
			btnSelectNone.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";
		}

		protected void gridStudent_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
		{
			// When the cell editor initializes, handle it
			if (e.Editor is ASPxComboBox &&
				cboFirstLetterLastName.SelectedItem != null)
			{
				//// Get the listing of students
				//string left1LastName = cboFirstLetterLastName.SelectedItem.GetValue(cboFirstLetterLastName.Columns[0].FieldName).ToString();
				//Search_VBS_StudentsCollection coll = new Search_VBS_StudentsCollection("sLeft1LastName = '" + left1LastName + "'");
				//coll.Sort(Search_VBS_Students.FN_FullNameLastFirst);

				// Get the class listing
				Search_VBS_ClassCodeCollection classes = new Search_VBS_ClassCodeCollection(string.Empty);
				classes.Sort(Search_VBS_ClassCode.FN_Length + ", " + Search_VBS_ClassCode.FN_ClassCode);

				// Update the name to show where this person is registered
				foreach (Search_VBS_ClassCode item in classes)
				{
					if (!String.IsNullOrEmpty(item.ClassCode))
					{
						item.GridCustom_5 = String.Format("{0} ({1})", item.ClassCode, item.Grade);
					}
				}

				ASPxComboBox combo = ((ASPxComboBox)e.Editor);
				combo.Columns.Clear();
				combo.Columns.Add(Search_VBS_ClassCode.FN_ClassCode, "Class Code");
				combo.Columns.Add(Search_VBS_ClassCode.FN_Grade, "Grade");
				combo.Columns.Add(Search_VBS_ClassCode.FN_StudentCount, "# Students in Class");
				//combo.Columns.Add(Search_VBS_Students.FN_GradeInFall, "Grade");
				//combo.Columns.Add(Search_VBS_Students.FN_GridCustom_5, "Currently Assigned Class");

				combo.DataSource = classes;
				combo.TextField = Search_VBS_ClassCode.FN_GridCustom_5;
				combo.ValueField = Search_VBS_ClassCode.FN_ClassCode;
				combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
				combo.TextFormatString = "{0} ({1})";
				combo.DataBindItems();
				combo.ReadOnly = false;
			}
		}

		protected void gridStudent_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			if (e.Column.FieldName.ToLower() == "ResponseID".ToLower() &&
				e.GetFieldValue(Search_VBS_Students.FN_ClassCodeWithDesc) != null)
			{
				e.DisplayText = e.GetFieldValue(Search_VBS_Students.FN_ClassCodeWithDesc).ToString();
			}
		}

		protected void gridStudent_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			List<string> deletedGUIDs = new List<string>();
			VBSClassLinkCollection coll = new VBSClassLinkCollection();
			List<string> newResponseIDs = new List<string>();

			// Now take care of the students
			foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
			{
				OrderedDictionary keys = val.Keys;
				OrderedDictionary newVals = val.NewValues;
				OrderedDictionary oldVals = val.OldValues;

				string oldRawGUID = (keys["RawGUID"] != null && !String.IsNullOrEmpty(keys["RawGUID"].ToString()) ?
					keys["RawGUID"].ToString().Trim() : string.Empty);
				string responseID = _assignments.Get(oldRawGUID).ResponseID.ToString().Trim();
				//string oldResponseID = (oldVals["ResponseID"] != null && !String.IsNullOrEmpty(oldVals["ResponseID"].ToString()) ?
				//	oldVals["ResponseID"].ToString().Trim() : string.Empty);
				//string newResponseID = oldResponseID;
				//string newResponseID = _assignments.Get(oldRawGUID).ResponseID.ToString().Trim();
				string newClassCode = newVals["ClassCodeWithDesc"].ToString().Trim();
				bool checkedInGrid = ((bool)newVals["CheckedInGrid"]);

				// Delete the old and new ones from the class listing first
				StringBuilder sb = new StringBuilder();
				if (!checkedInGrid)
				{
					// We're going to make an update here
					sb.Append(responseID);
					//if (!String.IsNullOrEmpty(oldResponseID)) { sb.Append((sb.ToString().Length > 0 ? ", " : "") + oldResponseID); }
					if (sb.ToString().Length > 0)
					{
						VBSClassLinkCollection existingColl = new VBSClassLinkCollection("iResponseID IN (" + sb.ToString() + ")");
						foreach (VBSClassLink link in existingColl)
						{
							//link.Delete();
							if (!deletedGUIDs.Contains(link.LinkGUID))
							{
								deletedGUIDs.Add(link.LinkGUID);
							}
						}
					}

					// Add the new row in
					if (!newResponseIDs.Contains(responseID))
					{
						VBSClassLink linkNew = new VBSClassLink();
						linkNew.LinkGUID = System.Guid.NewGuid().ToString();
						linkNew.ResponseID = int.Parse(responseID);
						newResponseIDs.Add(responseID);
						linkNew.ClassCode = newClassCode;
						linkNew.Student = true;
						linkNew.Teacher = false;
						linkNew.JuniorHelper = false;
						linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
						linkNew.UpdatedByUserID = linkNew.AddedByUserID;
						coll.Add(linkNew);
					}
				}
				else
				{
					// Just delete the row
					VBSClassLinkCollection existingColl = new VBSClassLinkCollection("iResponseID IN (" + responseID + ")");
					foreach (VBSClassLink link in existingColl)
					{
						//link.Delete();
						if (!deletedGUIDs.Contains(link.LinkGUID))
						{
							deletedGUIDs.Add(link.LinkGUID);
						}
					}
				}
			}

			// Delete any that need to be deleted first
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			if (deletedGUIDs.Count > 0)
			{
				VBSClassLinkCollection existingColl = new VBSClassLinkCollection("sLinkGUID IN ('" + Utils.JoinString(deletedGUIDs, "', '") + "')");
				foreach (VBSClassLink link in existingColl)
				{
					link.Delete();
					Logging.LogInformation("Deleting Student: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(existingColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();
			}

			// Update the collection
			if (coll.Count > 0)
			{
				foreach (VBSClassLink link in coll)
				{
					Logging.LogInformation("Adding Student: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(coll.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
			}

			// Go out and get the updated data from the collection
			if (_assignments.Count > 0)
			{
				Search_VBS_StudentsCollection newCollection = new Search_VBS_StudentsCollection();
				string firstLetter = cboFirstLetterLastName.SelectedItem.GetValue(Search_VBS_Students.FN_Left1LastName).ToString();
				if (firstLetter.ToLower().Contains("all"))
				{
					newCollection = new Search_VBS_StudentsCollection(string.Empty);
				}
				else if (firstLetter.ToLower().Contains("unassign"))
				{
					newCollection = new Search_VBS_StudentsCollection("sLinkGUID IS NULL");
				}
				else
				{
					newCollection = new Search_VBS_StudentsCollection("sLeft1LastName = '" + firstLetter + "'");
				}
					//new Search_VBS_StudentsCollection("sLeft1LastName = '" + _assignments[0].Left1LastName + "'");

				for (int i = _assignments.Count - 1; i >= 0; i--)
				{
					// Remove them all
					_assignments.RemoveAt(i);
				}

				// Add them back in
				foreach (Search_VBS_Students assign in newCollection)
				{
					_assignments.Add(assign);
				}

				// Fill out the list
				while (_assignments.Count > _maxRecsOnPage)
				{
					for (int i = _assignments.Count - 1; i >= 0; i--)
					{
						if (!String.IsNullOrEmpty(_assignments[i].GridCustom_6) &&
							_assignments[i].GridCustom_6.ToLower().StartsWith("zz"))
						{
							_assignments.RemoveAt(i);
							break;
						}
					}
				}

				//VBSClassCodeCollection classCode = new VBSClassCodeCollection("sClassCode = '" +
				//	cboFirstLetterLastName.SelectedItem.Value.ToString().Replace("'", "''") + "'");
				//if (classCode.Count > 0)
				//{
				//	while (_assignments.Count < _maxRecsOnPage)
				//	{
				//		Search_VBS_ClassAssignment assign = new Search_VBS_ClassAssignment();
				//		assign.LinkGUID = System.Guid.NewGuid().ToString();
				//		assign.RawGUID = System.Guid.NewGuid().ToString();
				//		assign.ClassCode = classCode[0].ClassCode;
				//		assign.Grade = classCode[0].Grade;
				//		assign.Length = classCode[0].Length;
				//		_assignments.Add(assign);
				//	}
				//}

				//// Update for the sort
				//foreach (Search_VBS_ClassAssignment assign in _assignments)
				//{
				//	assign.GridCustom_6 = (String.IsNullOrEmpty(assign.LastName) &&
				//		String.IsNullOrEmpty(assign.FirstName) ? "zzzzzzzzzz" : "aaaaaaaaa");
				//}

				_assignments.Sort(		//Search_VBS_ClassAssignment.FN_GridCustom_6 + ", " +
					Search_VBS_Students.FN_LastName + ", " +
					Search_VBS_Students.FN_FirstName);

				// Handle the numbering
				int count = 1;
				foreach (Search_VBS_Students a in _assignments)
				{
					a.Numbering = count;
					count++;
				}

				Session["StudentAssignmentsCollection"] = _assignments;
			}

			gridStudent.DataBind();

			// Reset the javascript code on the buttons
			ResetSelectionScript();

			// Handle the event internally
			e.Handled = true;
		}

		protected void cbSelectAll_Callback(object sender, CallbackEventArgsBase e)
		{
			ResetSelectionScript();
		}

		protected void gridStudent_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			gridStudent.UpdateEdit();
		}

		protected void btnSubmitHead_Click(object sender, EventArgs e)
		{
			gridStudent.UpdateEdit();
		}
	}
}