﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VBS.Reports;

namespace VBS.Assignments
{
	public partial class SendClassRosters : System.Web.UI.Page
	{
		protected Search_VBS_ClassAssignmentCollection _classAssignments = new Search_VBS_ClassAssignmentCollection();
		protected DataTable _dt = new DataTable();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Bind the grid
			GetData();
			gridList.KeyFieldName = Search_VBS_ResponseToGroup.FN_LinkGUID;
			gridList.DataSource = _dt;
			gridList.DataBind();

			foreach (GridViewDataColumn col in gridList.Columns)
			{
				if (col.FieldName != "CheckedInGrid")
				{
					col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
				}
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			if (!IsPostBack || Session["SendClassRoster"] == null)
			{
				// Get the assignments
				_classAssignments = new Search_VBS_ClassAssignmentCollection(string.Empty);
				_classAssignments.Sort(Search_VBS_ClassAssignment.FN_Length + ", " + Search_VBS_ClassAssignment.FN_ClassCode);
				Dictionary<string, int> classes = new Dictionary<string, int>();
				foreach (Search_VBS_ClassAssignment item in _classAssignments)
				{
					if (!classes.ContainsKey(item.ClassCode)) { classes.Add(item.ClassCode, 0); }
					classes[item.ClassCode]++;
				}

				// Load up the datatable
				_dt = new DataTable();
				_dt.Columns.Add("LinkGUID", typeof(System.String));
				_dt.Columns.Add("CheckedInGrid", typeof(System.Boolean));
				_dt.Columns.Add("ClassCode", typeof(System.String));
				_dt.Columns.Add("Count", typeof(System.Int32));
				foreach (KeyValuePair<string, int> kvp in classes)
				{
					_dt.Rows.Add(new object[] {
					System.Guid.NewGuid().ToString(),
					true,
					kvp.Key, 
					kvp.Value,
				});
				}

				Session["SendClassRoster"] = _dt;
			}

			// Bring it back
			_dt = (DataTable)Session["SendClassRoster"];
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			gridList.UpdateEdit();
		}

		protected void gridList_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void gridList_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
			{
				OrderedDictionary keys = val.Keys;
				OrderedDictionary newVals = val.NewValues;
				OrderedDictionary oldVals = val.OldValues;

				// Go through the collection and see if you can find the one they're dealing with
				string linkGUID = (keys["LinkGUID"] != null && !String.IsNullOrEmpty(keys["LinkGUID"].ToString()) ?
					keys["LinkGUID"].ToString().Trim() : string.Empty);
				bool chkd = (bool)newVals["CheckedInGrid"];
				foreach (DataRow row in _dt.Rows)
				{
					if (row["LinkGUID"].ToString().ToLower().Equals(linkGUID.ToLower()))
					{
						// Here's the one we need
						row["CheckedInGrid"] = chkd;
						break;
					}
				}
			}

			// Build and send the emails
			StringBuilder sb = new StringBuilder();
			foreach (DataRow row in _dt.Rows)
			{
				if ((bool)row["CheckedInGrid"])
				{
					string classCode = row["ClassCode"].ToString();

					// Get the collections we'll be using to build both reports
					Search_VBS_ClassAssignmentCollection classCollection = new Search_VBS_ClassAssignmentCollection("sClassCode IS NOT NULL AND sClassCode = '" + classCode.Replace("'", "''") + "'");
					classCollection.Sort(Search_VBS_Volunteer.FN_Length + ", " +
						Search_VBS_ClassAssignment.FN_ClassCode + ", " +
						Search_VBS_ClassAssignment.FN_Teacher + " DESC, " +
						Search_VBS_ClassAssignment.FN_JuniorHelper + " DESC, " +
						Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);
					foreach (Search_VBS_ClassAssignment item in classCollection)
					{
						item.GridCustom_1 = "Class: " + item.ClassCodeWithDesc;
						item.ClassColor = item.ClassColor.ToUpper();
					}

					// Run the team leader roster first
					// This is going to be the teachers in the classes
					rptTeamLeaderRoster rptteamleaderroster = new rptTeamLeaderRoster();
					rptteamleaderroster.DataSource = classCollection;
					rptteamleaderroster.DisplayName = "Team Leaders";
					rptteamleaderroster.CreateDocument();

					// Reset all the page numbers if they're set
					rptteamleaderroster.PrintingSystem.ContinuousPageNumbering = true;

					// Send the file
					MemoryStream strm = new MemoryStream();
					rptteamleaderroster.ExportToPdf(strm);
					strm.Seek(0, System.IO.SeekOrigin.Begin);

					// Get the people who are the leaders in this class
					List<MailAddress> leaders = new List<MailAddress>();
					List<string> existingLeaders = new List<string>();
					foreach (Search_VBS_ClassAssignment v in classCollection)
					{
						if (v.Teacher && !existingLeaders.Contains(v.Email.ToLower())) 
						{ 
							existingLeaders.Add(v.Email.ToLower()); 
							leaders.Add(new MailAddress(v.Email.ToLower(), v.FullNameFirstLast)); 
						}
						//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.Email.ToLower())) { leaders.Add(v.Email.ToLower()); }
						//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.JrHelperEmail.ToLower())) { leaders.Add(v.JrHelperEmail.ToLower()); }
					}

					//string body = "Attached is a copy of the latest class " + classCode + " roster.";
					string body = "Attached is a copy of the latest class " + classCode + " roster.\r\n\r\n" +
						"Please contact any new volunteers that have been assigned to the team.\r\n\r\n" +
						"If you have any questions, please feel free to contact the team leader coordinator.";
					if (leaders.Count == 0)
					{
						body += "\r\n\r\n" + "THERE ARE NO LEADERS IN THIS CLASS!!";
					}

					List<MailAddress> ccList = new List<MailAddress>();
					ccList.Add(new MailAddress(Session["UserName"].ToString()));

					List<MailAddress> bccList = new List<MailAddress>();
					
					// Send the email
					System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(strm, "Class_" + classCode.Replace(" ", "") + "_Roster.pdf", "application/pdf");
					Utils.SendEMailAsJen(leaders,
							ccList,
							bccList, 
							"Class " + classCode + " Roster", body, string.Empty,
								new List<System.Net.Mail.Attachment>(new System.Net.Mail.Attachment[] {
							attachment,
						}), Session["UserName"].ToString());

					// Kill off the memory string
					strm.Close();

					if (sb.ToString().Length > 0) { sb.Append("&nbsp; &nbsp;<br />"); }
					sb.Append("Message sent for class: " + classCode);
				}
			}

			MessageText.Text = sb.ToString();

			// Handle the event internally
			e.Handled = true;
		}
	}
}