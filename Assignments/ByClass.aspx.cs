﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using VBS.Reports;

namespace VBS.Assignments
{
	public partial class ByClass : System.Web.UI.Page
	{
		protected Search_VBS_VolunteerCollection _teachers = new Search_VBS_VolunteerCollection();
		protected Search_VBS_VolunteerCollection _helpers = new Search_VBS_VolunteerCollection();
		protected VBSClassCodeCollection _classCode = new VBSClassCodeCollection();
		protected Search_VBS_ClassAssignmentCollection _assignments = new Search_VBS_ClassAssignmentCollection();

		protected string _selectedTeacher1 = string.Empty;
		protected string _selectedTeacher2 = string.Empty;
		protected string _selectedHelper1 = string.Empty;
		protected string _selectedHelper2 = string.Empty;

		protected bool _userCanEdit = true;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			if (cboClassType.SelectedIndex == -1)
			{
				cboClassType.SelectedIndex = 0;
				cboClassType_SelectedIndexChanged(this, null);
			}

			// Bind the grid
			GetData();
			gridClass.KeyFieldName = Search_VBS_ClassAssignment.FN_LinkGUID;
			gridClass.DataSource = _assignments;
			gridClass.DataBind();

			// Bind the combos
			if (!IsPostBack && !IsCallback)
			{
				if (!String.IsNullOrEmpty(_selectedTeacher1)) { cboTeamLeader1.Value = _selectedTeacher1; } else { cboTeamLeader1.SelectedIndex = -1; }
				if (!String.IsNullOrEmpty(_selectedTeacher2)) { cboTeamLeader2.Value = _selectedTeacher2; } else { cboTeamLeader2.SelectedIndex = -1; }
				if (!String.IsNullOrEmpty(_selectedHelper1)) { cboHelper1.Value = _selectedHelper1; } else { cboHelper1.SelectedIndex = -1; }
				if (!String.IsNullOrEmpty(_selectedHelper2)) { cboHelper2.Value = _selectedHelper2; } else { cboHelper2.SelectedIndex = -1; }
			}

			// Tell if the user saw something
			if (!IsPostBack && !IsCallback)
			{
				Logging.LogInformation("User viewed class: " + cboClassType.SelectedItem.Text, null, string.Empty, HttpContext.Current.User.Identity.Name);
			}

			// Check to see if the user has the ability to edit.  If they don't, turn off the fields
			VBSUserCollection users = new VBSUserCollection("sUserName = '" + Session["UserName"].ToString().Replace("'", "''") + "'");
			_userCanEdit = true;
			if (users.Count > 0)
			{
				if (!users[0].CanEditClasses) { _userCanEdit = false; }
			}

			if (!_userCanEdit)
			{
				cboTeamLeader1.Enabled = false;
				cboTeamLeader2.Enabled = false;
				cboHelper1.Enabled = false;
				cboHelper2.Enabled = false;

				foreach (GridViewDataColumn col in gridClass.Columns)
				{
					col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
					if (col.FieldName == "CheckedInGrid")
					{
						col.Visible = false;
					}
				}

				btnSelectAll.Visible =
					btnSelectNone.Visible =
					btnSubmit.Visible = 
					false;
			}
		}

		/// <summary>
		/// Populate the volunteer collection
		/// </summary>
		protected void GetVolunteers()
		{
            if (Session["TeacherCollection"] == null)
            {
                _teachers = new Search_VBS_VolunteerCollection("bIsAdult <> 0");

                //// Add a blank row
                //Search_VBS_Volunteer item = new Search_VBS_Volunteer();
                //item.RawGUID = System.Guid.NewGuid().ToString();
                //_teachers.Add(item);

                _teachers.Sort(Search_VBS_Volunteer.FN_FullNameLastFirstUpper);
                Session["TeacherCollection"] = _teachers;
            }
            else
            {
                _teachers = (Search_VBS_VolunteerCollection)Session["TeacherCollection"];
            }
			if (!IsPostBack || Session["HelperCollection"] == null)
			{
				_helpers = new Search_VBS_VolunteerCollection("bIsAdult = 0");

				//// Add a blank row
				//Search_VBS_Volunteer item = new Search_VBS_Volunteer();
				//item.RawGUID = System.Guid.NewGuid().ToString();
				//_helpers.Add(item);

				_helpers.Sort(Search_VBS_Volunteer.FN_FullNameLastFirstUpper);
				Session["HelperCollection"] = _helpers;
			}
            else
            {
                _helpers = (Search_VBS_VolunteerCollection)Session["HelperCollection"];
            }
        }

		protected void Page_Init(object sender, EventArgs e)
		{
			// Create the teacher bindings
			GetVolunteers();
			cboTeamLeader1.DataBind();
			cboTeamLeader2.DataBind();
			cboHelper1.DataBind();
			cboHelper2.DataBind();

			cboClassType.DataBind();		// Bind the class type

			// Check the Session for a data table
			if ((_assignments == null ||
				_assignments.Count == 0) &&
				Session["ClassAssignmentsCollection"] != null)
			{
				_assignments = Session["ClassAssignmentsCollection"] as Search_VBS_ClassAssignmentCollection;
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			if (!IsPostBack || Session["ClassAssignmentsCollection"] == null)
			{
				string classCode = cboClassType.SelectedItem.GetValue(VBSClassCode.FN_ClassCode).ToString();
				string classGrade = cboClassType.SelectedItem.GetValue(VBSClassCode.FN_Grade).ToString();
				
				// Get the selected teachers
				_selectedTeacher1 =
					_selectedTeacher2 =
					_selectedHelper1 =
					_selectedHelper2 =
					string.Empty;
				_assignments = new Search_VBS_ClassAssignmentCollection("sClassCode = '" + classCode + "' AND (bTeacher <> 0 OR bJuniorHelper <> 0)");
				foreach (Search_VBS_ClassAssignment item in _assignments)
				{
					if (item.Teacher)
					{
						if (String.IsNullOrEmpty(_selectedTeacher1))
						{
							_selectedTeacher1 = item.ResponseID.ToString();
						}
						else
						{
							_selectedTeacher2 = item.ResponseID.ToString();
						}
					}
					else if (item.JuniorHelper)
					{
						if (String.IsNullOrEmpty(_selectedHelper1))
						{
							_selectedHelper1 = item.ResponseID.ToString();
						}
						else
						{
							_selectedHelper2 = item.ResponseID.ToString();
						}
					}
				}
				Session["SelectedTeacher1"] = _selectedTeacher1;
				Session["SelectedTeacher2"] = _selectedTeacher2;
				Session["SelectedHelper1"] = _selectedHelper1;
				Session["SelectedHelper2"] = _selectedHelper2;
				
				// Get the class assignments
				_assignments = new Search_VBS_ClassAssignmentCollection("sClassCode = '" + classCode + "' AND bStudent <> 0");
				_assignments.Sort("LastName, FirstName");

				// Build out the assignment collection to 25
				VBSClassCodeCollection classCollection = new VBSClassCodeCollection("sClassCode = '" + classCode + "'");
				if (classCollection.Count > 0)
				{
					while (_assignments.Count < 25)
					{
						Search_VBS_ClassAssignment assign = new Search_VBS_ClassAssignment();
						assign.LinkGUID = System.Guid.NewGuid().ToString();
						assign.RawGUID = System.Guid.NewGuid().ToString();
						assign.ClassCode = classCollection[0].ClassCode;
						assign.Grade = classCollection[0].Grade;
						assign.Length = classCollection[0].Length;
						_assignments.Add(assign);
					}
				}

				// Update for the sort
				foreach (Search_VBS_ClassAssignment assign in _assignments)
				{
					assign.GridCustom_6 = (String.IsNullOrEmpty(assign.LastName) &&
						String.IsNullOrEmpty(assign.FirstName) ? "zzzzzzzzzz" : "aaaaaaaaa");
				}

				int count = 1;
				foreach (Search_VBS_ClassAssignment a in _assignments)
				{
					a.Numbering = count;
					count++;
				}

				// Store it off in the session
				Session["ClassAssignmentsCollection"] = _assignments;
			}

			// Bring it back
			_selectedTeacher1 = (Session["SelectedTeacher1"] != null ? Session["SelectedTeacher1"].ToString() : string.Empty);
			_selectedTeacher2 = (Session["SelectedTeacher2"] != null ? Session["SelectedTeacher2"].ToString() : string.Empty);
			_selectedHelper1 = (Session["SelectedHelper1"] != null ? Session["SelectedHelper1"].ToString() : string.Empty);
			_selectedHelper2 = (Session["SelectedHelper2"] != null ? Session["SelectedHelper2"].ToString() : string.Empty);
			_assignments = (Search_VBS_ClassAssignmentCollection)Session["ClassAssignmentsCollection"];
		}

		protected void cboClassType_DataBinding(object sender, EventArgs e)
		{
			_classCode = new VBSClassCodeCollection(string.Empty);
			_classCode.Sort("Length, ClassCode");
			cboClassType.DataSource = _classCode;
		}

		protected void cboClassType_SelectedIndexChanged(object sender, EventArgs e)
		{
			// When the index changes, bind the grid
			Session["ClassAssignmentsCollection"] = null;
			
			// Get the dataset from the database
			GetData();
			gridClass.KeyFieldName = Search_VBS_ClassAssignment.FN_LinkGUID;
			gridClass.DataSource = _assignments;
			gridClass.DataBind();

			// Log the information
			Logging.LogInformation("User viewed class: " + cboClassType.SelectedItem.Text, null, string.Empty, Session["UserName"].ToString());

            cboTeamLeader1.DataBind();

			// Bind the combos
			if (!String.IsNullOrEmpty(_selectedTeacher1)) { cboTeamLeader1.Value = _selectedTeacher1; } else { cboTeamLeader1.SelectedIndex = -1; }
			if (!String.IsNullOrEmpty(_selectedTeacher2)) { cboTeamLeader2.Value = _selectedTeacher2; } else { cboTeamLeader2.SelectedIndex = -1; }
			if (!String.IsNullOrEmpty(_selectedHelper1)) { cboHelper1.Value = _selectedHelper1; } else { cboHelper1.SelectedIndex = -1; }
			if (!String.IsNullOrEmpty(_selectedHelper2)) { cboHelper2.Value = _selectedHelper2; } else { cboHelper2.SelectedIndex = -1; }

			ResetSelectionScript();
		}

		private void ResetSelectionScript()
		{
			StringBuilder sb = new StringBuilder();
			int cellCount = 0;
			for (int i = 0; i < _assignments.Count; i++)
			{
				if (_assignments[i].ResponseID != null)
				{
					cellCount++;
					sb.AppendLine(String.Format("gridClass.batchEditApi.SetCellValue({0}, 'CheckedInGrid', true);", i));
				}
			}
			btnSelectAll.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";

			sb = new StringBuilder();
			for (int i = 0; i < _assignments.Count; i++)
			{
				sb.AppendLine(String.Format("gridClass.batchEditApi.SetCellValue({0}, 'CheckedInGrid', false);", i));
			}
			btnSelectNone.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";
		}

		protected void gridClass_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
		{
			// Check to see if the user can even edit
			//VBSUserCollection users = new VBSUserCollection("sUserName = '" + Session["UserName"].ToString().Replace("'", "''") + "'");
			//_userCanEdit = true;
			//if (users.Count > 0)
			//{
			//	if (!users[0].CanEditClasses) { _userCanEdit = false; }
			//}
			//if (!_userCanEdit)
			//{
			//	e.Editor.ReadOnly = true;
			//	e.Editor.Enabled = false;
			//	return;
			//}

			// When the cell editor initializes, handle it
			if (e.Editor is ASPxComboBox &&
				cboClassType.SelectedItem != null)
			{
				// Get the listing of students
				string classGrade = cboClassType.SelectedItem.GetValue(VBSClassCode.FN_Grade).ToString();
				Search_VBS_StudentsCollection coll = new Search_VBS_StudentsCollection("sGradeInFall = '" + classGrade.Replace("'", "''") + "'");
				coll.Sort(Search_VBS_Students.FN_FullNameLastFirstUpper);

				// Update the name to show where this person is registered
				foreach (Search_VBS_Students s in coll)
				{
					if (!String.IsNullOrEmpty(s.ClassCode))
					{
						s.GridCustom_5 = String.Format("{0} ({1})", s.ClassCode, s.Grade);
					}
				}

				ASPxComboBox combo = ((ASPxComboBox)e.Editor);
				combo.Columns.Clear();
				combo.Columns.Add(Search_VBS_Students.FN_LastName, "Last Name");
				combo.Columns.Add(Search_VBS_Students.FN_FirstName, "First Name");
				combo.Columns.Add(Search_VBS_Students.FN_GradeInFall, "Grade");
				combo.Columns.Add(Search_VBS_Students.FN_Friend1FullNameFirstLast, "Friend Rqst 1");
				combo.Columns.Add(Search_VBS_Students.FN_Friend2FullNameFirstLast, "Friend Rqst 2");
				combo.Columns.Add(Search_VBS_Students.FN_GridCustom_5, "Curr. Assgnd. Class");

				combo.DataSource = coll;
				combo.TextField = Search_VBS_Students.FN_FullNameLastFirst;
				combo.ValueField = Search_VBS_Students.FN_ResponseID;
				combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
				combo.TextFormatString = "{0}, {1} ({2})";
				combo.DataBindItems();
				combo.ReadOnly = false;
			}
		}

		protected void gridClass_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			if (e.Column.FieldName.ToLower() == "ResponseID".ToLower() &&
				e.GetFieldValue(Search_VBS_ClassAssignment.FN_StudentNameFormatted) != null)
			{
				e.DisplayText = e.GetFieldValue(Search_VBS_ClassAssignment.FN_StudentNameFormatted).ToString();
			}
		}

		protected void gridClass_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			List<string> deletedGUIDs = new List<string>();
			List<string> responseToGroupDeletedGUIDs = new List<string>();
			VBSClassLinkCollection coll = new VBSClassLinkCollection();
			List<string> newResponseIDs = new List<string>();

			// Get the values of the drop downs
			string teacher1 = (cboTeamLeader1.SelectedIndex > -1 ? cboTeamLeader1.SelectedItem.Value.ToString() : string.Empty);
			string teacher2 = (cboTeamLeader2.SelectedIndex > -1 ? cboTeamLeader2.SelectedItem.Value.ToString() : string.Empty);
			string helper1 = (cboHelper1.SelectedIndex > -1 ? cboHelper1.SelectedItem.Value.ToString() : string.Empty);
			string helper2 = (cboHelper2.SelectedIndex > -1 ? cboHelper2.SelectedItem.Value.ToString() : string.Empty);

			#region Add Teachers
			// Blow away these teachers/helpers if they exist for another class
			List<string> list = new List<string>();
			if (!String.IsNullOrEmpty(teacher1)) { list.Add(teacher1); }
			if (!String.IsNullOrEmpty(teacher2)) { list.Add(teacher2); }
			if (!String.IsNullOrEmpty(helper1)) { list.Add(helper1); }
			if (!String.IsNullOrEmpty(helper2)) { list.Add(helper2); }
			if (list.Count > 0)
			{
				VBSClassLinkCollection otherClasses = new VBSClassLinkCollection("iResponseID IN (" + Utils.JoinString(list, ", ") + ")");
				foreach (VBSClassLink link in otherClasses)
				{
					if (!deletedGUIDs.Contains(link.LinkGUID))
					{
						deletedGUIDs.Add(link.LinkGUID);
					}
				}
				VBSResponseToGroupCollection reponseToGroupColl = new VBSResponseToGroupCollection("iResponseID IN (" + Utils.JoinString(list, ", ") + ")");
				foreach (VBSResponseToGroup link in reponseToGroupColl)
				{
					if (!responseToGroupDeletedGUIDs.Contains(link.LinkGUID))
					{
						responseToGroupDeletedGUIDs.Add(link.LinkGUID);
					}
				}
			}

			// Blow away any of them that exist for this class
			VBSClassLinkCollection existingSeniors = new VBSClassLinkCollection("sClassCode = '" + _assignments[0].ClassCode + "' AND " +
				"(bTeacher <> 0 OR bJuniorHelper <> 0)");
			foreach (VBSClassLink link in existingSeniors)
			{
				if (!deletedGUIDs.Contains(link.LinkGUID))
				{
					deletedGUIDs.Add(link.LinkGUID);
				}
			}

			// Add the new ones back in
			List<string> teacherResponseIDs = new List<string>();
			if (!String.IsNullOrEmpty(teacher1) &&
				!teacherResponseIDs.Contains(teacher1))
			{
				VBSClassLink linkNew = new VBSClassLink();
				linkNew.LinkGUID = System.Guid.NewGuid().ToString();
				linkNew.ResponseID = int.Parse(teacher1);
				newResponseIDs.Add(teacher1);
				linkNew.ClassCode = cboClassType.SelectedItem.Value.ToString();
				linkNew.Student = false;
				linkNew.Teacher = true;
				linkNew.JuniorHelper = false;
				linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
				linkNew.UpdatedByUserID = linkNew.AddedByUserID;
				coll.Add(linkNew);
			}
			if (!String.IsNullOrEmpty(teacher2) &&
				!teacherResponseIDs.Contains(teacher2))
			{
				VBSClassLink linkNew = new VBSClassLink();
				linkNew.LinkGUID = System.Guid.NewGuid().ToString();
				linkNew.ResponseID = int.Parse(teacher2);
				newResponseIDs.Add(teacher2);
				linkNew.ClassCode = cboClassType.SelectedItem.Value.ToString();
				linkNew.Student = false;
				linkNew.Teacher = true;
				linkNew.JuniorHelper = false;
				linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
				linkNew.UpdatedByUserID = linkNew.AddedByUserID;
				coll.Add(linkNew);
			}
			if (!String.IsNullOrEmpty(helper1) &&
				!teacherResponseIDs.Contains(helper1))
			{
				VBSClassLink linkNew = new VBSClassLink();
				linkNew.LinkGUID = System.Guid.NewGuid().ToString();
				linkNew.ResponseID = int.Parse(helper1);
				newResponseIDs.Add(helper1);
				linkNew.ClassCode = cboClassType.SelectedItem.Value.ToString();
				linkNew.Student = false;
				linkNew.Teacher = false;
				linkNew.JuniorHelper = true;
				linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
				linkNew.UpdatedByUserID = linkNew.AddedByUserID;
				coll.Add(linkNew);
			}
			if (!String.IsNullOrEmpty(helper2) &&
				!teacherResponseIDs.Contains(helper2))
			{
				VBSClassLink linkNew = new VBSClassLink();
				linkNew.LinkGUID = System.Guid.NewGuid().ToString();
				linkNew.ResponseID = int.Parse(helper2);
				newResponseIDs.Add(helper2);
				linkNew.ClassCode = cboClassType.SelectedItem.Value.ToString();
				linkNew.Student = false;
				linkNew.Teacher = false;
				linkNew.JuniorHelper = true;
				linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
				linkNew.UpdatedByUserID = linkNew.AddedByUserID;
				coll.Add(linkNew);
			}
			#endregion Add Teachers

			// Now take care of the students
			foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
			{
				OrderedDictionary keys = val.Keys;
				OrderedDictionary newVals = val.NewValues;
				OrderedDictionary oldVals = val.OldValues;
				
				string oldLinkGUID = (keys["LinkGUID"] != null && !String.IsNullOrEmpty(keys["LinkGUID"].ToString()) ?
					keys["LinkGUID"].ToString().Trim() : string.Empty);
				string oldResponseID = (oldVals["ResponseID"] != null && !String.IsNullOrEmpty(oldVals["ResponseID"].ToString()) ?
					oldVals["ResponseID"].ToString().Trim() : string.Empty);
				string newResponseID = newVals["ResponseID"].ToString().Trim();
				bool checkedInGrid = ((bool)newVals["CheckedInGrid"]);

				// Delete the old and new ones from the class listing first
				StringBuilder sb = new StringBuilder();
				if (!checkedInGrid)
				{
					// We're going to make an update here
					sb.Append(newResponseID);
					if (!String.IsNullOrEmpty(oldResponseID)) { sb.Append((sb.ToString().Length > 0 ? ", " : "") + oldResponseID); }
					if (sb.ToString().Length > 0)
					{
						VBSClassLinkCollection existingColl = new VBSClassLinkCollection("iResponseID IN (" + sb.ToString() + ")");
						foreach (VBSClassLink link in existingColl)
						{
							//link.Delete();
							if (!deletedGUIDs.Contains(link.LinkGUID))
							{
								deletedGUIDs.Add(link.LinkGUID);
							}
						}
					}

					// Add the new row in
					if (!newResponseIDs.Contains(newResponseID))
					{
						VBSClassLink linkNew = new VBSClassLink();
						linkNew.LinkGUID = System.Guid.NewGuid().ToString();
						linkNew.ResponseID = int.Parse(newResponseID);
						newResponseIDs.Add(newResponseID);
						linkNew.ClassCode = cboClassType.SelectedItem.Value.ToString();
						linkNew.Student = true;
						linkNew.Teacher = false;
						linkNew.JuniorHelper = false;
						linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
						linkNew.UpdatedByUserID = linkNew.AddedByUserID;
						coll.Add(linkNew);
					}
				}
				else
				{
					// Just delete the row
					VBSClassLinkCollection existingColl = new VBSClassLinkCollection("sLinkGUID IN ('" + oldLinkGUID + "')");
					foreach (VBSClassLink link in existingColl)
					{
						//link.Delete();
						if (!deletedGUIDs.Contains(link.LinkGUID))
						{
							deletedGUIDs.Add(link.LinkGUID);
						}
					}
				}
			}

			// Delete any that need to be deleted first
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			if (deletedGUIDs.Count > 0)
			{
				VBSClassLinkCollection existingColl = new VBSClassLinkCollection("sLinkGUID IN ('" + Utils.JoinString(deletedGUIDs, "', '") + "')");
				foreach (VBSClassLink link in existingColl)
				{
					link.Delete();
					if (link.Teacher)
					{
						Logging.LogInformation("Deleting Teacher: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
					}
					else if (link.JuniorHelper)
					{
						Logging.LogInformation("Deleting Helper: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
					}
					else if (link.Student)
					{
						Logging.LogInformation("Deleting Student: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
					}
				}
				errors.AddRange(existingColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();
			}
			if (responseToGroupDeletedGUIDs.Count > 0)
			{
				VBSResponseToGroupCollection respColl = new VBSResponseToGroupCollection("sLinkGUID IN ('" + Utils.JoinString(responseToGroupDeletedGUIDs, "', '") + "')");
				foreach (VBSResponseToGroup link in respColl)
				{
					link.Delete();
					Logging.LogInformation("Deleting Volunteer from team assignment: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(respColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();
			}

			// Update the collection
			if (coll.Count > 0)
			{
				foreach (VBSClassLink link in coll)
				{
					if (link.Teacher)
					{
						Logging.LogInformation("Adding Teacher: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
					}
					else if (link.JuniorHelper)
					{
						Logging.LogInformation("Adding Helper: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
					}
					else if (link.Student)
					{
						Logging.LogInformation("Adding Student: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
					}
				}
				errors.AddRange(coll.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
			}

			// Go out and get the updated data from the collection
			if (_assignments.Count > 0)
			{
				Search_VBS_ClassAssignmentCollection newCollection =
					new Search_VBS_ClassAssignmentCollection("sClassCode = '" + _assignments[0].ClassCode + "' AND bStudent <> 0");
				
				for (int i = _assignments.Count - 1; i >= 0; i--)
				{
					// Remove them all
					_assignments.RemoveAt(i);
				}

				// Add them back in
				foreach (Search_VBS_ClassAssignment assign in newCollection)
				{
					_assignments.Add(assign);
				}

				// Fill out the list
				while (_assignments.Count > 25)
				{
					for (int i = _assignments.Count - 1; i >= 0; i--)
					{
						if (!String.IsNullOrEmpty(_assignments[i].GridCustom_6) &&
							_assignments[i].GridCustom_6.ToLower().StartsWith("zz"))
						{
							_assignments.RemoveAt(i);
							break;
						}
					}
				}

				VBSClassCodeCollection classCode = new VBSClassCodeCollection("sClassCode = '" + 
					cboClassType.SelectedItem.Value.ToString().Replace("'", "''") + "'");
				if (classCode.Count > 0)
				{
					while (_assignments.Count < 25)
					{
						Search_VBS_ClassAssignment assign = new Search_VBS_ClassAssignment();
						assign.LinkGUID = System.Guid.NewGuid().ToString();
						assign.RawGUID = System.Guid.NewGuid().ToString();
						assign.ClassCode = classCode[0].ClassCode;
						assign.Grade = classCode[0].Grade;
						assign.Length = classCode[0].Length;
						_assignments.Add(assign);
					}
				}

				// Update for the sort
				foreach (Search_VBS_ClassAssignment assign in _assignments)
				{
					assign.GridCustom_6 = (String.IsNullOrEmpty(assign.LastName) &&
						String.IsNullOrEmpty(assign.FirstName) ? "zzzzzzzzzz" : "aaaaaaaaa");
				}

				_assignments.Sort(Search_VBS_ClassAssignment.FN_GridCustom_6 + ", " +
					Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);

				// Handle the numbering
				int count = 1;
				foreach (Search_VBS_ClassAssignment a in _assignments)
				{
					a.Numbering = count;
					count++;
				}

				Session["ClassAssignmentsCollection"] = _assignments;
			}

			// Take care of resetting the teachers if we need to
			if (String.IsNullOrEmpty(teacher1) &&
				!String.IsNullOrEmpty(teacher2))
			{
				Session["SelectedTeacher1"] = _selectedTeacher1 = teacher2;
				Session["SelectedTeacher2"] = null;
				_selectedTeacher2 = string.Empty;

				// Update the values in the combos
				cboTeamLeader1.Value = _selectedTeacher1;
				cboTeamLeader2.Value = _selectedTeacher2;
			}
			if (String.IsNullOrEmpty(helper1) &&
				!String.IsNullOrEmpty(helper2))
			{
				Session["SelectedHelper1"] = _selectedHelper1 = helper2;
				Session["SelectedHelper2"] = null;
				_selectedHelper2 = string.Empty;

				// Update the values in the combos
				cboHelper1.Value = _selectedHelper1;
				cboHelper2.Value = _selectedHelper2;
			}

			gridClass.DataBind();

			// Reset the javascript code on the buttons
			ResetSelectionScript();

			// Handle the event internally
			e.Handled = true;
		}

		//protected void gridClass_AfterPerformCallback(object sender, ASPxGridViewAfterPerformCallbackEventArgs e)
		//{
		//	ASPxGridView gv = (ASPxGridView)sender;
		//	gv.JSProperties.Add("cpIsCustomCallback", null);
		//	gv.JSProperties["cpIsCustomCallback"] = e.CallbackName == "CUSTOMCALLBACK";
		//}

		protected void cbSelectAll_Callback(object sender, CallbackEventArgsBase e)
		{
			ResetSelectionScript();	
		}

		protected void gridClass_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			gridClass.UpdateEdit();
		}

		protected void cboTeamLeader1_DataBinding(object sender, EventArgs e)
		{
			cboTeamLeader1.DataSource = _teachers;
		}

		protected void cboTeamLeader2_DataBinding(object sender, EventArgs e)
		{
			cboTeamLeader2.DataSource = _teachers;
		}

		protected void cboHelper1_DataBinding(object sender, EventArgs e)
		{
			cboHelper1.DataSource = _helpers;
		}

		protected void cboHelper2_DataBinding(object sender, EventArgs e)
		{
			cboHelper2.DataSource = _helpers;
		}

		protected void btnSendCoordinatorEmail_Click(object sender, EventArgs e)
		{
			// When they click on the send email button, generate the report and send it
			string className = cboClassType.SelectedItem.Text;
			if (className.Contains(" "))
			{
				className = className.Substring(0, className.IndexOf(" ")).Trim();
			}

			// Get the collections we'll be using to build both reports
			Search_VBS_ClassAssignmentCollection teamLeaderCollection = new Search_VBS_ClassAssignmentCollection("sClassCode IS NOT NULL AND sClassCode = '" + className.Replace("'", "''") + "'");
			teamLeaderCollection.Sort(Search_VBS_Volunteer.FN_Length + ", " +
				Search_VBS_ClassAssignment.FN_ClassCode + ", " +
				Search_VBS_ClassAssignment.FN_Teacher + " DESC, " +
				Search_VBS_ClassAssignment.FN_JuniorHelper + " DESC, " +
				Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);
			foreach (Search_VBS_ClassAssignment item in teamLeaderCollection)
			{
				item.GridCustom_1 = "Class: " + item.ClassCodeWithDesc;
				item.ClassColor = item.ClassColor.ToUpper();
			}

			// Run the team leader roster first
			// This is going to be the teachers in the classes
			rptTeamLeaderRoster rptteamleaderroster = new rptTeamLeaderRoster();
			rptteamleaderroster.DataSource = teamLeaderCollection;
			rptteamleaderroster.DisplayName = "Team Leaders";
			rptteamleaderroster.CreateDocument();

			// Reset all the page numbers if they're set
			rptteamleaderroster.PrintingSystem.ContinuousPageNumbering = true;

			// Send the file
			MemoryStream strm = new MemoryStream();
			rptteamleaderroster.ExportToPdf(strm);
			strm.Seek(0, System.IO.SeekOrigin.Begin);

			// Get the people who are the leaders in this class
			List<MailAddress> leaders = new List<MailAddress>();
			List<string> existingLeaders = new List<string>();
			foreach (Search_VBS_ClassAssignment v in teamLeaderCollection)
			{
				if (v.Teacher && !existingLeaders.Contains(v.Email.ToLower()))
				{
					existingLeaders.Add(v.Email.ToLower());
					leaders.Add(new MailAddress(v.Email.ToLower(), v.FullNameFirstLast));
				}
				//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.Email.ToLower())) { leaders.Add(v.Email.ToLower()); }
				//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.JrHelperEmail.ToLower())) { leaders.Add(v.JrHelperEmail.ToLower()); }
			}

			string body = "Attached is a copy of the latest class " + className + " roster.\r\n\r\n" +
				"Please contact any new volunteers that have been assigned to the team.\r\n\r\n" +
				"If you have any questions, please feel free to contact the team leader coordinator.";
			if (leaders.Count == 0)
			{
				body += "\r\n\r\n" + "THERE ARE NO LEADERS IN THIS CLASS!!";
			}

			List<MailAddress> ccList = new List<MailAddress>();
			ccList.Add(new MailAddress(Session["UserName"].ToString()));

			List<MailAddress> bccList = new List<MailAddress>();

			// Send the email
			System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(strm, "Class_" + className.Replace(" ", "") + "_Roster.pdf", "application/pdf");
			Utils.SendEMailAsJen(leaders, 
				ccList, 
				bccList,
				"Class " + className + " Roster", body, string.Empty,
			new List<System.Net.Mail.Attachment>(new System.Net.Mail.Attachment[] {
				attachment,
			}), Session["UserName"].ToString());

			// Kill off the memory string
			strm.Close();

			MessageText.Text = "Message sent.";
		}
	}
}