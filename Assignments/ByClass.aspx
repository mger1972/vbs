﻿<%@ Page Title="Assignments - By Class" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ByClass.aspx.cs" Inherits="VBS.Assignments.ByClass" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
				<h3>Use the screen below to make assignments by class.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script type="text/javascript">
		function OnConfirm(s, e) {
			if (myButtonClick == true) {
				e.cancel = true;
				myButtonClick = false;
			}

		}
		var myButtonClick = false;
		function OnClick(s, e) {
			myButtonClick = true;
			pnlLoading.Show();
		}
	</script>
	<table id="selectionTable" width="100%" align="center" border="0">
		<tr>
			<td width="80%">
				<table width="100%">
					<tr>
						<td width="1%" valign="middle">Class:&nbsp;</td>
						<td valign="top"><dx:ASPxComboBox runat="server" ID="cboClassType" ClientInstanceName="cboClassType" DropDownStyle="DropDownList"
							width="170px" DropDownWidth="400" OnDataBinding="cboClassType_DataBinding" AutoPostBack="true"
							ValueField="ClassCode" TextField="ClassCode"
							OnSelectedIndexChanged="cboClassType_SelectedIndexChanged" ValueType="System.String" 
							EnableCallbackMode="true" TextFormatString="{0} ({1})">
							<Columns>
								<dx:ListBoxColumn FieldName="ClassCode" Width="150px" />
								<dx:ListBoxColumn FieldName="Grade" Width="250px" />
							</Columns>
							<ClientSideEvents SelectedIndexChanged="function(s, e) { pnlLoading.Text = 'Loading Records...'; pnlLoading.Show(); }" />
						</dx:ASPxComboBox></td>
						<td width="20%" align="right" nowrap>
							<% if (!String.IsNullOrEmpty(MessageText.Text)) { %>
							<span class="validation-summary-errors"><asp:Literal runat="server" ID="MessageText" Text="" />&nbsp; &nbsp;</span>
							<% } %>
							<dx:ASPxButton ID="btnSendCoordinatorEmail" ClientInstanceName="btnSendCoordinatorEmail" runat="server" OnClick="btnSendCoordinatorEmail_Click" Text="Send Coordinator Email" AutoPostBack="true">
								<ClientSideEvents Click="OnClick" />
							</dx:ASPxButton>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table id="bottomPanel" width="100%" align="center" border="0">
		<tr>
			<td colspan="2"><hr /></td>
		</tr>
		<tr>
			<td colspan="2">
				<table style="padding:0px;border-spacing:0px;width:100%;border:none;" align="center">
					<tr>
						<td width="1%" nowrap>Team&nbsp;Leader&nbsp;1:&nbsp;</td>
						<td width="39%"><dx:ASPxComboBox runat="server" ID="cboTeamLeader1" ClientInstanceName="cboTeamLeader1" DropDownStyle="DropDownList"
							width="170px" DropDownWidth="430" DropDownHeight="250" AutoPostBack="false" OnDataBinding="cboTeamLeader1_DataBinding"
							ValueField="ResponseID" TextField="VolunteerNameFormatted" ClearButton-Visibility="True" 
							ValueType="System.String" EnableCallbackMode="true" TextFormatString="{0}">
                            <Columns>
                                <dx:ListBoxColumn FieldName="VolunteerNameFormatted" Width="160px" />
                                <dx:ListBoxColumn FieldName="GroupName" Width="100px" />
                                <dx:ListBoxColumn FieldName="ClassCode" Width="70px" />
                                <dx:ListBoxColumn FieldName="Grade" Width="100px" />
                            </Columns>
						</dx:ASPxComboBox></td>
						<td width="1%" nowrap>Team&nbsp;Leader&nbsp;2:&nbsp;</td>
						<td width="39%"><dx:ASPxComboBox runat="server" ID="cboTeamLeader2" ClientInstanceName="cboTeamLeader2" DropDownStyle="DropDownList"
							width="170px" DropDownWidth="430" DropDownHeight="250" AutoPostBack="false" OnDataBinding="cboTeamLeader2_DataBinding"
							ValueField="ResponseID" TextField="VolunteerNameFormatted" ClearButton-Visibility="True"
							ValueType="System.String" EnableCallbackMode="true" TextFormatString="{0}">
                            <Columns>
                                <dx:ListBoxColumn FieldName="VolunteerNameFormatted" Width="160px" />
                                <dx:ListBoxColumn FieldName="GroupName" Width="100px" />
                                <dx:ListBoxColumn FieldName="ClassCode" Width="70px" />
                                <dx:ListBoxColumn FieldName="Grade" Width="100px" />
                            </Columns>
						</dx:ASPxComboBox></td>
						<td width="20%">&nbsp;</td>
					</tr>
					<tr>
						<td width="1%" nowrap>Helper&nbsp;1:&nbsp;</td>
						<td width="39%"><dx:ASPxComboBox runat="server" ID="cboHelper1" ClientInstanceName="cboHelper1" DropDownStyle="DropDownList"
							width="170px" DropDownWidth="430" DropDownHeight="250" AutoPostBack="false" OnDataBinding="cboHelper1_DataBinding"
							ValueField="ResponseID" TextField="VolunteerNameFormatted" ClearButton-Visibility="True"
							ValueType="System.String" EnableCallbackMode="true" TextFormatString="{0}">
                            <Columns>
                                <dx:ListBoxColumn FieldName="VolunteerNameFormatted" Width="160px" />
                                <dx:ListBoxColumn FieldName="GroupName" Width="100px" />
                                <dx:ListBoxColumn FieldName="ClassCode" Width="70px" />
                                <dx:ListBoxColumn FieldName="Grade" Width="100px" />
                            </Columns>
						</dx:ASPxComboBox></td>
						<td width="1%" nowrap>Helper&nbsp;2:&nbsp;</td>
						<td width="39%"><dx:ASPxComboBox runat="server" ID="cboHelper2" ClientInstanceName="cboHelper2" DropDownStyle="DropDownList"
							width="170px" DropDownWidth="430" DropDownHeight="250" AutoPostBack="false" OnDataBinding="cboHelper2_DataBinding"
							ValueField="ResponseID" TextField="VolunteerNameFormatted" ClearButton-Visibility="True"
							ValueType="System.String" EnableCallbackMode="true" TextFormatString="{0}">
                            <Columns>
                                <dx:ListBoxColumn FieldName="VolunteerNameFormatted" Width="160px" />
                                <dx:ListBoxColumn FieldName="GroupName" Width="100px" />
                                <dx:ListBoxColumn FieldName="ClassCode" Width="70px" />
                                <dx:ListBoxColumn FieldName="Grade" Width="100px" />
                            </Columns>
						</dx:ASPxComboBox></td>
						<td width="20%" align="right" nowrap>
							<dx:ASPxButton ID="btnSelectAll" ClientInstanceName="btnSelectAll" Text="Check All" runat="server" AutoPostBack="false" />	
							&nbsp;
							<dx:ASPxButton ID="btnSelectNone" ClientInstanceName="btnSelectNone" Text="Uncheck All" runat="server" AutoPostBack="false" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<dx:ASPxGridView ID="gridClass" ClientInstanceName="gridClass" runat="server" OnCommandButtonInitialize="gridClass_CommandButtonInitialize"
					Width="100%" OnCellEditorInitialize="gridClass_CellEditorInitialize" OnBatchUpdate="gridClass_BatchUpdate"
					OnCustomColumnDisplayText="gridClass_CustomColumnDisplayText" 
					Paddings-PaddingLeft="10px" Paddings-PaddingRight="10px" Paddings-PaddingTop="0px" Paddings-PaddingBottom="0px"
					EnableViewState="false">
					<Border BorderWidth="0px" />
					<ClientSideEvents BatchEditConfirmShowing="function(s, e) { e.cancel = true; }" 
						BeginCallback="function(s, e) {
							if (e.command == 'UPDATEEDIT') {
								pnlLoading.Text = 'Saving Changes';
								pnlLoading.Show();
								isUpdateEdit = true;
							}
						}"
						EndCallback="function(s, e) {
							if (isUpdateEdit) {
								isUpdateEdit = false;
							}
							pnlLoading.Hide();
						}" />
					<Columns>
						<dx:GridViewDataColumn FieldName="NumberingAsString" Width="2px" Caption=" " CellStyle-HorizontalAlign="Right" VisibleIndex="1" ReadOnly="true">
							<EditItemTemplate>
								<dx:ASPxLabel ID="lbNumbering" runat="server" Value='<%# Bind("NumberingAsString") %>'>
								</dx:ASPxLabel>
							</EditItemTemplate>
						</dx:GridViewDataColumn>
						<dx:GridViewDataComboBoxColumn FieldName="ResponseID" Caption="Assigned Student" VisibleIndex="3">
							<PropertiesComboBox TextField="StudentNameFormatted" ValueField="ResponseID" ValueType="System.Int32"></PropertiesComboBox>
						</dx:GridViewDataComboBoxColumn>
						<dx:GridViewDataCheckColumn FieldName="CheckedInGrid" Width="2px" Caption="Delete"
							HeaderStyle-HorizontalAlign="Center" VisibleIndex="8">
						</dx:GridViewDataCheckColumn>
					</Columns>
					<Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowTitlePanel="false" />
					<SettingsBehavior FilterRowMode="OnClick" AllowSort="false" />
					<SettingsEditing Mode="Batch" />
					<SettingsPager PageSize="25" Visible="false" />
					<Styles>
						<InlineEditCell>
							<Border BorderStyle="None" />
						</InlineEditCell>
					</Styles>
				</dx:ASPxGridView>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right"><dx:ASPxButton ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Save Changes" AutoPostBack="true">
				<ClientSideEvents Click="OnClick" />
			</dx:ASPxButton></td>
		</tr>
	</table>
	<dx:ASPxLoadingPanel ID="pnlLoading" runat="server" ContainerElementID="bottomPanel" ClientInstanceName="pnlLoading" Modal="true"></dx:ASPxLoadingPanel>
</asp:Content>
