﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VBS.Reports;

namespace VBS.Assignments
{
	public partial class SendTeamRosters : System.Web.UI.Page
	{
		protected Search_VBS_ResponseToGroupCollection _groupAssignments = new Search_VBS_ResponseToGroupCollection();
		protected DataTable _dt = new DataTable();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Bind the grid
			GetData();
			gridList.KeyFieldName = Search_VBS_ResponseToGroup.FN_LinkGUID;
			gridList.DataSource = _dt;
			gridList.DataBind();

			foreach (GridViewDataColumn col in gridList.Columns)
			{
				if (col.FieldName != "CheckedInGrid")
				{
					col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
				}
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			if (!IsPostBack || Session["SendTeamRoster"] == null)
			{
				// Get the assignments
				_groupAssignments = new Search_VBS_ResponseToGroupCollection(string.Empty);
				_groupAssignments.Sort(Search_VBS_ResponseToGroup.FN_GroupName);
				Dictionary<string, int> groups = new Dictionary<string, int>();
				foreach (Search_VBS_ResponseToGroup item in _groupAssignments)
				{
					if (!groups.ContainsKey(item.GroupName)) { groups.Add(item.GroupName, 0); }
					groups[item.GroupName]++;
				}

				// Load up the datatable
				_dt = new DataTable();
				_dt.Columns.Add("LinkGUID", typeof(System.String));
				_dt.Columns.Add("CheckedInGrid", typeof(System.Boolean));
				_dt.Columns.Add("GroupName", typeof(System.String));
				_dt.Columns.Add("Count", typeof(System.Int32));
				foreach (KeyValuePair<string, int> kvp in groups)
				{
					_dt.Rows.Add(new object[] {
					System.Guid.NewGuid().ToString(),
					true,
					kvp.Key, 
					kvp.Value,
				});
				}

				Session["SendTeamRoster"] = _dt;
			}

			// Bring it back
			_dt = (DataTable)Session["SendTeamRoster"];
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			gridList.UpdateEdit();
		}

		protected void gridList_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void gridList_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
			{
				OrderedDictionary keys = val.Keys;
				OrderedDictionary newVals = val.NewValues;
				OrderedDictionary oldVals = val.OldValues;
			
				// Go through the collection and see if you can find the one they're dealing with
				string linkGUID = (keys["LinkGUID"] != null && !String.IsNullOrEmpty(keys["LinkGUID"].ToString()) ?
					keys["LinkGUID"].ToString().Trim() : string.Empty);
				bool chkd = (bool)newVals["CheckedInGrid"];
				foreach (DataRow row in _dt.Rows)
				{
					if (row["LinkGUID"].ToString().ToLower().Equals(linkGUID.ToLower()))
					{
						// Here's the one we need
						row["CheckedInGrid"] = chkd;
						break;
					}
				}
			}

			// Build and send the emails
			StringBuilder sb = new StringBuilder();
			foreach (DataRow row in _dt.Rows)
			{
				if ((bool)row["CheckedInGrid"])
				{
					string group = row["GroupName"].ToString();

					// Get the collections we'll be using to build both reports
					Search_VBS_VolunteerCollection groupCollection = new Search_VBS_VolunteerCollection("sGroupGUID IS NOT NULL AND sGroupName = '" + group.Replace("'", "''") + "'");
					groupCollection.Sort(Search_VBS_Volunteer.FN_GroupName + ", " +
						Search_VBS_Volunteer.FN_TeamCoordinator + " DESC, " +
						Search_VBS_Volunteer.FN_FullNameLastFirstUpper);

					Dictionary<string, int> _adultCount = new Dictionary<string, int>();
					Dictionary<string, int> _juniorCount = new Dictionary<string, int>();
					foreach (Search_VBS_Volunteer item in groupCollection)
					{
						item.GridCustom_1 = "Team: " + item.GroupName;
						if (!_adultCount.ContainsKey(item.GroupName)) { _adultCount.Add(item.GroupName, 0); }
						if (!_juniorCount.ContainsKey(item.GroupName)) { _juniorCount.Add(item.GroupName, 0); }
						if (item.IsAdult.HasValue && item.IsAdult.Value)
						{
							_adultCount[item.GroupName]++;
						}
						else
						{
							_juniorCount[item.GroupName]++;
						}
					}
					foreach (Search_VBS_Volunteer item in groupCollection)
					{
						item.GridCustom_2 = _adultCount[item.GroupName].ToString();
						item.GridCustom_3 = _juniorCount[item.GroupName].ToString();
					}

					// Next run the group listing
					rptGroupRoster rptgrouproster = new rptGroupRoster();
					rptgrouproster.DataSource = groupCollection;
					rptgrouproster.DisplayName = "Groups";
					rptgrouproster.CreateDocument();

					// Reset all the page numbers if they're set
					rptgrouproster.PrintingSystem.ContinuousPageNumbering = true;

					// Send the file
					MemoryStream strm = new MemoryStream();
					rptgrouproster.ExportToPdf(strm);
					strm.Seek(0, System.IO.SeekOrigin.Begin);

					// Get the people who are the leaders for this team
					List<MailAddress> leaders = new List<MailAddress>();
					List<string> existingLeaders = new List<string>();
					foreach (Search_VBS_Volunteer v in groupCollection)
					{
						if (v.TeamCoordinator.HasValue && v.TeamCoordinator.Value && !existingLeaders.Contains(v.Email.ToLower()))
						{
							existingLeaders.Add(v.Email.ToLower());
							leaders.Add(new MailAddress(v.Email.ToLower(), v.FullNameFirstLast));
						}
						//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.Email.ToLower())) { leaders.Add(v.Email.ToLower()); }
						//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.JrHelperEmail.ToLower())) { leaders.Add(v.JrHelperEmail.ToLower()); }
					}

					//string body = "Attached is a copy of the latest " + group + " roster.";
					string body = "Attached is a copy of the latest " + group + " roster.\r\n\r\n" +
						"Please contact any new volunteers that have been assigned to the team.\r\n\r\n" +
						"If you have any questions, please feel free to contact me at jen@gerlach5.com.";
					if (leaders.Count == 0)
					{
						body += "\r\n\r\n" + "THERE ARE NO LEADERS FOR THIS GROUP/TEAM!!";
					}

					List<MailAddress> ccList = new List<MailAddress>();
					ccList.Add(new MailAddress(Session["UserName"].ToString()));

					List<MailAddress> bccList = new List<MailAddress>();

					// Send the email
					System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(strm, group.Replace(" ", "") + "Roster.pdf", "application/pdf");
					Utils.SendEMailAsJen(leaders, 
						ccList, 
						bccList,
						group + " Roster", body, string.Empty,
							new List<System.Net.Mail.Attachment>(new System.Net.Mail.Attachment[] {
							attachment,
						}), Session["UserName"].ToString());

					// Kill off the memory string
					strm.Close();

					if (sb.ToString().Length > 0) { sb.Append("&nbsp; &nbsp;<br />"); }
					sb.Append("Message sent: " + group);
				}
			}

			MessageText.Text = sb.ToString();

			// Handle the event internally
			e.Handled = true;
		}
	}
}