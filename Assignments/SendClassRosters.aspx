﻿<%@ Page Title="Email Class Rosters" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SendClassRosters.aspx.cs" Inherits="VBS.Assignments.SendClassRosters" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
				<h3>Use the screen below to email class rosters to class leaders/teachers.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script type="text/javascript">
		function OnConfirm(s, e) {
			if (myButtonClick == true) {
				e.cancel = true;
				myButtonClick = false;
			}

		}
		var myButtonClick = false;
		function OnClick(s, e) {
			myButtonClick = true;
			pnlLoading.Show();
		}
	</script>
	<table id="bottomPanel" width="100%" align="center" border="0">
		<tr>
			<td colspan="2" align="right">&nbsp;<% if (!String.IsNullOrEmpty(MessageText.Text)) { %>
				<span class="validation-summary-errors"><asp:Literal runat="server" ID="MessageText" Text="" />&nbsp; &nbsp;</span>
				<% } %></td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<dx:ASPxGridView ID="gridList" ClientInstanceName="gridList" runat="server" Width="100%" 
					OnBatchUpdate="gridList_BatchUpdate" OnCommandButtonInitialize="gridList_CommandButtonInitialize"
					Paddings-PaddingLeft="10px" Paddings-PaddingRight="10px" Paddings-PaddingTop="0px" Paddings-PaddingBottom="0px"
					EnableViewState="false">
					<Border BorderWidth="0px" />
					<ClientSideEvents BatchEditConfirmShowing="function(s, e) { e.cancel = true; }" 
						BeginCallback="function(s, e) {
							if (e.command == 'UPDATEEDIT') {
								pnlLoading.Text = 'Saving Changes';
								pnlLoading.Show();
								isUpdateEdit = true;
							}
						}"
						EndCallback="function(s, e) {
							if (isUpdateEdit) {
								isUpdateEdit = false;
							}
							pnlLoading.Hide();
						}" />
					<Columns>
						<dx:GridViewDataCheckColumn FieldName="CheckedInGrid" Width="2px" Caption="Select"
							HeaderStyle-HorizontalAlign="Center" VisibleIndex="2">
						</dx:GridViewDataCheckColumn>
						<dx:GridViewDataComboBoxColumn FieldName="ClassCode" Caption="Class" VisibleIndex="3"></dx:GridViewDataComboBoxColumn>
						<dx:GridViewDataComboBoxColumn FieldName="Count" Caption="Child Count" VisibleIndex="4"></dx:GridViewDataComboBoxColumn>
					</Columns>
					<Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowTitlePanel="false" />
					<SettingsBehavior FilterRowMode="OnClick" AllowSort="false" />
					<SettingsEditing Mode="Batch" />
					<SettingsPager PageSize="50" Visible="false" />
					<Styles>
						<InlineEditCell>
							<Border BorderStyle="None" />
						</InlineEditCell>
					</Styles>
				</dx:ASPxGridView>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right"><dx:ASPxButton ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Send" AutoPostBack="true">
			</dx:ASPxButton></td>
		</tr>
	</table>
	<dx:ASPxLoadingPanel ID="pnlLoading" runat="server" ContainerElementID="bottomPanel" ClientInstanceName="pnlLoading" Modal="true"></dx:ASPxLoadingPanel>
</asp:Content>
