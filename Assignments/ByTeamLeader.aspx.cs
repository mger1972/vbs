﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Assignments
{
	public partial class ByTeamLeader : System.Web.UI.Page
	{
		protected DataTable _dt = null;
		//protected Search_VBS_VolunteerCollection _students = new Search_VBS_VolunteerCollection();
		protected Search_VBS_VolunteerCollection _assignments = new Search_VBS_VolunteerCollection();

		protected bool _userCanEdit = true;

		private int _maxRecsOnPage = 1000;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			if (cboFirstLetter.SelectedIndex == -1)
			{
				cboFirstLetter.SelectedIndex = 2;		// Hit the "A"
				cboFirstLetter_SelectedIndexChanged(this, null);
			}

			// Bind the grid
			GetData();
			gridVolunteer.KeyFieldName = Search_VBS_Volunteer.FN_RawGUID;
			gridVolunteer.DataSource = _assignments;
			gridVolunteer.DataBind();

			// Tell if the user saw something
			if (!IsPostBack && !IsCallback)
			{
				Logging.LogInformation("User viewed Team Leaders: " + cboFirstLetter.SelectedItem.Text, null, string.Empty, HttpContext.Current.User.Identity.Name);
			}

			// Check to see if the user has the ability to edit.  If they don't, turn off the fields
			VBSUserCollection users = new VBSUserCollection("sUserName = '" + Session["UserName"].ToString().Replace("'", "''") + "'");
			_userCanEdit = true;
			if (users.Count > 0)
			{
				if (!users[0].CanEditTeams) { _userCanEdit = false; }
			}

			if (!_userCanEdit)
			{
				btnSelectAll.Visible =
					btnSelectNone.Visible =
					btnSubmit.Visible =
					btnSubmitHead.Visible = 
					false;

				foreach (GridViewColumn col in gridVolunteer.Columns)
				{
					if (col is GridViewDataColumn)
					{
						((GridViewDataColumn)col).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
					}
				}
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			cboFirstLetter.DataBind();		// Bind the class type

			// Check the Session for a data table
			if ((_assignments == null ||
				_assignments.Count == 0) &&
				Session["VolunteerAssignmentsCollection"] != null)
			{
				_assignments = Session["VolunteerAssignmentsCollection"] as Search_VBS_VolunteerCollection;
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			if (!IsPostBack || Session["VolunteerAssignmentsCollection"] == null)
			{
				string firstLetter = cboFirstLetter.SelectedItem.GetValue(Search_VBS_Volunteer.FN_Left1LastName).ToString();

				// Get the class assignments
				if (firstLetter.ToLower().Contains("all"))
				{
					_assignments = new Search_VBS_VolunteerCollection(string.Empty);
				}
				else if (firstLetter.ToLower().Contains("unassign"))
				{
					_assignments = new Search_VBS_VolunteerCollection("sGroupGUID IS NULL AND sClassCode IS NULL");
				}
				else
				{
					_assignments = new Search_VBS_VolunteerCollection("sLeft1LastName = '" + firstLetter + "'");
				}

				//foreach (Search_VBS_Volunteer item in _assignments)
				//{
				//	item.FirstName = item.FirstName.ToUpper();
				//	item.LastName = item.LastName.ToUpper();
				//}

				_assignments.Sort(Search_VBS_Volunteer.FN_FullNameLastFirstUpper);

				int count = 1;
				foreach (Search_VBS_Volunteer a in _assignments)
				{
					// Set up the grid custom 8 so it shows up properly
					if (!String.IsNullOrEmpty(a.ClassCode))
					{
						if (a.Teacher.HasValue && a.Teacher.Value)
						{
							//a.GridCustom_8 = "CLASS|T|" + a.ClassCode;
							a.GridCustom_8 = "Class: " + String.Format("{0} ({1})", a.ClassCode, a.Grade) + ": Team Leader";
						}
						else if (a.JuniorHelper.HasValue && a.JuniorHelper.Value)
						{
							a.GridCustom_8 = "Class: " + String.Format("{0} ({1})", a.ClassCode, a.Grade) + ": Junior Helper";
						}
					}
					else if (!String.IsNullOrEmpty(a.GroupGUID))
					{
						//a.GridCustom_8 = "GROUP|" + a.GroupGUID;
						a.GridCustom_8 = "Team: " + a.GroupName;
					}

					a.Numbering = count;
					count++;
				}

				// Store it off in the session
				Session["VolunteerAssignmentsCollection"] = _assignments;
			}

			// Bring it back
			_assignments = (Search_VBS_VolunteerCollection)Session["VolunteerAssignmentsCollection"];
		}

		protected void cboFirstLetter_DataBinding(object sender, EventArgs e)
		{
			List<string> list = Search_VBS_Volunteer.GetDistinctFromDB(Search_VBS_VolunteerField.Left1LastName);
			list.Sort();
			_dt = new DataTable();
			_dt.Columns.Add("Left1LastName", typeof(System.String));
			_dt.Rows.Add(new object[] { "< ALL >" });
			_dt.Rows.Add(new object[] { "< Unassigned >" });
			foreach (string s in list)
			{
				_dt.Rows.Add(new object[] { s });
			}
			cboFirstLetter.DataSource = _dt;
		}

		protected void cboFirstLetter_SelectedIndexChanged(object sender, EventArgs e)
		{
			// When the index changes, bind the grid
			Session["VolunteerAssignmentsCollection"] = null;
			
			// Get the dataset from the database
			GetData();
			gridVolunteer.KeyFieldName = Search_VBS_Volunteer.FN_RawGUID;
			gridVolunteer.DataSource = _assignments;
			gridVolunteer.DataBind();

			// Log the information
			Logging.LogInformation("User viewed Team Leaders: " + cboFirstLetter.SelectedItem.Text, null, string.Empty, Session["UserName"].ToString());

			ResetSelectionScript();
		}

		private void ResetSelectionScript()
		{
			StringBuilder sb = new StringBuilder();
			int cellCount = 0;
			for (int i = 0; i < _assignments.Count; i++)
			{
				if (_assignments[i].ResponseID != null)
				{
					cellCount++;
					sb.AppendLine(String.Format("gridVolunteer.batchEditApi.SetCellValue({0}, 'CheckedInGrid', true);", i));
				}
			}
			btnSelectAll.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";

			sb = new StringBuilder();
			for (int i = 0; i < _assignments.Count; i++)
			{
				sb.AppendLine(String.Format("gridVolunteer.batchEditApi.SetCellValue({0}, 'CheckedInGrid', false);", i));
			}
			btnSelectNone.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";
		}

		protected void gridVolunteer_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
		{
			// When the cell editor initializes, handle it
			if (e.Editor is ASPxComboBox &&
				cboFirstLetter.SelectedItem != null)
			{
				// Build a hybrid listing of classes and groups
				DataTable dt = new DataTable();
				//dt.Columns.Add("Type", typeof(System.String));
				dt.Columns.Add("Ident", typeof(System.String));
				dt.Columns.Add("Text1", typeof(System.String));
				//dt.Columns.Add("Text2", typeof(System.String));
				//dt.Columns.Add("Text3", typeof(System.String));
				//dt.Columns.Add("Type", typeof(System.String));

				// Get the classes
				Search_VBS_ClassCodeCollection classes = new Search_VBS_ClassCodeCollection(string.Empty);
				classes.Sort(Search_VBS_ClassCode.FN_Length + ", " + Search_VBS_ClassCode.FN_ClassCode);

				// Update the name to show where this person is registered
				foreach (Search_VBS_ClassCode item in classes)
				{
					if (!String.IsNullOrEmpty(item.ClassCode))
					{
						item.GridCustom_5 = String.Format("{0} ({1})", item.ClassCode, item.Grade);
					}
					if (item.RemainingTeacherCount > 0)
					{
						dt.Rows.Add(new object[] {
							//"Class",
							"CLASS|T|" + item.ClassCode,
							"Class: " + item.GridCustom_5 + ": Team Leader",
						});
					}
					if (item.RemainingJuniorHelperCount > 0)
					{
						dt.Rows.Add(new object[] {
							//"Class",
							"CLASS|H|" + item.ClassCode,
							"Class: " + item.GridCustom_5 + ": Junior Helper",
						});
					}
				}

				// Get the groups/teams
				Search_VBS_GroupCollection groups = new Search_VBS_GroupCollection(string.Empty);
				groups.Sort(Search_VBS_Group.FN_GroupName);

				// Build the local collection
				foreach (Search_VBS_Group item in groups)
				{
					dt.Rows.Add(new object[] {
						//"Class",
						"GROUP|" + item.GroupGUID,
						"Team: " + item.GroupName,
					});
				}

				// Populate the editor
				ASPxComboBox combo = ((ASPxComboBox)e.Editor);
				combo.Columns.Clear();
				//combo.Columns.Add("Ident", "Class Code");
				combo.Columns.Add("Text1", "Description");
				//combo.Columns.Add(Search_VBS_ClassCode.FN_StudentCount, "# Students in Class");
				//combo.Columns.Add(Search_VBS_Volunteer.FN_GradeInFall, "Grade");
				//combo.Columns.Add(Search_VBS_Volunteer.FN_GridCustom_5, "Currently Assigned Class");

				combo.DataSource = dt;
				combo.TextField = "Text1";
				combo.ValueField = "Ident";
				combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
				combo.TextFormatString = "{0}";
				combo.DataBindItems();
				combo.ReadOnly = false;
			}
		}

		protected void gridVolunteer_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			if (e.Column.FieldName.ToLower() == "GridCustom_8".ToLower() &&
				e.GetFieldValue(Search_VBS_Volunteer.FN_GridCustom_8) != null)
			{
				if (e.GetFieldValue(Search_VBS_Volunteer.FN_GridCustom_8).ToString().ToUpper().StartsWith("CLASS|T|"))
				{
					e.DisplayText = "Class: " + e.GetFieldValue(Search_VBS_Volunteer.FN_ClassCodeWithDesc).ToString() + " : Team Leader";
				}
				else if (e.GetFieldValue(Search_VBS_Volunteer.FN_GridCustom_8).ToString().ToUpper().StartsWith("CLASS|H|"))
				{
					e.DisplayText = "Class: " + e.GetFieldValue(Search_VBS_Volunteer.FN_ClassCodeWithDesc).ToString() + " : Junior Helper";
				}
				else if (e.GetFieldValue(Search_VBS_Volunteer.FN_GridCustom_8).ToString().ToUpper().StartsWith("GROUP|"))
				{
					e.DisplayText = "Group: " + e.GetFieldValue(Search_VBS_Volunteer.FN_GroupName).ToString() + "";
				}
			}
			
			
			//&&
			//	e.GetFieldValue(Search_VBS_Volunteer.FN_ClassCodeWithDesc) != null)
			//{
			//	e.DisplayText = e.GetFieldValue(Search_VBS_Volunteer.FN_ClassCodeWithDesc).ToString();
			//}
		}

		protected void gridVolunteer_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			List<string> classLinkDeletedGUIDs = new List<string>();
			List<string> responseGroupDeletedGUIDs = new List<string>();
			VBSClassLinkCollection classLinkColl = new VBSClassLinkCollection();
			VBSResponseToGroupCollection responseToGroupColl = new VBSResponseToGroupCollection();
			List<string> newResponseIDs = new List<string>();

			// Now take care of the students
			foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
			{
				OrderedDictionary keys = val.Keys;
				OrderedDictionary newVals = val.NewValues;
				OrderedDictionary oldVals = val.OldValues;

				string rawGUID = (keys["RawGUID"] != null && !String.IsNullOrEmpty(keys["RawGUID"].ToString()) ?
					keys["RawGUID"].ToString().Trim() : string.Empty);
				string responseID = _assignments.Get(rawGUID).ResponseID.ToString().Trim();
				string newEntry = newVals["GridCustom_8"].ToString().Trim();
				bool checkedInGrid = ((bool)newVals["CheckedInGrid"]);

				// Parse out the new entry
				bool teacher = false, juniorHelper = false;
				string classCode = string.Empty, groupGUID = string.Empty;
				if (newEntry.ToUpper().StartsWith("CLASS|T|"))
				{
					teacher = true;
					classCode = Utils.SplitStringToGenericList(newEntry, "|")[2];
				}
				else if (newEntry.ToUpper().StartsWith("CLASS|H|"))
				{
					juniorHelper = true;
					classCode = Utils.SplitStringToGenericList(newEntry, "|")[2];
				}
				else if (newEntry.ToUpper().StartsWith("GROUP|"))
				{
					groupGUID = Utils.SplitStringToGenericList(newEntry, "|")[1];
				}

				// Delete the old and new ones from the class listing first
				StringBuilder sb = new StringBuilder();
				if (!checkedInGrid)
				{
					// We're going to make an update here
					sb.Append(responseID);
					if (sb.ToString().Length > 0)
					{
						VBSClassLinkCollection existingClassLinkColl = new VBSClassLinkCollection("iResponseID IN (" + sb.ToString() + ")");
						foreach (VBSClassLink link in existingClassLinkColl)
						{
							if (!classLinkDeletedGUIDs.Contains(link.LinkGUID)) { classLinkDeletedGUIDs.Add(link.LinkGUID); }
						}
						VBSResponseToGroupCollection existingColl = new VBSResponseToGroupCollection("iResponseID IN (" + sb.ToString() + ")");
						foreach (VBSResponseToGroup link in existingColl)
						{
							if (!responseGroupDeletedGUIDs.Contains(link.LinkGUID)) { responseGroupDeletedGUIDs.Add(link.LinkGUID); }
						}
					}

					// Write the new record
					if (teacher)
					{
						if (!newResponseIDs.Contains(responseID))
						{
							VBSClassLink linkNew = new VBSClassLink();
							linkNew.LinkGUID = System.Guid.NewGuid().ToString();
							linkNew.ResponseID = int.Parse(responseID);
							newResponseIDs.Add(responseID);
							linkNew.ClassCode = classCode;
							linkNew.Student = false;
							linkNew.Teacher = true;
							linkNew.JuniorHelper = false;
							linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
							linkNew.UpdatedByUserID = linkNew.AddedByUserID;
							classLinkColl.Add(linkNew);
						}
					}
					else if (juniorHelper)
					{
						if (!newResponseIDs.Contains(responseID))
						{
							VBSClassLink linkNew = new VBSClassLink();
							linkNew.LinkGUID = System.Guid.NewGuid().ToString();
							linkNew.ResponseID = int.Parse(responseID);
							newResponseIDs.Add(responseID);
							linkNew.ClassCode = classCode;
							linkNew.Student = false;
							linkNew.Teacher = false;
							linkNew.JuniorHelper = true;
							linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
							linkNew.UpdatedByUserID = linkNew.AddedByUserID;
							classLinkColl.Add(linkNew);
						}
					}
					else if (!String.IsNullOrEmpty(groupGUID))
					{
						if (!newResponseIDs.Contains(responseID))
						{
							VBSResponseToGroup linkNew = new VBSResponseToGroup();
							linkNew.LinkGUID = System.Guid.NewGuid().ToString();
							linkNew.ResponseID = int.Parse(responseID);
							newResponseIDs.Add(responseID);
							linkNew.GroupGUID = groupGUID;
							linkNew.TeamCoordinator = false;
							linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
							linkNew.UpdatedByUserID = linkNew.AddedByUserID;
							responseToGroupColl.Add(linkNew);
						}
					}
				}
				else
				{
					// Just delete the row
					VBSClassLinkCollection existingClassLinkColl = new VBSClassLinkCollection("iResponseID IN (" + responseID + ")");
					foreach (VBSClassLink link in existingClassLinkColl)
					{
						if (!classLinkDeletedGUIDs.Contains(link.LinkGUID))
						{
							classLinkDeletedGUIDs.Add(link.LinkGUID);
						}
					}
					VBSResponseToGroupCollection existingResponseGroupColl = new VBSResponseToGroupCollection("iResponseID IN (" + responseID + ")");
					foreach (VBSResponseToGroup link in existingResponseGroupColl)
					{
						if (!responseGroupDeletedGUIDs.Contains(link.LinkGUID))
						{
							responseGroupDeletedGUIDs.Add(link.LinkGUID);
						}
					}
				}
			}

			// Delete any that need to be deleted first
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			if (classLinkDeletedGUIDs.Count > 0)
			{
				VBSClassLinkCollection existingClassLinkColl = new VBSClassLinkCollection("sLinkGUID IN ('" + Utils.JoinString(classLinkDeletedGUIDs, "', '") + "')");
				foreach (VBSClassLink link in existingClassLinkColl)
				{
					link.Delete();
					Logging.LogInformation("Deleting Team Leader From Class: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(existingClassLinkColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();
			}
			if (responseGroupDeletedGUIDs.Count > 0)
			{
				VBSResponseToGroupCollection existingResponseGroupColl = new VBSResponseToGroupCollection("sLinkGUID IN ('" + Utils.JoinString(responseGroupDeletedGUIDs, "', '") + "')");
				foreach (VBSResponseToGroup link in existingResponseGroupColl)
				{
					link.Delete();
					Logging.LogInformation("Deleting Team Leader From Team: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(existingResponseGroupColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();
			}

			// Update the collections
			if (classLinkColl.Count > 0)
			{
				foreach (VBSClassLink link in classLinkColl)
				{
					Logging.LogInformation("Adding Team Leader to Class: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(classLinkColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();
			}
			if (responseToGroupColl.Count > 0)
			{
				foreach (VBSResponseToGroup link in responseToGroupColl)
				{
					Logging.LogInformation("Adding Team Leader to Team: " + link.LinkGUID, link.ResponseID, link.GetAsXML(false), Session["UserName"].ToString());
				}
				errors.AddRange(responseToGroupColl.AddUpdateAll());
				if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
				errors.Clear();
			}

			// Go out and get the updated data from the collection
			if (_assignments.Count > 0)
			{
				string firstLetter = cboFirstLetter.SelectedItem.GetValue(Search_VBS_Volunteer.FN_Left1LastName).ToString();

				// Get the class assignments
				Search_VBS_VolunteerCollection newCollection = new Search_VBS_VolunteerCollection();
				if (firstLetter.ToLower().Contains("all"))
				{
					newCollection = new Search_VBS_VolunteerCollection(string.Empty);
				}
				else if (firstLetter.ToLower().Contains("unassign"))
				{
					newCollection = new Search_VBS_VolunteerCollection("sGroupGUID IS NULL AND sClassCode IS NULL");
				}
				else
				{
					newCollection = new Search_VBS_VolunteerCollection("sLeft1LastName = '" + firstLetter + "'");
				}
				//_assignments.Sort("LastName, FirstName");
				//Search_VBS_VolunteerCollection newCollection =
				//	new Search_VBS_VolunteerCollection("sLeft1LastName = '" + _assignments[0].Left1LastName + "'");

				for (int i = _assignments.Count - 1; i >= 0; i--)
				{
					// Remove them all
					_assignments.RemoveAt(i);
				}

				// Add them back in
				foreach (Search_VBS_Volunteer assign in newCollection)
				{
					_assignments.Add(assign);
				}

				// Fill out the list
				while (_assignments.Count > _maxRecsOnPage)
				{
					for (int i = _assignments.Count - 1; i >= 0; i--)
					{
						if (!String.IsNullOrEmpty(_assignments[i].GridCustom_6) &&
							_assignments[i].GridCustom_6.ToLower().StartsWith("zz"))
						{
							_assignments.RemoveAt(i);
							break;
						}
					}
				}

				//VBSClassCodeCollection classCode = new VBSClassCodeCollection("sClassCode = '" +
				//	cboFirstLetter.SelectedItem.Value.ToString().Replace("'", "''") + "'");
				//if (classCode.Count > 0)
				//{
				//	while (_assignments.Count < _maxRecsOnPage)
				//	{
				//		Search_VBS_ClassAssignment assign = new Search_VBS_ClassAssignment();
				//		assign.LinkGUID = System.Guid.NewGuid().ToString();
				//		assign.RawGUID = System.Guid.NewGuid().ToString();
				//		assign.ClassCode = classCode[0].ClassCode;
				//		assign.Grade = classCode[0].Grade;
				//		assign.Length = classCode[0].Length;
				//		_assignments.Add(assign);
				//	}
				//}

				//// Update for the sort
				//foreach (Search_VBS_ClassAssignment assign in _assignments)
				//{
				//	assign.GridCustom_6 = (String.IsNullOrEmpty(assign.LastName) &&
				//		String.IsNullOrEmpty(assign.FirstName) ? "zzzzzzzzzz" : "aaaaaaaaa");
				//}

				_assignments.Sort(		//Search_VBS_ClassAssignment.FN_GridCustom_6 + ", " +
					Search_VBS_Volunteer.FN_FullNameLastFirstUpper);

				// Handle the numbering
				int count = 1;
				foreach (Search_VBS_Volunteer a in _assignments)
				{
					// Set up the grid custom 8 so it shows up properly
					if (!String.IsNullOrEmpty(a.ClassCode))
					{
						if (a.Teacher.HasValue && a.Teacher.Value)
						{
							//a.GridCustom_8 = "CLASS|T|" + a.ClassCode;
							a.GridCustom_8 = "Class: " + String.Format("{0} ({1})", a.ClassCode, a.Grade) + ": Team Leader";
						}
						else if (a.JuniorHelper.HasValue && a.JuniorHelper.Value)
						{
							a.GridCustom_8 = "Class: " + String.Format("{0} ({1})", a.ClassCode, a.Grade) + ": Junior Helper";
						}
					}
					else if (!String.IsNullOrEmpty(a.GroupGUID))
					{
						//a.GridCustom_8 = "GROUP|" + a.GroupGUID;
						a.GridCustom_8 = "Team: " + a.GroupName;
					}

					a.Numbering = count;
					count++;
				}

				Session["VolunteerAssignmentsCollection"] = _assignments;
			}

			gridVolunteer.DataBind();

			// Reset the javascript code on the buttons
			ResetSelectionScript();

			// Handle the event internally
			e.Handled = true;
		}

		protected void cbSelectAll_Callback(object sender, CallbackEventArgsBase e)
		{
			ResetSelectionScript();
		}

		protected void gridVolunteer_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			gridVolunteer.UpdateEdit();
		}

		protected void btnSubmitHead_Click(object sender, EventArgs e)
		{
			gridVolunteer.UpdateEdit();
		}
	}
}
