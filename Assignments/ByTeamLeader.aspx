﻿<%@ Page Title="Assignments - By Team Leader" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ByTeamLeader.aspx.cs" Inherits="VBS.Assignments.ByTeamLeader" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
				<h3>Use the screen below to make team assignments - by team.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script type="text/javascript">
		function OnClick(s, e) {
			pnlLoading.Show();
			btnSubmitHead.SetEnabled(false);
		}
		function OnHeadClick(s, e) {
			window.setTimeout("btnSubmitHead.SetEnabled(false)", 0);
			pnlLoading.Show();
		}
	</script>
	<table id="selectionTable" width="100%" align="center" border="0">
		<tr>
			<td width="80%">
				<table width="100%">
					<tr>
						<td width="1%" valign="middle" nowrap>First Letter of Volunteer's Last Name:&nbsp;</td>
						<td valign="top"><dx:ASPxComboBox runat="server" ID="cboFirstLetter" ClientInstanceName="cboFirstLetter" DropDownStyle="DropDownList"
							width="150px" DropDownWidth="250" OnDataBinding="cboFirstLetter_DataBinding" AutoPostBack="true"
							ValueField="Left1LastName" TextField="Left1LastName" 
							OnSelectedIndexChanged="cboFirstLetter_SelectedIndexChanged" ValueType="System.String" 
							EnableCallbackMode="true" TextFormatString="{0}">
							<Columns>
								<dx:ListBoxColumn FieldName="Left1LastName" Width="300px" />
							</Columns>
							<ClientSideEvents SelectedIndexChanged="function(s, e) { pnlLoading.Text = 'Loading Records...'; pnlLoading.Show(); btnSubmitHead.SetEnabled(false); }" />
						</dx:ASPxComboBox></td>
					</tr>
				</table>
			</td>
			<td width="20%" align="right" nowrap>
				<dx:ASPxButton ID="btnSubmitHead" ClientInstanceName="btnSubmitHead" runat="server" OnClick="btnSubmitHead_Click" Text="Save Changes" AutoPostBack="true">
					<ClientSideEvents Click="OnHeadClick" />
				</dx:ASPxButton>
			</td>
		</tr>
	</table>
	<table id="bottomPanel" width="100%" align="center" border="0">
		<tr>
			<td colspan="2"><hr /></td>
		</tr>
		<tr>
			<td width="80%">&nbsp;</td>
			<td width="20%" align="right" nowrap>
				<dx:ASPxButton ID="btnSelectAll" ClientInstanceName="btnSelectAll" Text="Check All" runat="server" AutoPostBack="false" />	
				&nbsp;
				<dx:ASPxButton ID="btnSelectNone" ClientInstanceName="btnSelectNone" Text="Uncheck All" runat="server" AutoPostBack="false" />
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<dx:ASPxGridView ID="gridVolunteer" ClientInstanceName="gridVolunteer" runat="server" OnCommandButtonInitialize="gridVolunteer_CommandButtonInitialize"
					Width="100%" OnCellEditorInitialize="gridVolunteer_CellEditorInitialize" OnBatchUpdate="gridVolunteer_BatchUpdate"
					OnCustomColumnDisplayText="gridVolunteer_CustomColumnDisplayText" 
					Paddings-PaddingLeft="10px" Paddings-PaddingRight="10px" Paddings-PaddingTop="0px" Paddings-PaddingBottom="0px"
					EnableViewState="false" Styles-Cell-VerticalAlign="Top" Styles-EditForm-VerticalAlign="Top">
					<Border BorderWidth="0px" />
					<ClientSideEvents BatchEditConfirmShowing="function(s, e) { e.cancel = true; }" 
						BeginCallback="function(s, e) {
							if (e.command == 'UPDATEEDIT') {
								pnlLoading.Text = 'Saving Changes';
								pnlLoading.Show();
								btnSubmitHead.SetEnabled(false);
								isUpdateEdit = true;
							}
						}"
						EndCallback="function(s, e) {
							if (isUpdateEdit) {
								isUpdateEdit = false;
							}
							btnSubmitHead.SetEnabled(true);
							pnlLoading.Hide();
						}" />
					<Columns>
						<dx:GridViewDataColumn FieldName="NumberingAsString" Width="2px" Caption=" " CellStyle-HorizontalAlign="Right" VisibleIndex="1" ReadOnly="true">
							<EditItemTemplate>
								<dx:ASPxLabel ID="lbNumbering" runat="server" Value='<%# Bind("NumberingAsString") %>'>
								</dx:ASPxLabel>
							</EditItemTemplate>
						</dx:GridViewDataColumn>
						<%--<dx:GridViewDataColumn FieldName="VolunteerNameFormatted" Width="250px" Caption="Volunteer Name" 
							VisibleIndex="2" ReadOnly="true">
							<EditFormSettings Visible="False" />
						</dx:GridViewDataColumn>--%>
						<dx:GridViewDataTextColumn FieldName="VolunteerNameHTML" Width="250px" Caption="Volunteer Name" 
							VisibleIndex="2" ReadOnly="true">
							<EditFormSettings Visible="False" />
							<PropertiesTextEdit EncodeHtml="false" />
						</dx:GridViewDataTextColumn>
						<dx:GridViewDataComboBoxColumn FieldName="GridCustom_8" Width="250px" Caption="Description" VisibleIndex="3">
							<PropertiesComboBox  ClearButton-Visibility="False"></PropertiesComboBox>
						</dx:GridViewDataComboBoxColumn>
						<dx:GridViewDataCheckColumn FieldName="CheckedInGrid" Width="2px" Caption="Delete" 
							HeaderStyle-HorizontalAlign="Center" VisibleIndex="8">
						</dx:GridViewDataCheckColumn>
					</Columns>
					<Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowTitlePanel="false" />
					<SettingsBehavior FilterRowMode="OnClick" AllowSort="false" />
					<SettingsEditing Mode="Batch" />
					<SettingsPager PageSize="200" Visible="false" />
					<Styles>
						<InlineEditCell>
							<Border BorderStyle="None" />
						</InlineEditCell>
					</Styles>
				</dx:ASPxGridView>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right"><dx:ASPxButton ID="btnSubmit" ClientInstanceName="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Save Changes" AutoPostBack="true">
				<ClientSideEvents Click="OnClick" />
			</dx:ASPxButton></td>
		</tr>
	</table>
	<dx:ASPxLoadingPanel ID="pnlLoading" runat="server" ContainerElementID="bottomPanel" ClientInstanceName="pnlLoading" Modal="true"></dx:ASPxLoadingPanel>
</asp:Content>
