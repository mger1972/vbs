﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Assignments
{
	public partial class NurseryAssignments : System.Web.UI.Page
	{
		protected DataTable _dt = null;
		protected Search_VBS_VolunteerChildCollection _assignments = new Search_VBS_VolunteerChildCollection();

		protected bool _userCanEdit = true;

		private int _maxRecsOnPage = 1000;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			if (cboFilterBy.SelectedIndex == -1)
			{
				cboFilterBy.SelectedIndex = 1;		// Hit the Unassigned
				cboFilterBy_SelectedIndexChanged(this, null);
			}

			// Bind the grid
			GetData();
			gridNursery.KeyFieldName = Search_VBS_VolunteerChild.FN_DetailGUID;
			gridNursery.DataSource = _assignments;
			gridNursery.DataBind();

			// Tell if the user saw something
			if (!IsPostBack && !IsCallback)
			{
				Logging.LogInformation("User viewed Team Leaders: " + cboFilterBy.SelectedItem.Text, null, string.Empty, HttpContext.Current.User.Identity.Name);
			}

			// Check to see if the user has the ability to edit.  If they don't, turn off the fields
			VBSUserCollection users = new VBSUserCollection("sUserName = '" + Session["UserName"].ToString().Replace("'", "''") + "'");
			_userCanEdit = true;
			if (users.Count > 0)
			{
				if (!users[0].CanEditTeams) { _userCanEdit = false; }
			}

			if (!_userCanEdit)
			{
				btnSelectAll.Visible =
					btnSelectNone.Visible =
					btnSubmit.Visible =
					btnSubmitHead.Visible =
					false;

				foreach (GridViewColumn col in gridNursery.Columns)
				{
					if (col is GridViewDataColumn)
					{
						((GridViewDataColumn)col).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
					}
				}
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			cboFilterBy.DataBind();		// Bind the class type

			// Check the Session for a data table
			if ((_assignments == null ||
				_assignments.Count == 0) &&
				Session["NurseryAssignmentsCollection"] != null)
			{
				_assignments = Session["NurseryAssignmentsCollection"] as Search_VBS_VolunteerChildCollection;
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			if (!IsPostBack || Session["NurseryAssignmentsCollection"] == null)
			{
				string firstLetter = cboFilterBy.SelectedItem.GetValue("FilterType").ToString();

				// Get the class assignments
				if (firstLetter.ToLower().Contains("all"))
				{
					_assignments = new Search_VBS_VolunteerChildCollection(string.Empty);
				}
				else if (firstLetter.ToLower().Contains("unassign"))
				{
					_assignments = new Search_VBS_VolunteerChildCollection("sCurrentAssignment IS NULL");
				}
				else
				{
					_assignments = new Search_VBS_VolunteerChildCollection("sCurrentAssignment = '" + firstLetter.Replace("'", "''") + "'");
				}

				_assignments.Sort(Search_VBS_VolunteerChild.FN_ChildNameLastFirst);

				int count = 1;
				foreach (Search_VBS_VolunteerChild a in _assignments)
				{
					// Set up the grid custom 8 so it shows up properly
					//if (!String.IsNullOrEmpty(a.ClassCode))
					//{
					//	if (a.Teacher.HasValue && a.Teacher.Value)
					//	{
					//		//a.GridCustom_8 = "CLASS|T|" + a.ClassCode;
					//		a.GridCustom_8 = "Class: " + String.Format("{0} ({1})", a.ClassCode, a.Grade) + ": Team Leader";
					//	}
					//	else if (a.JuniorHelper.HasValue && a.JuniorHelper.Value)
					//	{
					//		a.GridCustom_8 = "Class: " + String.Format("{0} ({1})", a.ClassCode, a.Grade) + ": Junior Helper";
					//	}
					//}
					//else if (!String.IsNullOrEmpty(a.GroupGUID))
					//{
					//	//a.GridCustom_8 = "GROUP|" + a.GroupGUID;
					//	a.GridCustom_8 = "Team: " + a.GroupName;
					//}

					a.Numbering = count;
					count++;
				}

				// Store it off in the session
				Session["NurseryAssignmentsCollection"] = _assignments;
			}

			// Bring it back
			_assignments = (Search_VBS_VolunteerChildCollection)Session["NurseryAssignmentsCollection"];
		}

		protected void cboFilterBy_DataBinding(object sender, EventArgs e)
		{
			List<string> list = new List<string>();
			list.Add("< ALL >");
			list.Add("< Unassigned >");
			list.Add("Pre-school");
			list.Add("Walkers");
			list.Add("Nursery");

			_dt = new DataTable();
			_dt.Columns.Add("FilterType", typeof(System.String));
			foreach (string s in list)
			{
				_dt.Rows.Add(new object[] { s });
			}
			cboFilterBy.DataSource = _dt;
		}

		protected void cboFilterBy_SelectedIndexChanged(object sender, EventArgs e)
		{
			// When the index changes, bind the grid
			Session["NurseryAssignmentsCollection"] = null;

			// Get the dataset from the database
			GetData();
			gridNursery.KeyFieldName = Search_VBS_VolunteerChild.FN_DetailGUID;
			gridNursery.DataSource = _assignments;
			gridNursery.DataBind();

			// Log the information
			Logging.LogInformation("User viewed Nursery Assignments: " + cboFilterBy.SelectedItem.Text, null, string.Empty, Session["UserName"].ToString());

			ResetSelectionScript();
		}

		private void ResetSelectionScript()
		{
			StringBuilder sb = new StringBuilder();
			int cellCount = 0;
			for (int i = 0; i < _assignments.Count; i++)
			{
				if (_assignments[i].ResponseID != null)
				{
					cellCount++;
					sb.AppendLine(String.Format("gridNursery.batchEditApi.SetCellValue({0}, 'CheckedInGrid', true);", i));
				}
			}
			btnSelectAll.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";

			sb = new StringBuilder();
			for (int i = 0; i < _assignments.Count; i++)
			{
				sb.AppendLine(String.Format("gridNursery.batchEditApi.SetCellValue({0}, 'CheckedInGrid', false);", i));
			}
			btnSelectNone.ClientSideEvents.Click = "function(s, e) {\r\n" + sb.ToString() + "}";
		}

		protected void gridNursery_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
		{
			// When the cell editor initializes, handle it
			if (e.Editor is ASPxComboBox &&
				cboFilterBy.SelectedItem != null)
			{
				DataTable dt = new DataTable();
				dt.Columns.Add("Type", typeof(System.String));
				dt.Rows.Add(new object[] { "Pre-school" });
				dt.Rows.Add(new object[] { "Walkers" });
				dt.Rows.Add(new object[] { "Nursery" });

				// Populate the editor
				ASPxComboBox combo = ((ASPxComboBox)e.Editor);
				combo.Columns.Clear();
				combo.Columns.Add("Type", "Type");

				combo.DataSource = dt;
				combo.TextField = "Type";
				combo.ValueField = "Type";
				combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
				combo.TextFormatString = "{0}";
				combo.DataBindItems();
				combo.ReadOnly = false;
			}
		}

		protected void gridNursery_CustomColumnDisplayText(object sender, ASPxGridViewColumnDisplayTextEventArgs e)
		{
			//if (e.Column.FieldName.ToLower() == "GridCustom_8".ToLower() &&
			//	e.GetFieldValue(Search_VBS_Volunteer.FN_GridCustom_8) != null)
			//{
			//	if (e.GetFieldValue(Search_VBS_Volunteer.FN_GridCustom_8).ToString().ToUpper().StartsWith("CLASS|T|"))
			//	{
			//		e.DisplayText = "Class: " + e.GetFieldValue(Search_VBS_Volunteer.FN_ClassCodeWithDesc).ToString() + " : Team Leader";
			//	}
			//	else if (e.GetFieldValue(Search_VBS_Volunteer.FN_GridCustom_8).ToString().ToUpper().StartsWith("CLASS|H|"))
			//	{
			//		e.DisplayText = "Class: " + e.GetFieldValue(Search_VBS_Volunteer.FN_ClassCodeWithDesc).ToString() + " : Junior Helper";
			//	}
			//	else if (e.GetFieldValue(Search_VBS_Volunteer.FN_GridCustom_8).ToString().ToUpper().StartsWith("GROUP|"))
			//	{
			//		e.DisplayText = "Group: " + e.GetFieldValue(Search_VBS_Volunteer.FN_GroupName).ToString() + "";
			//	}
			//}
		}

		protected void gridNursery_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			List<string> classLinkDeletedGUIDs = new List<string>();
			List<string> responseGroupDeletedGUIDs = new List<string>();
			VBSClassLinkCollection classLinkColl = new VBSClassLinkCollection();
			VBSResponseToGroupCollection responseToGroupColl = new VBSResponseToGroupCollection();
			List<string> newResponseIDs = new List<string>();

			// Now take care of the students
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
			{
				OrderedDictionary keys = val.Keys;
				OrderedDictionary newVals = val.NewValues;
				OrderedDictionary oldVals = val.OldValues;

				string detailGUID = (keys["DetailGUID"] != null && !String.IsNullOrEmpty(keys["DetailGUID"].ToString()) ?
					keys["DetailGUID"].ToString().Trim() : string.Empty);
				string responseID = _assignments.Get(detailGUID).ResponseID.ToString().Trim();
				string newEntry = (newVals[VBSVolunteerChild.FN_CurrentAssignment] != null ?
					newVals[VBSVolunteerChild.FN_CurrentAssignment].ToString().Trim() : string.Empty);
				bool checkedInGrid = ((bool)newVals["CheckedInGrid"]);

				// Update the item from the collection
				if (!checkedInGrid)
				{
					VBSVolunteerChildCollection children = new VBSVolunteerChildCollection("sDetailGUID = '" + detailGUID + "'");
					foreach (VBSVolunteerChild item in children)
					{
						item.CurrentAssignment = newEntry;
						Logging.LogInformation("Setting Nursery Assignment to: " + newEntry, int.Parse(responseID), item.GetAsXML(false), Session["UserName"].ToString());
					}

					// Update the element
					errors.AddRange(children.AddUpdateAll());
					if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
					errors.Clear();
				}
				else
				{
					// Just delete the row (i.e. make it blank)
					VBSVolunteerChildCollection children = new VBSVolunteerChildCollection("sDetailGUID = '" + detailGUID + "'");
					foreach (VBSVolunteerChild item in children)
					{
						item.CurrentAssignment = string.Empty;
						Logging.LogInformation("Setting Nursery Assignment to blank", int.Parse(responseID), item.GetAsXML(false), Session["UserName"].ToString());
					}

					// Update the element
					errors.AddRange(children.AddUpdateAll());
					if (errors.ViewableExceptionCount > 0) { Utils.ProcessErrorCollection(errors, Session["UserName"].ToString()); }
					errors.Clear();
				}
			}

			// Go out and get the updated data from the collection
			if (_assignments.Count > 0)
			{
				string firstLetter = cboFilterBy.SelectedItem.GetValue("FilterType").ToString();

				// Get the class assignments
				Search_VBS_VolunteerChildCollection newCollection = new Search_VBS_VolunteerChildCollection();
				// Get the class assignments
				if (firstLetter.ToLower().Contains("all"))
				{
					newCollection = new Search_VBS_VolunteerChildCollection(string.Empty);
				}
				else if (firstLetter.ToLower().Contains("unassign"))
				{
					newCollection = new Search_VBS_VolunteerChildCollection("sCurrentAssignment IS NULL");
				}
				else
				{
					newCollection = new Search_VBS_VolunteerChildCollection("sCurrentAssignment = '" + firstLetter.Replace("'", "''") + "'");
				}

				//newCollection.Sort(Search_VBS_VolunteerChild.FN_ChildNameLastFirst);
				//_assignments.Sort("LastName, FirstName");
				//Search_VBS_VolunteerCollection newCollection =
				//	new Search_VBS_VolunteerCollection("sLeft1LastName = '" + _assignments[0].Left1LastName + "'");

				for (int i = _assignments.Count - 1; i >= 0; i--)
				{
					// Remove them all
					_assignments.RemoveAt(i);
				}

				// Add them back in
				foreach (Search_VBS_VolunteerChild assign in newCollection)
				{
					_assignments.Add(assign);
				}

				// Fill out the list
				while (_assignments.Count > _maxRecsOnPage)
				{
					for (int i = _assignments.Count - 1; i >= 0; i--)
					{
						if (!String.IsNullOrEmpty(_assignments[i].GridCustom_6) &&
							_assignments[i].GridCustom_6.ToLower().StartsWith("zz"))
						{
							_assignments.RemoveAt(i);
							break;
						}
					}
				}

				_assignments.Sort(		//Search_VBS_ClassAssignment.FN_GridCustom_6 + ", " +
					Search_VBS_VolunteerChild.FN_ChildNameLastFirst);

				// Handle the numbering
				int count = 1;
				foreach (Search_VBS_VolunteerChild a in _assignments)
				{
					//// Set up the grid custom 8 so it shows up properly
					//if (!String.IsNullOrEmpty(a.ClassCode))
					//{
					//	if (a.Teacher.HasValue && a.Teacher.Value)
					//	{
					//		//a.GridCustom_8 = "CLASS|T|" + a.ClassCode;
					//		a.GridCustom_8 = "Class: " + String.Format("{0} ({1})", a.ClassCode, a.Grade) + ": Team Leader";
					//	}
					//	else if (a.JuniorHelper.HasValue && a.JuniorHelper.Value)
					//	{
					//		a.GridCustom_8 = "Class: " + String.Format("{0} ({1})", a.ClassCode, a.Grade) + ": Junior Helper";
					//	}
					//}
					//else if (!String.IsNullOrEmpty(a.GroupGUID))
					//{
					//	//a.GridCustom_8 = "GROUP|" + a.GroupGUID;
					//	a.GridCustom_8 = "Team: " + a.GroupName;
					//}

					a.Numbering = count;
					count++;
				}

				Session["NurseryAssignmentsCollection"] = _assignments;
			}

			gridNursery.DataBind();

			// Reset the javascript code on the buttons
			ResetSelectionScript();

			// Handle the event internally
			e.Handled = true;
		}

		protected void cbSelectAll_Callback(object sender, CallbackEventArgsBase e)
		{
			ResetSelectionScript();
		}

		protected void gridNursery_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			gridNursery.UpdateEdit();
		}

		protected void btnSubmitHead_Click(object sender, EventArgs e)
		{
			gridNursery.UpdateEdit();
		}
	}
}