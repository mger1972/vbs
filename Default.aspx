﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="VBS._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Sonrise Island VBS</h1>
                <h3>Use the links below (or above in the menu) to navigate the site.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <table width="100%" align="center">
		<tr>
			<td align="center" width="33%"><h3><u>Assignments</u></h3></td>
			<td align="center" width="33%"><h3><u>Reports</u></h3></td>
			<td align="center" width="33%"><h3><u>Sys. Admin.</u></h3></td>
		</tr>
		<tr>
			<td>
				<ol class="round">
					<li class="one">
						<h5>By Class</h5>
						Use this screen to assign team leaders, helpers and students to a single class.
						<a href="~/Assignments/ByClass.aspx" id="link1" runat="server">Edit By Class</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="one">
						<h5>Class Roster (By Student)</h5>
						Generate a listing of all students. All students are listed in order by name.
						<a href="~/Reports/ClassRosterByStudent.aspx" id="A1" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="one">
						<h5>Class Definitions</h5>
						Use this screen to create and edit the class definitions.
						<a href="~/SysAdmin/ClassDef.aspx" id="A7" runat="server">Modify Class Definitions</a>
					</li>
				</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">
					<li class="two">
						<h5>By Student</h5>
						Assign students to classes by looking at a list of students and their currently associated classes.
						<a href="~/Assignments/ByStudent.aspx" id="link2" runat="server">Edit By Student</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="two">
						<h5>Class Roster (By Class)</h5>
						Generate a listing of classes.  The roster is sorted/grouped by class.
						<a href="~/Reports/ClassRosterByClass.aspx" id="A2" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="two">
						<h5>Groups</h5>
						Use this screen to create and edit the groups (Rec, Kitchen, Crafts, etc.)
						<a href="~/SysAdmin/Groups.aspx" id="A6" runat="server">Modify Groups</a>
					</li>
				</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">
					<li class="three">
						<h5>By Team</h5>
						Assign team leaders/volunteers to teams (who is in Rec, Crafts, etc.)
						<a href="~/Assignments/ByTeam.aspx" id="link4" runat="server">Assign Team Leaders/Helpers to Groups</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="three">
						<h5>Junior Helper Grade Violation</h5>
                        Tells which junior helper is out of compliance with the five year class difference policy.
                        <a href="~/Reports/HelperGradeViolation.aspx" id="A3" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="three">
						<h5>Raw Data from MyVBC</h5>
						View the raw data imported from the MyVBC system.
						<a href="~/SysAdmin/RawData.aspx" id="A5" runat="server">View Raw Data</a>
					</li>
				</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">
					<li class="four">
						<h5>By Team Leader</h5>
						Assign team leaders to multiple classes/groups at the same time.
						<a href="~/Assignments/ByTeamLeader.aspx" id="link3" runat="server">Edit By Team Leader/Helper</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="four">
						<h5>Team Leader Roster</h5>
						Generates a roster of team leaders.
						<a href="~/Reports/TeamLeaderRoster.aspx" id="A4" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="four">
						<h5>Download From MyVBC</h5>
						Force a download of data from the MyVBC system.
						<a href="~/Download.aspx" id="A8" runat="server">Download Data</a>
					</li>
				</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">
					<li class="five">
						<h5>EMail Class Rosters</h5>
						EMail Class Rosters to the Team Leaders for each class.
						<a href="~/Assignments/SendClassRosters.aspx" id="A10" runat="server">EMail Class Rosters</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="five">
						<h5>T-Shirt Summary</h5>
						Generates a T-Shirt summary listing - giving the counts of shirts by class.
						<a href="~/Reports/TShirtSummary.aspx" id="link5" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="five">
						<h5>View Stats</h5>
						View the stats of data imported from the MyVBC system.
						<a href="~/Stats.aspx" id="A9" runat="server">View Stats</a>
					</li>
				</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">
					<li class="six">
						<h5>EMail Team Rosters</h5>
						EMail Team Rosters to the Team Coordinators.
						<a href="~/Assignments/SendTeamRosters.aspx" id="A11" runat="server">EMail Team Rosters</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="six">
						<h5>T-Shirt Detail</h5>
						A T-Shirt listing that shows what student gets what sized T-Shirt.  Listing appears by Student Name.
						<a href="~/Reports/TShirtDetail.aspx" id="link6" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">
					<li class="seven">
						<h5>Nursery Assignments</h5>
						Assign children to the nursery.
						<a href="~/Assignments/NurseryAssignments.aspx" id="A17" runat="server">Nursery Assignments</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">
					<li class="seven">
						<h5>Allergies/Special Needs</h5>
						An allergy/special needs report based on the values entered by the parents/guardians.
						<a href="~/Reports/AllergyReport.aspx" id="link7" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="eight">
						<h5>Class Count / Grade Count</h5>
						A total number of people in each class/grade.
						<a href="~/Reports/ClassCount.aspx" id="link8" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="nine">
						<h5>List of Adults</h5>
						A list of adults registered for Sonrise Island.
						<a href="~/Reports/ListOfAdults.aspx" id="A12" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="ten">
						<h5>Volunteer Child Care</h5>
						A list of adult volunteers that require child care.
						<a href="~/Reports/VolunteerChildCare.aspx" id="A13" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="eleven">
						<h5>Background Check</h5>
						A list of individuals requiring a background check.
						<a href="~/Reports/BackgroundCheck.aspx" id="A14" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twelve">
						<h5>People Requesting Team Leader</h5>
						A list of people requesting to be a team leader.
						<a href="~/Reports/TeamLeaderListing.aspx" id="A15" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="thirteen">
						<h5>Email Listing</h5>
						A list of email addresses for teams, groups, and students.
						<a href="~/Reports/EmailListing.aspx" id="A16" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="fourteen">
						<h5>Nursery Roster</h5>
						A list of nursery age children requiring care.
						<a href="~/Reports/NurseryReport.aspx" id="A19" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="fifteen">
						<h5>Walker Roster</h5>
						A list of walker/toddler children requiring care.
						<a href="~/Reports/NurseryReport.aspx?type=walker" id="A20" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="sixteen">
						<h5>Pre-school Roster</h5>
						A list of pre-school age children requiring care.
						<a href="~/Reports/NurseryReport.aspx?type=preschool" id="A21" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="seventeen">
						<h5>Student Export (Excel)</h5>
						Students, grade, friend requests, gender and assigned class.
						<a href="~/Reports/ExcelExtract.aspx" id="A18" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
         <tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="eighteen">
						<h5>Medical Release Form Comparisons</h5>
						A list of reports showing different users and their medical release form statuses.
						<a href="~/Reports/MedicalComparison.aspx" id="A24" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
        <tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="nineteen">
						<h5>Medical Release Form Check</h5>
						A list of individuals requiring a medical release form.
						<a href="~/Reports/MedicalCheck.aspx" id="A22" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
        <tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twenty">
						<h5>T-Shirt Errors</h5>
						A list of T-Shirt Errors.
						<a href="~/Reports/TShirtError.aspx" id="A23" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
        <tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twentyone">
						<h5>Previous Year Counts</h5>
						Counts from this day out in previous years.
						<a href="~/Reports/PrevYearCount.aspx" id="A25" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
        <tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twentytwo">
						<h5>T-Shirt Mailing Labels</h5>
						Generate T-Shirt mailing labels.
						<a href="~/Reports/TShirtMailingLabels.aspx" id="A26" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
        <tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twentythree">
						<h5>Non-Voyagers Volunteers</h5>
                        Get a list of the non-Voyagers volunteers.
                        <a href="~/Reports/NonVoyagersVolunteers.aspx" id="A28" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twentyfour">
						<h5>Top 20 Churches/Schools</h5>
                        Brings up a comparison of Churches/Schools for registered persons.
                        <a href="~/Reports/ChurchListing.aspx" id="A29" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twentyfive">
						<h5>Class Sign-In Sheets</h5>
                        Classroom Sign-In Sheets.
                        <a href="~/Reports/ClassSignIn.aspx" id="A27" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twentysix">
						<h5>Nursery Sign-In Sheets</h5>
						Nursery Sign-In Sheets.
						<a href="~/Reports/NurserySignIn.aspx" id="A30" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twentyseven">
						<h5>Walker Sign-In Sheets</h5>
						Walker Sign-In Sheets.
						<a href="~/Reports/NurserySignIn.aspx?type=walker" id="A31" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twentyeight">
						<h5>Pre-school Sign-In Sheets</h5>
						Pre-school Sign-In Sheets.
						<a href="~/Reports/NurserySignIn.aspx?type=preschool" id="A32" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="twentynine">
						<h5>Volunteer - Sign in Sheet</h5>
						Volunteer sign-in sheet with day of week.
						<a href="~/Reports/VolunteerDOW.aspx" id="A33" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="thirty">
						<h5>T-Shirt Sign In</h5>
						Sign-In Sheet for additional T-shirts.
						<a href="~/Reports/TShirt_SignIn.aspx" id="A34" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>

		<tr>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
			<td>
				<ol class="round">
					<li class="thirtyone">
						<h5>Pickup List</h5>
						Kid's Pickup List (approved people to pick up children).
						<a href="~/Reports/KidPickup.aspx" id="A35" runat="server">Run Report</a>
					</li>
				</ol>
			</td>
			<td>
				<ol class="round">&nbsp;</ol>
			</td>
		</tr>
	</table>
</asp:Content>
