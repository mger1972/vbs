using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace VBS
{
    /// <summary>
    /// Summary description for DAL.
    /// </summary>
    public partial class DAL //: DALBase
    {
		/// <summary>
        ///   The DB.ConnString property allows us to modify and retrieve the database connection string.
        /// </summary>
        /// <returns>The SQL database connection string.</returns>
        /// <param name="value">Value to set the SQL database connection string to.</param> 
        public static string ConnString
        {
            get
			{
				if (String.IsNullOrEmpty(m_sConn))
				{
					m_sConn = "Password=VBSUserpwd;Persist Security Info=True;User ID=VBSUser;Initial Catalog=VBS;Data Source=ws02.markgerlach.com";
					//if (System.Environment.MachineName.ToLower().Equals("mgerlach-2018"))
					//{
					//	m_sConn = "Password=VBSUserpwd;Persist Security Info=True;User ID=VBSUser;Initial Catalog=VBS;Data Source=(local)\\MGER_2014";
					//}
				}
				
				// Check to see if they're in last year's site
				if (Utils.LAST_YEARS_SITE)
				{
					m_sConn = "Password=VBSUserpwd;Persist Security Info=True;User ID=VBSUser;Initial Catalog=VBS_" + DateTime.Now.AddYears(-1).ToString("yyyy") + 
						";Data Source=ws02.markgerlach.com";
				}

				return m_sConn;
			}
            set { m_sConn = value; }
        }
	}
}
