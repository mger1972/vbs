using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

namespace VBS
{
	/// <summary>
	/// Class Name: TableFunction_Table_TShirtSummary
	/// This class can be modified by the user and will not be written over if the class is already found.
	/// Generated by GenClasses on 01/23/2015 01:56 PM
	/// </summary>
	public partial class TableFunction_Table_TShirtSummary
	{
	}


	public partial class TableFunction_Table_TShirtSummaryCollection : ClassGenBindingList<TableFunction_Table_TShirtSummary, string>
	{
	}
}

