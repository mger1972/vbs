using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

namespace VBS
{
	/// <summary>
	/// Class Name: Search_VBS_VolunterDOW
	/// This class can be modified by the user and will not be written over if the class is already found.
	/// Generated by GenClasses on 06/20/2017 05:17 PM
	/// </summary>
	public partial class Search_VBS_VolunterDOW
	{
		public string FullNameLastFirst
		{
			get
			{
				string rtv = string.Empty;
				if (!String.IsNullOrEmpty(_lastName)) { rtv = _lastName; }
				if (!String.IsNullOrEmpty(_firstName)) { rtv += ", " + _firstName; }
				return rtv;
			}
		}
		public static readonly string FN_FullNameLastFirst = "FullNameLastFirst";

		public string FullNameLastFirstUpper
		{
			get
			{
				string rtv = string.Empty;
				if (!String.IsNullOrEmpty(_lastName)) { rtv = _lastName; }
				if (!String.IsNullOrEmpty(_firstName)) { rtv += ", " + _firstName; }
				return rtv.ToUpper();
			}
		}
		public static readonly string FN_FullNameLastFirstUpper = "FullNameLastFirstUpper";

		public string FullNameFirstLast
		{
			get
			{
				string rtv = string.Empty;
				if (!String.IsNullOrEmpty(_firstName)) { rtv = _firstName; }
				if (!String.IsNullOrEmpty(_lastName)) { rtv += " " + _lastName; }
				return rtv;
			}
		}
		public static readonly string FN_FullNameFirstLast = "FullNameFirstLast";

		public string VolunteerNameFormatted
		{
			get
			{
				string rtv = string.Empty;
				if (!String.IsNullOrEmpty(_lastName))
				{
					rtv = _lastName + ", " + _firstName;
				}
				return rtv;
			}
		}
		public static readonly string FN_VolunteerNameFormatted = "VolunteerNameFormatted";
	}

	public partial class Search_VBS_VolunterDOWCollection : ClassGenBindingList<Search_VBS_VolunterDOW, string>
	{
	}
}

