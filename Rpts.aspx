﻿<%@ Page Title="Reports" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Rpts.aspx.cs" Inherits="VBS.Rpts" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>



<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Reports</h1>
				<h3>Below is a listing of reports the system can generate.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<ol class="round">
        <li class="one">
            <h5>Class Roster (By Student)</h5>
            Generate a listing of all students. All students are listed in order by name.
            <a href="~/Reports/ClassRosterByStudent.aspx" id="link1" runat="server">Run Report</a>
        </li>
		<li class="two">
            <h5>Class Roster (By Class)</h5>
            Generate a listing of classes.  The roster is sorted/grouped by class.
            <a href="~/Reports/ClassRosterByClass.aspx" id="link2" runat="server">Run Report</a>
        </li>
		<li class="three">
            <h5>Junior Helper Grade Violation</h5>
			Tells which junior helper is out of compliance with the five year class difference policy.
            <a href="~/Reports/HelperGradeViolation.aspx" id="link3" runat="server">Run Report</a>
        </li>
		<li class="four">
            <h5>Team Leader Roster</h5>
            Generates a roster of team leaders.
            <a href="~/Reports/TeamLeaderRoster.aspx" id="link4" runat="server">Run Report</a>
        </li>

		<li class="five">
            <h5>T-Shirt Summary</h5>
            Generates a T-Shirt summary listing - giving the counts of shirts by class.
            <a href="~/Reports/TShirtSummary.aspx" id="link5" runat="server">Run Report</a>
        </li>
		<li class="six">
            <h5>T-Shirt Detail</h5>
            A T-Shirt listing that shows what student gets what sized T-Shirt.  Listing appears by Student Name.
            <a href="~/Reports/TShirtDetail.aspx" id="link6" runat="server">Run Report</a>
        </li>
		<li class="seven">
            <h5>Allergies/Special Needs</h5>
            An allergy/special needs report based on the values entered by the parents/guardians.
            <a href="~/Reports/AllergyReport.aspx" id="link7" runat="server">Run Report</a>
        </li>
		<li class="eight">
            <h5>Class/Grade Count</h5>
            A total number of people in each class/grade.
            <a href="~/Reports/ClassCount.aspx" id="link8" runat="server">Run Report</a>
        </li>
		<li class="nine">
			<h5>List of Adults</h5>
			A list of adults registered for Sonrise Island.
			<a href="~/Reports/ListOfAdults.aspx" id="A12" runat="server">Run Report</a>
		</li>
		<li class="ten">
			<h5>Volunteer Child Care</h5>
			A list of adult volunteers that require child care.
			<a href="~/Reports/VolunteerChildCare.aspx" id="A13" runat="server">Run Report</a>
			<a name="elevenAnchor"></a>
		</li>
		<li class="eleven">
			<h5>Background Check</h5>
			A list of individuals requiring a background check.
			<a href="~/Reports/BackgroundCheck.aspx" id="A14" runat="server">Run Report</a>
		</li>
		<li class="twelve">
			<h5>People Requesting Team Leader</h5>
			A list of people requesting to be a team leader.
			<a href="~/Reports/TeamLeaderListing.aspx" id="A15" runat="server">Run Report</a>
		</li>
		<li class="thirteen">
			<h5>Email Listing</h5>
			A list of email addresses for teams, groups, and students.
			<a href="~/Reports/EmailListing.aspx" id="A16" runat="server">Run Report</a>
		</li>
		<li class="fourteen">
			<h5>Nursery Roster</h5>
			A list of nursery age children requiring care.
			<a href="~/Reports/NurseryReport.aspx" id="A19" runat="server">Run Report</a>
		</li>
		<li class="fifteen">
			<h5>Walker Roster</h5>
			A list of walker/toddler children requiring care.
			<a href="~/Reports/NurseryReport.aspx?type=walker" id="A20" runat="server">Run Report</a>
		</li>
		<li class="sixteen">
			<h5>Pre-school Roster</h5>
			A list of pre-school age children requiring care.
			<a href="~/Reports/NurseryReport.aspx?type=preschool" id="A21" runat="server">Run Report</a>
		</li>
		<li class="seventeen">
			<h5>Student Export (Excel)</h5>
			Students, grade, friend requests, gender and assigned class.
			<a href="~/Reports/ExcelExtract.aspx" id="A1" runat="server">Run Report</a>
		</li>
        <li class="eighteen">
			<h5>Medical Release Form Comparisons</h5>
			A list of reports showing different users and their medical release form statuses.
			<a href="~/Reports/MedicalComparison.aspx" id="A4" runat="server">Run Report</a>
		</li>
        <li class="nineteen">
			<h5>Medical Release Form Check</h5>
			A list of individuals requiring a medical release form.
			<a href="~/Reports/MedicalCheck.aspx" id="A2" runat="server">Run Report</a>
		</li>
        <li class="twenty">
			<h5>T-Shirt Errors</h5>
			A list of T-Shirt Errors.
			<a href="~/Reports/TShirtError.aspx" id="A3" runat="server">Run Report</a>
		</li>
        <li class="twentyone">
			<h5>Previous Year Counts</h5>
			Counts from this day out in previous years.
			<a href="~/Reports/PrevYearCount.aspx" id="A5" runat="server">Run Report</a>
		</li>
        <li class="twentytwo">
			<h5>T-Shirt Mailing Labels</h5>
			Generate T-Shirt mailing labels.
			<a href="~/Reports/TShirtMailingLabels.aspx" id="A6" runat="server">Run Report</a>
		</li>
        <li class="twentythree">
			<h5>Non-Voyagers Volunteers</h5>
			Get a list of the non-Voyagers volunteers.
			<a href="~/Reports/NonVoyagersVolunteers.aspx" id="A7" runat="server">Run Report</a>
		</li>
		<li class="twentyfour">
			<h5>Top 20 Churches/Schools</h5>
            Brings up a comparison of Churches and Schools for registered persons.
            <a href="~/Reports/ChurchListing.aspx" id="A29" runat="server">Run Report</a>
		</li>
		<li class="twentyfive">
			<h5>Class Sign-In Sheets</h5>
            Classroom Sign-In Sheets.
            <a href="~/Reports/ClassSignIn.aspx" id="A27" runat="server">Run Report</a>
		</li>
		<li class="twentysix">
			<h5>Nursery Sign-In Sheets</h5>
			Nursery Sign-In Sheets.
			<a href="~/Reports/NurserySignIn.aspx" id="A8" runat="server">Run Report</a>
		</li>
		<li class="twentyseven">
			<h5>Walker Sign-In Sheets</h5>
			Walker Sign-In Sheets.
			<a href="~/Reports/NurserySignIn.aspx?type=walker" id="A9" runat="server">Run Report</a>
		</li>
		<li class="twentyeight">
			<h5>Pre-school Sign-In Sheets</h5>
			Pre-school Sign-In Sheets.
			<a href="~/Reports/NurserySignIn.aspx?type=preschool" id="A10" runat="server">Run Report</a>
		</li>
		<li class="twentynine">
			<h5>Volunteer - Sign in Sheet</h5>
			Volunteer sign-in sheet with day of week.
			<a href="~/Reports/VolunteerDOW.aspx" id="A33" runat="server">Run Report</a>
		</li>
		<li class="thirty">
			<h5>T-Shirt Sign In</h5>
			Sign-In Sheet for additional T-shirts.
			<a href="~/Reports/TShirt_SignIn.aspx" id="A34" runat="server">Run Report</a>
		</li>
		<li class="thirtyone">
			<h5>Pickup List</h5>
			Kid's Pickup List (approved people to pick up children).
			<a href="~/Reports/KidPickup.aspx" id="A35" runat="server">Run Report</a>
		</li>
    </ol>
</asp:Content>

