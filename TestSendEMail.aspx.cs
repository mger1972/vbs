﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace VBS
{
	public partial class TestSendEMail : System.Web.UI.Page
	{
		private static StringBuilder _processString;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			//else
			//{
			//	Session["UserName"] = HttpContext.Current.User.Identity.Name;
			//}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			if (!IsPostBack &&
				!IsCallback)
			{
				_processString = new StringBuilder();       // Reset the progress
			}
		}

		/// <summary>
		/// Send the test email
		/// </summary>
		private void SendTestEmail()
		{
			List<MailAddress> listTo = new List<MailAddress>();
			List<MailAddress> listCC = new List<MailAddress>();
			List<MailAddress> listBCC = new List<MailAddress>();

			listTo.Add(new MailAddress("mark@gerlach5.com", "Mark Gerlach"));
			listCC.Add(new MailAddress("jen@gerlach5.com", "Jennifer Gerlach"));

			_processString = new StringBuilder();
			ClassGenExceptionCollection errors = 
				Utils.SendEMailAsJen(listTo, listCC, listBCC, "Test email from SonriseIslandVBS.com", "This is a test.", string.Empty, "System");
			if (errors.ViewableExceptionCount > 0)
			{
				foreach (ClassGenException ex in errors)
				{
					_processString.AppendLine(ex.Message + "<br />");
				}
			}
			else
			{
				_processString.AppendLine("Message sent successfully...");
			}
		}

		protected void cbProcess_Callback(object source, DevExpress.Web.CallbackEventArgs e)
		{
			// Send the test email
			SendTestEmail();
		}

		protected void cbGetStatus_Callback(object source, DevExpress.Web.CallbackEventArgs e)
		{
			e.Result = _processString.ToString();
		}
	}
}