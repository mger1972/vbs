﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace VBS.classes
{
    public class PrevYearCounts
    {
        /// <summary>
        /// Get the prev year sets that we can send to the reports
        /// </summary>
        /// <returns>A DataSet (collection of tables) with the information</returns>
        public static DataSet GetPrevYearDataSets()
        {
            DataSet ds = new DataSet();

            // Get the table from the stored proc
            try
            {
                string sql = string.Empty;
                SqlCommand cmd = null;
                DataTable dt = null;

                sql = "EXEC spComparePrevYears";
                cmd = new SqlCommand(sql);
                dt = DAL.SQLExecDT(cmd);

                DataView view = dt.DefaultView; view.Sort = "iSortOrder, iExtSortOrder"; view.RowFilter = "sRecordType = 'DaysOut'"; DataTable dtDaysOut = view.ToTable("DaysOut");
                view = dt.DefaultView; view.Sort = "iSortOrder, iExtSortOrder"; view.RowFilter = "sRecordType = 'Totals'"; DataTable dtTotals = view.ToTable("Totals");
                view = dt.DefaultView; view.Sort = "iSortOrder, iExtSortOrder"; view.RowFilter = "sRecordType = 'SheetType'"; DataTable dtSheetType = view.ToTable("SheetType");
                view = dt.DefaultView; view.Sort = "iSortOrder, iExtSortOrder"; view.RowFilter = "sRecordType = 'GradeInFall'"; DataTable dtGradeInFall = view.ToTable("GradeInFall");
                view = dt.DefaultView; view.Sort = "iSortOrder, iExtSortOrder"; view.RowFilter = "sRecordType = 'Gender'"; DataTable dtGender = view.ToTable("Gender");
                view = dt.DefaultView; view.Sort = "iSortOrder, iExtSortOrder"; view.RowFilter = "sRecordType <> 'DaysOut'"; DataTable dtComplete = view.ToTable("MainTable");

                dtComplete.Columns.Add("Desc", typeof(System.String));
                foreach (DataRow row in dtComplete.Rows)
                {
                    switch (row[0].ToString())
                    {
                        case "Totals":
                        case "Gender":
                            row["Desc"] = row[0].ToString();
                            break;
                        case "GradeInFall":
                            row["Desc"] = "Grade In Fall";
                            break;
                        case "SheetType":
                            row["Desc"] = "Sheet Type";
                            break;
                    }
                }

                // Build the dataset
                ds.Tables.AddRange(new DataTable[]
                {
                    dtComplete,
                    dtDaysOut,
                    dtTotals,
                    dtSheetType,
                    dtGradeInFall,
                    dtGender,
                });
            }
            catch (Exception ex)
            {
                Logging.LogError(ex.Message, null, string.Empty, string.Empty);
            }

            return ds;      // Send the dataset back
        }
    }
}