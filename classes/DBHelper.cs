﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VBS.classes
{
    public class DBHelper
    {
        /// <summary>
        /// Update the DB values after an import (
        /// </summary>
        public static void UpdateDBValuesAfterImport()
        {

        }

        /// <summary>
        /// Update the schools
        /// </summary>
        private static void UpdateSchools()
        {

        }

        /// <summary>
        /// Update the church they've attended
        /// </summary>
        private static void UpdateChurch()
        {
            List<string> list = new List<string>();

            // Add the sql statements
            list.Add("UPDATE tVBSData SET sChurchAttended = REPLACE(sChurchAttended, '  ', ' ')");
            list.Add("UPDATE tVBSDataPrevYear SET sChurchAttended = REPLACE(sChurchAttended, '  ', ' ')");

            list.Add("UPDATE tVBSData SET sChurchAttended = dbo.fnInitCaps(sChurchAttended)");
            list.Add("UPDATE tVBSDataPrevYear SET sChurchAttended = dbo.fnInitCaps(sChurchAttended)");

            // Fire the statements off at the database one at a time

        }
    }
}