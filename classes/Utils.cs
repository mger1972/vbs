﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace VBS
{
	public class Utils
	{
		//public static const string REPORT_OUTPUT_DIR = "";
		public static bool LAST_YEARS_SITE = false;

		//#region Temp Dir Handling
		///// <summary>
		///// Checks to make sure there is a temp dir located under the 
		///// main directory and creates one if there's not
		///// </summary>
		///// <returns>The full path to the temp directory</returns>
		//public static string CheckWinTempDir()
		//{
		//	if (String.IsNullOrEmpty(Utils.ApplicationStartupDirectory))
		//	{
		//		return string.Empty;
		//	}
		//	string tempDir = Utils.ApplicationStartupDirectory + @"\" + TempDirectoryName;
		//	if (!Directory.Exists(tempDir)) { Directory.CreateDirectory(tempDir); }
		//	return tempDir;
		//}

		///// <summary>
		///// Checks to make sure there is a temp dir located under the 
		///// main directory and creates one if there's not
		///// </summary>
		///// <returns>The full path to the temp directory</returns>
		//public static string GetWinTempDir()
		//{
		//	// Call the CheckWinTempDir function
		//	return CheckWinTempDir();
		//}

		///// <summary>
		///// Returns a new temp file name without the extension but with the path
		///// </summary>
		///// <returns>The temp file name</returns>
		//public static string GetWinTempFile()
		//{
		//	string rtv = string.Empty;
		//	string tempDir = CheckWinTempDir();
		//	if (!String.IsNullOrEmpty(tempDir))
		//	{
		//		rtv = tempDir + @"\" + DateTime.Now.ToString("yyyyMMdd-hhmmss-ffff");
		//	}

		//	return rtv;
		//}

		///// <summary>
		///// Returns a new temp file name with the extension appended
		///// </summary>
		///// <param name="extension">The extension to append to the file</param>
		///// <returns>The temp file complete with path</returns>
		//public static string GetWinTempFile(string extension)
		//{
		//	return GetWinTempFile() + "." +
		//		(extension.StartsWith(".") ? extension.Substring(1) : extension);
		//}

		///// <summary>
		///// Remove any files from the temp directory
		///// </summary>
		//public static void DeleteWinTempFiles(int olderThanDays)
		//{
		//	string tempDir = GetWinTempDir();
		//	foreach (string file in Directory.GetFiles(tempDir))
		//	{
		//		try
		//		{
		//			if (olderThanDays > 0)
		//			{
		//				// Check the file
		//				if (new FileInfo(file).LastAccessTime < DateTime.Now.AddDays(-1 * olderThanDays) ||
		//					new FileInfo(file).CreationTime < DateTime.Now.AddDays(-1 * olderThanDays) ||
		//					new FileInfo(file).LastWriteTime < DateTime.Now.AddDays(-1 * olderThanDays))
		//				{
		//					File.Delete(file);
		//				}
		//			}
		//			else
		//			{
		//				File.Delete(file);
		//			}
		//		}
		//		catch { }
		//	}
		//}

		///// <summary>
		///// Remove any files from the temp directory
		///// </summary>
		//public static void DeleteWinTempFiles()
		//{
		//	DeleteWinTempFiles(0);
		//}
		//#endregion Temp Dir Handling

		/// <summary>
		/// Splits a string by using the split string sent in
		/// </summary>
		/// <param name="stringValue">The value to split</param>
		/// <param name="splitChars">The character sequence to use as a split sequence</param>
		/// <returns>The string array that has been split</returns>
		public static string[] SplitString(string stringValue, string splitChars)
		{
			string[] rtv = new string[] { };
			try
			{
				StringBuilder phrase = new StringBuilder();
				for (int i = 0; i < splitChars.Length; i++)
				{
					if (splitChars.Substring(i, 1) != "\r" &&
						splitChars.Substring(i, 1) != "\n" &&
						splitChars.Substring(i, 1) != "\t" &&
						splitChars.Substring(i, 1) != "\\" &&
						splitChars.Substring(i, 1) != "\'" &&
						splitChars.Substring(i, 1) != "\"" &&
						splitChars.Substring(i, 1) != "\b" &&
						splitChars.Substring(i, 1) != "\f" &&
						splitChars.Substring(i, 1) != "\v" &&
						splitChars.Substring(i, 1) != "x" &&
						splitChars.Substring(i, 1) != "_")
					{
						phrase.Append(@"\");
					}
					phrase.Append(splitChars.Substring(i, 1));
				}
				rtv = Regex.Split(stringValue, phrase.ToString(), RegexOptions.IgnoreCase);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return rtv;
		}

		/// <summary>
		/// Split the string and return a list
		/// </summary>
		/// <param name="stringValue"></param>
		/// <param name="splitChars"></param>
		/// <returns></returns>
		public static List<string> SplitStringToGenericList(string stringValue, string splitChars)
		{
			return new List<string>(Utils.SplitString(stringValue, splitChars));
		}

		/// <summary>
		/// Joins a string array by using the join character string sent in
		/// </summary>
		/// <param name="stringValues">The values to join</param>
		/// <param name="joinChars">The character sequence to use as a join sequence</param>
		/// <returns>The string that has been joined</returns>
		public static string JoinString(string[] stringValues, string joinChars)
		{
			StringBuilder sb = new StringBuilder();
			foreach (string s in stringValues)
			{
				if (!String.IsNullOrEmpty(sb.ToString())) { sb.Append(joinChars); }
				sb.Append(s);
			}
			return sb.ToString();
		}

		/// <summary>
		/// Joins a string array by using the join character string sent in
		/// </summary>
		/// <param name="stringValues">The values to join</param>
		/// <param name="joinChars">The character sequence to use as a join sequence</param>
		/// <returns>The string that has been joined</returns>
		public static string JoinString(List<string> stringValues, string joinChars)
		{
			StringBuilder sb = new StringBuilder();
			foreach (string s in stringValues)
			{
				if (!String.IsNullOrEmpty(sb.ToString())) { sb.Append(joinChars); }
				sb.Append(s);
			}
			return sb.ToString();
		}

		/// <summary>
		/// Tells if the generic list contains a string (case insensitive search)
		/// </summary>
		/// <param name="list">The list to hunt through</param>
		/// <param name="val">The value to find</param>
		/// <returns>True if exists, otherwise, false</returns>
		public static bool ListContainsString(List<string> list, string val)
		{
			bool rtv = false;

			if (String.IsNullOrEmpty(val))
			{
				// Check for null and return it
				rtv = list.Contains(val);
			}
			else
			{
				rtv = (null != list.Find(delegate(string str)
				{ return str.ToLower().Equals(val.ToLower()); }));
			}

			return rtv;
		}

        /// <summary>
		/// Caps all chars after special chars, otherwise, makes them lower
		/// </summary>
		/// <param name="text">The text to be converted</param>
		/// <returns>A string that has been formatted</returns>
		public static string InitCaps(string text)
        {
            if (String.IsNullOrEmpty(text)) { return string.Empty; }
            StringBuilder sb = new StringBuilder();

            bool nextCharShouldBeCapped = true;
            //string specialChars = @" -/'.\r\n";
            for (int i = 0; i < text.Length; i++)
            {
                if (nextCharShouldBeCapped)
                {
                    sb.Append(text.Substring(i, 1).ToUpper());
                }
                else
                {
                    sb.Append(text.Substring(i, 1).ToLower());
                }

                // Figure out if the next character should be capped
                nextCharShouldBeCapped = false;
                if (text.Substring(i, 1) == " " ||
                    text.Substring(i, 1) == "-" ||
                    text.Substring(i, 1) == "/" ||
                    text.Substring(i, 1) == "'" ||
                    text.Substring(i, 1) == "\r" ||
                    text.Substring(i, 1) == "\n" ||
                    (i > 0 && text.Substring(i - 1, 2).ToUpper() == "MC") ||
                    text.Substring(i, 1) == ".")
                {
                    nextCharShouldBeCapped = true;
                }
                //nextCharShouldBeCapped = (specialChars.IndexOf(text.Substring(i, 1)) > -1);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Process the error collection back to the database
        /// </summary>
        /// <param name="errors">The error collection to process</param>
        /// <param name="userName">The user that ran into the error</param>
        public static void ProcessErrorCollection(ClassGenExceptionCollection errors, string userName)
		{
			foreach (ClassGenException ex in errors)
			{
				if (ex.ClassGenExceptionIconType.ToString().ToLower().Contains("info"))
				{
					Logging.LogInformation(ex.Message + " | " + ex.StackTrace, null, string.Empty, userName);
				}
				else if (ex.ClassGenExceptionIconType.ToString().ToLower().Contains("warn"))
				{
					Logging.LogWarning(ex.Message + " | " + ex.StackTrace, null, string.Empty, userName);
				}
				else if (ex.ClassGenExceptionIconType.ToString().ToLower().Contains("criti"))
				{
					Logging.LogError(ex.Message + " | " + ex.StackTrace, null, string.Empty, userName);
				}
			}
		}

		#region Send Email
		///// <summary>
		///// Send an email to multiple users
		///// </summary>
		///// <param name="to">The addresses to send the email to</param>
		///// <param name="cc">The addresses to send the email to as a CC</param>
		///// <param name="subject">The subject of the email</param>
		///// <param name="body">The body of the email</param>
		///// <param name="appVersion">The application version</param>
		///// <returns>An Error collection with the elements needed</returns>
		//public static ClassGenExceptionCollection SendEMail(List<string> to,
		//	List<string> cc,
		//	string subject,
		//	string body,
		//	string appVersion,
		//	List<System.Net.Mail.Attachment> attachments,
		//	string userNameForLog)
		//{
		//	return SendEMail("vbs@markgerlach.com",
		//		to,
		//		cc,
		//		subject,
		//		body,
		//		appVersion,
		//		attachments,
		//		userNameForLog);
		//}

		///// <summary>
		///// Send an email to multiple users
		///// </summary>
		///// <param name="to">The addresses to send the email to</param>
		///// <param name="cc">The addresses to send the email to as a CC</param>
		///// <param name="subject">The subject of the email</param>
		///// <param name="body">The body of the email</param>
		///// <param name="appVersion">The application version</param>
		///// <returns>An Error collection with the elements needed</returns>
		//public static ClassGenExceptionCollection SendEMail(List<string> to,
		//	List<string> cc,
		//	string subject,
		//	string body,
		//	string appVersion,
		//	string userNameForLog)
		//{
		//	return SendEMail("vbs@markgerlach.com",
		//		to,
		//		cc,
		//		subject,
		//		body,
		//		appVersion,
		//		new List<System.Net.Mail.Attachment>(),
		//		userNameForLog);
		//}

		///// <summary>
		///// Send an email to the respective client
		///// </summary>
		///// <param name="to">The address to send the email to</param>
		///// <param name="subject">The subject of the email</param>
		///// <param name="body">The body of the email</param>
		///// <param name="appVersion">The application version</param>
		///// <returns>An Error collection with the elements needed</returns>
		//public static ClassGenExceptionCollection SendEMail(string to,
		//	string subject,
		//	string body,
		//	string appVersion,
		//	string userNameForLog)
		//{
		//	return SendEMail("vbs@markgerlach.com",
		//		new List<string>(new string[] { to }),
		//		new List<string>(),
		//		subject,
		//		body,
		//		appVersion,
		//		new List<System.Net.Mail.Attachment>(),
		//		userNameForLog);
		//}

		///// <summary>
		///// Send an email to the respective client
		///// </summary>
		///// <param name="to">The addresses to send the email to</param>
		///// <param name="cc">The addresses to send the email to as a CC</param>
		///// <param name="subject">The subject of the email</param>
		///// <param name="body">The body of the email</param>
		///// <param name="from">The address send the email from</param>
		///// <param name="appVersion">The application version</param>
		///// <param name="attachments">A collection of attachments to send as part of the email</param>
		///// <returns>An Error collection with the elements needed</returns>
		//public static ClassGenExceptionCollection SendEMail(string from,
		//	List<string> to,
		//	List<string> cc,
		//	string subject,
		//	string body,
		//	string appVersion,
		//	List<System.Net.Mail.Attachment> attachments,
		//	string userNameForLog)
		//{
		//	ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
		//	string server = "mail.markgerlach.com";

		//	MailMessage message = new MailMessage();
		//	message.From = new MailAddress(from);
		//	message.To.Add(Utils.JoinString(to, ","));
		//	if (cc.Count > 0) { message.CC.Add(Utils.JoinString(cc, ",")); }
		//	message.Subject = subject;
		//	message.Body = body;

		//	if (attachments.Count > 0)
		//	{
		//		foreach (System.Net.Mail.Attachment att in attachments)
		//		{
		//			message.Attachments.Add(att);
		//		}
		//	}

		//	SmtpClient client = new SmtpClient(server, 587);

		//	// Credentials are necessary if the server requires the client 
		//	// to authenticate before it will send e-mail on the client's behalf.
		//	//client.UseDefaultCredentials = true;
		//	client.Credentials = new System.Net.NetworkCredential("vbs@markgerlach.com", "ce2$LM");

		//	try
		//	{
		//		client.Send(message);

		//		// Write a log trans messsage about who received the email
		//		//LogTrans.WriteRecordSendEmail(message.Body, appVersion, "System Import");
		//		Logging.LogInformation("Mail Message: " + message.Body, null, string.Empty, userNameForLog);
		//	}
		//	catch (Exception ex)
		//	{
		//		errors.Add(new ClassGenException(ex));
		//	}

		//	return errors;      // Return the collection
		//}
		#endregion Send Email

		#region Send Email As Jen
		/// <summary>
		/// Send an email to multiple users
		/// </summary>
		/// <param name="to">The addresses to send the email to</param>
		/// <param name="cc">The addresses to send the email to as a CC</param>
		/// <param name="bcc">The addresses to send the email to as a BCC</param>
		/// <param name="subject">The subject of the email</param>
		/// <param name="body">The body of the email</param>
		/// <param name="appVersion">The application version</param>
		/// <returns>An Error collection with the elements needed</returns>
		public static ClassGenExceptionCollection SendEMailAsJen(List<MailAddress> to,
			List<MailAddress> cc,
			List<MailAddress> bcc,
			string subject,
			string body,
			string appVersion,
			List<System.Net.Mail.Attachment> attachments,
			string userNameForLog)
		{
			return SendEMailAsJen(new MailAddress("jen@gerlach5.com", "Jennifer Gerlach"),
				to,
				cc,
				bcc,
				subject,
				body,
				appVersion,
				attachments,
				userNameForLog);
		}

		/// <summary>
		/// Send an email to multiple users
		/// </summary>
		/// <param name="to">The addresses to send the email to</param>
		/// <param name="cc">The addresses to send the email to as a CC</param>
		/// <param name="bcc">The addresses to send the email to as a BCC</param>
		/// <param name="subject">The subject of the email</param>
		/// <param name="body">The body of the email</param>
		/// <param name="appVersion">The application version</param>
		/// <returns>An Error collection with the elements needed</returns>
		public static ClassGenExceptionCollection SendEMailAsJen(List<MailAddress> to,
			List<MailAddress> cc,
			List<MailAddress> bcc,
			string subject,
			string body,
			string appVersion,
			string userNameForLog)
		{
			return SendEMailAsJen(new MailAddress("jen@gerlach5.com", "Jennifer Gerlach"),
				to,
				cc,
				bcc,
				subject,
				body,
				appVersion,
				new List<System.Net.Mail.Attachment>(),
				userNameForLog);
		}

		/// <summary>
		/// Send an email to the respective client
		/// </summary>
		/// <param name="to">The address to send the email to</param>
		/// <param name="subject">The subject of the email</param>
		/// <param name="body">The body of the email</param>
		/// <param name="appVersion">The application version</param>
		/// <returns>An Error collection with the elements needed</returns>
		public static ClassGenExceptionCollection SendEMailAsJen(string toName,
			string toEmail,
			string subject,
			string body,
			string appVersion,
			string userNameForLog)
		{
			return SendEMailAsJen(new MailAddress("jen@gerlach5.com", "Jennifer Gerlach"),
				new List<MailAddress>(new MailAddress[] { 
					new MailAddress(toEmail, toName)
				}),
				new List<MailAddress>(),
				new List<MailAddress>(),
				subject,
				body,
				appVersion,
				new List<System.Net.Mail.Attachment>(),
				userNameForLog);
		}

		/// <summary>
		/// Send an email to the respective client
		/// </summary>
		/// <param name="to">The addresses to send the email to</param>
		/// <param name="cc">The addresses to send the email to as a CC</param>
		/// <param name="bcc">The addresses to send the email to as a BCC</param>
		/// <param name="subject">The subject of the email</param>
		/// <param name="body">The body of the email</param>
		/// <param name="from">The address send the email from</param>
		/// <param name="appVersion">The application version</param>
		/// <param name="attachments">A collection of attachments to send as part of the email</param>
		/// <returns>An Error collection with the elements needed</returns>
		public static ClassGenExceptionCollection SendEMailAsJen(MailAddress from,
			List<MailAddress> to,
			List<MailAddress> cc,
			List<MailAddress> bcc,
			string subject,
			string body,
			string appVersion,
			List<System.Net.Mail.Attachment> attachments,
			string userNameForLog)
		{
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			string server = "smtp.office365.com";

			MailMessage message = new MailMessage();
			message.From = from;
			foreach (MailAddress addr in to)
			{
				message.To.Add(addr);
			}
			foreach (MailAddress addr in cc)
			{
				message.CC.Add(addr);
			}
			foreach (MailAddress addr in bcc)
			{
				message.Bcc.Add(addr);
			}

			message.Bcc.Add(new MailAddress("jen@gerlach5.com", "Jennifer Gerlach"));
			message.Bcc.Add(new MailAddress("mark@gerlach5.com", "Mark Gerlach"));

			//message.To.Add(Utils.JoinString(to, ","));
			//if (cc.Count > 0) { message.CC.Add(Utils.JoinString(cc, ",")); }
			message.Subject = subject;
			message.Body = body;
			message.IsBodyHtml = true;

			if (attachments.Count > 0)
			{
				foreach (System.Net.Mail.Attachment att in attachments)
				{
					message.Attachments.Add(att);
				}
			}

			SmtpClient client = new SmtpClient(server, 587);

			// Credentials are necessary if the server requires the client 
			// to authenticate before it will send e-mail on the client's behalf.
			//client.UseDefaultCredentials = false;
			client.Credentials = new System.Net.NetworkCredential("jen@gerlach5.com", "10511GroveOak");
			client.DeliveryMethod = SmtpDeliveryMethod.Network;
			client.EnableSsl = true;
			client.Timeout = 600000;		// Set the timeout to 10 minutes

			try
			{
				client.Send(message);

				// Write a log trans messsage about who received the email
				//LogTrans.WriteRecordSendEmail(message.Body, appVersion, "System Import");
				Logging.LogInformation("Mail Message: " + message.Body, null, string.Empty, userNameForLog);
			}
			catch (Exception ex)
			{
				errors.Add(new ClassGenException(ex));
			}

			return errors;      // Return the collection
		}
		#endregion Send Email As Jen

		#region Form IDs
		private static Dictionary<string, int> _formIDs = new Dictionary<string, int>();
		public static Dictionary<string, int> FormIDs
		{
			get
			{
				// Check to make sure you have something in the list
				if (_formIDs.Count == 0)
				{
					//if (type == ImportType.VolunteerUnder18) { fileIDs.Add("VolunteerUnder18", 423); }        // 2015 values
					//if (type == ImportType.VolunteerAdult) { fileIDs.Add("VolunteerAdult", 422); }
					//if (type == ImportType.Base) { fileIDs.Add("Base", 418); }
					//if (type == ImportType.AltPay) { fileIDs.Add("AltPay", 419); }

					//fileIDs.Add("Base", 455);           // 2016 Values
					//fileIDs.Add("AltPay", 518);
					//fileIDs.Add("VolunteerUnder18", 520);
					//fileIDs.Add("VolunteerAdult", 519);
					//fileIDs.Add("MedicalReleaseForm", 503);

					//_formIDs.Add("Base", 604);           // 2017 Values
					//_formIDs.Add("AltPay", 605);
					//_formIDs.Add("VolunteerUnder18", 607);
					//_formIDs.Add("VolunteerAdult", 606);
					//_formIDs.Add("MedicalReleaseFormCurrent", 589);        // 2017-2018 Medical form
					//_formIDs.Add("MedicalReleaseFormPrevYear", 503);        // 2016-2017 Medical form

					//_formIDs.Add("Base", 708);           // 2018 Values
					//_formIDs.Add("AltPay", 799);
					//_formIDs.Add("VolunteerUnder18", 705);
					//_formIDs.Add("VolunteerAdult", 706);
					//_formIDs.Add("MedicalReleaseFormCurrent", 783);        // 2018-2019 Medical form
					////_formIDs.Add("MedicalReleaseFormPrevYear", 503);        // 2017-2018 Medical form
					
					_formIDs.Add("Base", 772);           // 2019 Values
					_formIDs.Add("AltPay", 890);
					_formIDs.Add("VolunteerUnder18", 877);
					_formIDs.Add("VolunteerAdult", 878);
					_formIDs.Add("MedicalReleaseFormCurrent", 803);        // 2018-2019 Medical form
					//_formIDs.Add("MedicalReleaseFormPrevYear", 503);        // 2017-2018 Medical form
				}

				// Return the value they're looking for
				return _formIDs;
			}
		}

		public static string GetFormTypeByNumber(int num)
		{
			string val = string.Empty;
			foreach (KeyValuePair<string, int> kvp in _formIDs)
			{
				if (kvp.Value.Equals(num))
				{
					val = kvp.Key;
					break;
				}
			}
			return val;
		}
		#endregion Form IDs
	}
}