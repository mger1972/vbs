﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace VBS
{
	public class Logging
	{
		/// <summary>
		/// Log an information record to the logging table
		/// </summary>
		/// <param name="desc">The description to log</param>
		/// <param name="responseID">The response ID that you're dealing with</param>
		/// <param name="objectXML">Any XML that needs to be logged</param>
		/// <param name="userName">The user making the update</param>
		/// <param name="error">Tells if this was an error</param>
		/// <param name="information">Tells if this is informational only</param>
		/// <param name="warning">Tells if the error is a warning</param>
		/// <returns>A string.Empty if there wasn't an error.  Otherwise the error is returned.</returns>
		private static string LogItem(string desc, 
			int? responseID, 
			string objectXML, 
			string userName, 
			bool information, 
			bool warning,
			bool error)
		{
			string rtv = string.Empty;

			try
			{
				VBSLog logItem = new VBSLog();
				logItem.LogGUID = System.Guid.NewGuid().ToString();
				logItem.DateLogged = DateTime.Now;
				logItem.Description = desc;
				if (responseID.HasValue) { logItem.ResponseID = responseID; }
				if (!String.IsNullOrEmpty(objectXML)) { logItem.ObjectXML = objectXML; }

				logItem.Information = true;
				logItem.Error = logItem.Warning = false;
				logItem.AddedByUserID = logItem.UpdatedByUserID = userName;

				ClassGenExceptionCollection errors = logItem.AddUpdate();
				if (errors.ViewableExceptionCount > 0)
				{
					StringBuilder sb = new StringBuilder();
					foreach (ClassGenException ex in errors)
					{
						sb.AppendLine(ex.ClassGenExceptionIconType.ToString() + ": " + ex.Message + " | " + ex.StackTrace);
					}
				}

				#region Old Code
				//SqlCommand cmd = new SqlCommand("INSERT INTO tVBSLog (sDescription, iResponseID, sObjectXML, " +
				//	"bInformation, sAddedByUserID, sUpdatedByUserID) " +
				//	"SELECT @psDesc, @piResponseID, @psObjectXML, @pbInformation, @psAddedByUserID, @psUpdatedByUserID");
				//cmd.Parameters.Add("@psDesc", SqlDbType.VarChar, 900).Value = desc;
				//cmd.Parameters.Add("@piResponseID", SqlDbType.Int).Value = (responseID.HasValue ? responseID.Value : (object)null);
				//cmd.Parameters.Add("@psObjectXML", SqlDbType.Text).Value = (objectXML == null ? (object)DBNull.Value : (object)objectXML);

				//cmd.Parameters.Add("@pbInformation", SqlDbType.Bit).Value = true;
				//cmd.Parameters.Add("@psAddedByUserID", SqlDbType.VarChar, 50).Value = userName;
				//cmd.Parameters.Add("@psUpdatedByUserID", SqlDbType.VarChar, 50).Value = userName;
				//DataTable dt = ServiceController_DAL.SQLExecDataTable(cmd);
				#endregion Old Code
			}
			catch (Exception ex)
			{
				rtv = ex.Message + " | " + ex.StackTrace;
			}

			return rtv;
		}

		/// <summary>
		/// Log an informational record to the logging table
		/// </summary>
		/// <param name="desc">The description to log</param>
		/// <param name="responseID">The response ID that you're dealing with</param>
		/// <param name="objectXML">Any XML that needs to be logged</param>
		/// <param name="userName">The user making the update</param>
		/// <returns>A string.Empty if there wasn't an error.  Otherwise the error is returned.</returns>
		public static string LogInformation(string desc, int? responseID, string objectXML, string userName)
		{
			return LogItem(desc, responseID, objectXML, userName, true, false, false);
		}

		/// <summary>
		/// Log a warning record to the logging table
		/// </summary>
		/// <param name="desc">The description to log</param>
		/// <param name="responseID">The response ID that you're dealing with</param>
		/// <param name="objectXML">Any XML that needs to be logged</param>
		/// <param name="userName">The user making the update</param>
		/// <returns>A string.Empty if there wasn't an error.  Otherwise the error is returned.</returns>
		public static string LogWarning(string desc, int? responseID, string objectXML, string userName)
		{
			return LogItem(desc, responseID, objectXML, userName, false, true, false);
		}

		/// <summary>
		/// Log an error record to the logging table
		/// </summary>
		/// <param name="desc">The description to log</param>
		/// <param name="responseID">The response ID that you're dealing with</param>
		/// <param name="objectXML">Any XML that needs to be logged</param>
		/// <param name="userName">The user making the update</param>
		/// <returns>A string.Empty if there wasn't an error.  Otherwise the error is returned.</returns>
		public static string LogError(string desc, int? responseID, string objectXML, string userName)
		{
			return LogItem(desc, responseID, objectXML, userName, false, false, true);
		}
	}
}