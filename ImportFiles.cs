﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace VBS
{
	public enum ImportType : int
	{
		Base = 0,
		AltPay,
		VolunteerUnder18,
		VolunteerAdult,
        MedicalReleaseFormCurrent,
        MedicalReleaseFormPrevYear,
	}

	public class ImportFiles
	{
		/// <summary>
		/// Import the file based on the type and the file name
		/// </summary>
		/// <param name="type">The type of the file</param>
		/// <param name="fileName">The file to import</param>
		/// <returns>An error collection with anything wrong</returns>
		public static ClassGenExceptionCollection Import(ImportType type, string fileName)
		{
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();

			int modifiedCount = 0, newCount = 0, deletedCount = 0;

			try
			{
				// Create a License object
				Aspose.Cells.License license = new Aspose.Cells.License();
				license.SetLicense("Aspose.Total.lic");

				// Kill off the first set of rows in the sheet
				//errors.Add(new ClassGenException("FileName: " + fileName));
				Aspose.Cells.Workbook wb = new Aspose.Cells.Workbook(fileName);
				//errors.Add(new ClassGenException("Sheet Count: " + wb.Worksheets.Count.ToString()));
				Aspose.Cells.Worksheet wksht = wb.Worksheets[0];		// Get the first sheet - that's all these have
				//errors.Add(new ClassGenException("Sheet Name: " + wksht.Name));
				for (int i = 0; i < 600; i++)
				{
					if (wksht.Cells[i, 0].Value != null &&
						wksht.Cells[i, 1].Value != null &&
						wksht.Cells[i, 0].Value.ToString().ToLower().Contains("submit") &&
						wksht.Cells[i, 1].Value.ToString().ToLower().Contains("modified"))
					{
						for (int j = i - 1; j >= 0; j--)
						{
							wksht.Cells.DeleteRow(j);
						}
						break;
					}
				}
				wb.Save(fileName);		// Save the worksheet

				// Go through and get the max
				int maxRow = -1;
				for (int i = 0; i <= 600; i++)
				{
					if (wksht.Cells[i, 0].Value == null &&
						wksht.Cells[i, 1].Value == null)
					{
						maxRow = i - 1;
						break;
					}
				}
				if (maxRow < 0) { return errors; }		// Dump out

				// Find the starting row
				//int startingRow = 9999;
				//for (int row = 1; row <= maxRow; row++)
				//{
				//	if (wksht.Cells[row, 0] != null &&
				//		wksht.Cells[row, 0].StringValue.ToLower().Contains("submitted"))
				//	{
				//		startingRow = row + 1;
				//		break;
				//	}
				//}

				// Crack open the file and parse it into the database
				// First off, we have to find out which ones we're going to delete (if any)
				List<string> existingResponseIDs = new List<string>();
				for (int row = 1; row <= maxRow; row++)
				{
					existingResponseIDs.Add((wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1).ToString());
				}
				VBSDataCollection collDelete = null;
				VBSClassLinkCollection collClassLinks = null;
				VBSResponseToGroupCollection collResponseToGroups = null;
				//VBSVolunteerChildCollection collVolunteerChildren = null;
				if (existingResponseIDs.Count > 0)
				{
					switch (type)
					{
						case ImportType.Base:
							collDelete = new VBSDataCollection("sSheetType = 'Base' AND iResponseID NOT IN (" + Utils.JoinString(existingResponseIDs, ", ") + ")");
							break;
						case ImportType.AltPay:
							collDelete = new VBSDataCollection("sSheetType = 'AltPay' AND iResponseID NOT IN (" + Utils.JoinString(existingResponseIDs, ", ") + ")");
							break;
						case ImportType.VolunteerAdult:
							collDelete = new VBSDataCollection("sSheetType = 'VolunteerAdult' AND iResponseID NOT IN (" + Utils.JoinString(existingResponseIDs, ", ") + ")");
							break;
						case ImportType.VolunteerUnder18:
							collDelete = new VBSDataCollection("sSheetType = 'VolunteerUnder18' AND iResponseID NOT IN (" + Utils.JoinString(existingResponseIDs, ", ") + ")");
							break;
                        case ImportType.MedicalReleaseFormCurrent:
						case ImportType.MedicalReleaseFormPrevYear:
							//collDelete = new VBSDataCollection("sSheetType = 'MedicalReleaseForm' AND iResponseID NOT IN (" + Utils.JoinString(existingResponseIDs, ", ") + ")");
							collDelete = new VBSDataCollection();
							// Commented out as we're doing the delete prior - 5/26/2017 - MG
							break;

                    }
					if (collDelete.Count > 0)
					{
						collClassLinks = new VBSClassLinkCollection("iResponseID IN (" + Utils.JoinString(collDelete.GetDistinctFromCollection(VBSDataField.ResponseID), ", ") + ")");
						collResponseToGroups = new VBSResponseToGroupCollection("iResponseID IN (" + Utils.JoinString(collDelete.GetDistinctFromCollection(VBSDataField.ResponseID), ", ") + ")");
						//collVolunteerChildren = new VBSVolunteerChildCollection("sRawGUID IN ('" + Utils.JoinString(collDelete.GetDistinctFromCollection(VBSDataField.RawGUID), ", ") + "')");

						//foreach (VBSVolunteerChild child in collVolunteerChildren) { child.Delete(); }
						foreach (VBSResponseToGroup response in collResponseToGroups) { response.Delete(); }
						foreach (VBSClassLink link in collClassLinks) { link.Delete(); }
						foreach (VBSData dataItem in collDelete) { dataItem.Delete(); deletedCount++; }
						//collVolunteerChildren.AddUpdateAll();
						collResponseToGroups.AddUpdateAll();
						collClassLinks.AddUpdateAll();
						collDelete.AddUpdateAll();
					}
				}

				// Now go through the rows and find out which ones we need to add
				for (int row = 1; row <= maxRow; row++)
				{
					try
					{
						VBSData data = new VBSData();
						int responseID = -1;
						DateTime modified = DateTime.MinValue;
                        bool inactive = false;
						VBSDataCollection coll = null;
						switch (type)
						{
							case ImportType.Base:
								#region Base
								responseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								//if (responseID == 27081) { int test = 1; }
								//modified = (wksht.Cells[row, 1] != null ? DateTime.Parse(wksht.Cells[row, 1].StringValue) : DateTime.MinValue);
								modified = (wksht.Cells[row, 1] != null && !String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue) ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								if (wksht.Cells[row, 1] != null && String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue)) { modified = DateTime.MinValue.AddMinutes(2); }
								if (responseID < 0 || modified == DateTime.MinValue) { continue; }
								coll = new VBSDataCollection("iResponseID = " + responseID.ToString());
								if (coll.Count > 0)
								{
									//if (coll[0].DateModified < modified)		// Commented out on 5/12/2018 as Date Modified and Submitted don't include time
									//{
									data = coll[0];
									modifiedCount++;
									//}
									//else
									//{
									//	// We don't have to modify anything 
									//	continue;
									//}
								}
								else
								{
									data.RawGUID = System.Guid.NewGuid().ToString();
									newCount++;
								}

								// Load up object
								data.SheetType = "Base";

								data.DateSubmitted = (wksht.Cells[row, 0] != null ? (wksht.Cells[row, 0].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 0].DateTimeValue : DateTime.Parse(wksht.Cells[row, 0].StringValue)) : DateTime.MinValue);
								if (wksht.Cells[row, 1] != null &&
									String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue))
								{
									data.DateModified = data.DateSubmitted;
								}
								else
								{
									data.DateModified = (wksht.Cells[row, 1] != null ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
										wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								}
								data.ResponseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								data.ConfirmationNum = (wksht.Cells[row, 3] != null ? int.Parse(wksht.Cells[row, 3].StringValue) : -1);
								data.FormID = Utils.FormIDs["Base"];
								data.ProfileMatch = (wksht.Cells[row, 4] != null ? wksht.Cells[row, 4].StringValue : string.Empty);

								data.IndividualID = (wksht.Cells[row, 5] != null &&
									!String.IsNullOrEmpty(wksht.Cells[row, 5].StringValue) ? int.Parse(wksht.Cells[row, 5].StringValue) : -1);
								data.FirstName = (wksht.Cells[row, 6] != null ? wksht.Cells[row, 6].StringValue : string.Empty);
								data.LastName = (wksht.Cells[row, 7] != null ? wksht.Cells[row, 7].StringValue : string.Empty);

								if (data.LastName.ToLower().StartsWith("/")) { data.LastName = data.LastName.Substring(1); } 

								data.Email = (wksht.Cells[row, 8] != null ? wksht.Cells[row, 8].StringValue : string.Empty);
								data.Street = (wksht.Cells[row, 9] != null ? wksht.Cells[row, 9].StringValue : string.Empty);

								data.City = (wksht.Cells[row, 10] != null ? wksht.Cells[row, 10].StringValue : string.Empty);
								data.State = (wksht.Cells[row, 11] != null ? wksht.Cells[row, 11].StringValue : string.Empty);
								data.Zip = (wksht.Cells[row, 12] != null ? wksht.Cells[row, 12].StringValue : string.Empty);
								//data.HomePhone = (wksht.Cells[row, 13] != null ? wksht.Cells[row, 13].StringValue : string.Empty);
								//if (data.HomePhone.Length > 0) { data.HomePhone = FixPhoneNum(data.HomePhone); }
								data.MobilePhone = (wksht.Cells[row, 13] != null ? wksht.Cells[row, 13].StringValue : string.Empty);
								if (data.MobilePhone.Length > 0) { data.MobilePhone = FixPhoneNum(data.MobilePhone); }
								data.PaymentStatus = (wksht.Cells[row, 14] != null ? wksht.Cells[row, 14].StringValue : string.Empty);

								data.PaymentType = (wksht.Cells[row, 15] != null ? wksht.Cells[row, 15].StringValue : string.Empty);
								//data.FormTotal = (wksht.Cells[row, 16] != null ? decimal.Parse(wksht.Cells[row, 16].StringValue.Replace("$", "")) : -1);

								// Since they're putting a "-" in instead of a zero, I'm combatting that
								if (wksht.Cells[row, 16] != null &&
									wksht.Cells[row, 16].StringValue.Trim() == "-")
								{
									wksht.Cells[row, 16].Value = "0";
								}
								data.FormTotal = (wksht.Cells[row, 16] != null ? decimal.Parse(wksht.Cells[row, 16].StringValue.Replace("$", "")) : -1);

								data.ParentFirst = (wksht.Cells[row, 17] != null ? wksht.Cells[row, 17].StringValue : string.Empty);
								data.ParentLast = (wksht.Cells[row, 18] != null ? wksht.Cells[row, 18].StringValue : string.Empty);
								data.GradeInFall = (wksht.Cells[row, 19] != null ? wksht.Cells[row, 19].StringValue : string.Empty);

								data.School = (wksht.Cells[row, 20] != null ? wksht.Cells[row, 20].StringValue : string.Empty);
								data.Gender = (wksht.Cells[row, 21] != null ? wksht.Cells[row, 21].StringValue : string.Empty);
								data.Gender = (data.Gender.ToLower().StartsWith("m") ? "Male" : "Female");

								if (wksht.Cells[row, 22] != null &&
									wksht.Cells[row, 22].StringValue.Length > 0)
								{
									DateTime dob = DateTime.MinValue;
									bool dobParse = DateTime.TryParse(wksht.Cells[row, 22].StringValue, out dob);
									if (dobParse)
									{
										data.DateDOBChild = dob;
									}
									else
									{
										errors.Add(new ClassGenException("&nbsp; &nbsp;Child Date of Birth can't be parsed (Conf #: " +
											data.ConfirmationNum.ToString() + "): " + wksht.Cells[row, 22].StringValue));
									}
								}
								data.ShirtSize = (wksht.Cells[row, 23] != null ? wksht.Cells[row, 23].StringValue : string.Empty);

								data.WillingToVolunteer = (wksht.Cells[row, 26] != null && wksht.Cells[row, 26].StringValue.ToLower().StartsWith("y"));
								data.DonateMoneyToScholarship = (wksht.Cells[row, 27] != null ? wksht.Cells[row, 27].StringValue : string.Empty);
								data.Friend1First = (wksht.Cells[row, 28] != null ? wksht.Cells[row, 28].StringValue : string.Empty);
								data.Friend1Last = (wksht.Cells[row, 29] != null ? wksht.Cells[row, 29].StringValue : string.Empty);
								data.Friend2First = (wksht.Cells[row, 30] != null ? wksht.Cells[row, 30].StringValue : string.Empty);
								data.Friend2Last = (wksht.Cells[row, 31] != null ? wksht.Cells[row, 31].StringValue : string.Empty);

								data.ChurchAttended = (wksht.Cells[row, 32] != null ? wksht.Cells[row, 32].StringValue : string.Empty);
								if (data.ChurchAttended.Trim().Length > 0 &&
									data.ChurchAttended.ToLower().Trim() == "other")
								{
									data.ChurchAttended = (wksht.Cells[row, 33] != null ? wksht.Cells[row, 33].StringValue : string.Empty);
								}
								data.FoodAllergies = (wksht.Cells[row, 34] != null ? wksht.Cells[row, 34].StringValue : string.Empty);
								data.SpecialNeeds = (wksht.Cells[row, 35] != null ? wksht.Cells[row, 35].StringValue : string.Empty);
								data.MedicalReleaseForm = (wksht.Cells[row, 36] != null && wksht.Cells[row, 36].StringValue.ToLower().StartsWith("y"));

								data.Comments = (wksht.Cells[row, 45] != null ? wksht.Cells[row, 45].StringValue : string.Empty);
								//data.Inactive = (wksht.Cells[row, 43] != null && !String.IsNullOrEmpty(wksht.Cells[row, 43].StringValue));
								data.Inactive = !String.IsNullOrEmpty(wksht.Cells[row, 43].StringValue);
								if (wksht.Cells[row, 27] != null &&
									wksht.Cells[row, 27].StringValue.Length > 0)
								{
									decimal minVal = Decimal.MinValue;
									string donationVal = wksht.Cells[row, 27].StringValue;
									donationVal = donationVal.Substring(0, donationVal.IndexOf(" ")).Trim().Replace("$", "");
									bool minParse = Decimal.TryParse(donationVal, out minVal);
									if (minParse)
									{
										data.DonationAmount = minVal;
									}
									else
									{
										errors.Add(new ClassGenException("&nbsp; &nbsp;Donation amount can't be parsed (Conf #: " +
											data.ConfirmationNum.ToString() + "): " + donationVal));
									}
								}

								// Get who can pick up the kids
								data.PickupPerson1 = (wksht.Cells[row, 48] != null ? wksht.Cells[row, 48].StringValue : string.Empty);
								data.PickupPerson2 = (wksht.Cells[row, 49] != null ? wksht.Cells[row, 49].StringValue : string.Empty);
								data.PickupPerson3 = (wksht.Cells[row, 50] != null ? wksht.Cells[row, 50].StringValue : string.Empty);

								//data.ParentPhone = (wksht.Cells[row, 33] != null ? wksht.Cells[row, 33].StringValue : string.Empty);
								//if (data.ParentPhone.Length > 0) { data.ParentPhone = FixPhoneNum(data.ParentPhone); }
								//data.ParentWorkPhone = (wksht.Cells[row, 34] != null ? wksht.Cells[row, 34].StringValue : string.Empty);
								//if (data.ParentWorkPhone.Length > 0) { data.ParentWorkPhone = FixPhoneNum(data.ParentWorkPhone); }

								//data.ParentAtSameAddress = (wksht.Cells[row, 36] != null && wksht.Cells[row, 36].StringValue.ToLower().StartsWith("y"));
								//data.ParentStreet = (wksht.Cells[row, 37] != null ? wksht.Cells[row, 37].StringValue : string.Empty);
								//data.ParentCity = (wksht.Cells[row, 38] != null ? wksht.Cells[row, 38].StringValue : string.Empty);
								//data.ParentState = (wksht.Cells[row, 39] != null ? wksht.Cells[row, 39].StringValue : string.Empty);

								//data.ParentZip = (wksht.Cells[row, 40] != null ? wksht.Cells[row, 40].StringValue : string.Empty);

								// Save the object
								errors.AddRange(data.AddUpdate());

								#endregion Base
								break;
							case ImportType.AltPay:
								#region AltPay
								responseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								//modified = (wksht.Cells[row, 1] != null ? DateTime.Parse(wksht.Cells[row, 1].StringValue) : DateTime.MinValue);
								modified = (wksht.Cells[row, 1] != null && !String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue) ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								if (wksht.Cells[row, 1] != null && String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue)) { modified = DateTime.MinValue.AddMinutes(2); }
								if (responseID < 0 || modified == DateTime.MinValue) { continue; }
								coll = new VBSDataCollection("iResponseID = " + responseID.ToString());
								if (coll.Count > 0)
								{
									//if (coll[0].DateModified < modified)        // Commented out on 5/12/2018 as Date Modified and Submitted don't include time
									//{
										data = coll[0];
										modifiedCount++;
									//}
									//else
									//{
									//	// We don't have to modify anything 
									//	continue;
									//}
								}
								else
								{
									data.RawGUID = System.Guid.NewGuid().ToString();
									newCount++;
								}

								// Load up object
								data.SheetType = "AltPay";

								data.DateSubmitted = (wksht.Cells[row, 0] != null ? (wksht.Cells[row, 0].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 0].DateTimeValue : DateTime.Parse(wksht.Cells[row, 0].StringValue)) : DateTime.MinValue);
								if (wksht.Cells[row, 1] != null &&
									String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue))
								{
									data.DateModified = data.DateSubmitted;
								}
								else
								{
									data.DateModified = (wksht.Cells[row, 1] != null ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
										wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								}
								data.ResponseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								data.ConfirmationNum = (wksht.Cells[row, 3] != null ? int.Parse(wksht.Cells[row, 3].StringValue) : -1);
								data.FormID = Utils.FormIDs["AltPay"];
								data.ProfileMatch = (wksht.Cells[row, 4] != null ? wksht.Cells[row, 4].StringValue : string.Empty);

								data.IndividualID = (wksht.Cells[row, 5] != null &&
									!String.IsNullOrEmpty(wksht.Cells[row, 5].StringValue) ? int.Parse(wksht.Cells[row, 5].StringValue) : -1);
								data.FirstName = (wksht.Cells[row, 6] != null ? wksht.Cells[row, 6].StringValue : string.Empty);
								data.LastName = (wksht.Cells[row, 7] != null ? wksht.Cells[row, 7].StringValue : string.Empty);
								data.Email = (wksht.Cells[row, 8] != null ? wksht.Cells[row, 8].StringValue : string.Empty);
								data.Street = (wksht.Cells[row, 9] != null ? wksht.Cells[row, 9].StringValue : string.Empty);

								data.City = (wksht.Cells[row, 10] != null ? wksht.Cells[row, 10].StringValue : string.Empty);
								data.State = (wksht.Cells[row, 11] != null ? wksht.Cells[row, 11].StringValue : string.Empty);
								data.Zip = (wksht.Cells[row, 12] != null ? wksht.Cells[row, 12].StringValue : string.Empty);
								//data.HomePhone = (wksht.Cells[row, 13] != null ? wksht.Cells[row, 13].StringValue : string.Empty);
								//if (data.HomePhone.Length > 0) { data.HomePhone = FixPhoneNum(data.HomePhone); }
								data.MobilePhone = (wksht.Cells[row, 13] != null ? wksht.Cells[row, 13].StringValue : string.Empty);
								if (data.MobilePhone.Length > 0) { data.MobilePhone = FixPhoneNum(data.MobilePhone); }

								data.PaymentStatus = (wksht.Cells[row, 14] != null ? wksht.Cells[row, 14].StringValue : string.Empty);
								data.PaymentType = (wksht.Cells[row, 15] != null ? wksht.Cells[row, 15].StringValue : string.Empty);
								//data.FormTotal = (wksht.Cells[row, 16] != null ? decimal.Parse(wksht.Cells[row, 16].StringValue.Replace("$", "")) : -1);

								// Since they're putting a "-" in instead of a zero, I'm combatting that
								if (wksht.Cells[row, 16] != null &&
									wksht.Cells[row, 16].StringValue.Trim() == "-")
								{
									wksht.Cells[row, 16].Value = "0";
								}
								data.FormTotal = (wksht.Cells[row, 16] != null ? decimal.Parse(wksht.Cells[row, 16].StringValue.Replace("$", "")) : -1);

								data.ParentFirst = (wksht.Cells[row, 17] != null ? wksht.Cells[row, 17].StringValue : string.Empty);
								data.ParentLast = (wksht.Cells[row, 18] != null ? wksht.Cells[row, 18].StringValue : string.Empty);

								data.GradeInFall = (wksht.Cells[row, 19] != null ? wksht.Cells[row, 19].StringValue : string.Empty);
								data.School = (wksht.Cells[row, 20] != null ? wksht.Cells[row, 20].StringValue : string.Empty);
								data.Gender = (wksht.Cells[row, 21] != null ? wksht.Cells[row, 21].StringValue : string.Empty);
								data.Gender = (data.Gender.ToLower().StartsWith("m") ? "Male" : "Female");

								if (wksht.Cells[row, 22] != null &&
									wksht.Cells[row, 22].StringValue.Length > 0)
								{
									DateTime dob = DateTime.MinValue;
									bool dobParse = DateTime.TryParse(wksht.Cells[row, 22].StringValue, out dob);
									if (dobParse)
									{
										data.DateDOBChild = dob;
									}
									else
									{
										errors.Add(new ClassGenException("&nbsp; &nbsp;Child Date of Birth can't be parsed (Conf #: " +
											data.ConfirmationNum.ToString() + "): " + wksht.Cells[row, 22].StringValue));
									}
								}
								data.ShirtSize = (wksht.Cells[row, 23] != null ? wksht.Cells[row, 23].StringValue : string.Empty);

								data.DonateMoneyToScholarship = (wksht.Cells[row, 27] != null ? wksht.Cells[row, 27].StringValue : string.Empty);
								data.Friend1First = (wksht.Cells[row, 28] != null ? wksht.Cells[row, 28].StringValue : string.Empty);
								data.Friend1Last = (wksht.Cells[row, 29] != null ? wksht.Cells[row, 29].StringValue : string.Empty);
								data.Friend2First = (wksht.Cells[row, 30] != null ? wksht.Cells[row, 30].StringValue : string.Empty);
								data.Friend2Last = (wksht.Cells[row, 31] != null ? wksht.Cells[row, 31].StringValue : string.Empty);

								data.ChurchAttended = (wksht.Cells[row, 32] != null ? wksht.Cells[row, 32].StringValue : string.Empty);
								if (data.ChurchAttended.Trim().Length > 0 &&
									data.ChurchAttended.ToLower().Trim() == "other")
								{
									data.ChurchAttended = (wksht.Cells[row, 33] != null ? wksht.Cells[row, 33].StringValue : string.Empty);
								}
								data.FoodAllergies = (wksht.Cells[row, 34] != null ? wksht.Cells[row, 34].StringValue : string.Empty);
								data.SpecialNeeds = (wksht.Cells[row, 35] != null ? wksht.Cells[row, 35].StringValue : string.Empty);
								//data.WillingToVolunteer = (wksht.Cells[row, 36] != null && wksht.Cells[row, 36].StringValue.ToLower().StartsWith("y"));
								data.MedicalReleaseForm = (wksht.Cells[row, 36] != null && wksht.Cells[row, 36].StringValue.ToLower().StartsWith("y"));

								data.Comments = (wksht.Cells[row, 45] != null ? wksht.Cells[row, 45].StringValue : string.Empty);
								data.Inactive = !String.IsNullOrEmpty(wksht.Cells[row, 43].StringValue);
								if (wksht.Cells[row, 27] != null &&
									wksht.Cells[row, 27].StringValue.Length > 0)
								{
									decimal minVal = Decimal.MinValue;
									string donationVal = wksht.Cells[row, 27].StringValue;
									donationVal = donationVal.Substring(0, donationVal.IndexOf(" ")).Trim().Replace("$", "");
									bool minParse = Decimal.TryParse(donationVal, out minVal);
									if (minParse)
									{
										data.DonationAmount = minVal;
									}
									else
									{
										errors.Add(new ClassGenException("&nbsp; &nbsp;Donation amount can't be parsed (Conf #: " +
											data.ConfirmationNum.ToString() + "): " + donationVal));
									}
								}

								// Get who can pick up the kids
								data.PickupPerson1 = (wksht.Cells[row, 48] != null ? wksht.Cells[row, 48].StringValue : string.Empty);
								data.PickupPerson2 = (wksht.Cells[row, 49] != null ? wksht.Cells[row, 49].StringValue : string.Empty);
								data.PickupPerson3 = (wksht.Cells[row, 50] != null ? wksht.Cells[row, 50].StringValue : string.Empty);

								// Save the object
								errors.AddRange(data.AddUpdate());

								#endregion AltPay
								break;
							case ImportType.VolunteerUnder18:
								#region Volunteer Under 18
								responseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								//modified = (wksht.Cells[row, 1] != null ? DateTime.Parse(wksht.Cells[row, 1].StringValue) : DateTime.MinValue);
								modified = (wksht.Cells[row, 1] != null && !String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue) ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								if (wksht.Cells[row, 1] != null && String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue)) { modified = DateTime.MinValue.AddMinutes(2); }
								if (responseID < 0 || modified == DateTime.MinValue) { continue; }
								coll = new VBSDataCollection("iResponseID = " + responseID.ToString());
								if (coll.Count > 0)
								{
									//if (coll[0].DateModified < modified)        // Commented out on 5/12/2018 as Date Modified and Submitted don't include time
									//{
										data = coll[0];
										modifiedCount++;
									//}
									//else
									//{
									//	// We don't have to modify anything 
									//	continue;
									//}
								}
								else
								{
									data.RawGUID = System.Guid.NewGuid().ToString();
									newCount++;
								}

								// Load up object
								data.SheetType = "VolunteerUnder18";

								data.DateSubmitted = (wksht.Cells[row, 0] != null ? (wksht.Cells[row, 0].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 0].DateTimeValue : DateTime.Parse(wksht.Cells[row, 0].StringValue)) : DateTime.MinValue);
								if (wksht.Cells[row, 1] != null &&
									String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue))
								{
									data.DateModified = data.DateSubmitted;
								}
								else
								{
									data.DateModified = (wksht.Cells[row, 1] != null ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
										wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								}
								data.ResponseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								data.ConfirmationNum = (wksht.Cells[row, 3] != null ? int.Parse(wksht.Cells[row, 3].StringValue) : -1);
								data.FormID = Utils.FormIDs["VolunteerUnder18"];
								data.ProfileMatch = (wksht.Cells[row, 4] != null ? wksht.Cells[row, 4].StringValue : string.Empty);

								data.IndividualID = (wksht.Cells[row, 5] != null &&
									!String.IsNullOrEmpty(wksht.Cells[row, 5].StringValue) ? int.Parse(wksht.Cells[row, 5].StringValue) : -1);
								data.FirstName = (wksht.Cells[row, 6] != null ? wksht.Cells[row, 6].StringValue : string.Empty);
								data.LastName = (wksht.Cells[row, 7] != null ? wksht.Cells[row, 7].StringValue : string.Empty);
								data.Email = (wksht.Cells[row, 14] != null ? wksht.Cells[row, 14].StringValue : string.Empty);
								//data.HomePhone = (wksht.Cells[row, 9] != null ? wksht.Cells[row, 9].StringValue : string.Empty);
								//if (data.HomePhone.Length > 0) { data.HomePhone = FixPhoneNum(data.HomePhone); }
								data.MobilePhone = (wksht.Cells[row, 9] != null ? wksht.Cells[row, 9].StringValue : string.Empty);
								if (data.MobilePhone.Length > 0) { data.MobilePhone = FixPhoneNum(data.MobilePhone); }

								data.Gender = (wksht.Cells[row, 10] != null ? wksht.Cells[row, 10].StringValue : string.Empty);
								data.Gender = (data.Gender.ToLower().StartsWith("m") ? "Male" : "Female");
								data.RegVoyagersAttendee = (wksht.Cells[row, 11] != null && wksht.Cells[row, 11].StringValue.ToLower().Contains("y"));
								data.JrHelperGrade = (wksht.Cells[row, 12] != null ? wksht.Cells[row, 12].StringValue : string.Empty);
								data.ParentLast = (wksht.Cells[row, 13] != null ? wksht.Cells[row, 13].StringValue : string.Empty);

								data.JrHelperEmail = (wksht.Cells[row, 8] != null ? wksht.Cells[row, 8].StringValue : string.Empty);
								data.ContactByEmail = (wksht.Cells[row, 15] != null && wksht.Cells[row, 15].StringValue.ToLower().Contains("mail"));
								data.ContactByPhone = (wksht.Cells[row, 15] != null && wksht.Cells[row, 15].StringValue.ToLower().Contains("phone"));

								data.HelpInAreas = (wksht.Cells[row, 16] != null ? wksht.Cells[row, 16].StringValue : string.Empty);
								//data.GradeToHelpIn = (wksht.Cells[row, 18] != null ? wksht.Cells[row, 18].StringValue : string.Empty);
								string grade5Below = (wksht.Cells[row, 12] != null ? wksht.Cells[row, 12].StringValue : string.Empty);
								if (!String.IsNullOrEmpty(grade5Below.Trim()))
								{
									grade5Below = grade5Below.Substring(0, grade5Below.ToLower().IndexOf("th"));
									data.Grade5Below = int.Parse(grade5Below) - 5;
								}
								data.HelpDOW = (wksht.Cells[row, 18] != null ? wksht.Cells[row, 18].StringValue : string.Empty);
								data.ShirtSize = (wksht.Cells[row, 19] != null ? wksht.Cells[row, 19].StringValue : string.Empty);

								//data.Inactive = (wksht.Cells[row, 44] != null && !String.IsNullOrEmpty(wksht.Cells[row, 44].ToString()));

								//data.Street = (wksht.Cells[row, 9] != null ? wksht.Cells[row, 9].StringValue : string.Empty);

								//data.City = (wksht.Cells[row, 10] != null ? wksht.Cells[row, 10].StringValue : string.Empty);
								//data.State = (wksht.Cells[row, 11] != null ? wksht.Cells[row, 11].StringValue : string.Empty);
								//data.Zip = (wksht.Cells[row, 12] != null ? wksht.Cells[row, 12].StringValue : string.Empty);
								//data.ContactPhone = (wksht.Cells[row, 13] != null ? wksht.Cells[row, 13].StringValue : string.Empty);
								//if (data.ContactPhone.Length > 0) { data.ContactPhone = FixPhoneNum(data.ContactPhone); }
								//data.Gender = (wksht.Cells[row, 14] != null ? wksht.Cells[row, 14].StringValue : string.Empty);
								//data.Gender = (data.Gender.ToLower().StartsWith("m") ? "Male" : "Female");

								//data.ContactByEmail = (wksht.Cells[row, 15] != null && wksht.Cells[row, 15].StringValue.ToLower().Contains("mail"));
								//data.ContactByPhone = (wksht.Cells[row, 15] != null && wksht.Cells[row, 15].StringValue.ToLower().Contains("phone"));
								//data.CurrentBackgroundCheck = (wksht.Cells[row, 16] != null ? wksht.Cells[row, 16].StringValue : string.Empty);
								//data.HelpAdultName = (wksht.Cells[row, 22] != null ? wksht.Cells[row, 22].StringValue : string.Empty);
								//data.RequestingChild = (wksht.Cells[row, 23] != null ? wksht.Cells[row, 23].StringValue : string.Empty);
								//data.NeedChildCare = (wksht.Cells[row, 26] != null && wksht.Cells[row, 26].StringValue.ToLower().StartsWith("y"));

								//data.SpecialNeeds = (wksht.Cells[row, 41] != null ? wksht.Cells[row, 41].StringValue : string.Empty);

								//// Do the children of the volunteers
								//VBSVolunteerChildCollection childColl = new VBSVolunteerChildCollection("iResponseID = " + data.ResponseID);
								//foreach (VBSVolunteerChild child in childColl)
								//{
								//	child.Delete();		// Delete the row
								//}
								//for (int index = 27; index < 40; index += 5)
								//{
								//	if (wksht.Cells[row, index] != null &&
								//		wksht.Cells[row, index + 1] != null &&
								//		wksht.Cells[row, index].StringValue.Length > 0 &&
								//		wksht.Cells[row, index + 1].StringValue.Length > 0)
								//	{
								//		VBSVolunteerChild child = new VBSVolunteerChild();
								//		child.DetailGUID = System.Guid.NewGuid().ToString();
								//		child.RawGUID = data.RawGUID;
								//		if (index == 27) { child.ChildIndex = 1; }
								//		else if (index == 32) { child.ChildIndex = 2; }
								//		else if (index == 37) { child.ChildIndex = 3; }

								//		child.ChildFirstName = (wksht.Cells[row, index + 0] != null ? wksht.Cells[row, index + 0].StringValue : string.Empty);
								//		child.ChildLastName = (wksht.Cells[row, index + 1] != null ? wksht.Cells[row, index + 1].StringValue : string.Empty);
								//		child.AgeOfChild = (wksht.Cells[row, index + 2] != null ? wksht.Cells[row, index + 2].StringValue : string.Empty);
								//		child.PottyTrained = (wksht.Cells[row, index + 3] != null && wksht.Cells[row, index + 3].StringValue.ToLower().StartsWith("y"));
								//		child.Allergies = (wksht.Cells[row, index + 4] != null ? wksht.Cells[row, index + 4].StringValue : string.Empty);
								//		child.ResponseID = data.ResponseID;
								//		childColl.Add(child);		// Add the element to the collection
								//	}
								//}
								//errors.AddRange(childColl.AddUpdateAll());
								//for (int j = errors.Count - 1; j >= 0; j--)
								//{
								//	if (errors[j].ClassGenExceptionIconType == ClassGenExceptionIconType.Information ||
								//		errors[j].ClassGenExceptionIconType == ClassGenExceptionIconType.Warning ||
								//		errors[j].ClassGenExceptionIconType == ClassGenExceptionIconType.System)
								//	{
								//		errors.RemoveAt(j);		// Remove the error
								//	}
								//}

								// Save the object
								errors.AddRange(data.AddUpdate());

								#endregion Volunteer Under 18
								break;
							case ImportType.VolunteerAdult:
								#region Volunteer Adult
								responseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								//if (responseID == 23923) { int t = 1; }
								//modified = (wksht.Cells[row, 1] != null ? DateTime.Parse(wksht.Cells[row, 1].StringValue) : DateTime.MinValue);
								modified = (wksht.Cells[row, 1] != null && !String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue) ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								if (wksht.Cells[row, 1] != null && String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue)) { modified = DateTime.MinValue.AddMinutes(2); }
								if (responseID < 0 || modified == DateTime.MinValue) { continue; }
								coll = new VBSDataCollection("iResponseID = " + responseID.ToString());
								if (coll.Count > 0)
								{
									//if (coll[0].DateModified < modified)        // Commented out on 5/12/2018 as Date Modified and Submitted don't include time
									//{
										data = coll[0];
										modifiedCount++;
									//}
									//else
									//{
									//	// We don't have to modify anything 
									//	continue;
									//}
								}
								else
								{
									data.RawGUID = System.Guid.NewGuid().ToString();
									newCount++;
								}

								// Load up object
								data.SheetType = "VolunteerAdult";

								data.DateSubmitted = (wksht.Cells[row, 0] != null ? (wksht.Cells[row, 0].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 0].DateTimeValue : DateTime.Parse(wksht.Cells[row, 0].StringValue)) : DateTime.MinValue);
								if (wksht.Cells[row, 1] != null &&
									String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue))
								{
									data.DateModified = data.DateSubmitted;
								}
								else
								{
									data.DateModified = (wksht.Cells[row, 1] != null ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
										wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								}
								data.ResponseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								data.ConfirmationNum = (wksht.Cells[row, 3] != null ? int.Parse(wksht.Cells[row, 3].StringValue) : -1);
								data.FormID = Utils.FormIDs["VolunteerAdult"];
								data.ProfileMatch = (wksht.Cells[row, 4] != null ? wksht.Cells[row, 4].StringValue : string.Empty);

								data.IndividualID = (wksht.Cells[row, 5] != null &&
									!String.IsNullOrEmpty(wksht.Cells[row, 5].StringValue) ? int.Parse(wksht.Cells[row, 5].StringValue) : -1);
								data.FirstName = (wksht.Cells[row, 6] != null ? wksht.Cells[row, 6].StringValue : string.Empty);
								data.LastName = (wksht.Cells[row, 7] != null ? wksht.Cells[row, 7].StringValue : string.Empty);
								data.Email = (wksht.Cells[row, 8] != null ? wksht.Cells[row, 8].StringValue : string.Empty);
								data.HomePhone = (wksht.Cells[row, 9] != null ? wksht.Cells[row, 9].StringValue : string.Empty);
								if (data.HomePhone.Length > 0) { data.HomePhone = FixPhoneNum(data.HomePhone); }
								data.MobilePhone = (wksht.Cells[row, 10] != null ? wksht.Cells[row, 10].StringValue : string.Empty);
								if (data.MobilePhone.Length > 0) { data.MobilePhone = FixPhoneNum(data.MobilePhone); }

								data.Gender = (wksht.Cells[row, 11] != null ? wksht.Cells[row, 11].StringValue : string.Empty);
								data.Gender = (data.Gender.ToLower().StartsWith("m") ? "Male" : "Female");
								data.ContactByEmail = (wksht.Cells[row, 12] != null && wksht.Cells[row, 12].StringValue.ToLower().Contains("mail"));
								data.ContactByPhone = (wksht.Cells[row, 12] != null && wksht.Cells[row, 12].StringValue.ToLower().Contains("phone"));
								data.RegVoyagersAttendee = (wksht.Cells[row, 13] != null && wksht.Cells[row, 13].StringValue.ToLower().Contains("y"));
								data.CurrentBackgroundCheck = (wksht.Cells[row, 15] != null ? wksht.Cells[row, 15].StringValue : string.Empty);

								data.HelpInAreas = (wksht.Cells[row, 16] != null ? wksht.Cells[row, 16].StringValue : string.Empty);
								data.HelpDOW = (wksht.Cells[row, 17] != null ? wksht.Cells[row, 17].StringValue : string.Empty);
								data.ShirtSize = (wksht.Cells[row, 18] != null ? wksht.Cells[row, 18].StringValue : string.Empty);
								data.GradeToHelpIn = (wksht.Cells[row, 19] != null ? wksht.Cells[row, 19].StringValue : string.Empty);
								data.HelpAdultName = (wksht.Cells[row, 20] != null ? wksht.Cells[row, 20].StringValue : string.Empty);

								data.RequestingChild = (wksht.Cells[row, 21] != null ? wksht.Cells[row, 21].StringValue : string.Empty);
								data.NeedChildCare = (wksht.Cells[row, 22] != null && wksht.Cells[row, 22].StringValue.ToLower().StartsWith("y"));


								//data.Inactive = (wksht.Cells[row, 44] != null && !String.IsNullOrEmpty(wksht.Cells[row, 44].ToString()));


								//data.Street = (wksht.Cells[row, 9] != null ? wksht.Cells[row, 9].StringValue : string.Empty);

								//data.City = (wksht.Cells[row, 10] != null ? wksht.Cells[row, 10].StringValue : string.Empty);
								//data.State = (wksht.Cells[row, 11] != null ? wksht.Cells[row, 11].StringValue : string.Empty);
								//data.Zip = (wksht.Cells[row, 12] != null ? wksht.Cells[row, 12].StringValue : string.Empty);
								//data.ContactPhone = (wksht.Cells[row, 13] != null ? wksht.Cells[row, 13].StringValue : string.Empty);
								//if (data.ContactPhone.Length > 0) { data.ContactPhone = FixPhoneNum(data.ContactPhone); }
								//data.Gender = (wksht.Cells[row, 14] != null ? wksht.Cells[row, 14].StringValue : string.Empty);
								//data.Gender = (data.Gender.ToLower().StartsWith("m") ? "Male" : "Female");

								//data.ContactByEmail = (wksht.Cells[row, 15] != null && wksht.Cells[row, 15].StringValue.ToLower().Contains("mail"));
								//data.ContactByPhone = (wksht.Cells[row, 15] != null && wksht.Cells[row, 15].StringValue.ToLower().Contains("phone"));
								//data.HelpInAreas = (wksht.Cells[row, 18] != null ? wksht.Cells[row, 18].StringValue : string.Empty);
								//data.HelpDOW = (wksht.Cells[row, 19] != null ? wksht.Cells[row, 19].StringValue : string.Empty);

								//data.GradeToHelpIn = (wksht.Cells[row, 21] != null ? wksht.Cells[row, 21].StringValue : string.Empty);
								//data.JrHelperGrade = (wksht.Cells[row, 24] != null ? wksht.Cells[row, 24].StringValue : string.Empty);

								//data.JrHelperEmail = (wksht.Cells[row, 25] != null ? wksht.Cells[row, 25].StringValue : string.Empty);


								//data.SpecialNeeds = (wksht.Cells[row, 41] != null ? wksht.Cells[row, 41].StringValue : string.Empty);

								errors.AddRange(data.AddUpdate());

								// Do the children of the volunteers
								//VBSVolunteerChildCollection childColl = new VBSVolunteerChildCollection("iResponseID = " + data.ResponseID);
								VBSVolunteerChildCollection childColl = null;
								//foreach (VBSVolunteerChild child in childColl)
								//{
								//	child.Delete();     // Delete the row
								//}
								for (int index = 23; index < 40; index += 6)
								{
									if (wksht.Cells[row, index] != null &&
										wksht.Cells[row, index + 1] != null &&
										wksht.Cells[row, index].StringValue.Length > 0 &&
										wksht.Cells[row, index + 1].StringValue.Length > 0)
									{
										int childIndex = 0;
										if (index == 23) { childIndex = 1; }
										else if (index == 29) { childIndex = 2; }
										else if (index == 35) { childIndex = 3; }

										//if (data.ResponseID == 23348 && childIndex == 1) { int test = 1; }

										childColl = new VBSVolunteerChildCollection("iResponseID = " + data.ResponseID + 
											" AND iChildIndex = " + childIndex);
										VBSVolunteerChild child = new VBSVolunteerChild();
										if (childColl.Count > 0)
										{
											child = childColl[0];
											child.DateUpdated = DateTime.Now;
										}
										else
										{
											child.DetailGUID = System.Guid.NewGuid().ToString();
										}
										
										child.RawGUID = data.RawGUID;

										// Add the index
										child.ChildIndex = childIndex;
										
										child.ChildFirstName = (wksht.Cells[row, index + 0] != null ? wksht.Cells[row, index + 0].StringValue : string.Empty);
										child.ChildLastName = (wksht.Cells[row, index + 1] != null ? wksht.Cells[row, index + 1].StringValue : string.Empty);
										child.AgeOfChild = (wksht.Cells[row, index + 2] != null ? wksht.Cells[row, index + 2].StringValue : string.Empty);
										child.PottyTrained = (wksht.Cells[row, index + 3] != null && wksht.Cells[row, index + 3].StringValue.ToLower().StartsWith("y"));
										child.ShirtSize = (wksht.Cells[row, index + 4] != null ? wksht.Cells[row, index + 4].StringValue : string.Empty);
										child.Allergies = (wksht.Cells[row, index + 5] != null ? wksht.Cells[row, index + 5].StringValue : string.Empty);
										child.ResponseID = data.ResponseID;

										if (childColl.Count == 0)
										{
											childColl.Add(child);       // Add the element to the collection
										}

										if (childColl != null &&
											childColl.Count > 0)
										{
											errors.AddRange(childColl.AddUpdateAll());
										}
									}
								}

								for (int j = errors.Count - 1; j >= 0; j--)
								{
									if (errors[j].ClassGenExceptionIconType == ClassGenExceptionIconType.Information ||
										errors[j].ClassGenExceptionIconType == ClassGenExceptionIconType.Warning ||
										errors[j].ClassGenExceptionIconType == ClassGenExceptionIconType.System)
									{
										errors.RemoveAt(j);     // Remove the error
									}
								}

								// Save the object
								//errors.AddRange(data.AddUpdate());

								#endregion Volunteer Adult
								break;
							case ImportType.MedicalReleaseFormCurrent:
							case ImportType.MedicalReleaseFormPrevYear:
								#region MedicalReleaseForm
								responseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								//modified = (wksht.Cells[row, 1] != null ? DateTime.Parse(wksht.Cells[row, 1].StringValue) : DateTime.MinValue);
								modified = (wksht.Cells[row, 1] != null && !String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue) ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								if (wksht.Cells[row, 1] != null && String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue)) { modified = DateTime.MinValue.AddMinutes(2); }
								if (responseID < 0 || modified == DateTime.MinValue) { continue; }
								coll = new VBSDataCollection("iResponseID = " + responseID.ToString());
								if (coll.Count > 0)
								{
									//if (coll[0].DateModified < modified)        // Commented out on 5/12/2018 as Date Modified and Submitted don't include time
									//{
										data = coll[0];
										modifiedCount++;
									//}
									//else
									//{
									//	// We don't have to modify anything 
									//	continue;
									//}
								}
								else
								{
									data.RawGUID = System.Guid.NewGuid().ToString();
									newCount++;
								}

								// Load up object
								data.SheetType = "MedicalReleaseForm";

								data.DateSubmitted = (wksht.Cells[row, 0] != null ? (wksht.Cells[row, 0].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 0].DateTimeValue : DateTime.Parse(wksht.Cells[row, 0].StringValue)) : DateTime.MinValue);

								// If the date submitted is before 7/1/2016 then continue
								if (data.DateSubmitted < DateTime.Parse("7/1/2016")) { newCount--; continue; }

								if (wksht.Cells[row, 1] != null &&
									String.IsNullOrEmpty(wksht.Cells[row, 1].StringValue))
								{
									data.DateModified = data.DateSubmitted;
								}
								else
								{
									data.DateModified = (wksht.Cells[row, 1] != null ? (wksht.Cells[row, 1].Type == Aspose.Cells.CellValueType.IsDateTime ?
										wksht.Cells[row, 1].DateTimeValue : DateTime.Parse(wksht.Cells[row, 1].StringValue)) : DateTime.MinValue);
								}
								data.ResponseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
								data.ConfirmationNum = (wksht.Cells[row, 3] != null ? int.Parse(wksht.Cells[row, 3].StringValue) : -1);

								if (type == ImportType.MedicalReleaseFormCurrent) { data.FormID = Utils.FormIDs["MedicalReleaseFormCurrent"]; }
								else if (type == ImportType.MedicalReleaseFormPrevYear) { data.FormID = Utils.FormIDs["MedicalReleaseFormPrevYear"]; }

								data.ProfileMatch = (wksht.Cells[row, 4] != null ? wksht.Cells[row, 4].StringValue : string.Empty);

								data.IndividualID = (wksht.Cells[row, 5] != null &&
									!String.IsNullOrEmpty(wksht.Cells[row, 5].StringValue) ? int.Parse(wksht.Cells[row, 5].StringValue) : -1);
								data.FirstName = (wksht.Cells[row, 6] != null ? wksht.Cells[row, 6].StringValue : string.Empty);
								data.LastName = (wksht.Cells[row, 7] != null ? wksht.Cells[row, 7].StringValue : string.Empty);
								data.Street = (wksht.Cells[row, 8] != null ? wksht.Cells[row, 8].StringValue : string.Empty);
								data.City = (wksht.Cells[row, 9] != null ? wksht.Cells[row, 9].StringValue : string.Empty);

								//Console.WriteLine(data.LastName + ", " + data.FirstName);
								//sbTest.AppendLine(data.LastName + ", " + data.FirstName);
								//if (data.LastName.ToLower().Equals("Boomsma".ToLower()) &&
								//	data.FirstName.ToLower().Equals("Andrew".ToLower()))
								//{
								//	int test1 = 0;
								//}

								data.State = (wksht.Cells[row, 10] != null ? wksht.Cells[row, 10].StringValue : string.Empty);
								data.Zip = (wksht.Cells[row, 11] != null ? wksht.Cells[row, 11].StringValue : string.Empty);
								data.HomePhone = (wksht.Cells[row, 12] != null ? wksht.Cells[row, 12].StringValue : string.Empty);
								if (data.HomePhone.Length > 0) { data.HomePhone = FixPhoneNum(data.HomePhone); }
								if (wksht.Cells[row, 13] != null &&
								   wksht.Cells[row, 13].StringValue.Length > 0)
								{
									DateTime dob = DateTime.MinValue;
									bool dobParse = DateTime.TryParse(wksht.Cells[row, 13].StringValue, out dob);
									if (dobParse)
									{
										data.DateDOBChild = dob;
									}
									else
									{
										errors.Add(new ClassGenException("&nbsp; &nbsp;Child Date of Birth can't be parsed (Conf #: " +
											data.ConfirmationNum.ToString() + "): " + wksht.Cells[row, 13].StringValue));
									}
								}
								data.Gender = (wksht.Cells[row, 14] != null ? wksht.Cells[row, 14].StringValue : string.Empty);
								data.Gender = (data.Gender.ToLower().StartsWith("m") ? "Male" : "Female");

								data.RegVoyagersAttendee = (wksht.Cells[row, 15] != null && wksht.Cells[row, 15].StringValue.ToLower().StartsWith("y"));
								data.GradeInFall = (wksht.Cells[row, 16] != null ? wksht.Cells[row, 16].StringValue : string.Empty);
								data.School = (wksht.Cells[row, 17] != null ? wksht.Cells[row, 17].StringValue : string.Empty);
								data.ParentName1 = (wksht.Cells[row, 18] != null ? wksht.Cells[row, 18].StringValue : string.Empty);
								data.ParentHome1 = (wksht.Cells[row, 19] != null ? wksht.Cells[row, 19].StringValue : string.Empty);
								if (data.ParentHome1.Length > 0) { data.ParentHome1 = FixPhoneNum(data.ParentHome1); }

								data.ParentCell1 = (wksht.Cells[row, 20] != null ? wksht.Cells[row, 20].StringValue : string.Empty);
								if (data.ParentCell1.Length > 0) { data.ParentCell1 = FixPhoneNum(data.ParentCell1); }
								data.ParentWork1 = (wksht.Cells[row, 21] != null ? wksht.Cells[row, 21].StringValue : string.Empty);
								if (data.ParentWork1.Length > 0) { data.ParentWork1 = FixPhoneNum(data.ParentWork1); }
								data.ParentEMail1 = (wksht.Cells[row, 22] != null ? wksht.Cells[row, 22].StringValue : string.Empty);
								data.ParentName2 = (wksht.Cells[row, 23] != null ? wksht.Cells[row, 23].StringValue : string.Empty);
								data.ParentHome2 = (wksht.Cells[row, 24] != null ? wksht.Cells[row, 24].StringValue : string.Empty);
								if (data.ParentHome2.Length > 0) { data.ParentHome2 = FixPhoneNum(data.ParentHome2); }

								data.ParentCell2 = (wksht.Cells[row, 25] != null ? wksht.Cells[row, 25].StringValue : string.Empty);
								if (data.ParentCell2.Length > 0) { data.ParentCell2 = FixPhoneNum(data.ParentCell2); }
								data.ParentWork2 = (wksht.Cells[row, 26] != null ? wksht.Cells[row, 26].StringValue : string.Empty);
								if (data.ParentWork2.Length > 0) { data.ParentWork2 = FixPhoneNum(data.ParentWork2); }
								data.ParentEMail2 = (wksht.Cells[row, 27] != null ? wksht.Cells[row, 27].StringValue : string.Empty);
								data.EmerContactName1 = (wksht.Cells[row, 28] != null ? wksht.Cells[row, 28].StringValue : string.Empty);
								data.EmerContactPhone1 = (wksht.Cells[row, 29] != null ? wksht.Cells[row, 29].StringValue : string.Empty);
								if (data.EmerContactPhone1.Length > 0) { data.EmerContactPhone1 = FixPhoneNum(data.EmerContactPhone1); }

								data.EmerContactName2 = (wksht.Cells[row, 30] != null ? wksht.Cells[row, 30].StringValue : string.Empty);
								data.EmerContactPhone2 = (wksht.Cells[row, 31] != null ? wksht.Cells[row, 31].StringValue : string.Empty);
								if (data.EmerContactPhone2.Length > 0) { data.EmerContactPhone2 = FixPhoneNum(data.EmerContactPhone2); }
								data.PhotoRelease = (wksht.Cells[row, 32] != null && wksht.Cells[row, 32].StringValue.ToLower().StartsWith("y"));
								data.MedicalHistory = (wksht.Cells[row, 33] != null ? (wksht.Cells[row, 33].StringValue.Length > 500 ?
									wksht.Cells[row, 33].StringValue.Substring(0, 500) : wksht.Cells[row, 33].StringValue) : string.Empty);
								data.HealthRelease = (wksht.Cells[row, 34] != null && wksht.Cells[row, 34].StringValue.ToLower().StartsWith("i agree"));

								data.DateMedicalDate = (wksht.Cells[row, 35] != null ? (wksht.Cells[row, 35].Type == Aspose.Cells.CellValueType.IsDateTime ?
									wksht.Cells[row, 35].DateTimeValue : DateTime.Parse(wksht.Cells[row, 35].StringValue)) : DateTime.MinValue);
								data.InsuranceCompany = (wksht.Cells[row, 36] != null ? wksht.Cells[row, 36].StringValue : string.Empty);
								data.InsurancePolicyNum = (wksht.Cells[row, 37] != null ? wksht.Cells[row, 37].StringValue : string.Empty);
								data.FamilyPhysician = (wksht.Cells[row, 38] != null ? wksht.Cells[row, 38].StringValue : string.Empty);
								data.PhysicianPhone = (wksht.Cells[row, 39] != null ? wksht.Cells[row, 39].StringValue : string.Empty);
								if (data.PhysicianPhone.Length > 0) { data.PhysicianPhone = FixPhoneNum(data.PhysicianPhone); }

								// See if the person already exists in the medical table
								bool medicalAlreadyExists = false;
								if (data.IndividualID.HasValue &&
									!String.IsNullOrEmpty(data.FirstName) &&
									!String.IsNullOrEmpty(data.LastName) &&
									!String.IsNullOrEmpty(data.Street))
								{
									string searchVal = data.IndividualID.Value.ToString() + "_" + data.FirstName + "_" + data.LastName + "_" + data.Street;
									VBSDataCollection existingDataColl = new VBSDataCollection("sSheetType LIKE 'med%' AND CAST(iIndividualID AS VARCHAR(10)) + '_' + " +
										"sFirstName + '_' + sLastName + '_' + sStreet = '" + searchVal + "'");
									medicalAlreadyExists = (existingDataColl.Count > 0);
								}

								// Save the object
								if (!medicalAlreadyExists) { errors.AddRange(data.AddUpdate()); }

								#endregion MedicalReleaseForm
								break;
						}
					}
					catch (Exception ex)
					{
						ClassGenException cge = new ClassGenException(ex);
						cge.Description = "Response ID: " + (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1) + "<br />" +
							cge.Description + "<br />" + 
							ex.StackTrace;
						errors.Add(cge);

						// Log the error to the database
						VBSLog log = new VBSLog();
						log.LogGUID = System.Guid.NewGuid().ToString();
						log.Description = cge.Description;
						log.ResponseID = (wksht.Cells[row, 2] != null ? int.Parse(wksht.Cells[row, 2].StringValue) : -1);
						log.AddUpdate();
					}
				}
			}
			catch (Exception ex)
			{
				errors.Add(new ClassGenException(ex));
			}

			// Add the informational messages
			errors.Add(new ClassGenException(String.Format("{0} records added", newCount), ClassGenExceptionIconType.Information));
			errors.Add(new ClassGenException(String.Format("{0} records updated", modifiedCount), ClassGenExceptionIconType.Information));
			errors.Add(new ClassGenException(String.Format("{0} records deleted", deletedCount), ClassGenExceptionIconType.Information));

			return errors;	// Returrn the error collection
		}

		/// <summary>
		/// Fix the phone number listing
		/// </summary>
		/// <param name="phoneNum">The phone number to alter</param>
		/// <returns>The phone number</returns>
		private static string FixPhoneNum(string phoneNum)
		{
			string rtv = string.Empty;

			// Remove any characters that aren't a number
			for (int i = 0; i < phoneNum.Length; i++)
			{
				if ("0123456789".IndexOf(phoneNum.Substring(i, 1)) > -1)
				{
					rtv += phoneNum.Substring(i, 1);
				}
			}
			
			// Parse it out
			if (rtv.Length == 11) { rtv = rtv.Substring(1); }
			if (rtv.Length == 7) { rtv = "949" + rtv; }
			if (rtv.Length == 10)
			{
				rtv = "(" + rtv.Substring(0, 3) + ") " + rtv.Substring(3, 3) + "-" + rtv.Substring(6);
			}

			return rtv;		// Return the number
		}
	}
}