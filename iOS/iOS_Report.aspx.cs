﻿using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.iOS
{
	public partial class iOS_Report : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			using (MemoryStream ms = new MemoryStream())
			{
				// Get the collection for the master
				Search_VBS_SchoolListingCollection schoolCollection = new Search_VBS_SchoolListingCollection(string.Empty);
				schoolCollection.Sort(Search_VBS_SchoolListing.FN_Count + " DESC, " + Search_VBS_SchoolListing.FN_School);

				// Take off values for the subreport
				while (schoolCollection.Count > 20)
				{
					schoolCollection.RemoveAt(schoolCollection.Count - 1);
				}

				// Put in the percentages
				int totalCount = 0;
				foreach (Search_VBS_SchoolListing item in schoolCollection)
				{
					totalCount += (item.Count.HasValue ? item.Count.Value : 0);
				}
				foreach (Search_VBS_SchoolListing item in schoolCollection)
				{
					if (item.Count.HasValue)
					{
						item.GridCustom_4 = ((item.Count.Value / (decimal)totalCount) * 100).ToString("##0.0") + "%";
					}
				}

				Search_VBS_SchoolListingCollection schoolCollectionChart = new Search_VBS_SchoolListingCollection(string.Empty);
				schoolCollectionChart.Sort(Search_VBS_SchoolListing.FN_Count + " DESC, " + Search_VBS_SchoolListing.FN_School);

				while (schoolCollectionChart.Count > 20)
				{
					schoolCollectionChart.RemoveAt(schoolCollectionChart.Count - 1);
				}

				VBS.Reports.rptSchoolListing rpt = new VBS.Reports.rptSchoolListing();
				rpt.DataSource = schoolCollectionChart;
				rpt.SchoolCollectionDetail = schoolCollection;
				rpt.DisplayName = "Top 20 Schools";

				rpt.CreateDocument();
				PdfExportOptions opts = new PdfExportOptions();
				opts.ShowPrintDialogOnOpen = true;
				rpt.ExportToPdf(ms, opts);
				ms.Seek(0, SeekOrigin.Begin);
				byte[] report = ms.ToArray();
				Page.Response.ContentType = "application/pdf";
				Page.Response.Clear();
				Page.Response.OutputStream.Write(report, 0, report.Length);
				Page.Response.End();
			}
		}
	}
}