﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.iOS
{
	public partial class iOS_Default2 : System.Web.UI.Page
	{
		protected StringBuilder _sb = new StringBuilder();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Set the last page url
			Session["CurrentPageURL"] = Request.Url.PathAndQuery;

			var pageType = HttpUtility.UrlEncode(Request.QueryString["pageType"]);
			var extType = HttpUtility.UrlEncode(Request.QueryString["extType"]);

			// Redirect for the full site
			if (!String.IsNullOrEmpty(pageType) &&
				pageType.ToLower().Equals("FullSite".ToLower()))
			{
				Session["MobileFullSite"] = "Yup!";
				Response.Redirect("/Default.aspx", true);
				return;
			}

			_sb.AppendLine("<table width=\"95%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">");

			if (String.IsNullOrEmpty(pageType))
			{
				#region Blank Page
				// Build the default page
				_sb.AppendLine("<tr height=\"30px\"><td style=\"vertical-align:middle;\" width=\"1%\" nowrap><a style=\"color:white;\" " + 
					"href=\"iOS_Default2.aspx?pageType=StudentLetter\"><img border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
					"/></a></td><td nowrap>&nbsp;<a style=\"color:white;\" href=\"iOS_Default2.aspx?pageType=StudentLetter\">Student Last Name</a></td></tr>");
				_sb.AppendLine("<tr height=\"30px\"><td style=\"vertical-align:middle;\" width=\"1%\" nowrap><a style=\"color:white;\" " +
					"href=\"iOS_Default2.aspx?pageType=VolunteerLetter\"><img border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
					"/></a></td><td nowrap>&nbsp;<a style=\"color:white;\" href=\"iOS_Default2.aspx?pageType=VolunteerLetter\">Volunteer Last Name</a></td></tr>");
				_sb.AppendLine("<tr height=\"30px\"><td style=\"vertical-align:middle;\" width=\"1%\" nowrap><a style=\"color:white;\" " +
					"href=\"iOS_Default2.aspx?pageType=Classes\"><img border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
					"/></a></td><td nowrap>&nbsp;<a style=\"color:white;\" href=\"iOS_Default2.aspx?pageType=Classes\">Classes</a></td></tr>");
				_sb.AppendLine("<tr height=\"30px\"><td style=\"vertical-align:middle;\" width=\"1%\" nowrap><a style=\"color:white;\" " +
					"href=\"iOS_Default2.aspx?pageType=Groups\"><img border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
					"/></a></td><td nowrap>&nbsp;<a style=\"color:white;\" href=\"iOS_Default2.aspx?pageType=Groups\">Groups</a></td></tr>");
				_sb.AppendLine("<tr height=\"30px\"><td style=\"vertical-align:middle;\" width=\"1%\" nowrap><a style=\"color:white;\" " +
					"href=\"iOS_Default2.aspx?pageType=FullSite\"><img border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
					"/></a></td><td nowrap>&nbsp;<a style=\"color:white;\" href=\"iOS_Default2.aspx?pageType=FullSite\">Full Site</a></td></tr>");
				#endregion Blank Page
			}
			else if (pageType.ToLower().StartsWith("Student".ToLower()))
			{
				#region Student
				// Student List
				if (String.IsNullOrEmpty(extType))
				{
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap><img " +
						"border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
						"/>&nbsp;</td><td style=\"vertical-align:middle;\" colspan=\"3\" nowrap>Student Last Name</td></tr>");

					// Generate the list of letters
					List<string> studentLetters = Search_VBS_Students.GetDistinctFromDB(Search_VBS_StudentsField.Left1LastName);
					List<string> newLetters = new List<string>();
					foreach (string s in studentLetters)
					{
						newLetters.Add(s.ToUpper());
					}
					newLetters.Sort();
					for (int i = 65; i <= 90; i++)
					{
						string s = ((char)i).ToString();
						if (newLetters.Contains(s.ToUpper()))
						{
							_sb.AppendLine("<tr height=\"30px\"><td colspan=\"2\" width=\"1%\">&nbsp;&nbsp;</td><td style=\"vertical-align:middle;\" width=\"24px\" nowrap><a " +
								"href=\"iOS_Default2.aspx?pageType=StudentLetter&extType=" + s + "\"><img border=\"0\" " +
								"src=\"/iOS/Content/Images/bullet_triangle_red_01_24.png\" " +
								"/></a></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;<a style=\"color:white;\" href=\"iOS_Default2.aspx?pageType=StudentLetter&extType=" + s + "\">" + s + "</a></td></tr>");
						}
						else
						{
							_sb.AppendLine("<tr height=\"30px\"><td colspan=\"2\" width=\"1%\">&nbsp;&nbsp;</td><td style=\"vertical-align:middle;\" width=\"24px\" nowrap><img border=\"0\" " +
								"src=\"/iOS/Content/Images/spacer16.png\" " +
								"/></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;" + s + "</td></tr>");
						}
					}
				}
				else
				{
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap><img " +
						"border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
						"/>&nbsp;</td><td style=\"vertical-align:middle;\" colspan=\"4\" nowrap>Student Last Name</td></tr>");
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap>&nbsp;</td><td width=\"1%\"><a " + 
						"href=\"iOS_Default2.aspx?pageType=StudentLetter\"><img border=\"0\" " +
						"src=\"/iOS/Content/Images/bullet_triangle_red_01_24.png\" " +
						"/></a></td><td " +
						"style=\"vertical-align:middle;color:#82E0E0;\" colspan=\"3\" nowrap><a style=\"color:#82E0E0;\" " + 
						"href=\"iOS_Default2.aspx?pageType=StudentLetter\">Starting with: " + extType + "</a></td></tr>");

					// Generate the list of names
					Search_VBS_StudentsCollection studentCollection = new Search_VBS_StudentsCollection("sLeft1LastName = '" + extType + "'");
					studentCollection.Sort(Search_VBS_Students.FN_FullNameLastFirst);
					foreach (Search_VBS_Students student in studentCollection)
					{
						_sb.AppendLine("<tr height=\"30px\"><td>&nbsp;</td><td>&nbsp;</td><td width=\"1%\">&nbsp;&nbsp;</td>" + 
							"<td style=\"vertical-align:middle;\" width=\"1%\" nowrap><img border=\"0\" " +
							"src=\"/iOS/Content/Images/spacer16.png\" " +
							"/></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;<a href=\"iOS_Detail.aspx?respID=" + student.ResponseID.Value.ToString() + 
							"\" style=\"color:white;\">" + student.FullNameLastFirst + "</a></td></tr>");
					}
				}
				#endregion Student
			}
			else if (pageType.ToLower().StartsWith("Volunteer".ToLower()))
			{
				#region Volunteer
				// Volunteer List
				if (String.IsNullOrEmpty(extType))
				{
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap><img " +
						"border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
						"/>&nbsp;</td><td style=\"vertical-align:middle;\" colspan=\"3\" nowrap>Volunteer Last Name</td></tr>");

					// Generate the list of letters
					List<string> volunteerLetters = Search_VBS_Volunteer.GetDistinctFromDB(Search_VBS_VolunteerField.Left1LastName);
					List<string> newLetters = new List<string>();
					foreach (string s in volunteerLetters)
					{
						newLetters.Add(s.ToUpper());
					}
					newLetters.Sort();
					for (int i = 65; i <= 90; i++)
					{
						string s = ((char)i).ToString();
						if (newLetters.Contains(s.ToUpper()))
						{
							_sb.AppendLine("<tr height=\"30px\"><td colspan=\"2\" width=\"1%\">&nbsp;&nbsp;</td><td style=\"vertical-align:middle;\" width=\"24px\" nowrap><a " +
								"href=\"iOS_Default2.aspx?pageType=VolunteerLetter&extType=" + s + "\"><img border=\"0\" " +
								"src=\"/iOS/Content/Images/bullet_triangle_green_01_24.png\" " +
								"/></a></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;<a style=\"color:white;\" href=\"iOS_Default2.aspx?pageType=VolunteerLetter&extType=" + s + "\">" + s + "</a></td></tr>");
						}
						else
						{
							_sb.AppendLine("<tr height=\"30px\"><td colspan=\"2\" width=\"1%\">&nbsp;&nbsp;</td><td style=\"vertical-align:middle;\" width=\"24px\" nowrap><img border=\"0\" " +
								"src=\"/iOS/Content/Images/spacer16.png\" " +
								"/></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;" + s + "</td></tr>");
						}
					}
				}
				else
				{
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap><img " +
						"border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
						"/>&nbsp;</td><td style=\"vertical-align:middle;\" colspan=\"4\" nowrap>Volunteer Last Name</td></tr>");
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap>&nbsp;</td><td width=\"1%\"><a " +
						"href=\"iOS_Default2.aspx?pageType=VolunteerLetter\"><img border=\"0\" " +
						"src=\"/iOS/Content/Images/bullet_triangle_green_01_24.png\" " +
						"/></a></td><td " +
						"style=\"vertical-align:middle;color:#82E0E0;\" colspan=\"3\" nowrap><a style=\"color:#82E0E0;\" " +
						"href=\"iOS_Default2.aspx?pageType=VolunteerLetter\">Starting with: " + extType + "</a></td></tr>");

					// Generate the list of names
					Search_VBS_VolunteerCollection volunteerCollection = new Search_VBS_VolunteerCollection("sLeft1LastName = '" + extType + "'");
					volunteerCollection.Sort(Search_VBS_Volunteer.FN_FullNameLastFirst);
					foreach (Search_VBS_Volunteer volunteer in volunteerCollection)
					{
						_sb.AppendLine("<tr height=\"30px\"><td>&nbsp;</td><td>&nbsp;</td><td width=\"1%\">&nbsp;&nbsp;</td>" +
							"<td style=\"vertical-align:middle;\" width=\"1%\" nowrap><img border=\"0\" " +
							"src=\"/iOS/Content/Images/spacer16.png\" " +
							"/></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;<a href=\"iOS_Detail.aspx?respID=" + volunteer.ResponseID.Value.ToString() +
							"\" style=\"color:white;\">" + volunteer.FullNameLastFirst + "</a></td></tr>");
					}
				}
				#endregion Volunteer
			}
			else if (pageType.ToLower().StartsWith("Class".ToLower()))
			{
				#region Class
				// Student List
				if (String.IsNullOrEmpty(extType))
				{
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap><img " +
						"border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
						"/>&nbsp;</td><td style=\"vertical-align:middle;\" colspan=\"3\" nowrap>Classes</td></tr>");

					// Generate the list of letters
					List<string> list1 = Search_VBS_ClassCode.GetDistinctFromDB(Search_VBS_ClassCodeField.ClassCode, "iLength = 1");
					List<string> list2 = Search_VBS_ClassCode.GetDistinctFromDB(Search_VBS_ClassCodeField.ClassCode, "iLength = 2");
					list1.Sort();
					list2.Sort();
					List<string> finalLetters = new List<string>();
					foreach (string s in list1) { finalLetters.Add(s.ToUpper()); }
					foreach (string s in list2) { finalLetters.Add(s.ToUpper()); }
					Search_VBS_ClassCodeCollection coll = new Search_VBS_ClassCodeCollection(string.Empty);
					int totalCount = 0;

					foreach (string s in finalLetters)
					{
						foreach (Search_VBS_ClassCode code in coll)
						{
							if (code.ClassCode.ToLower().Equals(s.ToLower()))
							{
								totalCount = code.JuniorHelperCount +
									code.StudentCount +
									code.JuniorHelperCount;
								break;
							}
						}
						if (totalCount > 0)
						{
							_sb.AppendLine("<tr height=\"30px\"><td colspan=\"2\" width=\"1%\">&nbsp;&nbsp;</td><td style=\"vertical-align:middle;\" width=\"24px\" nowrap><a " +
								"href=\"iOS_Default2.aspx?pageType=Class&extType=" + s + "\"><img border=\"0\" " +
								"src=\"/iOS/Content/Images/bullet_triangle_yellow_01_24.png\" " +
								"/></a></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;<a style=\"color:white;\" href=\"iOS_Default2.aspx?pageType=Class&extType=" + s + "\">" + s + "</a></td></tr>");
						}
						else
						{
							_sb.AppendLine("<tr height=\"30px\"><td colspan=\"2\" width=\"1%\">&nbsp;&nbsp;</td><td style=\"vertical-align:middle;\" width=\"24px\" nowrap><img border=\"0\" " +
								"src=\"/iOS/Content/Images/spacer16.png\" " +
								"/></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;" + s + "</td></tr>");
						}
					}
				}
				else
				{
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap><img " +
						"border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
						"/>&nbsp;</td><td style=\"vertical-align:middle;\" colspan=\"4\" nowrap>Classes</td></tr>");
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap>&nbsp;</td><td width=\"1%\"><a " +
						"href=\"iOS_Default2.aspx?pageType=Class\"><img border=\"0\" " +
						"src=\"/iOS/Content/Images/bullet_triangle_yellow_01_24.png\" " +
						"/></a></td><td " +
						"style=\"vertical-align:middle;color:#82E0E0;\" colspan=\"3\" nowrap><a style=\"color:#82E0E0;\" " +
						"href=\"iOS_Default2.aspx?pageType=Class\">Class: " + extType + "</a></td></tr>");

					// Generate the list of names
					Search_VBS_ClassAssignmentCollection assignmentsCollection = new Search_VBS_ClassAssignmentCollection("sClassCode = '" + extType + "'");
					assignmentsCollection.Sort(Search_VBS_ClassAssignment.FN_Teacher + " DESC, " +
						Search_VBS_ClassAssignment.FN_JuniorHelper + " DESC, " +
						Search_VBS_ClassAssignment.FN_Student + " DESC, " +
						Search_VBS_ClassAssignment.FN_FullNameLastFirst);
					foreach (Search_VBS_ClassAssignment assignment in assignmentsCollection)
					{
						_sb.AppendLine("<tr height=\"30px\"><td>&nbsp;</td><td>&nbsp;</td><td width=\"1%\">&nbsp;&nbsp;</td>" +
							"<td style=\"vertical-align:middle;\" width=\"1%\" nowrap><img border=\"0\" " +
							"src=\"/iOS/Content/Images/spacer16.png\" " +
							"/></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;<a href=\"iOS_Detail.aspx?respID=" + assignment.ResponseID.Value.ToString() +
							"\" style=\"color:white;\">" + assignment.FullNameLastFirst + "</a>" + 
							(assignment.Teacher ? " (Teacher)" : string.Empty) + 
							(assignment.JuniorHelper ? " (Jr. Helper)" : string.Empty) + 
							"</td></tr>");
					}
				}
				#endregion Class
			}
			else if (pageType.ToLower().StartsWith("Group".ToLower()))
			{
				#region Group
				// Group List
				if (String.IsNullOrEmpty(extType))
				{					
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap><img " +
						"border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
						"/>&nbsp;</td><td style=\"vertical-align:middle;\" colspan=\"3\" nowrap>Groups</td></tr>");

					// Generate the list of groups
					List<string> groups = Search_VBS_Group.GetDistinctFromDB(Search_VBS_GroupField.GroupName);
					List<string> newLetters = new List<string>();
					foreach (string s in groups)
					{
						newLetters.Add(s.ToUpper());
					}
					newLetters.Sort();

					Search_VBS_GroupCollection groupColl = new Search_VBS_GroupCollection(string.Empty);
					int totalCount = 0;
					foreach (string s in groups)
					{
						foreach (Search_VBS_Group grp in groupColl)
						{
							if (grp.GroupName.ToLower().Equals(s.ToLower()))
							{
								totalCount = grp.Count;
								break;
							}
						}
						if (totalCount > 0)
						{
							_sb.AppendLine("<tr height=\"30px\"><td colspan=\"2\" width=\"1%\">&nbsp;&nbsp;</td><td style=\"vertical-align:middle;\" width=\"24px\" nowrap><a " +
								"href=\"iOS_Default2.aspx?pageType=Group&extType=" + s + "\"><img border=\"0\" " +
								"src=\"/iOS/Content/Images/bullet_triangle_blue_01_24.png\" " +
								"/></a></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;<a style=\"color:white;\" href=\"iOS_Default2.aspx?pageType=Group&extType=" + s + "\">" + s + "</a></td></tr>");
						}
						else
						{
							_sb.AppendLine("<tr height=\"30px\"><td colspan=\"2\" width=\"1%\">&nbsp;&nbsp;</td><td style=\"vertical-align:middle;\" width=\"24px\" nowrap><img border=\"0\" " +
								"src=\"/iOS/Content/Images/spacer16.png\" " +
								"/></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;" + s + "</td></tr>");
						}
					}
				}
				else
				{
					if (extType.Contains("+")) { extType = extType.Replace("+", " "); }

					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap><img " +
						"border=\"0\" src=\"Content/Images/Navigation_Right_2_3_24_ent_vl.png\" " +
						"/>&nbsp;</td><td style=\"vertical-align:middle;\" colspan=\"4\" nowrap>Groups</td></tr>");
					_sb.AppendLine("<tr height=\"40px\"><td width=\"1%\" style=\"vertical-align:middle;\" nowrap>&nbsp;</td><td width=\"1%\"><a " +
						"href=\"iOS_Default2.aspx?pageType=Group\"><img border=\"0\" " +
						"src=\"/iOS/Content/Images/bullet_triangle_blue_01_24.png\" " +
						"/></a></td><td " +
						"style=\"vertical-align:middle;color:#82E0E0;\" colspan=\"3\" nowrap><a style=\"color:#82E0E0;\" " +
						"href=\"iOS_Default2.aspx?pageType=Group\">Group: " + extType + "</a></td></tr>");

					// Generate the list of names
					Search_VBS_ResponseToGroupCollection groupAssignmentCollection = new Search_VBS_ResponseToGroupCollection("sGroupName = '" + extType + "'");
					groupAssignmentCollection.Sort(Search_VBS_ResponseToGroup.FN_TeamCoordinator + " DESC, " +
						Search_VBS_ResponseToGroup.FN_GroupName + ", " + 
						Search_VBS_ResponseToGroup.FN_FullNameLastFirst);

					foreach (Search_VBS_ResponseToGroup item in groupAssignmentCollection)
					{
						_sb.AppendLine("<tr height=\"30px\"><td>&nbsp;</td><td>&nbsp;</td><td width=\"1%\">&nbsp;&nbsp;</td>" +
							"<td style=\"vertical-align:middle;\" width=\"1%\" nowrap><img border=\"0\" " +
							"src=\"/iOS/Content/Images/spacer16.png\" " +
							"/></td><td style=\"vertical-align:middle;\" nowrap>&nbsp;<a href=\"iOS_Detail.aspx?respID=" + item.ResponseID.Value.ToString() +
							"\" style=\"color:white;\">" + item.FullNameLastFirst + "</a>" + 
							(item.TeamCoordinator ? " (Team Coordinator)" : string.Empty) + 
							"</td></tr>");
					}
				}
				#endregion Group
			}

			_sb.AppendLine("</table>");
		}

		protected void homeButton_Click(object sender, EventArgs e)
		{
			Response.Redirect("iOS_Default2.aspx", true);
		}

		protected void homeButton2_Click(object sender, EventArgs e)
		{
			Response.Redirect("iOS_Default2.aspx", true);
		}
	}
}