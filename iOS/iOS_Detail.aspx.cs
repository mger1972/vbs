﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.iOS
{
	public partial class iOS_Detail : System.Web.UI.Page
	{
		protected StringBuilder _sb = new StringBuilder();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Read the response code
			var respID = HttpUtility.UrlEncode(Request.QueryString["respID"]);
			//var personType = HttpUtility.UrlEncode(Request.QueryString["personType"]);
			if (respID.ToLower().StartsWith("mobilefull"))
			{
				// Go to the full site
				Session["MobileFullSite"] = "Yup!";
				Response.Redirect("/Default.aspx", true);
			}
			else
			{
				// Build out the string for the person you're looking at
				_sb = new StringBuilder();

				//_sb.AppendLine("<span class=\"normalText\">Person Type: " + personType + "</span>");
				//_sb.AppendLine("<span class=\"normalText\">Response ID: " + respID + "</span>");

				// Get the collections for the response type
				Search_VBS_StudentsCollection students = new Search_VBS_StudentsCollection("iResponseID = " + respID + "");
				Search_VBS_VolunteerCollection volunteers = new Search_VBS_VolunteerCollection("iResponseID = " + respID + "");
				//Search_VBS_ClassAssignmentCollection assignments = new Search_VBS_ClassAssignmentCollection("iResponseID = " + respID + "");
				//Search_VBS_ResponseToGroupCollection volunteerToGroup = new Search_VBS_ResponseToGroupCollection("iResponseID = " + respID + "");

				_sb.AppendLine("<div class=\"normalText\">");
				_sb.AppendLine("<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">");

				//_sb.AppendLine("<tr><td colspan=\"3\" nowrap><br /></td></tr>");

				// Build out the screen showing the person
				if (students.Count > 0)
				{
					_sb.AppendLine("<tr><td align=\"center\" class=\"normalTextHeader\" colspan=\"3\" nowrap><b>Student:</b>&nbsp;" + students[0].FullNameLastFirst + "</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Resp. ID:</td><td>" + students[0].ResponseID.Value.ToString() + "</td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Street:</td><td>" + students[0].Street + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">City:</td><td>" + students[0].City + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">State:</td><td>" + students[0].State + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Zip:</td><td>" + students[0].Zip + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Home Phone:</td><td>" + 
						GeneratePhoneNumberLink(students[0].HomePhone).Replace("a href=", "a style=\"color:white;\" href=") + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Mobile Phone:</td><td>" +
						GeneratePhoneNumberLink(students[0].MobilePhone).Replace("a href=", "a style=\"color:white;\" href=") + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Email:</td><td>" + 
						(!String.IsNullOrEmpty(students[0].Email) ?
						"<a style=\"color:white;\" href=\"mailto:" + students[0].Email + "\">" + students[0].Email + "</a>" : string.Empty) + 
						"&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Gender:</td><td>" + students[0].Gender + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">DOB:</td><td>" + students[0].DateDOBChild.Value.ToShortDateString() + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Shirt Size:</td><td>" + students[0].ShirtSize + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Grade:</td><td>" + students[0].GradeInFall + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">School:</td><td>" + students[0].School + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Church:</td><td>" + students[0].ChurchAttended + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Friend Req. 1:</td><td>" + students[0].Friend1FullNameFirstLast + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Friend Req. 2:</td><td>" + students[0].Friend2FullNameFirstLast + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Parent:</td><td>" + students[0].ParentNameFormatted + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Med. Release:</td><td>" + (students[0].MedicalReleaseForm.Value ? "Yes" : "No") + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Allergies:</td><td>" + students[0].FoodAllergies + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Special Needs:</td><td>" + students[0].SpecialNeeds + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Comments:</td><td>" + students[0].Comments + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Class Assgn.:</td><td>" + students[0].ClassCode + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Grade:</td><td>" + students[0].Grade + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Rotation:</td><td>" + students[0].Rotation + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Submitted:</td><td>" + 
						students[0].DateSubmitted.Value.ToString("MM/dd/yyyy hh:mm tt") + "</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Modified:</td><td>" +
						students[0].DateModified.Value.ToString("MM/dd/yyyy hh:mm tt") + "</td></tr>");
				}
				if (volunteers.Count > 0)
				{
					_sb.AppendLine("<tr><td align=\"center\" class=\"normalTextHeaderGreen\" colspan=\"3\" nowrap><b>Volunteer:</b>&nbsp;" + volunteers[0].FullNameLastFirst + "</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Resp. ID:</td><td>" + volunteers[0].ResponseID.Value.ToString() + "</td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Street:</td><td>" + volunteers[0].Street + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">City:</td><td>" + volunteers[0].City + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">State:</td><td>" + volunteers[0].State + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Zip:</td><td>" + volunteers[0].Zip + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Home Phone:</td><td>" + 
						GeneratePhoneNumberLink(volunteers[0].HomePhone).Replace("a href=", "a style=\"color:white;\" href=") + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Mobile Phone:</td><td>" +
						GeneratePhoneNumberLink(volunteers[0].MobilePhone).Replace("a href=", "a style=\"color:white;\" href=") + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Email:</td><td>" +
						(!String.IsNullOrEmpty(volunteers[0].Email) ?
						"<a style=\"color:white;\" href=\"mailto:" + volunteers[0].Email + "\">" + volunteers[0].Email + "</a>" : string.Empty) + 
						"&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Cont. By Email:</td><td>" + (volunteers[0].ContactByEmail.Value ? "Yes" : "No") + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Cont. By Phone:</td><td>" + (volunteers[0].ContactByPhone.Value ? "Yes" : "No") + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Gender:</td><td>" + volunteers[0].Gender + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Shirt Size:</td><td>" + volunteers[0].ShirtSize + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Help in Areas:</td><td>" + volunteers[0].HelpInAreas + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Help DOW:</td><td>" + volunteers[0].HelpDOW + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Grade to Help In:</td><td>" + volunteers[0].GradeToHelpIn + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Work with:</td><td>" + volunteers[0].HelpAdultName + "&nbsp;</td></tr>");

					if (volunteers[0].IsAdult.Value)
					{
						_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Requesting Child:</td><td>" + volunteers[0].RequestingChild + "&nbsp;</td></tr>");
						_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Needs Child Care:</td><td>" + (volunteers[0].NeedChildCare.Value ? "Yes" : "No") + "&nbsp;</td></tr>");
					}
					else
					{
						_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Jr. Helper Grade:</td><td>" + volunteers[0].JrHelperGrade + "&nbsp;</td></tr>");
						_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Jr. Helper Email:</td><td>" +
							(!String.IsNullOrEmpty(volunteers[0].JrHelperEmail) ?
							"<a style=\"color:white;\" href=\"mailto:" + volunteers[0].JrHelperEmail + "\">" + volunteers[0].JrHelperEmail + "</a>" : string.Empty) +
							"&nbsp;</td></tr>");
					}

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Special Needs:</td><td>" + volunteers[0].SpecialNeeds + "&nbsp;</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\">Comments:</td><td>" + volunteers[0].Comments + "&nbsp;</td></tr>");

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					if (!String.IsNullOrEmpty(volunteers[0].GroupGUID))
					{
						_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Group Name:</td><td>" + volunteers[0].GroupName + "&nbsp;</td></tr>");
						if (volunteers[0].TeamCoordinator.Value)
						{
							_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" colspan=\"2\" nowrap>&nbsp;&nbsp;Team Coordinator</td></tr>");
						}
					}
					else if (!String.IsNullOrEmpty(volunteers[0].ClassCode))
					{
						_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Class Assgn.:</td><td>" + volunteers[0].ClassCode + "&nbsp;</td></tr>");
						_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Grade:</td><td>" + volunteers[0].Grade + "&nbsp;</td></tr>");
						_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Rotation:</td><td>" + volunteers[0].Rotation + "&nbsp;</td></tr>");
						if (volunteers[0].Teacher.Value)
						{
							_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" colspan=\"2\" nowrap>&nbsp;&nbsp;Teacher</td></tr>");
						}
						else if (volunteers[0].JuniorHelper.Value)
						{
							_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" colspan=\"2\" nowrap>&nbsp;&nbsp;Junior Helper</td></tr>");
						}
					}

					_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Submitted:</td><td>" +
						volunteers[0].DateSubmitted.Value.ToString("MM/dd/yyyy hh:mm tt") + "</td></tr>");
					_sb.AppendLine("<tr><td width=\"1%\" nowrap>&nbsp;</td><td class=\"normalTextCaption\" nowrap>Modified:</td><td>" +
						volunteers[0].DateModified.Value.ToString("MM/dd/yyyy hh:mm tt") + "</td></tr>");
				}

				_sb.AppendLine("<tr><td colspan=\"3\" nowrap><hr /></td></tr>");

				_sb.AppendLine("</table>");
				_sb.AppendLine("</div>");
			}
		}

		/// <summary>
		/// Generate a phone number link
		/// </summary>
		/// <param name="phoneNum">The phone number provided</param>
		/// <returns>The link or blank text</returns>
		protected string GeneratePhoneNumberLink(string phoneNum)
		{
			string tempNum = string.Empty;
			string rtv = phoneNum;
			
			// Remove all the characters but the number
			for (int i = 0; i < phoneNum.Length; i++)
			{
				if (phoneNum.Substring(i, 1).IndexOfAny("0123456789".ToCharArray()) > -1)
				{
					tempNum += phoneNum.Substring(i, 1);
				}
			}

			// If it doesn't start with a 1, and it has an area code, make it
			if (tempNum.Length == 7) { tempNum = "949" + tempNum; }
			if (tempNum.Length == 10 && !tempNum.StartsWith("1")) { tempNum = "1" + tempNum; }
			if (tempNum.Length == 11)
			{
				// This is the moment of truth
				rtv = "<a href=\"tel:+" +
					tempNum.Substring(0, 1) + "-" +
					tempNum.Substring(1, 3) + "-" +
					tempNum.Substring(4, 3) + "-" +
					tempNum.Substring(7, 4) + "\">" + phoneNum + "</a>";
			}
			else
			{
				rtv = phoneNum;		// Just return the standard number
			}

			return rtv;
		}

		protected void homeButton_Click(object sender, EventArgs e)
		{
			Response.Redirect("iOS_Default2.aspx", true);
		}

		protected void homeButton2_Click(object sender, EventArgs e)
		{
			Response.Redirect("iOS_Default2.aspx", true);
		}

		protected void backButton2_Click(object sender, EventArgs e)
		{
			if (Session["CurrentPageURL"] != null &&
				!String.IsNullOrEmpty(Session["CurrentPageURL"].ToString()))
			{
				Response.Redirect(Session["CurrentPageURL"].ToString(), true);
			}
		}

		protected void backButton_Click(object sender, EventArgs e)
		{
			if (Session["CurrentPageURL"] != null &&
				!String.IsNullOrEmpty(Session["CurrentPageURL"].ToString()))
			{
				Response.Redirect(Session["CurrentPageURL"].ToString(), true);
			}
		}
	}
}

