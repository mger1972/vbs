﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iOS_ReportViewer.aspx.cs" Inherits="VBS.iOS.iOS_ReportViewer" %>

<%@ Register Assembly="DevExpress.Web.v17.1" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Test iOS Page</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/landscape.png" media="screen and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 1)" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/portrait.png" media="screen and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 1)" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/landscape2x.png" media="screen and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/portrait2x.png" media="screen and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 2)" />
    <link rel="apple-touch-icon" sizes="76x76" href="Content/Images/ios-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="Content/Images/ios-icon-152x152.png" />
	<script type="text/javascript">
		function printReport() {

			var iframe = document.getElementById('reportout');
			if (iframe != null) {
				document.body.removeChild(iframe);
			}
			iframe = document.createElement("iframe");
			iframe.setAttribute("id", "reportout");
			iframe.style.width = 0 + "px";
			iframe.style.height = 0 + "px";
			document.body.appendChild(iframe);
			document.getElementById('reportout').contentWindow.location = 'iOS_Report.aspx';
		}
</script>
</head>
<body runat="server" id="Body">
     <form id="form1" runat="server">
    <div>
        <input id="Button1" type="button" value="Print Report via Adobe PDF plugin" onclick="printReport()"/></div>
    </form>
        <iframe id="reportout" width="0" height="0"></iframe>
</body>
</html>
