﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VBS.iOS
{
	public interface IPageUserControl
	{
		void LoadState(SearchState state);
	}
}