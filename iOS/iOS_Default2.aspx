﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="iOS_Default2.aspx.cs" Inherits="VBS.iOS.iOS_Default2" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<style>
	body 
		{
			margin: 0px;
			padding: 0px;
			height: 100%;
			background: #000000 url("Content/Images/blue_gradient_holograph.jpg") no-repeat fixed center;
			color: white;
			font-weight: bold;
			font-size: medium;
			font-family: Verdana, Geneva, 'DejaVu Sans', sans-serif
			/*text-align: center;*/
		}
	/*.dxtvControl_iOS 
		{
			margin: 0px !important;
			padding: 0px !important;
		}
	 */
	 .dxtv-elbNoLn
        {
            display: none;
        }
	 .dxtv-subnd
		{
			margin-left: 24px !important;
			margin-right: 0px !important;
			margin-bottom: 0px !important;
			margin-top: 0px !important;
			padding: 2px !important;
		}
	 .dxtv-nd
		{
			margin: 0px !important;
			padding: 4px !important;
		}

	 .dxtv-ndTxt
        {
            margin: 0px !important;
			padding-left: 6px !important;
			padding-top: 4px !important;
        }
	 .dxtv-ndChk
        {
            margin: 0px !important;
			padding: 0px !important;
        }
	 .dxtv-ndImg, .dxtv-btn
        {
            margin: 0px !important;
			padding-bottom: 0px !important;
			padding-top: 0px !important;
			padding-right: 0px !important;
			padding-left: 0px !important;
        }
	</style>
    <title>Test iOS Page</title>
    <meta name="viewport" content="width=device-width, user-scalable=1.0, initial-scale=1.0, maximum-scale=3.0, minimum-scale=1.0" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/landscape.png" media="screen and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 1)" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/portrait.png" media="screen and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 1)" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/landscape2x.png" media="screen and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/portrait2x.png" media="screen and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 2)" />
    <link rel="apple-touch-icon" sizes="76x76" href="Content/Images/VoyagersIcon_76x76.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="Content/Images/VoyagersIcon_152x152.png" />
</head>
<body runat="server" id="Body">
    <form id="frmMain" runat="server">
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center"><u>VBS Quick Search</u>&nbsp;&nbsp;<dx:ASPxButton id="homeButton" runat="server" text="Start Over" OnClick="homeButton_Click" /></td>
		</tr>
	</table><br />
	<% =_sb.ToString() %><br />
	&nbsp;<br /><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td align="center"><dx:ASPxButton id="homeButton2" runat="server" text="Start Over" OnClick="homeButton2_Click" /></td>
		</tr>
	</table>
	<br />&nbsp;
    </form>
</body>
</html>

