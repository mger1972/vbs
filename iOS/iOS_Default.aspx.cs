﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.iOS
{
	public partial class iOS_Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			// Set the images on the treeview
			//treeView.Images.ExpandButton.Url = "/iOS/Content/Images/Navigation_Right_2_3_24_ent_vl.png";
			//treeView.Images.CollapseButton.Url = "/iOS/Content/Images/nav_down_right_blue_01_24.png";

			//treeView.Images.ExpandButton.Url = "/iOS/Content/Images/Navigation_Right_2_3_16_ent_vl.png";
			//treeView.Images.CollapseButton.Url = "/iOS/Content/Images/Navigation_Down_Right_2_3_16_ent_vl.png";

			//treeView.Images.ExpandButton.Url = string.Empty;
			//treeView.Images.CollapseButton.Url = string.Empty;
			//treeView.ShowExpandButtons = false;
			//bool rtv = treeView.SaveStateToCookies;

			if (IsPostBack)
			{
				Session["iOS_ScrollPosition"] = HttpUtility.UrlEncode(Request.Form["div_position"]);
			}

			//treeView.Images.CollapseButton = new ImageProperties("/iOS/Content/Images/Navigation_Right_2_3_24_ent_vl.png");
			//treeView.Images.ExpandButton = new ImageProperties("/iOS/Content/Images/nav_down_right_blue_01_24.png");
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			// Load up the tree view
			LoadTreeView();
		}

		/// <summary>
		/// Load the tree view with elements
		/// </summary>
		protected void LoadTreeView()
		{
			treeView.Nodes.Clear();		// Clear out the control first

			TreeViewNode lastNode = null;		// Allow us to track the last node added
			TreeViewNode itemNode = null;		// Allow us to track the last child node that was added
			TreeViewNode detailNode = null;		// The detail node
			string baseTargetURL = "/iOS/iOS_Detail.aspx?respID=";

			#region Students
			lastNode = treeView.Nodes.Add("Student Last Name", "Student Last Name", "/iOS/Content/Images/Navigation_Right_2_3_24_ent_vl.png");
			Search_VBS_StudentsCollection studentCollection = new Search_VBS_StudentsCollection(string.Empty);
			studentCollection.Sort(Search_VBS_Students.FN_FullNameLastFirst);

			// Add the students
			List<string> list = Search_VBS_Students.GetDistinctFromDB(Search_VBS_StudentsField.Left1LastName);
			list.Sort();
			for (int i = 65; i <= 90; i++)
			{
				string s = ((char)i).ToString();
				TreeViewNode childNode = new TreeViewNode(s,
					"Student_" + s,
					(list.Contains(s) ? "/iOS/Content/Images/bullet_triangle_red_01_24.png" : "/iOS/Content/Images/spacer24.png"));
				lastNode.Nodes.Add(childNode);
				itemNode = lastNode.Nodes[lastNode.Nodes.Count - 1];

				// Add all the elements that match
				foreach (Search_VBS_Students item in studentCollection)
				{
					if (item.Left1LastName.ToLower().Equals(s.ToLower()))
					{
						detailNode = itemNode.Nodes.Add(item.FullNameLastFirst, "ResponseID_Student_" + item.ResponseID.Value.ToString(), "/iOS/Content/Images/spacer24.png");
						detailNode.NavigateUrl = baseTargetURL + item.ResponseID.Value.ToString() + "&personType=Student";
					}
				}
			}
			#endregion Students

			#region Volunteers
			lastNode = treeView.Nodes.Add("Volunteer Last Name", "Volunteer Last Name", "/iOS/Content/Images/Navigation_Right_2_3_24_ent_vl.png");
			Search_VBS_VolunteerCollection volunteerCollection = new Search_VBS_VolunteerCollection(string.Empty);
			volunteerCollection.Sort(Search_VBS_Volunteer.FN_FullNameLastFirst);

			list = Search_VBS_Volunteer.GetDistinctFromDB(Search_VBS_VolunteerField.Left1LastName);
			list.Sort();
			for (int i = 65; i <= 90; i++)
			{
				string s = ((char)i).ToString();
				TreeViewNode childNode = new TreeViewNode(s,
					"Volunteer_" + s,
					(list.Contains(s) ? "/iOS/Content/Images/bullet_triangle_green_01_24.png" : "/iOS/Content/Images/spacer24.png"));
				lastNode.Nodes.Add(childNode);
				itemNode = lastNode.Nodes[lastNode.Nodes.Count - 1];

				// Add all the elements that match
				foreach (Search_VBS_Volunteer item in volunteerCollection)
				{
					if (item.Left1LastName.ToLower().Equals(s.ToLower()))
					{
						detailNode = itemNode.Nodes.Add(item.FullNameLastFirst, "ResponseID_Volunteer_" + item.ResponseID.Value.ToString(), "/iOS/Content/Images/spacer24.png");
						detailNode.NavigateUrl = baseTargetURL + item.ResponseID.Value.ToString() + "&personType=Volunteer";
					}
				}
			}
			#endregion Volunteers

			#region Classes
			lastNode = treeView.Nodes.Add("Classes", "Classes", "/iOS/Content/Images/Navigation_Right_2_3_24_ent_vl.png");
			Search_VBS_ClassAssignmentCollection assignmentCollection = new Search_VBS_ClassAssignmentCollection(string.Empty);
			assignmentCollection.Sort(Search_VBS_ClassAssignment.FN_ClassCode + ", " + Search_VBS_ClassAssignment.FN_FullNameLastFirst);

			List<string> list1 = Search_VBS_ClassCode.GetDistinctFromDB(Search_VBS_ClassCodeField.ClassCode, "iLength = 1");
			List<string> list2 = Search_VBS_ClassCode.GetDistinctFromDB(Search_VBS_ClassCodeField.ClassCode, "iLength = 2");
			list1.Sort();
			list2.Sort();
			Search_VBS_ClassCodeCollection coll = new Search_VBS_ClassCodeCollection(string.Empty);
			int totalCount = 0;
			foreach (string s in list1)
			{
				TreeViewNode childNode = new TreeViewNode(s, "Class_" + s);
					//s,
					//(list.Contains(s) ? "/iOS/Content/Images/bullet_triangle_green_01_24.png" : "/iOS/Content/Images/spacer24.png"));
				totalCount = 0;
				foreach (Search_VBS_ClassCode code in coll)
				{
					if (code.ClassCode.ToLower().Equals(s.ToLower()))
					{
						totalCount = code.JuniorHelperCount +
							code.StudentCount +
							code.JuniorHelperCount;
						break;
					}
				}
				if (totalCount > 0)
				{
					childNode.Image.Url = "/iOS/Content/Images/bullet_triangle_yellow_01_24.png";
				}
				else
				{
					childNode.Image.Url = "/iOS/Content/Images/spacer24.png";
				}
				lastNode.Nodes.Add(childNode);
				itemNode = lastNode.Nodes[lastNode.Nodes.Count - 1];

				// Add all the elements that match
				foreach (Search_VBS_ClassAssignment item in assignmentCollection)
				{
					if (item.ClassCode.ToLower().Equals(s.ToLower()))
					{
						detailNode = itemNode.Nodes.Add(item.FullNameLastFirst, "ResponseID_Class_" + item.ResponseID.Value.ToString(), "/iOS/Content/Images/spacer24.png");
						detailNode.NavigateUrl = baseTargetURL + item.ResponseID.Value.ToString() + "&personType=Class";
					}
				}
			}
			#endregion Classes

			#region Groups
			lastNode = treeView.Nodes.Add("Groups", "Groups", "/iOS/Content/Images/Navigation_Right_2_3_24_ent_vl.png");
			Search_VBS_ResponseToGroupCollection groupAssignmentCollection = new Search_VBS_ResponseToGroupCollection(string.Empty);
			groupAssignmentCollection.Sort(Search_VBS_ResponseToGroup.FN_GroupName + ", " + Search_VBS_ResponseToGroup.FN_FullNameLastFirst);

			list = Search_VBS_Group.GetDistinctFromDB(Search_VBS_GroupField.GroupName);
			list.Sort();
			Search_VBS_GroupCollection groupColl = new Search_VBS_GroupCollection(string.Empty);
			totalCount = 0;
			foreach (string s in list)
			{
				TreeViewNode childNode = new TreeViewNode(s, "Group_" + s);
				totalCount = 0;
				foreach (Search_VBS_Group grp in groupColl)
				{
					if (grp.GroupName.ToLower().Equals(s.ToLower()))
					{
						totalCount = grp.Count;
						break;
					}
				}
				if (totalCount > 0)
				{
					childNode.Image.Url = "/iOS/Content/Images/bullet_triangle_blue_01_24.png";
				}
				else
				{
					childNode.Image.Url = "/iOS/Content/Images/spacer24.png";
				}
				lastNode.Nodes.Add(childNode);
				itemNode = lastNode.Nodes[lastNode.Nodes.Count - 1];

				// Add all the elements that match
				foreach (Search_VBS_ResponseToGroup item in groupAssignmentCollection)
				{
					if (item.GroupName.ToLower().Equals(s.ToLower()))
					{
						detailNode = itemNode.Nodes.Add(item.FullNameLastFirst, "ResponseID_Group_" + item.ResponseID.Value.ToString(), "/iOS/Content/Images/spacer24.png");
						detailNode.NavigateUrl = baseTargetURL + item.ResponseID.Value.ToString() + "&personType=Group";
					}
				}
			}
			#endregion Groups

			#region Full Site

			lastNode = treeView.Nodes.Add("Full Site", "Full Site", "/iOS/Content/Images/Navigation_Right_2_3_24_ent_vl.png");
			lastNode.NavigateUrl = baseTargetURL + "MobileFullSite";

			#endregion Full Site

			// Create a session collection to hold the nodes
			//TreeViewNodeCollection nodeCollection = new TreeViewNodeCollection();
			List<TreeViewNode> nodeCollection = new List<TreeViewNode>();
			foreach (TreeViewNode tvn in treeView.Nodes)
			{
				nodeCollection.Add(tvn);
				if (tvn.Nodes.Count > 0)
				{
					foreach (TreeViewNode tvnChild in tvn.Nodes)
					{
						nodeCollection.Add(tvnChild);
						if (tvnChild.Nodes.Count > 0)
						{
							foreach (TreeViewNode tvnDetail in tvnChild.Nodes)
							{
								nodeCollection.Add(tvnDetail);
							}
						}
					}
				}
			}

			// Expand and select all the nodes based on previous selection
			if (Session["iOS_Nodes"] != null)
			{
				for (int i = 0; i < ((List<TreeViewNode>)Session["iOS_Nodes"]).Count; i++)
				{
					foreach (TreeViewNode tvn in treeView.Nodes)
					{
						if (tvn.Name.Equals(((List<TreeViewNode>)Session["iOS_Nodes"])[i].Name)) { tvn.Expanded = ((List<TreeViewNode>)Session["iOS_Nodes"])[i].Expanded; }
						if (Session["iOS_SelectedNode"] != null && tvn.Name.Equals(Session["iOS_SelectedNode"])) { treeView.ExpandToNode(tvn); }

						if (tvn.Nodes.Count > 0)
						{
							foreach (TreeViewNode tvnChild in tvn.Nodes)
							{
								if (tvnChild.Name.Equals(((List<TreeViewNode>)Session["iOS_Nodes"])[i].Name)) { tvnChild.Expanded = ((List<TreeViewNode>)Session["iOS_Nodes"])[i].Expanded; }
								if (Session["iOS_SelectedNode"] != null && tvnChild.Name.Equals(Session["iOS_SelectedNode"])) { treeView.ExpandToNode(tvnChild); }

								if (tvnChild.Nodes.Count > 0)
								{
									foreach (TreeViewNode tvnDetail in tvnChild.Nodes)
									{
										if (tvnDetail.Name.Equals(((List<TreeViewNode>)Session["iOS_Nodes"])[i].Name)) { tvnDetail.Expanded = ((List<TreeViewNode>)Session["iOS_Nodes"])[i].Expanded; }
										if (Session["iOS_SelectedNode"] != null && tvnDetail.Name.Equals(Session["iOS_SelectedNode"])) { treeView.ExpandToNode(tvnDetail); }
									}
								}
							}
						}
					}
				}
			}

			// Reset the session
			Session["iOS_Nodes"] = null;
			Session["iOS_Nodes"] = nodeCollection;
			Session["iOS_ScrollPosition"] = HttpUtility.UrlEncode(Request.Form["div_position"]);
		}

		protected void treeView_NodeClick(object sender, TreeViewNodeEventArgs e)
		{
			if (e.Node.Nodes.Count > 0)
			{
				e.Node.Expanded = !e.Node.Expanded;
				if (Session["iOS_Nodes"] != null)
				{
					for (int i = 0; i < ((List<TreeViewNode>)Session["iOS_Nodes"]).Count; i++)
					{
						if (((List<TreeViewNode>)Session["iOS_Nodes"])[i].Name.Equals(e.Node.Name))
						{
							((List<TreeViewNode>)Session["iOS_Nodes"])[i].Expanded = e.Node.Expanded;
							break;
						}
					}
				}
				Session["iOS_SelectedNode"] = e.Node.Name;
				Session["iOS_ScrollPosition"] = HttpUtility.UrlEncode(Request.Form["div_position"]);
			}

			////string val = e.Node.Text;
			////int test = 1;
			////Response.Redirect("iOS_ReportViewer.aspx", true);

			//// Depending on the link they click, bring up different information
			//string val = e.Node.Text;
			//string parentVal = (e.Node.Parent != treeView.RootNode ? e.Node.Parent.Text : string.Empty);
			//if (String.IsNullOrEmpty(parentVal))
			//{
			//	if (!((TreeViewVirtualNode)e.Node).IsLeaf)
			//	{
			//		// Expand or collapse the content
			//		e.Node.Expanded = !e.Node.Expanded;
			//	}
			//	else
			//	{
			//		// Go to the full site
			//		Session["MobileFullSite"] = "Yup!";
			//		Response.Redirect("/Default.aspx", true);
			//	}
			//}
			//else
			//{
			//	// Handle a child click
			//	if (!((TreeViewVirtualNode)e.Node).IsLeaf)
			//	{
			//		// Expand or collapse the content
			//		e.Node.Expanded = !e.Node.Expanded;
			//	}
			//}
		}

		protected void treeView_VirtualModeCreateChildren(object sender, TreeViewVirtualModeCreateChildrenEventArgs e)
		{
			//// If this is the root, just establish the basic nodes
			//List<TreeViewVirtualNode> children = new List<TreeViewVirtualNode>();
			//if (String.IsNullOrEmpty(e.NodeName))
			//{
			//	Dictionary<string, bool> list = new Dictionary<string, bool>();
			//	list.Add("Student Last Name", false);
			//	list.Add("Volunteer Last Name", false);
			//	list.Add("Classes", false);
			//	list.Add("Groups", false);
			//	list.Add("Full Site", true);

			//	foreach (KeyValuePair<string, bool> kvp in list)
			//	{
			//		TreeViewVirtualNode childNode = new TreeViewVirtualNode(kvp.Key, kvp.Key);
			//		childNode.IsLeaf = kvp.Value;
			//		childNode.Expanded = false;
			//		//if (kvp.Value) { childNode.Image.Url = string.Empty; }
			//		childNode.Image.Url = "/iOS/Content/Images/Navigation_Right_2_3_24_ent_vl.png";
			//		children.Add(childNode);
			//	}

			//	//treeView.Images.ExpandButton.Url = "/iOS/Content/Images/Navigation_Right_2_3_24_ent_vl.png";
			//	//treeView.Images.CollapseButton.Url = "/iOS/Content/Images/nav_down_right_blue_01_24.png";
			//}
			//else
			//{
			//	// Put in the kiddies
			//	if (e.NodeName.ToLower().Equals("Student Last Name".ToLower()))
			//	{
			//		List<string> list = Search_VBS_Students.GetDistinctFromDB(Search_VBS_StudentsField.Left1LastName);
			//		list.Sort();
			//		for (int i = 65; i <= 90; i++)
			//		{
			//			string s = ((char)i).ToString();
			//			TreeViewVirtualNode childNode = new TreeViewVirtualNode("Student_" + s, s);
			//			childNode.IsLeaf = true;
			//			childNode.Expanded = false;
			//			if (list.Contains(s))
			//			{
			//				childNode.Image.Url = "/iOS/Content/Images/bullet_triangle_red_01_24.png";
			//				childNode.IsLeaf = false;
			//			}
			//			else
			//			{
			//				childNode.Image.Url = "/iOS/Content/Images/spacer24.png";
			//			}
			//			children.Add(childNode);
			//		}
			//	}
			//	else if (e.NodeName.ToLower().Equals("Volunteer Last Name".ToLower()))
			//	{
			//		List<string> list = Search_VBS_Volunteer.GetDistinctFromDB(Search_VBS_VolunteerField.Left1LastName);
			//		list.Sort();
			//		for (int i = 65; i <= 90; i++)
			//		{
			//			string s = ((char)i).ToString();
			//			TreeViewVirtualNode childNode = new TreeViewVirtualNode("Volunteer_" + s, s);
			//			childNode.IsLeaf = true;
			//			childNode.Expanded = false;
			//			if (list.Contains(s))
			//			{
			//				childNode.Image.Url = "/iOS/Content/Images/bullet_triangle_green_01_24.png";
			//				childNode.IsLeaf = false;
			//			}
			//			else
			//			{
			//				childNode.Image.Url = "/iOS/Content/Images/spacer24.png";
			//			}
			//			children.Add(childNode);
			//		}
			//	}
			//	else if (e.NodeName.ToLower().Equals("Classes".ToLower()))
			//	{
			//		List<string> list1 = Search_VBS_ClassCode.GetDistinctFromDB(Search_VBS_ClassCodeField.ClassCode, "iLength = 1");
			//		List<string> list2 = Search_VBS_ClassCode.GetDistinctFromDB(Search_VBS_ClassCodeField.ClassCode, "iLength = 2");
			//		list1.Sort();
			//		list2.Sort();
			//		Search_VBS_ClassCodeCollection coll = new Search_VBS_ClassCodeCollection(string.Empty);
			//		int totalCount = 0;
			//		foreach (string s in list1)
			//		{
			//			TreeViewVirtualNode childNode = new TreeViewVirtualNode("Class" + s, s);
			//			childNode.IsLeaf = true;
			//			childNode.Expanded = false;
			//			totalCount = 0;
			//			foreach (Search_VBS_ClassCode code in coll)
			//			{
			//				if (code.ClassCode.ToLower().Equals(s.ToLower()))
			//				{
			//					totalCount = code.JuniorHelperCount +
			//						code.StudentCount +
			//						code.JuniorHelperCount;
			//					break;
			//				}
			//			}
			//			if (totalCount > 0)
			//			{
			//				childNode.Image.Url = "/iOS/Content/Images/bullet_triangle_yellow_01_24.png";
			//				childNode.IsLeaf = false;
			//			}
			//			else
			//			{
			//				childNode.Image.Url = "/iOS/Content/Images/spacer24.png";
			//			}
			//			children.Add(childNode);
			//		}
			//	}
			//	else if (e.NodeName.ToLower().Equals("Groups".ToLower()))
			//	{
			//		List<string> list = Search_VBS_Group.GetDistinctFromDB(Search_VBS_GroupField.GroupName);
			//		list.Sort();
			//		Search_VBS_GroupCollection coll = new Search_VBS_GroupCollection(string.Empty);
			//		int totalCount = 0;
			//		foreach (string s in list)
			//		{
			//			TreeViewVirtualNode childNode = new TreeViewVirtualNode("Group_" + s, s);
			//			childNode.IsLeaf = true;
			//			childNode.Expanded = false;
			//			totalCount = 0;
			//			foreach (Search_VBS_Group grp in coll)
			//			{
			//				if (grp.GroupName.ToLower().Equals(s.ToLower()))
			//				{
			//					totalCount = grp.Count;
			//					break;
			//				}
			//			}
			//			if (totalCount > 0)
			//			{
			//				childNode.Image.Url = "/iOS/Content/Images/bullet_triangle_blue_01_24.png";
			//				childNode.IsLeaf = false;
			//			}
			//			else
			//			{
			//				childNode.Image.Url = "/iOS/Content/Images/spacer24.png";
			//			}
			//			children.Add(childNode);
			//		}
			//	}
			//	else
			//	{
			//		// Go through and figure out which letter we have here to populate the people
			//		string clickedVal = e.NodeName.ToLower();
			//		if (clickedVal.StartsWith("Student_".ToLower()))
			//		{
			//			Search_VBS_StudentsCollection coll = new Search_VBS_StudentsCollection("sLeft1LastName = '" + 
			//				clickedVal.Substring(clickedVal.Length - 1) + "'");
			//			coll.Sort(Search_VBS_Students.FN_LastName);
			//			foreach (Search_VBS_Students student in coll)
			//			{
			//				TreeViewVirtualNode childNode = new TreeViewVirtualNode("Student_" + student.ResponseID.Value.ToString(), student.FullNameLastFirst);
			//				childNode.IsLeaf = false;
			//				childNode.Image.Url = "/iOS/Content/Images/spacer24.png";
			//				children.Add(childNode);
			//			}
			//		}
			//		else if (clickedVal.StartsWith("Volunteer_".ToLower()))
			//		{

			//		}
			//		else if (clickedVal.StartsWith("Class_".ToLower()))
			//		{

			//		}
			//		else if (clickedVal.StartsWith("Group_".ToLower()))
			//		{

			//		}
			//	}
			//}

			////string parentNodePath = string.IsNullOrEmpty(e.NodeName) ? Page.MapPath("~/") : e.NodeName;
			
			////if (Directory.Exists(parentNodePath))
			////{
			////	PopulateChildNodes(Directory.GetDirectories(parentNodePath), false,
			////		DirImageUrl, children);
			////	PopulateChildNodes(Directory.GetFiles(parentNodePath), true,
			////		FileImageUrl, children);
			////}
			//e.Children = children;
		}

		protected void treeView_ExpandedChanged(object source, TreeViewNodeEventArgs e)
		{
			// When the expansion for a node changes, set that node's change in the SessionState
			int test = 1;
		}

		//private void PopulateChildNodes(string[] paths, bool isLeaf, string imageUrl, List<TreeViewVirtualNode> children)
		//{
		//	foreach (string childPath in paths)
		//	{
		//		string childFileName = Path.GetFileName(childPath);
		//		if (IsSystemName(childFileName))
		//			continue;
		//		TreeViewVirtualNode childNode = new TreeViewVirtualNode(childPath, childFileName);
		//		childNode.IsLeaf = isLeaf;
		//		childNode.Image.Url = imageUrl;
		//		children.Add(childNode);
		//	}
		//}

		//private bool IsSystemName(string name)
		//{
		//	name = name.ToLower();
		//	return name.StartsWith("app_") || name == "bin"
		//		|| name.EndsWith(".aspx.cs") || name.EndsWith(".aspx.vb");
		//}
	}
}