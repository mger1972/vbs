﻿<%@ Page Theme="iOS" Language="C#" AutoEventWireup="true" CodeBehind="iOS_Default.aspx.cs" Inherits="VBS.iOS.iOS_Default" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<style>
	body 
		{
			margin: 0px;
			padding: 0px;
			height: 100%;
			overflow: hidden;
			background: #000000 url("Content/Images/blue_gradient_holograph.jpg") no-repeat fixed center;
			color: white;
			/*text-align: center;*/
		}
	/*.dxtvControl_iOS 
		{
			margin: 0px !important;
			padding: 0px !important;
		}
	 */
	 .dxtv-elbNoLn
        {
            display: none;
        }
	 .dxtv-subnd
		{
			margin-left: 24px !important;
			margin-right: 0px !important;
			margin-bottom: 0px !important;
			margin-top: 0px !important;
			padding: 2px !important;
		}
	 .dxtv-nd
		{
			margin: 0px !important;
			padding: 4px !important;
		}

	 .dxtv-ndTxt
        {
            margin: 0px !important;
			padding-left: 6px !important;
			padding-top: 4px !important;
        }
	 .dxtv-ndChk
        {
            margin: 0px !important;
			padding: 0px !important;
        }
	 .dxtv-ndImg, .dxtv-btn
        {
            margin: 0px !important;
			padding-bottom: 0px !important;
			padding-top: 0px !important;
			padding-right: 0px !important;
			padding-left: 0px !important;
        }
	</style>
    <title>Test iOS Page</title>
    <meta name="viewport" content="width=device-width, user-scalable=no, maximum-scale=1.5, minimum-scale=1.5" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/landscape.png" media="screen and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 1)" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/portrait.png" media="screen and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 1)" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/landscape2x.png" media="screen and (orientation:landscape) and (-webkit-min-device-pixel-ratio: 2)" />
    <link rel="apple-touch-startup-image" href="Content/Images/splash/portrait2x.png" media="screen and (orientation:portrait) and (-webkit-min-device-pixel-ratio: 2)" />
    <link rel="apple-touch-icon" sizes="76x76" href="Content/Images/ios-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="Content/Images/ios-icon-152x152.png" />
</head>
<body runat="server" id="Body">
	<script type="text/javascript">
		window.onload = function () {
			//alert('hi1');
			//var selectedNode = treeView.GetSelectedNode();
			//alert('hi3');
			//var scrollElement = treeView.GetMainElement().parentNode;
			//alert('hi4');
			//var scrollValue = scrollElement.scrollTop + ASPxClientUtils.GetAbsoluteY(selectedNode.GetHtmlElement()) -
			//   Math.round(scrollElement.offsetHeight / 2);
			//alert('hi5');
			//if (scrollValue >= 0)
			//	scrollElement.scrollTop = scrollValue;

			var div = document.getElementById("dvScroll");
			//alert('hi1');
			var div_position = document.getElementById("div_position");
			//alert('hi2');
			var position = parseInt('<%=Session["iOS_ScrollPosition"] %>');
			//alert('hi3');
			if (isNaN(position)) {
    			position = 0;
			}
			//alert('hi4');
			div.scrollTop = position;
			//alert('hi5');
			//div.onscroll = function () {
			//	var div = document.getElementById("dvScroll");
			//	alert(div.scrollTop);
			//	//div_position.value = div.scrollTop;
			//	//alert('hi7');
			//};
			//alert('hi6');
		//}
    </script>
    <form id="frmMain" runat="server">
	<div id="dvScroll" onscroll='javascript:var div = document.getElementById("dvScroll"); var div_position = document.getElementById("div_position"); div_position.value = div.scrollTop;' 
		
		style="overflow:auto; position:absolute; top: 0px; left:0px; right:0px; bottom:0px" >
		<div style="margin-left:auto; margin-right:auto; width: 265px; text-align: center; padding: 3px">
			<dx:ASPxLabel runat="server" ID="lblHeader" Text="VBS Quick Search" Font-Underline="true" ForeColor="White" />
		</div>
		<dx:ASPxTreeView EnableViewState="false" OnNodeClick="treeView_NodeClick" Border-BorderWidth="0px" 
			Images-ExpandButton-Width="0px" Images-CollapseButton-Width="0px" ForeColor="White"
			ShowTreeLines="false" Styles-Link-Color="White"
			ID="treeView" runat="server" EnableCallBacks="true" >
			<Images NodeImage-Width="16px" />
			<Styles>
				<Node Border-BorderWidth="0px" Paddings-Padding="0px" VerticalAlign="Middle"   />
				<NodeImage VerticalAlign="Middle" HorizontalAlign="Center" />
				<NodeText ForeColor="White" />
			</Styles>
		</dx:ASPxTreeView>
	</div>
	<input type="hidden" id="div_position" name="div_position" />
    </form>
</body>
</html>
