﻿<%@ Page Title="Stats Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Stats.aspx.cs" Inherits="VBS.Stats" %>
<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Sonrise Island VBS</h1>
                <h3>Downloading statistics now...</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<table width="100%" align="center">
		<tr>
			<asp:UpdatePanel ID="pnlCounts" runat="server">
				<ContentTemplate>
					&nbsp;<br /><b>Data Counts</b><br /><asp:Label ID="lblCounts" runat="server">&nbsp; &nbsp;Loading...</asp:Label>
				</ContentTemplate>
			</asp:UpdatePanel>
		</tr>
		<tr>
			<asp:UpdatePanel ID="pnlLog" runat="server">
				<ContentTemplate>
					&nbsp;<br /><b>Log</b><br /><asp:Label ID="lblLog" runat="server">&nbsp; &nbsp;Loading...</asp:Label>
				</ContentTemplate>
			</asp:UpdatePanel>
		</tr>
	</table>
</asp:Content>

