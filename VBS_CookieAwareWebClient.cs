﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace SmarterASPNetTestProject
{
	public class VBS_CookieAwareWebClient : WebClient
	{
		public void Login(string loginPageAddress, string loginData)
		{
            try {
                CookieContainer container;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(loginPageAddress);

                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                string val = loginData.ToString();
                byte[] buffer = Encoding.ASCII.GetBytes(loginData.ToString());
                request.ContentLength = buffer.Length;
                System.IO.Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Close();

                container = request.CookieContainer = new CookieContainer();

                WebResponse response = request.GetResponse();
                response.Close();
                CookieContainer = container;
            }
            catch (Exception ex)
            {
                int test = 1;
            }
		}

		public VBS_CookieAwareWebClient(CookieContainer container)
		{
			CookieContainer = container;
		}

		public VBS_CookieAwareWebClient()
			: this(new CookieContainer())
		{ }

		public CookieContainer CookieContainer { get; private set; }

		protected override WebRequest GetWebRequest(Uri address)
		{
			HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(address);
			request.CookieContainer = CookieContainer;
			return request;
		}
	}
}