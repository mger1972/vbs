﻿<%@ Page Title="System Administration" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SystemAdmin.aspx.cs" Inherits="VBS.SystemAdmin" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>System Administration</h1>
				<h3>Use the screen below to edit the class definitions and other system items.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<ol class="round">
        <li class="one">
            <h5>Class Definitions</h5>
            Use this screen to create and edit the class definitions.
            <a href="~/SysAdmin/ClassDef.aspx" id="link1" runat="server">Modify Class Definitions</a>
        </li>
		<li class="two">
            <h5>Groups</h5>
            Use this screen to create and edit the groups (Rec, Kitchen, Crafts, etc.)
            <a href="~/SysAdmin/Groups.aspx" id="link2" runat="server">Modify Groups</a>
        </li>
		<li class="three">
            <h5>Raw Data from MyVBC</h5>
            View the raw data imported from the MyVBC system.
            <a href="~/SysAdmin/RawData.aspx" id="link3" runat="server">View Raw Data</a>
        </li>
		<li class="four">
            <h5>Download From MyVBC</h5>
            Force a download of data from the MyVBC system.
            <a href="~/Download.aspx" id="link4" runat="server">Download Data</a>
        </li>
		<li class="five">
            <h5>View Stats</h5>
            View the stats of data imported from the MyVBC system.
            <a href="~/Stats.aspx" id="link5" runat="server">View Stats</a>
        </li>
    </ol>
</asp:Content>
