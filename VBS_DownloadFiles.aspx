﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VBS_DownloadFiles.aspx.cs" Inherits="SmarterASPNetTestProject.VBS_DownloadFiles" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<asp:PlaceHolder id="MetaPlaceHolder" runat="server" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
		<% =_sb.ToString() %>
    </div>
	<br />
	<div>
		<% =_sbLog.ToString() %>
    </div>
    </form>
</body>
</html>
