﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS
{
	public partial class Assignments : System.Web.UI.Page
	{
		protected VBSClassCodeCollection _classCode = new VBSClassCodeCollection();
		protected Search_VBS_ClassAssignmentCollection _assignments = new Search_VBS_ClassAssignmentCollection();
		protected GridViewCommandColumn ComandColumn { get { return (GridViewCommandColumn)gridClass.Columns[2]; } }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (cboClassType.SelectedIndex == -1)
			{
				cboClassType.SelectedIndex = 0;
			}
			
			// Set the datasource on the grid
			if (!IsPostBack)
			{
				gridClass.KeyFieldName = Search_VBS_ClassAssignment.FN_LinkGUID;
				gridClass.DataBind();
			}
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			cboClassType.DataBind();		// Bind the class type
		}

		protected void GridView_CustomJSProperties(object sender, ASPxGridViewClientJSPropertiesEventArgs e)
		{
			e.Properties["cpVisibleRowCount"] = gridClass.VisibleRowCount;
		}

		protected void gridClass_DataBinding(object sender, EventArgs e)
		{
			// When the data binding happens, set up the collection
			if (cboClassType.Items.Count < 1) { return; }
			string classCode = cboClassType.SelectedItem.GetValue(VBSClassCode.FN_ClassCode).ToString();
			string classGrade = cboClassType.SelectedItem.GetValue(VBSClassCode.FN_Grade).ToString();
			_assignments = new Search_VBS_ClassAssignmentCollection("sClassCode = '" + classCode + "'");
			_assignments.Sort("LastName, FirstName");

			// Build out the assignment collection to 15
			VBSClassCodeCollection classCollection = new VBSClassCodeCollection("sClassCode = '" + classCode + "'");
			if (classCollection.Count > 0)
			{
				while (_assignments.Count < 15)
				{
					Search_VBS_ClassAssignment assign = new Search_VBS_ClassAssignment();
					assign.LinkGUID = System.Guid.NewGuid().ToString();
					assign.RawGUID = System.Guid.NewGuid().ToString();
					assign.ClassCode = classCollection[0].ClassCode;
					assign.Grade = classCollection[0].Grade;
					assign.Length = classCollection[0].Length;
					_assignments.Add(assign);
				}
			}

			int count = 1;
			foreach (Search_VBS_ClassAssignment a in _assignments)
			{
				a.Numbering = count;
				count++;
			}

			gridClass.DataSource = _assignments;
		}

		protected void byStudent_Click(object sender, EventArgs e)
		{
			gridClass.Visible = false;
			secondary.Visible = true;
		}

		protected void byClass_Click(object sender, EventArgs e)
		{
			gridClass.Visible = true;
			secondary.Visible = false;
		}

		protected void cboClassType_DataBinding(object sender, EventArgs e)
		{
			_classCode = new VBSClassCodeCollection(string.Empty);
			_classCode.Sort("Length, ClassCode");
			cboClassType.DataSource = _classCode; 
		}

		protected void cboClassType_SelectedIndexChanged(object sender, EventArgs e)
		{
			// When the index changes, bind the grid
			int test = 1;
			//gridClass.DataBind();
		}

		protected void gridClass_CellEditorInitialize(object sender, DevExpress.Web.ASPxGridViewEditorEventArgs e)
		{
			// When the cell editor initializes, handle it
			if (e.Editor is ASPxComboBox &&
				cboClassType.SelectedItem != null)
			{
				// Get the listing of students
				string classGrade = cboClassType.SelectedItem.GetValue(VBSClassCode.FN_Grade).ToString();
				Search_VBS_StudentsCollection coll = new Search_VBS_StudentsCollection("sGradeInFall = '" + classGrade.Replace("'", "''") + "'");
				coll.Sort(Search_VBS_Students.FN_FullNameLastFirst);

				ASPxComboBox combo = ((ASPxComboBox)e.Editor);
				combo.Columns.Clear();
				combo.Columns.Add(Search_VBS_Students.FN_LastName, "Last Name");
				combo.Columns.Add(Search_VBS_Students.FN_FirstName, "First Name");
				combo.Columns.Add(Search_VBS_Students.FN_GradeInFall, "Grade");

				combo.DataSource = coll;
				combo.TextField = Search_VBS_Students.FN_FullNameLastFirst;
				combo.ValueField = Search_VBS_Students.FN_ResponseID;
				combo.IncrementalFilteringMode = IncrementalFilteringMode.StartsWith;
				combo.TextFormatString = "{0}, {1} ({2})";
				combo.DataBindItems();
			}
		}

		protected void gridClass_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
		{
			// When they update, run the row back to the collection
			if (e.NewValues["ResponseID"].ToString().Trim().Length > 0)
			{
				string responseID = e.NewValues["ResponseID"].ToString().Trim();
				
				// Delete the one from the collection first
				VBSClassLinkCollection coll = new VBSClassLinkCollection("iResponseID = " + responseID + "");
				foreach (VBSClassLink link in coll)
				{
					link.Delete();
				}
				
				// Add the new one in there for the linkage that exists
				VBSClassLink linkNew = new VBSClassLink();
				linkNew.LinkGUID = System.Guid.NewGuid().ToString();
				linkNew.ResponseID = int.Parse(responseID);
				linkNew.ClassCode = cboClassType.SelectedItem.ValueString;
				linkNew.AddedByUserID = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
				linkNew.UpdatedByUserID = linkNew.AddedByUserID;

				// Update the collection
				coll.AddUpdateAll();

				// Bind the grid
				gridClass.DataBind();
			}

			// Handle the event internally
			e.Cancel = true;
		}

		protected void gridClass_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
		{
			// Shouldn't need this one - it's all done as part of the update
		}

		protected void gridClass_RowDeleted(object sender, DevExpress.Web.Data.ASPxDataDeletedEventArgs e)
		{
			// Handle this one using the checkboxes
		}

		protected void cboClassType_Init(object sender, EventArgs e)
		{
			//if (!IsCallback && !IsPostBack)
			//{
			//	cboClassType.DataBind();
			//}
		}
	}
}