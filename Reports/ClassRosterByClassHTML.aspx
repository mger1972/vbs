﻿<%@ Page Title="Class Roster - Send Email" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClassRosterByClassHTML.aspx.cs" Inherits="VBS.Reports.ClassRosterByClassHTML" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraReports.v17.1.Web, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
				<h3>Send an email to the team leaders. Edit the text below or click "Select Teams".</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script language="javascript">
		$(document).ready(function () {
			$("#MainContent_documentViewer_Splitter_0").css("background-color", "#FFFFFF");
			$("#MainContent_documentViewer_Splitter_0").css("border", "1px solid black");
			$("#MainContent_documentViewer_Splitter_1i1i0").css("border", "0px solid black");
		})
	 </script>
	<table width="100%" align="center" border="1">
		<tr>
			<td colspan="2" align="right">&nbsp;<% if (!String.IsNullOrEmpty(MessageText.Text)) { %>
				<span class="validation-summary-errors"><asp:Literal runat="server" ID="MessageText" Text="" />&nbsp; &nbsp;</span>
				<% } %></td>
		</tr>
		<tr>
			<td>
				<iframe frameborder="0" runat="server" src="HTMLEditorPageClassRoster.aspx" width="100%" height="400px"></iframe>
			</td>
		</tr>
	</table>

</asp:Content>
