﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.Parameters;
using System.Drawing.Printing;
using DevExpress.XtraReports.Web;

namespace VBS.Reports
{
	public partial class ClassRoster : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			//Search_VBS_ClassAssignmentCollection assignments = new Search_VBS_ClassAssignmentCollection("iLength IS NOT NULL AND bStudent <> 0");
			//assignments.Sort(Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);

			//// Add some unique text in there for the class
			//foreach (Search_VBS_ClassAssignment item in assignments)
			//{
			//	item.GridCustom_5 = String.Format("{0} ({1})", item.ClassCode, item.Grade);
			//}

			//rptClassRosterByStudent rpt = new rptClassRosterByStudent();

			//rpt.DataSource = assignments;
			//rpt.DisplayName = "Class Roster By Student";
			//rpt.CreateDocument();
			////rpt.PrintingSystem.Document.ScaleFactor = .7F;
			//documentViewer.Report = rpt;
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			Search_VBS_ClassAssignmentCollection assignments = new Search_VBS_ClassAssignmentCollection("iLength IS NOT NULL AND bStudent <> 0");
			assignments.Sort(Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);

			// Add some unique text in there for the class
			foreach (Search_VBS_ClassAssignment item in assignments)
			{
				item.GridCustom_5 = String.Format("{0} ({1})", item.ClassCode, item.Grade);
			}

			rptClassRosterByStudent rpt = new rptClassRosterByStudent();

			rpt.DataSource = assignments;
			rpt.DisplayName = "Class Roster By Student";

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 1.28f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}
}