﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Web;

namespace VBS.Reports
{
	public partial class rptHelperGradeViolation : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptHelperGradeViolation()
		{
			InitializeComponent();

			// Detail Fields
			lblName.DataBindings.Add("Text", null, Search_VBS_GradeHelperViolation.FN_FullNameLastFirst);
			lblHelperGrade.DataBindings.Add("Text", null, Search_VBS_GradeHelperViolation.FN_JrHelperGrade);
			lblAssignedTo.DataBindings.Add("Text", null, Search_VBS_GradeHelperViolation.FN_Grade);
			lblHighestGrade.DataBindings.Add("Text", null, Search_VBS_GradeHelperViolation.FN_HighestGradeCanTeach);
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			// Color the row if it's not right
			bool violation = (GetCurrentColumnValue(Search_VBS_GradeHelperViolation.FN_GradeDiff) != null  &&
				int.Parse(GetCurrentColumnValue(Search_VBS_GradeHelperViolation.FN_GradeDiff).ToString()) < 5);
			if (violation)
			{
				lblName.Font = new Font(lblName.Font, FontStyle.Bold);
				lblName.ForeColor = Color.Red;
				lblHelperGrade.Font = new Font(lblName.Font, FontStyle.Bold);
				lblHelperGrade.ForeColor = Color.Red;
				lblAssignedTo.Font = new Font(lblName.Font, FontStyle.Bold);
				lblAssignedTo.ForeColor = Color.Red;
				lblHighestGrade.Font = new Font(lblName.Font, FontStyle.Bold);
				lblHighestGrade.ForeColor = Color.Red;
			}
			else
			{
				lblName.Font = new Font(lblName.Font, FontStyle.Regular);
				lblName.ForeColor = Color.Black;
				lblHelperGrade.Font = new Font(lblName.Font, FontStyle.Regular);
				lblHelperGrade.ForeColor = Color.Black;
				lblAssignedTo.Font = new Font(lblName.Font, FontStyle.Regular);
				lblAssignedTo.ForeColor = Color.Black;
				lblHighestGrade.Font = new Font(lblName.Font, FontStyle.Regular);
				lblHighestGrade.ForeColor = Color.Black;
			}
		}
	}
}
