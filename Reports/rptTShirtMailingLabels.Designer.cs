﻿namespace VBS.Reports
{
    partial class rptTShirtMailingLabels
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.lblGroup = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTeacher = new DevExpress.XtraReports.UI.XRLabel();
            this.lblShirtSize = new DevExpress.XtraReports.UI.XRLabel();
            this.lblClassCode = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTch = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSS = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCC = new DevExpress.XtraReports.UI.XRLabel();
            this.lblName = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
            this.Detail.HeightF = 100F;
            this.Detail.MultiColumn.ColumnSpacing = 13.00001F;
            this.Detail.MultiColumn.ColumnWidth = 262F;
            this.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown;
            this.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
            // 
            // xrPanel1
            // 
            this.xrPanel1.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrPanel1.CanGrow = false;
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblGroup,
            this.lblTeacher,
            this.lblShirtSize,
            this.lblClassCode,
            this.lblTch,
            this.lblSS,
            this.lblCC,
            this.lblName});
            this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.SizeF = new System.Drawing.SizeF(262F, 100F);
            this.xrPanel1.StylePriority.UseBorders = false;
            // 
            // lblGroup
            // 
            this.lblGroup.CanGrow = false;
            this.lblGroup.Font = new System.Drawing.Font("Calibri", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblGroup.LocationFloat = new DevExpress.Utils.PointFloat(86.45834F, 26.99995F);
            this.lblGroup.Name = "lblGroup";
            this.lblGroup.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGroup.SizeF = new System.Drawing.SizeF(151.5833F, 23.00001F);
            this.lblGroup.StylePriority.UseFont = false;
            this.lblGroup.StylePriority.UseTextAlignment = false;
            this.lblGroup.Text = "Class Code:";
            this.lblGroup.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTeacher
            // 
            this.lblTeacher.CanGrow = false;
            this.lblTeacher.Font = new System.Drawing.Font("Calibri", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblTeacher.LocationFloat = new DevExpress.Utils.PointFloat(86.45834F, 73.00002F);
            this.lblTeacher.Name = "lblTeacher";
            this.lblTeacher.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTeacher.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblTeacher.StylePriority.UseFont = false;
            this.lblTeacher.StylePriority.UseTextAlignment = false;
            this.lblTeacher.Text = "Class Code:";
            this.lblTeacher.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTeacher.WordWrap = false;
            // 
            // lblShirtSize
            // 
            this.lblShirtSize.BackColor = System.Drawing.Color.Yellow;
            this.lblShirtSize.CanGrow = false;
            this.lblShirtSize.Font = new System.Drawing.Font("Calibri", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblShirtSize.LocationFloat = new DevExpress.Utils.PointFloat(86.4584F, 50.00003F);
            this.lblShirtSize.Name = "lblShirtSize";
            this.lblShirtSize.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblShirtSize.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblShirtSize.StylePriority.UseBackColor = false;
            this.lblShirtSize.StylePriority.UseFont = false;
            this.lblShirtSize.StylePriority.UseTextAlignment = false;
            this.lblShirtSize.Text = "Class Code:";
            this.lblShirtSize.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblClassCode
            // 
            this.lblClassCode.CanGrow = false;
            this.lblClassCode.Font = new System.Drawing.Font("Calibri", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblClassCode.LocationFloat = new DevExpress.Utils.PointFloat(86.4584F, 26.99995F);
            this.lblClassCode.Name = "lblClassCode";
            this.lblClassCode.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblClassCode.SizeF = new System.Drawing.SizeF(100F, 23F);
            this.lblClassCode.StylePriority.UseFont = false;
            this.lblClassCode.Text = "Class Code:";
            // 
            // lblTch
            // 
            this.lblTch.CanGrow = false;
            this.lblTch.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.lblTch.LocationFloat = new DevExpress.Utils.PointFloat(0F, 73F);
            this.lblTch.Name = "lblTch";
            this.lblTch.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTch.SizeF = new System.Drawing.SizeF(86.45834F, 23F);
            this.lblTch.StylePriority.UseFont = false;
            this.lblTch.StylePriority.UseTextAlignment = false;
            this.lblTch.Text = "Teacher(s):";
            this.lblTch.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSS
            // 
            this.lblSS.CanGrow = false;
            this.lblSS.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.lblSS.LocationFloat = new DevExpress.Utils.PointFloat(0F, 49.99998F);
            this.lblSS.Name = "lblSS";
            this.lblSS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSS.SizeF = new System.Drawing.SizeF(86.45833F, 23F);
            this.lblSS.StylePriority.UseFont = false;
            this.lblSS.StylePriority.UseTextAlignment = false;
            this.lblSS.Text = "Shirt Size:";
            this.lblSS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblCC
            // 
            this.lblCC.CanGrow = false;
            this.lblCC.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Italic);
            this.lblCC.LocationFloat = new DevExpress.Utils.PointFloat(0F, 26.99996F);
            this.lblCC.Name = "lblCC";
            this.lblCC.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCC.SizeF = new System.Drawing.SizeF(86.45834F, 23F);
            this.lblCC.StylePriority.UseFont = false;
            this.lblCC.StylePriority.UseTextAlignment = false;
            this.lblCC.Text = "Group Name:";
            this.lblCC.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblName
            // 
            this.lblName.CanGrow = false;
            this.lblName.Font = new System.Drawing.Font("Calibri", 14F, System.Drawing.FontStyle.Bold);
            this.lblName.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.lblName.Name = "lblName";
            this.lblName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblName.SizeF = new System.Drawing.SizeF(262F, 26.99996F);
            this.lblName.StylePriority.UseFont = false;
            this.lblName.StylePriority.UseTextAlignment = false;
            this.lblName.Text = "lblName";
            this.lblName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 50F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Font = new System.Drawing.Font("Calibri", 12F);
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.StylePriority.UseFont = false;
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // rptTShirtMailingLabels
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(19, 0, 50, 0);
            this.ReportPrintOptions.DetailCountOnEmptyDataSource = 30;
            this.Version = "15.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRLabel lblTeacher;
        private DevExpress.XtraReports.UI.XRLabel lblShirtSize;
        private DevExpress.XtraReports.UI.XRLabel lblClassCode;
        private DevExpress.XtraReports.UI.XRLabel lblTch;
        private DevExpress.XtraReports.UI.XRLabel lblSS;
        private DevExpress.XtraReports.UI.XRLabel lblCC;
        private DevExpress.XtraReports.UI.XRLabel lblName;
        private DevExpress.XtraReports.UI.XRLabel lblGroup;
    }
}
