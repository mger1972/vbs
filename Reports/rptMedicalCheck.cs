﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
    public partial class rptMedicalCheck : DevExpress.XtraReports.UI.XtraReport
    {
        private Color _detailBackColor = Color.White;

		public rptMedicalCheck()
		{
			InitializeComponent();
		}
		
		public rptMedicalCheck(int totalCount)
        {
            InitializeComponent();

            // Detail Fields
            lblName.DataBindings.Add("Text", null, Search_VBS_MedicalCheck.FN_MedicalCheckNameFormatted);
            lblName.DataBindings.Add("NavigateUrl", null, Search_VBS_MedicalCheck.FN_EmailHyperlink);

            lblContactPhone.DataBindings.Add("Text", null, Search_VBS_MedicalCheck.FN_HomePhone);
            lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_MedicalCheck.FN_MobilePhone);
            lblEmail.DataBindings.Add("Text", null, Search_VBS_MedicalCheck.FN_Email);
            lblEmail.DataBindings.Add("NavigateUrl", null, Search_VBS_MedicalCheck.FN_EmailHyperlink);
            lblDateSubmitted.DataBindings.Add("Text", null, Search_VBS_MedicalCheck.FN_DateSubmitted);
            lblDateSubmitted.DataBindings[0].FormatString = "{0:MM/dd/yyyy hh:mm tt}";

            lblClass.DataBindings.Add("Text", null, Search_VBS_MedicalCheck.FN_ClassCode);

			lblTotalCount.Text = String.Format("Total Count: {0:###,##0}", totalCount);
		}

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            pnlColor.BackColor = _detailBackColor;
            _detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

            string emailHyperlink = (GetCurrentColumnValue(Search_VBS_MedicalCheck.FN_EmailHyperlink) != null ?
                GetCurrentColumnValue(Search_VBS_MedicalCheck.FN_EmailHyperlink).ToString() : string.Empty);
            if (!String.IsNullOrEmpty(emailHyperlink))
            {
                lblName.Font = new Font(lblName.Font, FontStyle.Underline);
                lblName.ForeColor = Color.Blue;
                lblEmail.Font = new Font(lblName.Font, FontStyle.Underline);
                lblEmail.ForeColor = Color.Blue;
            }
            else
            {
                lblName.Font = new Font(lblName.Font, FontStyle.Regular);
                lblName.ForeColor = Color.Black;
                lblEmail.Font = new Font(lblName.Font, FontStyle.Regular);
                lblEmail.ForeColor = Color.Black;
            }
        }
    }
}
