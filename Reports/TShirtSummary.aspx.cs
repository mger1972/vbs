﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class TShirtSummary : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			//Search_VBS_TShirtSummaryCollection summary = new Search_VBS_TShirtSummaryCollection(string.Empty);
			//summary.Sort(Search_VBS_TShirtSummary.FN_Grouper + ", " +
			//	Search_VBS_TShirtSummary.FN_Length + ", " +
			//	Search_VBS_TShirtSummary.FN_Desc);

			//// Go through the list and set up the desc
			//foreach (Search_VBS_TShirtSummary item in summary)
			//{
			//	if (item.Type.ToLower().StartsWith("c"))
			//	{
			//		item.GridCustom_1 = "Class: " + item.Desc;
			//	}
			//	else if (item.Type.ToLower().StartsWith("g"))
			//	{
			//		item.GridCustom_1 = "Team: " + item.Desc;
			//	}
			//	else if (item.Type.ToLower().StartsWith("u"))
			//	{
			//		item.GridCustom_1 = "Unassigned";
			//	}
			//}

			//rptTShirtSummary rpt = new rptTShirtSummary();
			//rpt.DataSource = summary;
			//rpt.DisplayName = "T-Shirt Summary";
			//rpt.CreateDocument();
			//documentViewer.Report = rpt;
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			Search_VBS_TShirtSummaryCollection summary = new Search_VBS_TShirtSummaryCollection(string.Empty);
			summary.Sort(Search_VBS_TShirtSummary.FN_Grouper + ", " +
				Search_VBS_TShirtSummary.FN_Length + ", " +
				Search_VBS_TShirtSummary.FN_Desc);

			// Go through the list and set up the desc
			foreach (Search_VBS_TShirtSummary item in summary)
			{
				if (item.Type.ToLower().StartsWith("c"))
				{
					item.GridCustom_1 = "Class: " + item.Desc;
				}
				else if (item.Type.ToLower().StartsWith("g"))
				{
					item.GridCustom_1 = "Team: " + item.Desc;
				}
				else if (item.Type.ToLower().StartsWith("u"))
				{
					item.GridCustom_1 = "Unassigned";
				}
			}

			// Find out how many shirts don't have an assigned size
			//string noShirtMessage = string.Empty;
			//long baseShirtCount = VBSDataCollection.GetCountFromDB("(sSheetType LIKE 'b%' OR sSheetType LIKE 'a%') AND LTRIM(RTRIM(sShirtSize)) <> ''");
			//long baseShirtCountEmpty = VBSDataCollection.GetCountFromDB("(sSheetType LIKE 'b%' OR sSheetType LIKE 'a%') AND LTRIM(RTRIM(sShirtSize)) = ''");
			//long volunteerShirtCount = VBSDataCollection.GetCountFromDB("sSheetType LIKE 'v%' AND LTRIM(RTRIM(sShirtSize)) <> ''");
			//long volunteerShirtCountEmpty = VBSDataCollection.GetCountFromDB("sSheetType LIKE 'v%' AND LTRIM(RTRIM(sShirtSize)) = ''");
			//long volunteerChildShirtCount = VBSVolunteerChildCollection.GetCountFromDB("sSheetType LIKE 'v%' AND LTRIM(RTRIM(sShirtSize)) <> ''");
			//long volunteerChildCountEmpty = VBSVolunteerChildCollection.GetCountFromDB("sSheetType LIKE 'v%' AND LTRIM(RTRIM(sShirtSize)) = ''");

			rptTShirtSummary rpt = new rptTShirtSummary();
			rpt.DataSource = summary;
			rpt.DisplayName = "T-Shirt Summary";

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.98f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}
}