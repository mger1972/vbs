﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class EmailListing : System.Web.UI.Page
	{
		protected StringBuilder _sbList = new StringBuilder();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Set up the type
			if (cboType.SelectedIndex == -1)
			{
				cboType.SelectedIndex = 0;
				cboType_SelectedIndexChanged(this, null);
			}

			// Build out the list of emails for this group
			_sbList = new StringBuilder(BuildEmailList(cboType.SelectedItem.Text));

			// Tell if the user saw something
			if (!IsPostBack && !IsCallback)
			{
				Logging.LogInformation("User viewed email list for: " + cboType.SelectedItem.Text, null, string.Empty, HttpContext.Current.User.Identity.Name);
			}
		}

		/// <summary>
		/// Build the email list based on the selected type
		/// </summary>
		/// <param name="type">The type to build the list for</param>
		/// <returns>The finished list</returns>
		protected string BuildEmailList(string type)
		{
			StringBuilder sb = new StringBuilder();
			StringBuilder sbEmail = new StringBuilder();
			string rtv = string.Empty;
			string userName = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
			List<string> existingEmails = new List<string>();
			if (String.IsNullOrEmpty(userName))
			{
				return "Can't send email: User not logged in.";
			}

			switch (type)
			{
				case "All Students":
					#region All Students
					Search_VBS_StudentsCollection students = new Search_VBS_StudentsCollection(string.Empty);
					students.Sort(Search_VBS_Students.FN_LastName + ", " + Search_VBS_Students.FN_FirstName);
					foreach (Search_VBS_Students item in students)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.ParentFirst + " " + item.ParentLast + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Students
					break;
				case "All Adult Volunteers":
					#region All Adult Volunteers
					Search_VBS_VolunteerCollection volunteersAdult = new Search_VBS_VolunteerCollection("bIsAdult <> 0");
					volunteersAdult.Sort(Search_VBS_Volunteer.FN_LastName + ", " + Search_VBS_Volunteer.FN_FirstName);
					foreach (Search_VBS_Volunteer item in volunteersAdult)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Adult Volunteers
					break;
				case "All Under 18 Volunteers":
					#region All Under 18 Volunteers
					Search_VBS_VolunteerCollection volunteersUnder18 = new Search_VBS_VolunteerCollection("bIsAdult = 0");
					volunteersUnder18.Sort(Search_VBS_Volunteer.FN_LastName + ", " + Search_VBS_Volunteer.FN_FirstName);
					foreach (Search_VBS_Volunteer item in volunteersUnder18)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Under 18 Volunteers
					break;
				case "All Under 18 Volunteers - Jr Class Leaders":
					#region All Under 18 Volunteers - Jr Class Leaders
					Search_VBS_VolunteerCollection volunteersUnder18ClassLeaders = new Search_VBS_VolunteerCollection("bIsAdult = 0 AND sClassCode IS NOT NULL");
					volunteersUnder18ClassLeaders.Sort(Search_VBS_Volunteer.FN_LastName + ", " + Search_VBS_Volunteer.FN_FirstName);
					foreach (Search_VBS_Volunteer item in volunteersUnder18ClassLeaders)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Under 18 Volunteers - Jr Class Leaders
					break;
				case "All Under 18 Volunteers - Non-Class Leaders":
					#region All Under 18 Volunteers - Non-Class Leaders
					Search_VBS_VolunteerCollection volunteersUnder18NonClassLeaders = new Search_VBS_VolunteerCollection("bIsAdult = 0 AND sGroupName IS NOT NULL");
					volunteersUnder18NonClassLeaders.Sort(Search_VBS_Volunteer.FN_LastName + ", " + Search_VBS_Volunteer.FN_FirstName);
					foreach (Search_VBS_Volunteer item in volunteersUnder18NonClassLeaders)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Under 18 Volunteers - Non-Class Leaders
					break;
				case "All Allergy People":
					#region All Allergy People
					Search_VBS_AllergiesCollection allergyPeople = new Search_VBS_AllergiesCollection(string.Empty);
					allergyPeople.Sort(Search_VBS_Allergies.FN_LastName + ", " + Search_VBS_Allergies.FN_FirstName);
					foreach (Search_VBS_Allergies item in allergyPeople)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Allergy People
					break;
				case "All Team Leaders":
					#region All Team Leaders
					Search_VBS_TeamLeaderListCollection teamLeaderList = new Search_VBS_TeamLeaderListCollection("bIsAdult <> 0");
					teamLeaderList.Sort(Search_VBS_TeamLeaderList.FN_LastName + ", " + Search_VBS_TeamLeaderList.FN_FirstName);
					foreach (Search_VBS_TeamLeaderList item in teamLeaderList)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Team Leaders
					break;
				case "All Class Junior Helpers":
					#region All Class Junior Helpers
					Search_VBS_TeamLeaderListCollection juniorHelperList = new Search_VBS_TeamLeaderListCollection("bIsAdult = 0");
					juniorHelperList.Sort(Search_VBS_TeamLeaderList.FN_LastName + ", " + Search_VBS_TeamLeaderList.FN_FirstName);
					foreach (Search_VBS_TeamLeaderList item in juniorHelperList)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Class Junior Helpers
					break;
				case "All Group Coordinators":
					#region All Group Coordinators
					Search_VBS_VolunteerCollection groupCoordinators = new Search_VBS_VolunteerCollection("sGroupGUID IS NOT NULL AND bTeamCoordinator <> 0");
					groupCoordinators.Sort(Search_VBS_Volunteer.FN_LastName + ", " + Search_VBS_Volunteer.FN_FirstName);
					foreach (Search_VBS_Volunteer item in groupCoordinators)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Group Coordinators
					break;
				case "All Group Volunteers":
					#region All Group Volunteers
					Search_VBS_VolunteerCollection groupVolunteers = new Search_VBS_VolunteerCollection("sGroupGUID IS NOT NULL");
					groupVolunteers.Sort(Search_VBS_Volunteer.FN_LastName + ", " + Search_VBS_Volunteer.FN_FirstName);
					foreach (Search_VBS_Volunteer item in groupVolunteers)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Group Volunteers
					break;
				case "All Volunteers With Nursery Age Children":
					#region All Volunteers With Nursery Age Children
					Search_VBS_VolunteerChildCollection groupNursery = new Search_VBS_VolunteerChildCollection("sCurrentAssignment LIKE 'N%'");
					groupNursery.Sort(Search_VBS_VolunteerChild.FN_LastName + ", " + Search_VBS_VolunteerChild.FN_FirstName);
					foreach (Search_VBS_VolunteerChild item in groupNursery)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Volunteers With Nursery Age Children
					break;
				case "All Volunteers With Walker Age Children":
					#region All Volunteers With Walker Age Children
					Search_VBS_VolunteerChildCollection groupWalker = new Search_VBS_VolunteerChildCollection("sCurrentAssignment LIKE 'W%'");
					groupWalker.Sort(Search_VBS_VolunteerChild.FN_LastName + ", " + Search_VBS_VolunteerChild.FN_FirstName);
					foreach (Search_VBS_VolunteerChild item in groupWalker)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Volunteers With Walker Age Children
					break;
				case "All Volunteers With Pre-school Age Children":
					#region All Volunteers With Pre-school Age Children
					Search_VBS_VolunteerChildCollection groupPreschool = new Search_VBS_VolunteerChildCollection("sCurrentAssignment LIKE 'P%'");
					groupPreschool.Sort(Search_VBS_VolunteerChild.FN_LastName + ", " + Search_VBS_VolunteerChild.FN_FirstName);
					foreach (Search_VBS_VolunteerChild item in groupPreschool)
					{
						if (!existingEmails.Contains(item.Email.ToLower()))
						{
							bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
							if (isEmail)
							{
								sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
								if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
								sbEmail.Append(item.Email);
								existingEmails.Add(item.Email.ToLower());
							}
						}
					}
					#endregion All Volunteers With Pre-school Age Children
					break;
				default:
					if (type.ToLower().StartsWith("class:"))
					{
						// Build the list for the class
						#region Class
						string className = type.Substring(6).Trim();
						Search_VBS_VolunteerCollection singleClass = new Search_VBS_VolunteerCollection("sClassCode = '" + className.Replace("'", "''") + "'");
						singleClass.Sort(Search_VBS_Volunteer.FN_LastName + ", " + Search_VBS_Volunteer.FN_FirstName);
						foreach (Search_VBS_Volunteer item in singleClass)
						{
							if (!existingEmails.Contains(item.Email.ToLower()))
							{
								bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
								if (isEmail)
								{
									sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
									if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
									sbEmail.Append(item.Email);
									existingEmails.Add(item.Email.ToLower());
								}
							}
						}
						#endregion Class
					}
					else if (type.ToLower().StartsWith("team:"))
					{
						// Build the list for the team
						#region Team
						string teamName = type.Substring(5).Trim();
						Search_VBS_VolunteerCollection singleTeam = new Search_VBS_VolunteerCollection("sGroupName = '" + teamName.Replace("'", "''") + "'");
						singleTeam.Sort(Search_VBS_Volunteer.FN_LastName + ", " + Search_VBS_Volunteer.FN_FirstName);
						foreach (Search_VBS_Volunteer item in singleTeam)
						{
							if (!existingEmails.Contains(item.Email.ToLower()))
							{
								bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
								if (isEmail)
								{
									sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
									if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
									sbEmail.Append(item.Email);
									existingEmails.Add(item.Email.ToLower());
								}
							}
						}
						#endregion Team
					}
					else
					{
						return "Type not found.";
					}
					break;
			}

			if (sbEmail.ToString().Length > 2000)
			{
				// The email string is too long to generate
				sbEmail = new StringBuilder("The <em>automated</em> email string is too long to generate. Highlight and copy the emails below to send an email.<br />&nbsp;<br />" + 
					"<b>PLEASE PASTE THE EMAIL ADDRESSES AS A BCC (BLIND CARBON COPY) SO PEOPLE CAN'T SEE OTHER PEOPLE'S ADDRESSES.</b>");
				rtv = sbEmail.ToString() + "<br>&nbsp;<br>" + sb.ToString().Replace(" (", "<").Replace(")", ">");
			}
			else if (sb.ToString().Length > 0)
			{
				rtv = "<a href=\"mailto:" + userName + "&bcc=" + sbEmail.ToString() + "\">Click here to send an email to: </a><br />&nbsp;" + sb.ToString();
			}
			if (String.IsNullOrEmpty(rtv))
			{
				rtv = "No records/emails were found for this group.";
			}

			return rtv;
		}

		protected void Page_Init(object sender, EventArgs e)
		{
			// Create the teacher bindings
			//GetVolunteers();
			//cboTeamLeader1.DataBind();
			//cboTeamLeader2.DataBind();
			//cboHelper1.DataBind();
			//cboHelper2.DataBind();

			cboType.DataBind();		// Bind the class type

			// Check the Session for a data table
			//if ((_assignments == null ||
			//	_assignments.Count == 0) &&
			//	Session["ClassAssignmentsCollection"] != null)
			//{
			//	_assignments = Session["ClassAssignmentsCollection"] as Search_VBS_ClassAssignmentCollection;
			//}
		}

		protected void cboType_DataBinding(object sender, EventArgs e)
		{
			DataTable types = new DataTable();
			types.Columns.Add("RawType", typeof(System.String));
			types.Columns.Add("TypeText", typeof(System.String));

			// Add the types
			types.Rows.Add(new object[] { "All Students".Replace(" ", ""), "All Students" });
			types.Rows.Add(new object[] { "All Adult Volunteers".Replace(" ", ""), "All Adult Volunteers" });
			types.Rows.Add(new object[] { "All Under 18 Volunteers".Replace(" ", ""), "All Under 18 Volunteers" });
			types.Rows.Add(new object[] { "All Under 18 Volunteers - Jr Class Leaders".Replace(" ", ""), "All Under 18 Volunteers - Jr Class Leaders" });
			types.Rows.Add(new object[] { "All Under 18 Volunteers - Non-Class Leaders".Replace(" ", ""), "All Under 18 Volunteers - Non-Class Leaders" });
			types.Rows.Add(new object[] { "All Allergy People".Replace(" ", ""), "All Allergy People" });
			types.Rows.Add(new object[] { "All Team Leaders".Replace(" ", ""), "All Team Leaders" });
			types.Rows.Add(new object[] { "All Class Junior Helpers".Replace(" ", ""), "All Class Junior Helpers" });
			types.Rows.Add(new object[] { "All Group Coordinators".Replace(" ", ""), "All Group Coordinators" });
			types.Rows.Add(new object[] { "All Group Volunteers".Replace(" ", ""), "All Group Volunteers" });
			types.Rows.Add(new object[] { "All Volunteers With Nursery Age Children".Replace(" ", ""), "All Volunteers With Nursery Age Children" });
			types.Rows.Add(new object[] { "All Volunteers With Walker Age Children".Replace(" ", ""), "All Volunteers With Walker Age Children" });
			types.Rows.Add(new object[] { "All Volunteers With Pre-school Age Children".Replace(" ", ""), "All Volunteers With Pre-school Age Children" });

			// Add the classes
			Search_VBS_ClassCodeCollection _classCodes = new Search_VBS_ClassCodeCollection(string.Empty);
			_classCodes.Sort(Search_VBS_ClassCode.FN_Length + ", " + Search_VBS_ClassCode.FN_ClassCode);
			foreach (Search_VBS_ClassCode item in _classCodes)
			{
				types.Rows.Add(new object[] { ("Class: " + item.ClassCode).Replace(" ", ""), "Class: " + item.ClassCode });
			}

			// Add the groups
			Search_VBS_GroupCollection _groups = new Search_VBS_GroupCollection(string.Empty);
			_groups.Sort(Search_VBS_Group.FN_GroupName);
			foreach (Search_VBS_Group item in _groups)
			{
				types.Rows.Add(new object[] { ("Team: " + item.GroupName).Replace(" ", ""), "Team: " + item.GroupName });
			}

			cboType.DataSource = types;
		}

		///// <summary>
		///// Populate the types collection
		///// </summary>
		//protected void GetTypes()
		//{
		//	if (!IsPostBack || Session["EmailTypeCollection"] == null)
		//	{
		//		_teachers = new Search_VBS_VolunteerCollection("bIsAdult <> 0");
		//		_teachers.Sort(Search_VBS_Volunteer.FN_FullNameLastFirstUpper);
		//		Session["EmailTypeCollection"] = _teachers;
		//	}
		//}

		protected void cboType_SelectedIndexChanged(object sender, EventArgs e)
		{
			//// When the index changes, bind the grid
			string text = cboType.SelectedItem.Text;
			int test = 1;
			//Session["ClassAssignmentsCollection"] = null;

			//// Get the dataset from the database
			//GetData();
			//gridClass.KeyFieldName = Search_VBS_ClassAssignment.FN_LinkGUID;
			//gridClass.DataSource = _assignments;
			//gridClass.DataBind();

			//// Log the information
			//Logging.LogInformation("User viewed class: " + cboClassType.SelectedItem.Text, null, string.Empty, Session["UserName"].ToString());

			//// Bind the combos
			//if (!String.IsNullOrEmpty(_selectedTeacher1)) { cboTeamLeader1.Value = _selectedTeacher1; } else { cboTeamLeader1.SelectedIndex = -1; }
			//if (!String.IsNullOrEmpty(_selectedTeacher2)) { cboTeamLeader2.Value = _selectedTeacher2; } else { cboTeamLeader2.SelectedIndex = -1; }
			//if (!String.IsNullOrEmpty(_selectedHelper1)) { cboHelper1.Value = _selectedHelper1; } else { cboHelper1.SelectedIndex = -1; }
			//if (!String.IsNullOrEmpty(_selectedHelper2)) { cboHelper2.Value = _selectedHelper2; } else { cboHelper2.SelectedIndex = -1; }

			//ResetSelectionScript();
		}
	}
}