﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptSchoolDetail : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptSchoolDetail()
		{
			InitializeComponent();

			// Set the bindings
			lblSchool.DataBindings.Add("Text", null, Search_VBS_SchoolListing.FN_School);
			lblCount.DataBindings.Add("Text", null, Search_VBS_SchoolListing.FN_Count);
			lblPercentage.DataBindings.Add("Text", null, Search_VBS_SchoolListing.FN_GridCustom_4);
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);
		}
	}
}
