﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Web;

namespace VBS.Reports
{
	public partial class rptTShirtDetail : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptTShirtDetail()
		{
			InitializeComponent();
		}

		public rptTShirtDetail(string reportTitle)
		{
			InitializeComponent();

			// Set up the report
			if (reportTitle.ToLower().Contains("class"))
			{
				xrLabel1.Text = "Team / Group:";
				lblLastName.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_GridCustom_5);
			}
			else
			{
				xrLabel1.Text = "Name Starts With:";
				lblLastName.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_LastNameStartsWith);
			}

			xrLabel2.Text = reportTitle;

			// Set the bindings
			lblName.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_FullNameLastFirst);
			//lblLastName.DataBindings.Add("Bookmark", null, Search_VBS_TShirtList.FN_Left1LastName);

			// Detail Fields
			//lblName.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_StudentNameFormatted);
			lblClass.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_GridCustom_5);
			lblShirtSize.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_Size);

			// Bind the groupheader
			ghGroup.GroupFields.Add(new GroupField(Search_VBS_TShirtList.FN_GridCustom_7, XRColumnSortOrder.Ascending));
			if (reportTitle.ToLower().Contains("class"))
			{
				ghGroup.GroupFields.Add(new GroupField(Search_VBS_TShirtList.FN_Length, XRColumnSortOrder.Ascending));
				ghGroup.GroupFields.Add(new GroupField(Search_VBS_TShirtList.FN_GridCustom_5, XRColumnSortOrder.Ascending));			
			}
			else
			{
				ghGroup.GroupFields.Add(new GroupField(Search_VBS_TShirtList.FN_LastNameStartsWith, XRColumnSortOrder.Ascending));
			}
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);
		}

		private void rptClassRosterSinglePage_ParametersRequestBeforeShow(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
		{
			//foreach (ParameterInfo info in e.ParametersInformation)
			//{
			//	if (info.Parameter.Name.ToLower() == "ClassCodeParam".ToLower())
			//	{
			//		ASPxGridLookup lookUpEdit = new ASPxGridLookup();
			//		//LookUpEdit lookUpEdit = new LookUpEdit();
			//		//lookUpEdit.Properties.DataSource = dataSet.Categories;
			//		lookUpEdit.Properties.DisplayMember = "CategoryName";
			//		lookUpEdit.Properties.ValueMember = "CategoryID";
			//		lookUpEdit.Properties.Columns.Add(new LookUpColumnInfo("CategoryName", 0, "Category Name"));
			//		lookUpEdit.Properties.NullText = "<Select Category>";

			//		info.Editor = lookUpEdit;
			//		info.Parameter.Value = DBNull.Value;
			//	}
			//}
		}
	}
}
