﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class VolunteerChildCare : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			//Search_VBS_VolunteerChildCollection summary = new Search_VBS_VolunteerChildCollection(string.Empty);
			//summary.Sort(Search_VBS_VolunteerChild.FN_LastName + ", " +
			//	Search_VBS_VolunteerChild.FN_FirstName + ", " +
			//	Search_VBS_VolunteerChild.FN_ChildLastName + ", " + 
			//	Search_VBS_VolunteerChild.FN_ChildFirstName);

			//rptVolunteerChildCare rpt = new rptVolunteerChildCare();
			//rpt.DataSource = summary;
			//rpt.DisplayName = "Volunteer Child Care";

			//rpt.CreateDocument();
			//documentViewer.Report = rpt;
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			Search_VBS_VolunteerChildCollection summary = new Search_VBS_VolunteerChildCollection(string.Empty);
			summary.Sort(Search_VBS_VolunteerChild.FN_LastName + ", " +
				Search_VBS_VolunteerChild.FN_FirstName + ", " +
				Search_VBS_VolunteerChild.FN_ChildLastName + ", " +
				Search_VBS_VolunteerChild.FN_ChildFirstName);

			rptVolunteerChildCare rpt = new rptVolunteerChildCare();
			rpt.DataSource = summary;
			rpt.DisplayName = "Volunteer Child Care";

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 1.28f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}
}