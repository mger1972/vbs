﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Web;

namespace VBS.Reports
{
	public partial class rptClassCount : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		//private int _totalCount = 0;

		//public int TotalCount
		//{
		//	get { return _totalCount; }
		//	set { _totalCount = value; }
		//}

		public rptClassCount()
		{
			InitializeComponent();

			// Detail Fields
			lblClassCode.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_ClassCode);
			lblGrade.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_Grade);
			lblRotation.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_Rotation);

			lblTeacherCount.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_TeacherCount);
			lblJuniorHelperCount.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_JuniorHelperCount);
			lblStudentCount.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_StudentCount);

			lblTotalTeacher.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_TeacherCount);
			lblTotalJuniorHelper.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_JuniorHelperCount);
			lblTotalStudent.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_StudentCount);

			//lblLineNum.DataBindings.Add("Text", null, Search_VBS_ClassCode.FN_GridCustom_8);
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);
		}

		private void rptClassRosterSinglePage_ParametersRequestBeforeShow(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
		{
			//foreach (ParameterInfo info in e.ParametersInformation)
			//{
			//	if (info.Parameter.Name.ToLower() == "ClassCodeParam".ToLower())
			//	{
			//		ASPxGridLookup lookUpEdit = new ASPxGridLookup();
			//		//LookUpEdit lookUpEdit = new LookUpEdit();
			//		//lookUpEdit.Properties.DataSource = dataSet.Categories;
			//		lookUpEdit.Properties.DisplayMember = "CategoryName";
			//		lookUpEdit.Properties.ValueMember = "CategoryID";
			//		lookUpEdit.Properties.Columns.Add(new LookUpColumnInfo("CategoryName", 0, "Category Name"));
			//		lookUpEdit.Properties.NullText = "<Select Category>";

			//		info.Editor = lookUpEdit;
			//		info.Parameter.Value = DBNull.Value;
			//	}
			//}
		}

		private void ReportFooter_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			// Put in the total
			//lblTotalSum.Text = _totalCount.ToString("###,##0");
		}
	}
}
