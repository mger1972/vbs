﻿using DevExpress.Web;
using DevExpress.Export;
using DevExpress.XtraPrinting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class ExcelExtract : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Set the datasource on the grid
			if (!IsPostBack)
			{
				grid.KeyFieldName = Search_VBS_Students.FN_RawGUID;
				grid.DataBind();
			}

			// Turn off editing in the grid
			foreach (GridViewColumn col in grid.Columns)
			{
				if (col is GridViewDataColumn)
				{
					((GridViewDataColumn)col).EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
				}
			}
		}

		protected void grid_DataBinding(object sender, EventArgs e)
		{
			Search_VBS_StudentsCollection coll = new Search_VBS_StudentsCollection(string.Empty);
			coll.Sort("LastName, FirstName");
			grid.DataSource = coll;
		}

		protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
		{
			if (e.Parameters != string.Empty)
			{
				(sender as ASPxGridView).Columns[e.Parameters].Visible = false;
				(sender as ASPxGridView).Columns[e.Parameters].ShowInCustomizationForm = true;
			}
		}

		protected void grid_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void btnXlsxExport_Click(object sender, EventArgs e)
		{
			gridExport.WriteXlsxToResponse("VBS_StudentExtract", new XlsxExportOptionsEx { ExportType = ExportType.DataAware });
		}

		protected void btnXlsExport_Click(object sender, EventArgs e)
		{
			gridExport.WriteXlsxToResponse("VBS_StudentExtract", new XlsxExportOptionsEx { ExportType = ExportType.DataAware });
		}
	}
}