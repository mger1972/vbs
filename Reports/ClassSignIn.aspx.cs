﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.Web;
using System.Drawing.Printing;

namespace VBS.Reports
{
	public partial class ClassSignIn : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			Search_VBS_ClassAssignmentCollection assignments = new Search_VBS_ClassAssignmentCollection("iLength IS NOT NULL");
			assignments.Sort(Search_VBS_ClassAssignment.FN_Length + ", " +
				Search_VBS_ClassAssignment.FN_ClassCode + ", " +
				Search_VBS_ClassAssignment.FN_Teacher + " DESC, " +
				Search_VBS_ClassAssignment.FN_JuniorHelper + " DESC, " +
				Search_VBS_ClassAssignment.FN_Student + " DESC, " +
				Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);

			// Count up the values in the collection
			Dictionary<string, int> counts = new Dictionary<string, int>();
			foreach (Search_VBS_ClassAssignment item in assignments)
			{
				if (String.IsNullOrEmpty(item.ClassCode)) { continue; }
				if (!counts.ContainsKey(item.ClassCode)) { counts.Add(item.ClassCode, 0); }
				counts[item.ClassCode]++;
			}

			string lastValue = string.Empty;
			string teacher1 = string.Empty, teacher2 = string.Empty;
			string helper1 = string.Empty, helper2 = string.Empty;
			int counter = 1, kidCounter = 1;
			foreach (Search_VBS_ClassAssignment a in assignments)
			{
				if (lastValue.ToLower() != a.ClassCode.ToLower())
				{
					counter = 1;
					kidCounter = 1;
					teacher1 = string.Empty;
					teacher2 = string.Empty;
					helper1 = string.Empty;
					helper2 = string.Empty;
					lastValue = a.ClassCode;
				}

				if (String.IsNullOrEmpty(teacher1) && a.Teacher) { teacher1 = a.FirstName + " " + a.LastName; }
				else if (String.IsNullOrEmpty(teacher2) && a.Teacher) { teacher2 = a.FirstName + " " + a.LastName; }
				else if (String.IsNullOrEmpty(helper1) && a.JuniorHelper) { helper1 = a.FirstName + " " + a.LastName; }
				else if (String.IsNullOrEmpty(helper2) && a.JuniorHelper) { helper2 = a.FirstName + " " + a.LastName; }
				else if (a.Student)
				{
					a.GridCustom_7 = kidCounter.ToString() + ".";
					kidCounter++;
					a.GridCustom_3 = teacher1;
					a.GridCustom_4 = teacher2;
					a.GridCustom_5 = helper1;
					a.GridCustom_6 = helper2;

					//a.GridCustom_0 = "_______";
				}

				//a.GridCustom_0 = a.FirstName + " " + a.LastName;
				//a.GridCustom_1 = a.ParentFirst + " " + a.ParentLast;
				//a.GridCustom_7 = "Class: " + a.ClassCode;
				a.GridCustom_8 = counter.ToString() + ".";
				a.GridCustom_9 = "Rotation: " + a.Rotation;
				if (!String.IsNullOrEmpty(a.ClassCode)) { a.GridCustom_2 = counts[a.ClassCode].ToString(); }
				//a.GridCustom_9 = (a.Grade.Length > 3 ? a.Grade : a.Grade + " Grade");
				counter++;
			}

			// Remove the ones that are teacher or helper
			for (int i = assignments.Count - 1; i >= 0; i--)
			{
				if (assignments[i].Teacher ||
					assignments[i].JuniorHelper)
				{
					assignments.RemoveAt(i);
				}
			}

			rptClassSignIn2 rpt = new rptClassSignIn2();

			rpt.DataSource = assignments;
			rpt.DisplayName = "Classroom Sign-In Sheets";

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 1.28f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}
}