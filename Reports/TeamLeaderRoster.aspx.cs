﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.Web;
using System.Drawing.Printing;

namespace VBS.Reports
{
	public partial class TeacherRoster : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

            if (Session["ShowStudents"] == null) { Session["ShowStudents"] = true; }

            //// Get the collections we'll be using to build both reports
            //Search_VBS_VolunteerCollection teamLeaderCollection = new Search_VBS_VolunteerCollection("sClassCode IS NOT NULL");
            //teamLeaderCollection.Sort(Search_VBS_Volunteer.FN_Length + ", " +
            //	Search_VBS_Volunteer.FN_ClassCode + ", " +
            //	Search_VBS_Volunteer.FN_Teacher + " DESC, " +
            //	Search_VBS_Volunteer.FN_JuniorHelper + " DESC, " +
            //	Search_VBS_Volunteer.FN_FullNameLastFirstUpper);
            //foreach (Search_VBS_Volunteer item in teamLeaderCollection)
            //{
            //	item.GridCustom_1 = "Class: " + item.ClassCodeWithDesc;
            //}

                //Search_VBS_VolunteerCollection groupCollection = new Search_VBS_VolunteerCollection("sGroupGUID IS NOT NULL");
                //groupCollection.Sort(Search_VBS_Volunteer.FN_GroupName + ", " +
                //	Search_VBS_Volunteer.FN_TeamCoordinator + " DESC, " +
                //	Search_VBS_Volunteer.FN_FullNameLastFirstUpper);
                //foreach (Search_VBS_Volunteer item in groupCollection)
                //{
                //	item.GridCustom_1 = "Team: " + item.GroupName;
                //}

                //// Run the team leader roster first
                //// This is going to be the teachers in the classes
                //rptTeamLeaderRoster rptteamleaderroster = new rptTeamLeaderRoster();
                //rptteamleaderroster.DataSource = teamLeaderCollection;
                //rptteamleaderroster.DisplayName = "Team Leaders";
                //rptteamleaderroster.CreateDocument();

                //// Next run the group listing
                //rptGroupRoster rptgrouproster = new rptGroupRoster();
                //rptgrouproster.DataSource = groupCollection;
                //rptgrouproster.DisplayName = "Groups";
                //rptgrouproster.CreateDocument();

                //// Add all the pages from the first report
                //rptteamleaderroster.Pages.AddRange(rptgrouproster.Pages);
                //if (teamLeaderCollection.Count == 0)
                //{
                //	rptteamleaderroster.Pages.RemoveAt(0);
                //}
                //if (groupCollection.Count == 0)
                //{
                //	rptteamleaderroster.Pages.RemoveAt(rptteamleaderroster.Pages.Count - 1);
                //}

                //// Reset all the page numbers if they're set
                //rptteamleaderroster.PrintingSystem.ContinuousPageNumbering = true;

                // Show the report
                //documentViewer.Report = rptteamleaderroster;
            }

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

            bool studentsChecked = true;
            if (Session["ShowStudents"] != null) { studentsChecked = (bool)Session["ShowStudents"]; }

            // Get the collections we'll be using to build both reports
            Search_VBS_ClassAssignmentCollection teamLeaderCollection = new Search_VBS_ClassAssignmentCollection("sClassCode IS NOT NULL");
            if (!studentsChecked)
            {
                teamLeaderCollection = new Search_VBS_ClassAssignmentCollection("sClassCode IS NOT NULL AND sSheetType LIKE 'v%'");
            }
			teamLeaderCollection.Sort(Search_VBS_Volunteer.FN_Length + ", " +
				Search_VBS_ClassAssignment.FN_ClassCode + ", " +
				Search_VBS_ClassAssignment.FN_Teacher + " DESC, " +
				Search_VBS_ClassAssignment.FN_JuniorHelper + " DESC, " +
				Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);
			foreach (Search_VBS_ClassAssignment item in teamLeaderCollection)
			{
				item.GridCustom_1 = "Class: " + item.ClassCodeWithDesc;
				if (!String.IsNullOrEmpty(item.ClassColor))
				{
					item.ClassColor = item.ClassColor.ToUpper();
				}
			}

			Search_VBS_VolunteerCollection groupCollection = new Search_VBS_VolunteerCollection("sGroupGUID IS NOT NULL");
			groupCollection.Sort(Search_VBS_Volunteer.FN_GroupName + ", " +
				Search_VBS_Volunteer.FN_TeamCoordinator + " DESC, " +
				Search_VBS_Volunteer.FN_FullNameLastFirstUpper);
			Dictionary<string, int> _adultCount = new Dictionary<string, int>();
			Dictionary<string, int> _juniorCount = new Dictionary<string, int>();
			foreach (Search_VBS_Volunteer item in groupCollection)
			{
				item.GridCustom_1 = "Team: " + item.GroupName;
				if (!_adultCount.ContainsKey(item.GroupName)) { _adultCount.Add(item.GroupName, 0); }
				if (!_juniorCount.ContainsKey(item.GroupName)) { _juniorCount.Add(item.GroupName, 0); }
				if (item.IsAdult.HasValue && item.IsAdult.Value)
				{
					_adultCount[item.GroupName]++;
				}
				else
				{
					_juniorCount[item.GroupName]++;
				}
			}
			foreach (Search_VBS_Volunteer item in groupCollection)
			{
				item.GridCustom_2 = _adultCount[item.GroupName].ToString();
				item.GridCustom_3 = _juniorCount[item.GroupName].ToString();
				//if (!String.IsNullOrEmpty(item.ClassColor))
				//{
				//	item.ClassColor = item.ClassColor.ToUpper();
				//}
			}

			// Run the team leader roster first
			// This is going to be the teachers in the classes
			rptTeamLeaderRoster rptteamleaderroster = new rptTeamLeaderRoster();
			rptteamleaderroster.DataSource = teamLeaderCollection;
			rptteamleaderroster.DisplayName = "Team Leaders";
			//rptteamleaderroster.CreateDocument();

			// Next run the group listing
			rptGroupRoster rptgrouproster = new rptGroupRoster();
			rptgrouproster.DataSource = groupCollection;
			rptgrouproster.DisplayName = "Groups";
			//rptgrouproster.CreateDocument();

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidthTeamLeader = rptteamleaderroster.PageWidth * 1.28f;
				float scaleFactorTeamLeader = reportViewerWidth / reportWidthTeamLeader;
				float reportWidthGroup = rptgrouproster.PageWidth * 1.28f;
				float scaleFactorGroup = reportViewerWidth / reportWidthGroup;

				Margins newMarginsTeamLeader = new Margins(Convert.ToInt32(rptteamleaderroster.Margins.Left * scaleFactorTeamLeader),
					Convert.ToInt32(rptteamleaderroster.Margins.Right * scaleFactorTeamLeader),
					Convert.ToInt32(rptteamleaderroster.Margins.Top * scaleFactorTeamLeader),
					Convert.ToInt32(rptteamleaderroster.Margins.Bottom * scaleFactorTeamLeader));
				rptteamleaderroster.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rptteamleaderroster.PageWidth = (int)(rptteamleaderroster.PageWidth * scaleFactorTeamLeader);
				rptteamleaderroster.PageHeight = (int)(rptteamleaderroster.PageHeight * scaleFactorTeamLeader);
				rptteamleaderroster.Margins = newMarginsTeamLeader;
				rptteamleaderroster.CreateDocument();

				rptteamleaderroster.PrintingSystem.Document.ScaleFactor = scaleFactorTeamLeader;

				Margins newMarginsGroup = new Margins(Convert.ToInt32(rptgrouproster.Margins.Left * scaleFactorGroup),
					Convert.ToInt32(rptgrouproster.Margins.Right * scaleFactorGroup),
					Convert.ToInt32(rptgrouproster.Margins.Top * scaleFactorGroup),
					Convert.ToInt32(rptgrouproster.Margins.Bottom * scaleFactorGroup));
				rptgrouproster.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rptgrouproster.PageWidth = (int)(rptgrouproster.PageWidth * scaleFactorGroup);
				rptgrouproster.PageHeight = (int)(rptgrouproster.PageHeight * scaleFactorGroup);
				rptgrouproster.Margins = newMarginsGroup;
				rptgrouproster.CreateDocument();

				rptgrouproster.PrintingSystem.Document.ScaleFactor = scaleFactorGroup;

				// Add all the pages from the first report
				rptteamleaderroster.Pages.AddRange(rptgrouproster.Pages);
				if (teamLeaderCollection.Count == 0)
				{
					rptteamleaderroster.Pages.RemoveAt(0);
				}
				if (groupCollection.Count == 0)
				{
					rptteamleaderroster.Pages.RemoveAt(rptteamleaderroster.Pages.Count - 1);
				}

				// Reset all the page numbers if they're set
				rptteamleaderroster.PrintingSystem.ContinuousPageNumbering = true;
			}

			documentViewer.Report = rptteamleaderroster;
		}

        protected void chkShowStudents_CheckedChanged(object sender, EventArgs e)
        {
            // When the checkbox changes, reset the data
            Session["ShowStudents"] = chkShowStudents.Checked;
        }
    }
}