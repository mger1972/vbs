﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class ClassRosterByClassSelectTeams : System.Web.UI.Page
	{
		protected Search_VBS_ClassAssignmentCollection _classAssignments = new Search_VBS_ClassAssignmentCollection();
		protected DataTable _dt = new DataTable();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Get the data
			_dt = (DataTable)Session["SendClassRosterLeaderInfo"];

			// Bind the grid
			GetData();
			gridList.KeyFieldName = Search_VBS_ResponseToGroup.FN_LinkGUID;
			gridList.DataSource = _dt;
			gridList.DataBind();

			foreach (GridViewDataColumn col in gridList.Columns)
			{
				if (col.FieldName != "CheckedInGrid")
				{
					col.EditFormSettings.Visible = DevExpress.Utils.DefaultBoolean.False;
				}
			}
		}

		/// <summary>
		/// Get the data from the database
		/// </summary>
		private void GetData()
		{
			if (Session["SendClassRosterLeaderInfo"] != null)
			{
				_dt = (DataTable)Session["SendClassRosterLeaderInfo"];
				return;
			}
			else if (!IsPostBack || Session["SendClassRosterLeaderInfo"] == null)
			{
				// Get the assignments
				_classAssignments = new Search_VBS_ClassAssignmentCollection(string.Empty);
				_classAssignments.Sort(Search_VBS_ClassAssignment.FN_Length + ", " + Search_VBS_ClassAssignment.FN_ClassCode);
				Dictionary<string, int> classes = new Dictionary<string, int>();
				foreach (Search_VBS_ClassAssignment item in _classAssignments)
				{
					if (!classes.ContainsKey(item.ClassCode)) { classes.Add(item.ClassCode, 0); }
					classes[item.ClassCode]++;
				}

				// Load up the datatable
				_dt = new DataTable();
				_dt.Columns.Add("LinkGUID", typeof(System.String));
				_dt.Columns.Add("CheckedInGrid", typeof(System.Boolean));
				_dt.Columns.Add("ClassCode", typeof(System.String));
				_dt.Columns.Add("Count", typeof(System.Int32));
				foreach (KeyValuePair<string, int> kvp in classes)
				{
					_dt.Rows.Add(new object[] {
						System.Guid.NewGuid().ToString(),
						false,
						kvp.Key,
						kvp.Value,
					});
				}

				Session["SendClassRosterLeaderInfo"] = _dt;
			}

			// Bring it back
			_dt = (DataTable)Session["SendClassRosterLeaderInfo"];
		}

		//protected void btnSendEmail_Click(object sender, EventArgs e)
		//{
		//	// Send out an email
		//	//Utils.SendEMailAsJen("Mark Gerlach", "mark@gerlach5.com", "test", "testing the email", string.Empty, Session["UserName"].ToString());
		//}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			gridList.UpdateEdit();
		}

		protected void btnCheckUncheckAll_Click(object sender, EventArgs e)
		{
			// Check or uncheck all in the grid
			bool chkd = (_dt.Rows.Count > 0 ? (bool)_dt.Rows[0]["CheckedInGrid"] : false);
			foreach (DataRow row in _dt.Rows)
			{
				row["CheckedInGrid"] = !chkd;
			}
			Session["SendClassRosterLeaderInfo"] = _dt;
			gridList.DataSource = _dt;
			gridList.DataBind();
			//Response.Redirect("ClassRosterByClassSelectTeams.aspx");
			//gridList.edit();
		}

		protected void chk_Init(object sender, EventArgs e)
		{
			ASPxCheckBox chk = sender as ASPxCheckBox;
			GridViewDataItemTemplateContainer container = chk.NamingContainer as GridViewDataItemTemplateContainer;

			chk.ClientSideEvents.CheckedChanged = String.Format("function (s, e) {{ cb.PerformCallback('{0}|' + s.GetChecked()); }}", container.KeyValue);
		}

		protected void cb_Callback(object source, CallbackEventArgs e)
		{
			String[] p = e.Parameter.Split('|');

			DataRow[] vw = _dt.Select("LinkGUID = '" + p[0] + "'");
			vw[0]["CheckedInGrid"] = bool.Parse(p[1]);
			//MyObject obj = session.GetObjectByKey<MyObject>(Convert.ToInt32(p[0])); // get the record from the Session
			//obj.Active = Convert.ToBoolean(p[1]);

			//obj.Save();
		}

		protected void gridList_CommandButtonInitialize(object sender, ASPxGridViewCommandButtonEventArgs e)
		{
			// Hide the buttons
			if (e.ButtonType == ColumnCommandButtonType.Update)
			{
				e.Visible = false;
			}
			else if (e.ButtonType == ColumnCommandButtonType.Cancel)
			{
				e.Visible = false;
			}
		}

		protected void gridList_BatchUpdate(object sender, DevExpress.Web.Data.ASPxDataBatchUpdateEventArgs e)
		{
			foreach (DevExpress.Web.Data.ASPxDataUpdateValues val in e.UpdateValues)
			{
				OrderedDictionary keys = val.Keys;
				OrderedDictionary newVals = val.NewValues;
				OrderedDictionary oldVals = val.OldValues;

				// Go through the collection and see if you can find the one they're dealing with
				string linkGUID = (keys["LinkGUID"] != null && !String.IsNullOrEmpty(keys["LinkGUID"].ToString()) ?
					keys["LinkGUID"].ToString().Trim() : string.Empty);
				bool chkd = (bool)newVals["CheckedInGrid"];
				foreach (DataRow row in _dt.Rows)
				{
					if (row["LinkGUID"].ToString().ToLower().Equals(linkGUID.ToLower()))
					{
						// Here's the one we need
						row["CheckedInGrid"] = chkd;
						break;
					}
				}
			}

			// Build a string of all the classes
			string outVal = string.Empty;
			foreach (DataRow row in _dt.Rows)
			{
				if ((bool)row["CheckedInGrid"])
				{
					outVal += "|" + row["ClassCode"].ToString();
				}
			}
			if (String.IsNullOrEmpty(outVal))
			{
				// There are no classes checked
				MessageText.Text = "You must check at least one value to continue.";
				return;	
			}

			// Build and send the emails
			StringBuilder sb = new StringBuilder();
			foreach (DataRow row in _dt.Rows)
			{
				if ((bool)row["CheckedInGrid"])
				{
					string classCode = row["ClassCode"].ToString();

					// Get the collections we'll be using to build both reports
					Search_VBS_ClassAssignmentCollection classCollection = new Search_VBS_ClassAssignmentCollection("sClassCode IS NOT NULL AND sClassCode = '" + classCode.Replace("'", "''") + "'");
					classCollection.Sort(Search_VBS_Volunteer.FN_Length + ", " +
						Search_VBS_ClassAssignment.FN_ClassCode + ", " +
						Search_VBS_ClassAssignment.FN_Teacher + " DESC, " +
						Search_VBS_ClassAssignment.FN_JuniorHelper + " DESC, " +
						Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);
					//foreach (Search_VBS_ClassAssignment item in classCollection)
					//{
					//	item.GridCustom_1 = "Class: " + item.ClassCodeWithDesc;
					//	item.ClassColor = item.ClassColor.ToUpper();
					//}

					// Count up the values in the collection
					Dictionary<string, int> counts = new Dictionary<string, int>();
					foreach (Search_VBS_ClassAssignment item in classCollection)
					{
						if (String.IsNullOrEmpty(item.ClassCode)) { continue; }
						if (!counts.ContainsKey(item.ClassCode)) { counts.Add(item.ClassCode, 0); }
						counts[item.ClassCode]++;
					}

					// This is going to be the teachers in the classes
					rptClassRosterByClass rpt = new rptClassRosterByClass();

					string lastValue = string.Empty;
					string teacher1 = string.Empty, teacher2 = string.Empty;
					string helper1 = string.Empty, helper2 = string.Empty;
					int counter = 1, kidCounter = 1;
					foreach (Search_VBS_ClassAssignment a in classCollection)
					{
						if (lastValue.ToLower() != a.ClassCode.ToLower())
						{
							counter = 1;
							kidCounter = 1;
							teacher1 = string.Empty;
							teacher2 = string.Empty;
							helper1 = string.Empty;
							helper2 = string.Empty;
							lastValue = a.ClassCode;
						}

						if (String.IsNullOrEmpty(teacher1) && a.Teacher)
						{
							teacher1 = a.FirstName + " " + a.LastName;
							teacher1 += "\r\n" + a.MobilePhone + "\r\n" + a.Email;
						}
						else if (String.IsNullOrEmpty(teacher2) && a.Teacher)
						{
							teacher2 = a.FirstName + " " + a.LastName;
							teacher2 += "\r\n" + a.MobilePhone + "\r\n" + a.Email;
						}
						else if (String.IsNullOrEmpty(helper1) && a.JuniorHelper)
						{
							helper1 = a.FirstName + " " + a.LastName;
							helper1 += "\r\n" + a.MobilePhone + "\r\n" + a.Email;
						}
						else if (String.IsNullOrEmpty(helper2) && a.JuniorHelper)
						{
							helper2 = a.FirstName + " " + a.LastName;
							helper2 += "\r\n" + a.MobilePhone + "\r\n" + a.Email;
						}
						else if (a.Student)
						{
							a.GridCustom_7 = kidCounter.ToString() + ".";
							kidCounter++;
							a.GridCustom_3 = teacher1;
							a.GridCustom_4 = teacher2;
							a.GridCustom_5 = helper1;
							a.GridCustom_6 = helper2;
						}

						//a.GridCustom_0 = a.FirstName + " " + a.LastName;
						//a.GridCustom_1 = a.ParentFirst + " " + a.ParentLast;
						//a.GridCustom_7 = "Class: " + a.ClassCode;
						a.GridCustom_8 = counter.ToString() + ".";
						a.GridCustom_9 = "Rotation: " + a.Rotation;
						if (!String.IsNullOrEmpty(a.ClassColor))
						{
							a.GridCustom_1 = "Team Color: " + a.ClassColor.ToUpper();
						}
						if (!String.IsNullOrEmpty(a.ClassCode)) { a.GridCustom_2 = counts[a.ClassCode].ToString(); }
						//a.GridCustom_9 = (a.Grade.Length > 3 ? a.Grade : a.Grade + " Grade");
						counter++;
					}

					// Remove the ones that are teacher or helper
					// Clone the set first
					Search_VBS_ClassAssignmentCollection tempColl = new Search_VBS_ClassAssignmentCollection();
					for (int i = classCollection.Count - 1; i >= 0; i--)
					{
						if (classCollection[i].Teacher ||
							classCollection[i].JuniorHelper)
						{
							tempColl.Add(classCollection[i]);
							classCollection.RemoveAt(i);
						}
					}

					rpt.DataSource = classCollection;
					rpt.DisplayName = "Class Roster - " + classCode;
					rpt.CreateDocument();

					// Reset all the page numbers if they're set
					rpt.PrintingSystem.ContinuousPageNumbering = true;

					// Send the file
					MemoryStream strm = new MemoryStream();
					rpt.ExportToPdf(strm);
					strm.Seek(0, System.IO.SeekOrigin.Begin);

					// Get the people who are the leaders in this class
					List<MailAddress> leaders = new List<MailAddress>();
					List<string> existingLeaders = new List<string>();
					foreach (Search_VBS_ClassAssignment v in tempColl)
					{
						if (v.Teacher && !existingLeaders.Contains(v.Email.ToLower()))
						{
							existingLeaders.Add(v.Email.ToLower());
							leaders.Add(new MailAddress(v.Email.ToLower(), v.FullNameFirstLast));
						}
						//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.Email.ToLower())) { leaders.Add(v.Email.ToLower()); }
						//if (v.JuniorHelper.HasValue && v.JuniorHelper.Value && !leaders.Contains(v.JrHelperEmail.ToLower())) { leaders.Add(v.JrHelperEmail.ToLower()); }
					}

					//string body = "Attached is a copy of the latest class " + classCode + " roster.";
					//string body = "Attached is a copy of the latest class " + classCode + " roster.\r\n\r\n" +
					//	"Please contact any new volunteers that have been assigned to the team.\r\n\r\n" +
					//	"If you have any questions, please feel free to contact the Team Leader Coordinator.";
					string body = Session["ClassRosterHTML"].ToString();

					// Replace the class code
					body = body.Replace("||className||", "<b>" + classCode + "</b>");

					if (leaders.Count == 0)
					{
						body += "\r\n\r\n" + "THERE ARE NO LEADERS IN THIS CLASS!!";
					}

					List<MailAddress> ccList = new List<MailAddress>();
					//ccList.Add(new MailAddress(Session["UserName"].ToString()));

					// Add some admin
					List<MailAddress> bccList = new List<MailAddress>();
					bool jenFound = false, markFound = false;
					foreach (MailAddress addr in bccList)
					{
						if (addr.Address == "jen@gerlach5.com") { jenFound = true; }
						if (addr.Address == "mark@gerlach5.com") { markFound = true; }
					}
					if (!jenFound)
					{
						bccList.Add(new MailAddress("jen@gerlach5.com", "Jennifer Gerlach"));
					}
					if (!markFound)
					{
						bccList.Add(new MailAddress("mark@gerlach5.com", "Mark Gerlach"));
					}

					// Send the email
					System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(strm, "Class_" + classCode.Replace(" ", "") + "_Roster.pdf", "application/pdf");
					Utils.SendEMailAsJen(leaders,
						ccList,
						bccList,
						"Class " + classCode + " Roster", body, string.Empty,
							new List<System.Net.Mail.Attachment>(new System.Net.Mail.Attachment[] {
						attachment,
					}), Session["UserName"].ToString());

					// Kill off the memory string
					strm.Close();

					if (sb.ToString().Length > 0) { sb.Append("&nbsp; &nbsp;<br />"); }
					sb.Append("Message sent for class: " + classCode);
				}
			}

			// Send the user back to the class roster page
			Response.Write("<script>top.location='ClassRosterByClass.aspx';parent.location='ClassRosterByClass.aspx';</script>");

			//MessageText.Text = sb.ToString();

			// Handle the event internally
			e.Handled = true;
		}
	}
}