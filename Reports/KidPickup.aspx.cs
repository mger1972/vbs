﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing.Printing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DevExpress.XtraReports.Web;

namespace VBS.Reports
{
	public partial class KidPickup : System.Web.UI.Page
	{
		protected KidPickupReportType _type = KidPickupReportType.GroupByClass;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Figure out which type of report this is
			_type = KidPickupReportType.GroupByClass;      // Set the default
			foreach (string key in Request.QueryString.AllKeys)
			{
				if (key.ToLower().Equals("type"))
				{
					// Get the value
					string val = Request.QueryString[key];
					if (val.ToLower().StartsWith("os")) { _type = KidPickupReportType.TotalRoster; }
					else { _type = KidPickupReportType.GroupByClass; }
					break;
				}
			}

			if (_type == KidPickupReportType.GroupByClass)
			{
				Search_VBS_ClassAssignmentCollection assignments = new Search_VBS_ClassAssignmentCollection("iLength IS NOT NULL");
				assignments.Sort(Search_VBS_ClassAssignment.FN_Length + ", " +
					Search_VBS_ClassAssignment.FN_ClassCode + ", " +
					Search_VBS_ClassAssignment.FN_Teacher + " DESC, " +
					Search_VBS_ClassAssignment.FN_JuniorHelper + " DESC, " +
					Search_VBS_ClassAssignment.FN_Student + " DESC, " +
					Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);

				// Count up the values in the collection
				Dictionary<string, int> counts = new Dictionary<string, int>();
				foreach (Search_VBS_ClassAssignment item in assignments)
				{
					// Fix the line wraps

#region Pickup Person
					// Pickup Person 1
					if (!String.IsNullOrEmpty(item.PickupPerson1))
					{
						item.PickupPerson1 = item.PickupPerson1.Replace("(", "").Replace(")", "").Replace("-", "");
						string phoneNum = string.Empty;
						int numCount = 0, currPos = item.PickupPerson1.Length - 1;
						for (int i = item.PickupPerson1.Length - 1; i >= 0; i--)
						{
							if (item.PickupPerson1.Substring(i, 1).IndexOfAny("0123456789".ToCharArray()) > -1)
							{
								currPos = i - 1;
								phoneNum = item.PickupPerson1.Substring(i, 1) + phoneNum;
								if (phoneNum.Length >= 10) { break; }
							}
						}
						if (phoneNum.Length == 10)
						{
							item.PickupPerson1 = item.PickupPerson1.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 3) + "-" +
								phoneNum.Substring(6, 4) : "");
						}
						else if (phoneNum.Length == 7)
						{
							item.PickupPerson1 = item.PickupPerson1.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 4) : "");
						}
					}

					// Pickup Person 2
					if (!String.IsNullOrEmpty(item.PickupPerson2))
					{
						item.PickupPerson2 = item.PickupPerson2.Replace("(", "").Replace(")", "").Replace("-", "");
						string phoneNum = string.Empty;
						int numCount = 0, currPos = item.PickupPerson2.Length - 1;
						for (int i = item.PickupPerson2.Length - 1; i >= 0; i--)
						{
							if (item.PickupPerson2.Substring(i, 1).IndexOfAny("0123456789".ToCharArray()) > -1)
							{
								currPos = i - 1;
								phoneNum = item.PickupPerson2.Substring(i, 1) + phoneNum;
								if (phoneNum.Length >= 10) { break; }
							}
						}
						if (phoneNum.Length == 10)
						{
							item.PickupPerson2 = item.PickupPerson2.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 3) + "-" +
								phoneNum.Substring(6, 4) : "");
						}
						else if (phoneNum.Length == 7)
						{
							item.PickupPerson2 = item.PickupPerson2.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 4) : "");
						}
					}

					// Pickup Person 3
					if (!String.IsNullOrEmpty(item.PickupPerson3))
					{
						item.PickupPerson3 = item.PickupPerson3.Replace("(", "").Replace(")", "").Replace("-", "");
						string phoneNum = string.Empty;
						int numCount = 0, currPos = item.PickupPerson3.Length - 1;
						for (int i = item.PickupPerson3.Length - 1; i >= 0; i--)
						{
							if (item.PickupPerson3.Substring(i, 1).IndexOfAny("0123456789".ToCharArray()) > -1)
							{
								currPos = i - 1;
								phoneNum = item.PickupPerson3.Substring(i, 1) + phoneNum;
								if (phoneNum.Length >= 10) { break; }
							}
						}
						if (phoneNum.Length == 10)
						{
							item.PickupPerson3 = item.PickupPerson3.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 3) + "-" +
								phoneNum.Substring(6, 4) : "");
						}
						else if (phoneNum.Length == 7)
						{
							item.PickupPerson3 = item.PickupPerson3.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 4) : "");
						}
					}
#endregion Pickup Person

					// Do some other maint
					if (String.IsNullOrEmpty(item.ClassCode)) { continue; }
					if (!counts.ContainsKey(item.ClassCode)) { counts.Add(item.ClassCode, 0); }
					counts[item.ClassCode]++;
				}

				string lastValue = string.Empty;
				string teacher1 = string.Empty, teacher2 = string.Empty;
				string helper1 = string.Empty, helper2 = string.Empty;
				int counter = 1, kidCounter = 1;
				foreach (Search_VBS_ClassAssignment a in assignments)
				{
					if (lastValue.ToLower() != a.ClassCode.ToLower())
					{
						counter = 1;
						kidCounter = 1;
						teacher1 = string.Empty;
						teacher2 = string.Empty;
						helper1 = string.Empty;
						helper2 = string.Empty;
						lastValue = a.ClassCode;
					}

					if (String.IsNullOrEmpty(teacher1) && a.Teacher) { teacher1 = a.FirstName + " " + a.LastName; }
					else if (String.IsNullOrEmpty(teacher2) && a.Teacher) { teacher2 = a.FirstName + " " + a.LastName; }
					else if (String.IsNullOrEmpty(helper1) && a.JuniorHelper) { helper1 = a.FirstName + " " + a.LastName; }
					else if (String.IsNullOrEmpty(helper2) && a.JuniorHelper) { helper2 = a.FirstName + " " + a.LastName; }
					else if (a.Student)
					{
						a.GridCustom_7 = kidCounter.ToString() + ".";
						kidCounter++;
						a.GridCustom_3 = teacher1;
						a.GridCustom_4 = teacher2;
						a.GridCustom_5 = helper1;
						a.GridCustom_6 = helper2;
					}

					//a.GridCustom_0 = a.FirstName + " " + a.LastName;
					//a.GridCustom_1 = a.ParentFirst + " " + a.ParentLast;
					//a.GridCustom_7 = "Class: " + a.ClassCode;
					a.GridCustom_8 = counter.ToString() + ".";
					a.GridCustom_9 = "Rotation: " + a.Rotation;
					if (!String.IsNullOrEmpty(a.ClassCode)) { a.GridCustom_2 = counts[a.ClassCode].ToString(); }
					//a.GridCustom_9 = (a.Grade.Length > 3 ? a.Grade : a.Grade + " Grade");
					counter++;
				}

				// Remove the ones that are teacher or helper
				for (int i = assignments.Count - 1; i >= 0; i--)
				{
					if (assignments[i].Teacher ||
						assignments[i].JuniorHelper)
					{
						assignments.RemoveAt(i);
					}
				}

				rptKidPickupByClass rpt = new rptKidPickupByClass();

				rpt.DataSource = assignments;
				rpt.DisplayName = "Pickup List (By Class)";

				int reportViewerWidth;
				if (Request["ReportViewerWidth"] != null &&
					Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
				{
					float reportWidth = rpt.PageWidth * 1.28f;
					float scaleFactor = reportViewerWidth / reportWidth;

					Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
						Convert.ToInt32(rpt.Margins.Right * scaleFactor),
						Convert.ToInt32(rpt.Margins.Top * scaleFactor),
						Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
					rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
					rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
					rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
					rpt.Margins = newMargins;
					rpt.CreateDocument();

					rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
				}

				documentViewer.Report = rpt;
			}
			else if (_type == KidPickupReportType.TotalRoster)
			{
				Search_VBS_ClassAssignmentCollection assignments = new Search_VBS_ClassAssignmentCollection("iLength IS NOT NULL AND bStudent <> 0");
				assignments.Sort(Search_VBS_ClassAssignment.FN_FullNameLastFirstUpper);

				// Add some unique text in there for the class
				foreach (Search_VBS_ClassAssignment item in assignments)
				{
					item.GridCustom_5 = String.Format("{0} ({1})", item.ClassCode, item.Grade);

					#region Pickup Person
					// Pickup Person 1
					if (!String.IsNullOrEmpty(item.PickupPerson1))
					{
						item.PickupPerson1 = item.PickupPerson1.Replace("(", "").Replace(")", "").Replace("-", "");
						string phoneNum = string.Empty;
						int numCount = 0, currPos = item.PickupPerson1.Length - 1;
						for (int i = item.PickupPerson1.Length - 1; i >= 0; i--)
						{
							if (item.PickupPerson1.Substring(i, 1).IndexOfAny("0123456789".ToCharArray()) > -1)
							{
								currPos = i - 1;
								phoneNum = item.PickupPerson1.Substring(i, 1) + phoneNum;
								if (phoneNum.Length >= 10) { break; }
							}
						}
						if (phoneNum.Length == 10)
						{
							item.PickupPerson1 = item.PickupPerson1.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 3) + "-" +
								phoneNum.Substring(6, 4) : "");
						}
						else if (phoneNum.Length == 7)
						{
							item.PickupPerson1 = item.PickupPerson1.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 4) : "");
						}
					}

					// Pickup Person 2
					if (!String.IsNullOrEmpty(item.PickupPerson2))
					{
						item.PickupPerson2 = item.PickupPerson2.Replace("(", "").Replace(")", "").Replace("-", "");
						string phoneNum = string.Empty;
						int numCount = 0, currPos = item.PickupPerson2.Length - 1;
						for (int i = item.PickupPerson2.Length - 1; i >= 0; i--)
						{
							if (item.PickupPerson2.Substring(i, 1).IndexOfAny("0123456789".ToCharArray()) > -1)
							{
								currPos = i - 1;
								phoneNum = item.PickupPerson2.Substring(i, 1) + phoneNum;
								if (phoneNum.Length >= 10) { break; }
							}
						}
						if (phoneNum.Length == 10)
						{
							item.PickupPerson2 = item.PickupPerson2.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 3) + "-" +
								phoneNum.Substring(6, 4) : "");
						}
						else if (phoneNum.Length == 7)
						{
							item.PickupPerson2 = item.PickupPerson2.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 4) : "");
						}
					}

					// Pickup Person 3
					if (!String.IsNullOrEmpty(item.PickupPerson3))
					{
						item.PickupPerson3 = item.PickupPerson3.Replace("(", "").Replace(")", "").Replace("-", "");
						string phoneNum = string.Empty;
						int numCount = 0, currPos = item.PickupPerson3.Length - 1;
						for (int i = item.PickupPerson3.Length - 1; i >= 0; i--)
						{
							if (item.PickupPerson3.Substring(i, 1).IndexOfAny("0123456789".ToCharArray()) > -1)
							{
								currPos = i - 1;
								phoneNum = item.PickupPerson3.Substring(i, 1) + phoneNum;
								if (phoneNum.Length >= 10) { break; }
							}
						}
						if (phoneNum.Length == 10)
						{
							item.PickupPerson3 = item.PickupPerson3.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 3) + "-" +
								phoneNum.Substring(6, 4) : "");
						}
						else if (phoneNum.Length == 7)
						{
							item.PickupPerson3 = item.PickupPerson3.Substring(0, currPos) +
								(!String.IsNullOrEmpty(phoneNum) ? "\r\n" +
								phoneNum.Substring(0, 3) + "-" +
								phoneNum.Substring(3, 4) : "");
						}
					}
					#endregion Pickup Person
				}

				rptKidPickupByStudent rpt = new rptKidPickupByStudent();

				rpt.DataSource = assignments;
				rpt.DisplayName = "Pickup List (By Student)";

				int reportViewerWidth;
				if (Request["ReportViewerWidth"] != null &&
					Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
				{
					float reportWidth = rpt.PageWidth * 1.28f;
					float scaleFactor = reportViewerWidth / reportWidth;

					Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
						Convert.ToInt32(rpt.Margins.Right * scaleFactor),
						Convert.ToInt32(rpt.Margins.Top * scaleFactor),
						Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
					rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
					rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
					rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
					rpt.Margins = newMargins;
					rpt.CreateDocument();

					rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
				}

				documentViewer.Report = rpt;
			}
		}
	}

	public enum KidPickupReportType : int
	{
		GroupByClass = 0,
		TotalRoster,
	}
}