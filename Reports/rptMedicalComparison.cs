﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using VBS;

/// <summary>
/// Summary description for rptMedicalComparison
/// </summary>
public class rptMedicalComparison : DevExpress.XtraReports.UI.XtraReport
{
    private BottomMarginBand BottomMargin;
    private XRLabel lblHomePhone;
    private XRLabel xrLabel3;
    private XRPanel xrPanel1;
    private XRCheckBox chkMedRelForm;
    private XRLabel lblMobilePhone;
    private XRLabel lblName;
    private XRLabel lblEMail;
    private XRLabel lblType;
    private XRPageInfo xrPageInfo1;
    private XRLabel xrLabel15;
    private XRLabel rptTitle;
    private XRLabel xrLabel5;
    private XRLabel xrLabel1;
    private ReportHeaderBand ReportHeader;
    private XRPictureBox picHeader;
    private PageFooterBand PageFooter;
    private XRPageInfo xrPageInfo2;
    private TopMarginBand TopMargin;
    private PageHeaderBand PageHeader;
    private XRLabel xrLabel2;
    private XRLabel xrLabel14;
    private DetailBand Detail;

    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;
    private XRLabel lblClass;
    private XRLabel xrLabel4;
	private ReportFooterBand ReportFooter;
	private XRLabel lblTotalCount;
	private Color _detailBackColor = Color.White;

    public rptMedicalComparison()
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
    }

    public rptMedicalComparison(string reportTitle, int totalCount)
    {
        InitializeComponent();
        //
        // TODO: Add constructor logic here
        //
        // Set the title
        rptTitle.Text = reportTitle;

        // Set the bindings
        lblName.DataBindings.Add("Text", null, Search_VBS_MedicalComparison.FN_FullNameLastFirst);
        lblName.DataBindings.Add("NavigateUrl", null, Search_VBS_MedicalComparison.FN_GridCustom_5);
        lblName.DataBindings.Add("Target", null, Search_VBS_MedicalComparison.FN_GridCustom_6);

        lblEMail.DataBindings.Add("Text", null, Search_VBS_MedicalComparison.FN_Email);
        lblEMail.DataBindings.Add("NavigateUrl", null, Search_VBS_MedicalComparison.FN_EmailHyperlink);
        lblType.DataBindings.Add("Text", null, Search_VBS_MedicalComparison.FN_Type);

        lblHomePhone.DataBindings.Add("Text", null, Search_VBS_MedicalComparison.FN_HomePhone);
        lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_MedicalComparison.FN_MobilePhone);

        chkMedRelForm.DataBindings.Add("Checked", null, Search_VBS_MedicalComparison.FN_MedicalReleaseForm);

        lblClass.DataBindings.Add("Text", null, Search_VBS_MedicalComparison.FN_ClassCode);

		lblTotalCount.Text = String.Format("Total Count: {0:###,##0}", totalCount);
	}

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMedicalComparison));
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.lblHomePhone = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.lblClass = new DevExpress.XtraReports.UI.XRLabel();
			this.chkMedRelForm = new DevExpress.XtraReports.UI.XRCheckBox();
			this.lblMobilePhone = new DevExpress.XtraReports.UI.XRLabel();
			this.lblName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblEMail = new DevExpress.XtraReports.UI.XRLabel();
			this.lblType = new DevExpress.XtraReports.UI.XRLabel();
			this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
			this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.picHeader = new DevExpress.XtraReports.UI.XRPictureBox();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
			this.lblTotalCount = new DevExpress.XtraReports.UI.XRLabel();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// BottomMargin
			// 
			this.BottomMargin.HeightF = 50F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// lblHomePhone
			// 
			this.lblHomePhone.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHomePhone.LocationFloat = new DevExpress.Utils.PointFloat(235.208F, 0F);
			this.lblHomePhone.Name = "lblHomePhone";
			this.lblHomePhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHomePhone.SizeF = new System.Drawing.SizeF(108.5417F, 15F);
			this.lblHomePhone.StylePriority.UseFont = false;
			this.lblHomePhone.StylePriority.UseTextAlignment = false;
			this.lblHomePhone.Text = "714-832-8664";
			this.lblHomePhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel3
			// 
			this.xrLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel3.ForeColor = System.Drawing.Color.White;
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(255.6246F, 80.20834F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(108.5417F, 20.20833F);
			this.xrLabel3.StylePriority.UseBackColor = false;
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.StylePriority.UseForeColor = false;
			this.xrLabel3.StylePriority.UseTextAlignment = false;
			this.xrLabel3.Text = "Home Phone";
			this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrPanel1
			// 
			this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblClass,
            this.chkMedRelForm,
            this.lblMobilePhone,
            this.lblHomePhone,
            this.lblName,
            this.lblEMail,
            this.lblType});
			this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.xrPanel1.Name = "xrPanel1";
			this.xrPanel1.SizeF = new System.Drawing.SizeF(963.3333F, 17.5F);
			// 
			// lblClass
			// 
			this.lblClass.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblClass.LocationFloat = new DevExpress.Utils.PointFloat(757.5002F, 0F);
			this.lblClass.Name = "lblClass";
			this.lblClass.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblClass.SizeF = new System.Drawing.SizeF(94.79156F, 15F);
			this.lblClass.StylePriority.UseFont = false;
			this.lblClass.StylePriority.UseTextAlignment = false;
			this.lblClass.Text = "714-832-8664";
			this.lblClass.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// chkMedRelForm
			// 
			this.chkMedRelForm.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.chkMedRelForm.LocationFloat = new DevExpress.Utils.PointFloat(894.1667F, 0F);
			this.chkMedRelForm.Name = "chkMedRelForm";
			this.chkMedRelForm.SizeF = new System.Drawing.SizeF(16.24994F, 15F);
			this.chkMedRelForm.StylePriority.UseBorders = false;
			this.chkMedRelForm.StylePriority.UseTextAlignment = false;
			this.chkMedRelForm.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// lblMobilePhone
			// 
			this.lblMobilePhone.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMobilePhone.LocationFloat = new DevExpress.Utils.PointFloat(343.7497F, 0F);
			this.lblMobilePhone.Name = "lblMobilePhone";
			this.lblMobilePhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblMobilePhone.SizeF = new System.Drawing.SizeF(108.5417F, 15F);
			this.lblMobilePhone.StylePriority.UseFont = false;
			this.lblMobilePhone.StylePriority.UseTextAlignment = false;
			this.lblMobilePhone.Text = "714-832-8664";
			this.lblMobilePhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblName
			// 
			this.lblName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblName.LocationFloat = new DevExpress.Utils.PointFloat(3.814697E-06F, 0F);
			this.lblName.Name = "lblName";
			this.lblName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblName.SizeF = new System.Drawing.SizeF(235.208F, 15F);
			this.lblName.StylePriority.UseFont = false;
			this.lblName.Text = "Kindergarten";
			// 
			// lblEMail
			// 
			this.lblEMail.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblEMail.LocationFloat = new DevExpress.Utils.PointFloat(452.2913F, 0F);
			this.lblEMail.Name = "lblEMail";
			this.lblEMail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblEMail.SizeF = new System.Drawing.SizeF(210.4174F, 15F);
			this.lblEMail.StylePriority.UseFont = false;
			this.lblEMail.StylePriority.UseTextAlignment = false;
			this.lblEMail.Text = "714-832-8664";
			this.lblEMail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// lblType
			// 
			this.lblType.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblType.LocationFloat = new DevExpress.Utils.PointFloat(662.7087F, 0F);
			this.lblType.Name = "lblType";
			this.lblType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblType.SizeF = new System.Drawing.SizeF(94.79156F, 15F);
			this.lblType.StylePriority.UseFont = false;
			this.lblType.StylePriority.UseTextAlignment = false;
			this.lblType.Text = "714-832-8664";
			this.lblType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrPageInfo1
			// 
			this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo1.Format = "Page {0} of {1}";
			this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(811.2501F, 10.00001F);
			this.xrPageInfo1.Name = "xrPageInfo1";
			this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo1.SizeF = new System.Drawing.SizeF(178.7499F, 23F);
			this.xrPageInfo1.StylePriority.UseFont = false;
			this.xrPageInfo1.StylePriority.UseTextAlignment = false;
			this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// xrLabel15
			// 
			this.xrLabel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel15.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel15.ForeColor = System.Drawing.Color.White;
			this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(364.1663F, 80.20837F);
			this.xrLabel15.Name = "xrLabel15";
			this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel15.SizeF = new System.Drawing.SizeF(108.5417F, 20.20833F);
			this.xrLabel15.StylePriority.UseBackColor = false;
			this.xrLabel15.StylePriority.UseFont = false;
			this.xrLabel15.StylePriority.UseForeColor = false;
			this.xrLabel15.StylePriority.UseTextAlignment = false;
			this.xrLabel15.Text = "Mobile Phone";
			this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// rptTitle
			// 
			this.rptTitle.Font = new System.Drawing.Font("Verdana", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rptTitle.ForeColor = System.Drawing.Color.Blue;
			this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.rptTitle.Name = "rptTitle";
			this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.rptTitle.SizeF = new System.Drawing.SizeF(609.375F, 56.25F);
			this.rptTitle.StylePriority.UseFont = false;
			this.rptTitle.StylePriority.UseForeColor = false;
			this.rptTitle.Text = "Medical Comparison Report";
			// 
			// xrLabel5
			// 
			this.xrLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.ForeColor = System.Drawing.Color.White;
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(683.1255F, 80.20837F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(94.79144F, 20.20833F);
			this.xrLabel5.StylePriority.UseBackColor = false;
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.StylePriority.UseForeColor = false;
			this.xrLabel5.StylePriority.UseTextAlignment = false;
			this.xrLabel5.Text = "Type";
			this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel1
			// 
			this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.ForeColor = System.Drawing.Color.White;
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(472.708F, 80.20834F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(210.4174F, 20.20833F);
			this.xrLabel1.StylePriority.UseBackColor = false;
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseForeColor = false;
			this.xrLabel1.Text = "EMail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.HeightF = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// picHeader
			// 
			this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
			this.picHeader.LocationFloat = new DevExpress.Utils.PointFloat(619.7917F, 0F);
			this.picHeader.Name = "picHeader";
			this.picHeader.SizeF = new System.Drawing.SizeF(380.2083F, 56.25F);
			this.picHeader.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
			this.PageFooter.HeightF = 33.00001F;
			this.PageFooter.Name = "PageFooter";
			// 
			// xrPageInfo2
			// 
			this.xrPageInfo2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo2.Format = "Printed on {0:MM/dd/yyyy} at {0:hh:mm:ss tt}";
			this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
			this.xrPageInfo2.Name = "xrPageInfo2";
			this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
			this.xrPageInfo2.SizeF = new System.Drawing.SizeF(289.1666F, 23F);
			this.xrPageInfo2.StylePriority.UseFont = false;
			this.xrPageInfo2.StylePriority.UseTextAlignment = false;
			this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// TopMargin
			// 
			this.TopMargin.HeightF = 50F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel1,
            this.xrLabel2,
            this.xrLabel5,
            this.xrLabel14,
            this.xrLabel15,
            this.rptTitle,
            this.picHeader});
			this.PageHeader.HeightF = 100.4167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// xrLabel4
			// 
			this.xrLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel4.ForeColor = System.Drawing.Color.White;
			this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(777.917F, 80.20834F);
			this.xrLabel4.Name = "xrLabel4";
			this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel4.SizeF = new System.Drawing.SizeF(94.79144F, 20.20833F);
			this.xrLabel4.StylePriority.UseBackColor = false;
			this.xrLabel4.StylePriority.UseFont = false;
			this.xrLabel4.StylePriority.UseForeColor = false;
			this.xrLabel4.StylePriority.UseTextAlignment = false;
			this.xrLabel4.Text = "Class";
			this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel2
			// 
			this.xrLabel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel2.ForeColor = System.Drawing.Color.White;
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 80.20834F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(235.2079F, 20.20833F);
			this.xrLabel2.StylePriority.UseBackColor = false;
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.StylePriority.UseForeColor = false;
			this.xrLabel2.Text = "Name";
			// 
			// xrLabel14
			// 
			this.xrLabel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel14.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel14.ForeColor = System.Drawing.Color.White;
			this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(872.7084F, 80.20834F);
			this.xrLabel14.Name = "xrLabel14";
			this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel14.SizeF = new System.Drawing.SizeF(111.0416F, 20.20833F);
			this.xrLabel14.StylePriority.UseBackColor = false;
			this.xrLabel14.StylePriority.UseFont = false;
			this.xrLabel14.StylePriority.UseForeColor = false;
			this.xrLabel14.StylePriority.UseTextAlignment = false;
			this.xrLabel14.Text = "Med. Rel. Form";
			this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
			this.Detail.HeightF = 17.5F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTotalCount});
			this.ReportFooter.HeightF = 38.54167F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// lblTotalCount
			// 
			this.lblTotalCount.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTotalCount.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 10.00001F);
			this.lblTotalCount.Name = "lblTotalCount";
			this.lblTotalCount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTotalCount.SizeF = new System.Drawing.SizeF(204.5833F, 15F);
			this.lblTotalCount.StylePriority.UseFont = false;
			this.lblTotalCount.Text = "714-832-8664";
			// 
			// rptMedicalComparison
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.PageFooter,
            this.ReportFooter});
			this.Landscape = true;
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
			this.PageHeight = 850;
			this.PageWidth = 1100;
			this.Version = "17.1";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

    }

    #endregion

    private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {
        xrPanel1.BackColor = _detailBackColor;
        _detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

        chkMedRelForm.Visible = (GetCurrentColumnValue(Search_VBS_MedicalComparison.FN_MedicalReleaseForm) != null);

        string emailHyperlink = (GetCurrentColumnValue(Search_VBS_MedicalComparison.FN_EmailHyperlink) != null ?
            GetCurrentColumnValue(Search_VBS_MedicalComparison.FN_EmailHyperlink).ToString() : string.Empty);
        if (!String.IsNullOrEmpty(emailHyperlink))
        {
            lblEMail.Font = new Font(lblHomePhone.Font, FontStyle.Underline);
            lblEMail.ForeColor = Color.Blue;
            //lblName.Font = new Font(lblHomePhone.Font, FontStyle.Underline);
            //lblName.ForeColor = Color.Blue;
        }
        else
        {
            lblEMail.Font = new Font(lblHomePhone.Font, FontStyle.Regular);
            lblEMail.ForeColor = Color.Black;
            //lblName.Font = new Font(lblHomePhone.Font, FontStyle.Regular);
            //lblName.ForeColor = Color.Black;
        }

		string responseIDHyperlink = (GetCurrentColumnValue(Search_VBS_MedicalComparison.FN_GridCustom_5) != null ?
			GetCurrentColumnValue(Search_VBS_MedicalComparison.FN_GridCustom_5).ToString() : string.Empty);
		if (!String.IsNullOrEmpty(responseIDHyperlink))
		{
			lblName.Font = new Font(lblName.Font, FontStyle.Underline);
			lblName.ForeColor = Color.Blue;
		}
		else
		{
			lblName.Font = new Font(lblName.Font, FontStyle.Regular);
			lblName.ForeColor = Color.Black;
		}
	}
}
