﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptClassSignIn : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptClassSignIn()
		{
			InitializeComponent();

			// Set the bindings
			lblTeacher1.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_3);
			lblTeacher2.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_4);
			lblHelper1.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_5);
			lblHelper2.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_6);

			lblClass.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ClassCodeWithDesc);
			lblClass.DataBindings.Add("Bookmark", null, Search_VBS_ClassAssignment.FN_ClassCodeWithDesc);

			lblRotation.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_9);
			lblCount.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_2);
			lblKidNum.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_7);

			//lblAllergy.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds);

			//lblGrade.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_9);

			// Detail Fields
			lblStudentName.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_StudentNameFormatted);
			//lblChurch.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ChurchAttended);
			lblGender.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_Gender);
			lblParentName.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ParentNameFormatted);
			lblParentName.DataBindings.Add("NavigateUrl", null, Search_VBS_ClassAssignment.FN_EmailHyperlink);
			//lblEMail.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_Email);
			//lblEMail.DataBindings.Add("NavigateUrl", null, Search_VBS_ClassAssignment.FN_EmailHyperlink);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_MobilePhone);
			lblTShirt.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ShirtSize);

			// Bind the groupheader
			ghClassCode.GroupFields.Add(new GroupField(Search_VBS_ClassAssignment.FN_Length, XRColumnSortOrder.Ascending));
			ghClassCode.GroupFields.Add(new GroupField(Search_VBS_ClassAssignment.FN_ClassCode, XRColumnSortOrder.Ascending));
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel2.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblParentName.Font = new Font(lblParentName.Font, FontStyle.Underline);
				lblParentName.ForeColor = Color.Blue;
				//lblEMail.Font = new Font(lblParentName.Font, FontStyle.Underline);
				//lblEMail.ForeColor = Color.Blue;
			}
			else
			{
				lblParentName.Font = new Font(lblParentName.Font, FontStyle.Regular);
				lblParentName.ForeColor = Color.Black;
				//lblEMail.Font = new Font(lblParentName.Font, FontStyle.Regular);
				//lblEMail.ForeColor = Color.Black;
			}

			//// Set the height depending on allergies
			////16.45836
			//lblAllergy.Visible = true;
			//if (String.IsNullOrEmpty(GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds) != null ?
			//	GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds).ToString() : string.Empty))
			//{
			//	lblAllergy.Visible = false;
			//	xrPanel2.HeightF = 16.45836F;
			//	Detail.HeightF = 16.45836F;
			//}
		}
	}
}
