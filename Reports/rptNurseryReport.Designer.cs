﻿namespace VBS.Reports
{
	partial class rptNurseryReport
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNurseryReport));
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.lblMobilePhone = new DevExpress.XtraReports.UI.XRLabel();
			this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAge = new DevExpress.XtraReports.UI.XRLabel();
			this.lblParentsName = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
			this.picHeader = new DevExpress.XtraReports.UI.XRPictureBox();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.lblAssignment = new DevExpress.XtraReports.UI.XRLabel();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.lblEmail = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAllergies = new DevExpress.XtraReports.UI.XRLabel();
			this.chkPottyTrained = new DevExpress.XtraReports.UI.XRCheckBox();
			this.lblHomePhone = new DevExpress.XtraReports.UI.XRLabel();
			this.lblChildsName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblRotation = new DevExpress.XtraReports.UI.XRLabel();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblLineNum = new DevExpress.XtraReports.UI.XRLabel();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// xrLabel2
			// 
			this.xrLabel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel2.ForeColor = System.Drawing.Color.White;
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(49.58327F, 80.20834F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(160.6245F, 20.20833F);
			this.xrLabel2.StylePriority.UseBackColor = false;
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.StylePriority.UseForeColor = false;
			this.xrLabel2.Text = "Child\'s Name";
			// 
			// xrLabel15
			// 
			this.xrLabel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel15.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel15.ForeColor = System.Drawing.Color.White;
			this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(764.1667F, 80.20834F);
			this.xrLabel15.Name = "xrLabel15";
			this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel15.SizeF = new System.Drawing.SizeF(108.5417F, 20.20833F);
			this.xrLabel15.StylePriority.UseBackColor = false;
			this.xrLabel15.StylePriority.UseFont = false;
			this.xrLabel15.StylePriority.UseForeColor = false;
			this.xrLabel15.StylePriority.UseTextAlignment = false;
			this.xrLabel15.Text = "Mobile Phone";
			this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// TopMargin
			// 
			this.TopMargin.HeightF = 50F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// lblMobilePhone
			// 
			this.lblMobilePhone.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMobilePhone.LocationFloat = new DevExpress.Utils.PointFloat(743.7496F, 0F);
			this.lblMobilePhone.Name = "lblMobilePhone";
			this.lblMobilePhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblMobilePhone.SizeF = new System.Drawing.SizeF(108.5417F, 15F);
			this.lblMobilePhone.StylePriority.UseFont = false;
			this.lblMobilePhone.StylePriority.UseTextAlignment = false;
			this.lblMobilePhone.Text = "714-832-8664";
			this.lblMobilePhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// rptTitle
			// 
			this.rptTitle.Font = new System.Drawing.Font("Verdana", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rptTitle.ForeColor = System.Drawing.Color.Blue;
			this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.rptTitle.Name = "rptTitle";
			this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.rptTitle.SizeF = new System.Drawing.SizeF(470.8333F, 31.33333F);
			this.rptTitle.StylePriority.UseFont = false;
			this.rptTitle.StylePriority.UseForeColor = false;
			this.rptTitle.Text = "Nursery Report";
			// 
			// lblAge
			// 
			this.lblAge.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAge.LocationFloat = new DevExpress.Utils.PointFloat(189.7911F, 0F);
			this.lblAge.Name = "lblAge";
			this.lblAge.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAge.SizeF = new System.Drawing.SizeF(69.79164F, 15F);
			this.lblAge.StylePriority.UseFont = false;
			this.lblAge.StylePriority.UseTextAlignment = false;
			this.lblAge.Text = "714-832-8664";
			this.lblAge.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// lblParentsName
			// 
			this.lblParentsName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblParentsName.LocationFloat = new DevExpress.Utils.PointFloat(271.0411F, 0F);
			this.lblParentsName.Name = "lblParentsName";
			this.lblParentsName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblParentsName.SizeF = new System.Drawing.SizeF(153.1249F, 15F);
			this.lblParentsName.StylePriority.UseFont = false;
			this.lblParentsName.Text = "714-832-8664";
			// 
			// xrLabel14
			// 
			this.xrLabel14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel14.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel14.ForeColor = System.Drawing.Color.White;
			this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(872.7084F, 80.20834F);
			this.xrLabel14.Name = "xrLabel14";
			this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel14.SizeF = new System.Drawing.SizeF(111.0416F, 20.20833F);
			this.xrLabel14.StylePriority.UseBackColor = false;
			this.xrLabel14.StylePriority.UseFont = false;
			this.xrLabel14.StylePriority.UseForeColor = false;
			this.xrLabel14.StylePriority.UseTextAlignment = false;
			this.xrLabel14.Text = "Potty Trained";
			this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// picHeader
			// 
			this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
			this.picHeader.LocationFloat = new DevExpress.Utils.PointFloat(619.7917F, 0F);
			this.picHeader.Name = "picHeader";
			this.picHeader.SizeF = new System.Drawing.SizeF(380.2083F, 56.25F);
			this.picHeader.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
			// 
			// ReportHeader
			// 
			this.ReportHeader.HeightF = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// lblAssignment
			// 
			this.lblAssignment.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAssignment.LocationFloat = new DevExpress.Utils.PointFloat(436.6661F, 0F);
			this.lblAssignment.Name = "lblAssignment";
			this.lblAssignment.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAssignment.SizeF = new System.Drawing.SizeF(187.0836F, 15F);
			this.lblAssignment.StylePriority.UseFont = false;
			this.lblAssignment.Text = "714-832-8664";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel3,
            this.xrLabel1,
            this.xrLabel2,
            this.xrLabel4,
            this.xrLabel5,
            this.xrLabel14,
            this.xrLabel15,
            this.rptTitle,
            this.picHeader});
			this.PageHeader.HeightF = 120.625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// xrLabel8
			// 
			this.xrLabel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel8.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel8.ForeColor = System.Drawing.Color.White;
			this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(872.7084F, 100.4167F);
			this.xrLabel8.Name = "xrLabel8";
			this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel8.SizeF = new System.Drawing.SizeF(111.0416F, 20.20833F);
			this.xrLabel8.StylePriority.UseBackColor = false;
			this.xrLabel8.StylePriority.UseFont = false;
			this.xrLabel8.StylePriority.UseForeColor = false;
			this.xrLabel8.StylePriority.UseTextAlignment = false;
			this.xrLabel8.Text = "Rotation";
			this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel7
			// 
			this.xrLabel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel7.ForeColor = System.Drawing.Color.White;
			this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(655.6246F, 100.4167F);
			this.xrLabel7.Name = "xrLabel7";
			this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel7.SizeF = new System.Drawing.SizeF(217.0834F, 20.20833F);
			this.xrLabel7.StylePriority.UseBackColor = false;
			this.xrLabel7.StylePriority.UseFont = false;
			this.xrLabel7.StylePriority.UseForeColor = false;
			this.xrLabel7.Text = "Parent\'s Email";
			// 
			// xrLabel6
			// 
			this.xrLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel6.ForeColor = System.Drawing.Color.White;
			this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(49.58327F, 100.4167F);
			this.xrLabel6.Name = "xrLabel6";
			this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel6.SizeF = new System.Drawing.SizeF(606.0414F, 20.20833F);
			this.xrLabel6.StylePriority.UseBackColor = false;
			this.xrLabel6.StylePriority.UseFont = false;
			this.xrLabel6.StylePriority.UseForeColor = false;
			this.xrLabel6.Text = "Allergies";
			// 
			// xrLabel3
			// 
			this.xrLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel3.ForeColor = System.Drawing.Color.White;
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(655.6245F, 80.20834F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(108.5417F, 20.20833F);
			this.xrLabel3.StylePriority.UseBackColor = false;
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.StylePriority.UseForeColor = false;
			this.xrLabel3.StylePriority.UseTextAlignment = false;
			this.xrLabel3.Text = "Home Phone";
			this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel1
			// 
			this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.ForeColor = System.Drawing.Color.White;
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(210.2078F, 80.20834F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(81.24997F, 20.20833F);
			this.xrLabel1.StylePriority.UseBackColor = false;
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseForeColor = false;
			this.xrLabel1.Text = "Age";
			// 
			// xrLabel4
			// 
			this.xrLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel4.ForeColor = System.Drawing.Color.White;
			this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(291.4578F, 80.20834F);
			this.xrLabel4.Name = "xrLabel4";
			this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel4.SizeF = new System.Drawing.SizeF(165.6249F, 20.20833F);
			this.xrLabel4.StylePriority.UseBackColor = false;
			this.xrLabel4.StylePriority.UseFont = false;
			this.xrLabel4.StylePriority.UseForeColor = false;
			this.xrLabel4.Text = "Parent\'s Name";
			// 
			// xrLabel5
			// 
			this.xrLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.ForeColor = System.Drawing.Color.White;
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(457.0828F, 80.20834F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(198.5417F, 20.20833F);
			this.xrLabel5.StylePriority.UseBackColor = false;
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.StylePriority.UseForeColor = false;
			this.xrLabel5.Text = "Parent\'s Assignment";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
			this.PageFooter.HeightF = 33.00001F;
			this.PageFooter.Name = "PageFooter";
			// 
			// xrPageInfo2
			// 
			this.xrPageInfo2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo2.Format = "Printed on {0:MM/dd/yyyy} at {0:hh:mm:ss tt}";
			this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
			this.xrPageInfo2.Name = "xrPageInfo2";
			this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
			this.xrPageInfo2.SizeF = new System.Drawing.SizeF(289.1666F, 23F);
			this.xrPageInfo2.StylePriority.UseFont = false;
			this.xrPageInfo2.StylePriority.UseTextAlignment = false;
			this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrPageInfo1
			// 
			this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo1.Format = "Page {0} of {1}";
			this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(811.2501F, 10.00001F);
			this.xrPageInfo1.Name = "xrPageInfo1";
			this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo1.SizeF = new System.Drawing.SizeF(178.7499F, 23F);
			this.xrPageInfo1.StylePriority.UseFont = false;
			this.xrPageInfo1.StylePriority.UseTextAlignment = false;
			this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
			this.Detail.HeightF = 31.04166F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
			// 
			// xrPanel1
			// 
			this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblLineNum,
            this.lblEmail,
            this.lblAllergies,
            this.chkPottyTrained,
            this.lblMobilePhone,
            this.lblAssignment,
            this.lblHomePhone,
            this.lblChildsName,
            this.lblAge,
            this.lblParentsName,
            this.lblRotation});
			this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.xrPanel1.Name = "xrPanel1";
			this.xrPanel1.SizeF = new System.Drawing.SizeF(963.3333F, 31.04166F);
			// 
			// lblEmail
			// 
			this.lblEmail.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblEmail.LocationFloat = new DevExpress.Utils.PointFloat(635.2079F, 14.99999F);
			this.lblEmail.Name = "lblEmail";
			this.lblEmail.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblEmail.SizeF = new System.Drawing.SizeF(217.0834F, 15F);
			this.lblEmail.StylePriority.UseFont = false;
			this.lblEmail.StylePriority.UseTextAlignment = false;
			this.lblEmail.Text = "714-832-8664";
			this.lblEmail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// lblAllergies
			// 
			this.lblAllergies.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Italic);
			this.lblAllergies.ForeColor = System.Drawing.Color.Red;
			this.lblAllergies.LocationFloat = new DevExpress.Utils.PointFloat(29.1666F, 14.99999F);
			this.lblAllergies.Name = "lblAllergies";
			this.lblAllergies.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAllergies.SizeF = new System.Drawing.SizeF(594.5831F, 15F);
			this.lblAllergies.StylePriority.UseFont = false;
			this.lblAllergies.StylePriority.UseForeColor = false;
			this.lblAllergies.Text = "Kindergarten";
			// 
			// chkPottyTrained
			// 
			this.chkPottyTrained.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.chkPottyTrained.LocationFloat = new DevExpress.Utils.PointFloat(894.1667F, 0F);
			this.chkPottyTrained.Name = "chkPottyTrained";
			this.chkPottyTrained.SizeF = new System.Drawing.SizeF(16.24994F, 15F);
			this.chkPottyTrained.StylePriority.UseBorders = false;
			this.chkPottyTrained.StylePriority.UseTextAlignment = false;
			this.chkPottyTrained.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// lblHomePhone
			// 
			this.lblHomePhone.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHomePhone.LocationFloat = new DevExpress.Utils.PointFloat(635.2079F, 0F);
			this.lblHomePhone.Name = "lblHomePhone";
			this.lblHomePhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHomePhone.SizeF = new System.Drawing.SizeF(108.5417F, 15F);
			this.lblHomePhone.StylePriority.UseFont = false;
			this.lblHomePhone.StylePriority.UseTextAlignment = false;
			this.lblHomePhone.Text = "714-832-8664";
			this.lblHomePhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblChildsName
			// 
			this.lblChildsName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblChildsName.LocationFloat = new DevExpress.Utils.PointFloat(29.1666F, 0F);
			this.lblChildsName.Name = "lblChildsName";
			this.lblChildsName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblChildsName.SizeF = new System.Drawing.SizeF(150.2079F, 15F);
			this.lblChildsName.StylePriority.UseFont = false;
			this.lblChildsName.Text = "Kindergarten";
			// 
			// lblRotation
			// 
			this.lblRotation.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblRotation.ForeColor = System.Drawing.Color.Green;
			this.lblRotation.LocationFloat = new DevExpress.Utils.PointFloat(852.2917F, 14.99999F);
			this.lblRotation.Name = "lblRotation";
			this.lblRotation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblRotation.SizeF = new System.Drawing.SizeF(111.0416F, 14.5833F);
			this.lblRotation.StylePriority.UseFont = false;
			this.lblRotation.StylePriority.UseForeColor = false;
			this.lblRotation.StylePriority.UseTextAlignment = false;
			this.lblRotation.Text = "714-832-8664";
			this.lblRotation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// BottomMargin
			// 
			this.BottomMargin.HeightF = 50F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// xrLabel9
			// 
			this.xrLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel9.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel9.ForeColor = System.Drawing.Color.White;
			this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 80.20834F);
			this.xrLabel9.Name = "xrLabel9";
			this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel9.SizeF = new System.Drawing.SizeF(29.1666F, 20.20833F);
			this.xrLabel9.StylePriority.UseBackColor = false;
			this.xrLabel9.StylePriority.UseFont = false;
			this.xrLabel9.StylePriority.UseForeColor = false;
			this.xrLabel9.Text = " ";
			// 
			// lblLineNum
			// 
			this.lblLineNum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblLineNum.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.lblLineNum.Name = "lblLineNum";
			this.lblLineNum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblLineNum.SizeF = new System.Drawing.SizeF(29.1666F, 15F);
			this.lblLineNum.StylePriority.UseFont = false;
			this.lblLineNum.Text = "Kindergarten";
			// 
			// rptNurseryReport
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.PageFooter});
			this.Landscape = true;
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
			this.PageHeight = 850;
			this.PageWidth = 1100;
			this.Version = "17.1";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRLabel xrLabel15;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.XRLabel lblMobilePhone;
		private DevExpress.XtraReports.UI.XRLabel rptTitle;
		private DevExpress.XtraReports.UI.XRLabel lblAge;
		private DevExpress.XtraReports.UI.XRLabel lblParentsName;
		private DevExpress.XtraReports.UI.XRLabel xrLabel14;
		private DevExpress.XtraReports.UI.XRPictureBox picHeader;
		private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
		private DevExpress.XtraReports.UI.XRLabel lblAssignment;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.XRLabel xrLabel4;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.XRPanel xrPanel1;
		private DevExpress.XtraReports.UI.XRLabel lblHomePhone;
		private DevExpress.XtraReports.UI.XRLabel lblChildsName;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
		private DevExpress.XtraReports.UI.XRCheckBox chkPottyTrained;
		private DevExpress.XtraReports.UI.XRLabel lblEmail;
		private DevExpress.XtraReports.UI.XRLabel lblAllergies;
		private DevExpress.XtraReports.UI.XRLabel xrLabel7;
		private DevExpress.XtraReports.UI.XRLabel xrLabel6;
		private DevExpress.XtraReports.UI.XRLabel xrLabel8;
		private DevExpress.XtraReports.UI.XRLabel lblRotation;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
		private DevExpress.XtraReports.UI.XRLabel xrLabel9;
		private DevExpress.XtraReports.UI.XRLabel lblLineNum;
	}
}
