﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class SchoolListing : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			//// Get the collection for the master
			//Search_VBS_SchoolListingCollection schoolCollection = new Search_VBS_SchoolListingCollection(string.Empty);
			//schoolCollection.Sort(Search_VBS_SchoolListing.FN_Count + " DESC, " + Search_VBS_SchoolListing.FN_School);

			//// Take off values for the subreport
			//while (schoolCollection.Count > 20)
			//{
			//	schoolCollection.RemoveAt(schoolCollection.Count - 1);
			//}

			//// Put in the percentages
			//int totalCount = 0;
			//foreach (Search_VBS_SchoolListing item in schoolCollection)
			//{
			//	totalCount += (item.Count.HasValue ? item.Count.Value : 0);
			//}
			//foreach (Search_VBS_SchoolListing item in schoolCollection)
			//{
			//	if (item.Count.HasValue)
			//	{
			//		item.GridCustom_4 = ((item.Count.Value / (decimal)totalCount) * 100).ToString("##0.0") + "%";
			//	}
			//}
			
			//Search_VBS_SchoolListingCollection schoolCollectionChart = new Search_VBS_SchoolListingCollection(string.Empty);
			//schoolCollectionChart.Sort(Search_VBS_SchoolListing.FN_Count + " DESC, " + Search_VBS_SchoolListing.FN_School);

			//while (schoolCollectionChart.Count > 20)
			//{
			//	schoolCollectionChart.RemoveAt(schoolCollectionChart.Count - 1);
			//}

			// For the chart, take out the schools that are more than the count
			//int count = 15;
			//Search_VBS_SchoolListing newItem = new Search_VBS_SchoolListing();
			//newItem.RecGUID = System.Guid.NewGuid().ToString();
			//newItem.School = "< Other >";
			//newItem.Count = 0;
			//while (schoolCollectionChart.Count > count - 1)
			//{
			//	newItem.Count += schoolCollectionChart[schoolCollectionChart.Count - 1].Count;
			//	schoolCollectionChart.RemoveAt(schoolCollectionChart.Count - 1);
			//}

			//// Add the item to the collection
			//schoolCollectionChart.Add(newItem);

			//rptSchoolListing rpt = new rptSchoolListing();
			//rpt.DataSource = schoolCollectionChart;
			//rpt.SchoolCollectionDetail = schoolCollection;
			//rpt.DisplayName = "Top 20 Schools";
			//rpt.CreateDocument();
			//documentViewer.Report = rpt;
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Get the collection for the master
			Search_VBS_SchoolListingCollection schoolCollection = new Search_VBS_SchoolListingCollection(string.Empty);
			schoolCollection.Sort(Search_VBS_SchoolListing.FN_Count + " DESC, " + Search_VBS_SchoolListing.FN_School);

			// Take off values for the subreport
			while (schoolCollection.Count > 20)
			{
				schoolCollection.RemoveAt(schoolCollection.Count - 1);
			}

			// Put in the percentages
			int totalCount = 0;
			foreach (Search_VBS_SchoolListing item in schoolCollection)
			{
				totalCount += (item.Count.HasValue ? item.Count.Value : 0);
			}
			foreach (Search_VBS_SchoolListing item in schoolCollection)
			{
				if (item.Count.HasValue)
				{
					item.GridCustom_4 = ((item.Count.Value / (decimal)totalCount) * 100).ToString("##0.0") + "%";
				}
			}

			Search_VBS_SchoolListingCollection schoolCollectionChart = new Search_VBS_SchoolListingCollection(string.Empty);
			schoolCollectionChart.Sort(Search_VBS_SchoolListing.FN_Count + " DESC, " + Search_VBS_SchoolListing.FN_School);

			while (schoolCollectionChart.Count > 20)
			{
				schoolCollectionChart.RemoveAt(schoolCollectionChart.Count - 1);
			}

			rptSchoolListing rpt = new rptSchoolListing();
			rpt.DataSource = schoolCollectionChart;
			rpt.SchoolCollectionDetail = schoolCollection;
			rpt.DisplayName = "Top 20 Schools";
			
			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.98f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}
	
}