﻿namespace VBS.Reports
{
	partial class rptKidPickupByClass
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptKidPickupByClass));
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
			this.lblPerson3 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblPerson2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblPerson1 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblKidNum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblStudentName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblParentName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblMobilePhone = new DevExpress.XtraReports.UI.XRLabel();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
			this.picHeader = new DevExpress.XtraReports.UI.XRPictureBox();
			this.ghClassCode = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblCount = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblRotation = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblHelper1 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblHelper2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTeacher2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTeacher1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblClass = new DevExpress.XtraReports.UI.XRLabel();
			this.gfClassCode = new DevExpress.XtraReports.UI.GroupFooterBand();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel2});
			this.Detail.HeightF = 20.41667F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
			// 
			// xrPanel2
			// 
			this.xrPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(206)))), ((int)(((byte)(235)))));
			this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblPerson3,
            this.lblPerson2,
            this.lblPerson1,
            this.lblKidNum,
            this.lblStudentName,
            this.lblParentName,
            this.lblMobilePhone});
			this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.xrPanel2.Name = "xrPanel2";
			this.xrPanel2.SizeF = new System.Drawing.SizeF(963.3333F, 20.41667F);
			this.xrPanel2.StylePriority.UseBackColor = false;
			// 
			// lblPerson3
			// 
			this.lblPerson3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPerson3.LocationFloat = new DevExpress.Utils.PointFloat(814.3751F, 0F);
			this.lblPerson3.Multiline = true;
			this.lblPerson3.Name = "lblPerson3";
			this.lblPerson3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblPerson3.SizeF = new System.Drawing.SizeF(148.7501F, 20.41667F);
			this.lblPerson3.StylePriority.UseFont = false;
			this.lblPerson3.StylePriority.UseTextAlignment = false;
			this.lblPerson3.Text = "714-832-8664";
			this.lblPerson3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblPerson2
			// 
			this.lblPerson2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPerson2.LocationFloat = new DevExpress.Utils.PointFloat(665.625F, 0F);
			this.lblPerson2.Multiline = true;
			this.lblPerson2.Name = "lblPerson2";
			this.lblPerson2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblPerson2.SizeF = new System.Drawing.SizeF(148.7501F, 20.41667F);
			this.lblPerson2.StylePriority.UseFont = false;
			this.lblPerson2.StylePriority.UseTextAlignment = false;
			this.lblPerson2.Text = "714-832-8664";
			this.lblPerson2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblPerson1
			// 
			this.lblPerson1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPerson1.LocationFloat = new DevExpress.Utils.PointFloat(516.875F, 0F);
			this.lblPerson1.Multiline = true;
			this.lblPerson1.Name = "lblPerson1";
			this.lblPerson1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblPerson1.SizeF = new System.Drawing.SizeF(148.7501F, 20.41667F);
			this.lblPerson1.StylePriority.UseFont = false;
			this.lblPerson1.StylePriority.UseTextAlignment = false;
			this.lblPerson1.Text = "714-832-8664";
			this.lblPerson1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblKidNum
			// 
			this.lblKidNum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblKidNum.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.lblKidNum.Name = "lblKidNum";
			this.lblKidNum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblKidNum.SizeF = new System.Drawing.SizeF(30.20852F, 20.41667F);
			this.lblKidNum.StylePriority.UseFont = false;
			this.lblKidNum.StylePriority.UseTextAlignment = false;
			this.lblKidNum.Text = "714-832-8664";
			this.lblKidNum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblStudentName
			// 
			this.lblStudentName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStudentName.LocationFloat = new DevExpress.Utils.PointFloat(30.20852F, 0F);
			this.lblStudentName.Name = "lblStudentName";
			this.lblStudentName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblStudentName.SizeF = new System.Drawing.SizeF(206.6666F, 20.41667F);
			this.lblStudentName.StylePriority.UseFont = false;
			this.lblStudentName.StylePriority.UseTextAlignment = false;
			this.lblStudentName.Text = "714-832-8664";
			this.lblStudentName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblParentName
			// 
			this.lblParentName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblParentName.LocationFloat = new DevExpress.Utils.PointFloat(236.8751F, 0F);
			this.lblParentName.Name = "lblParentName";
			this.lblParentName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblParentName.SizeF = new System.Drawing.SizeF(171.458F, 20.41667F);
			this.lblParentName.StylePriority.UseFont = false;
			this.lblParentName.StylePriority.UseTextAlignment = false;
			this.lblParentName.Text = "714-832-8664";
			this.lblParentName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblMobilePhone
			// 
			this.lblMobilePhone.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMobilePhone.LocationFloat = new DevExpress.Utils.PointFloat(408.3332F, 0F);
			this.lblMobilePhone.Name = "lblMobilePhone";
			this.lblMobilePhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblMobilePhone.SizeF = new System.Drawing.SizeF(108.5417F, 20.41667F);
			this.lblMobilePhone.StylePriority.UseFont = false;
			this.lblMobilePhone.StylePriority.UseTextAlignment = false;
			this.lblMobilePhone.Text = "714-832-8664";
			this.lblMobilePhone.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// TopMargin
			// 
			this.TopMargin.HeightF = 50F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// BottomMargin
			// 
			this.BottomMargin.HeightF = 50F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// ReportHeader
			// 
			this.ReportHeader.HeightF = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.rptTitle,
            this.picHeader});
			this.PageHeader.HeightF = 63.54167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// rptTitle
			// 
			this.rptTitle.Font = new System.Drawing.Font("Verdana", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rptTitle.ForeColor = System.Drawing.Color.Blue;
			this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.rptTitle.Name = "rptTitle";
			this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.rptTitle.SizeF = new System.Drawing.SizeF(554.1667F, 31.33333F);
			this.rptTitle.StylePriority.UseFont = false;
			this.rptTitle.StylePriority.UseForeColor = false;
			this.rptTitle.Text = "Student Pickup List (By Class)";
			// 
			// picHeader
			// 
			this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
			this.picHeader.LocationFloat = new DevExpress.Utils.PointFloat(619.7917F, 0F);
			this.picHeader.Name = "picHeader";
			this.picHeader.SizeF = new System.Drawing.SizeF(380.2083F, 56.25F);
			this.picHeader.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
			// 
			// ghClassCode
			// 
			this.ghClassCode.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel11,
            this.xrLabel15,
            this.xrLabel16,
            this.lblCount,
            this.xrLabel14,
            this.lblRotation,
            this.xrLabel1,
            this.xrLabel12,
            this.xrLabel9,
            this.xrLabel6,
            this.xrLabel4,
            this.xrLabel5,
            this.lblHelper1,
            this.lblHelper2,
            this.lblTeacher2,
            this.lblTeacher1,
            this.xrLabel3,
            this.xrLabel2,
            this.lblClass});
			this.ghClassCode.HeightF = 115.6283F;
			this.ghClassCode.KeepTogether = true;
			this.ghClassCode.Name = "ghClassCode";
			// 
			// xrLabel11
			// 
			this.xrLabel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel11.BorderColor = System.Drawing.Color.White;
			this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel11.ForeColor = System.Drawing.Color.White;
			this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(834.7918F, 95.41833F);
			this.xrLabel11.Name = "xrLabel11";
			this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel11.SizeF = new System.Drawing.SizeF(148.75F, 20.21F);
			this.xrLabel11.StylePriority.UseBackColor = false;
			this.xrLabel11.StylePriority.UseBorderColor = false;
			this.xrLabel11.StylePriority.UseBorders = false;
			this.xrLabel11.StylePriority.UseFont = false;
			this.xrLabel11.StylePriority.UseForeColor = false;
			this.xrLabel11.Text = "Person 3";
			// 
			// xrLabel15
			// 
			this.xrLabel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel15.BorderColor = System.Drawing.Color.White;
			this.xrLabel15.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel15.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel15.ForeColor = System.Drawing.Color.White;
			this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(686.0418F, 95.41667F);
			this.xrLabel15.Name = "xrLabel15";
			this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel15.SizeF = new System.Drawing.SizeF(148.75F, 20.21F);
			this.xrLabel15.StylePriority.UseBackColor = false;
			this.xrLabel15.StylePriority.UseBorderColor = false;
			this.xrLabel15.StylePriority.UseBorders = false;
			this.xrLabel15.StylePriority.UseFont = false;
			this.xrLabel15.StylePriority.UseForeColor = false;
			this.xrLabel15.Text = "Person 2";
			// 
			// xrLabel16
			// 
			this.xrLabel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel16.BorderColor = System.Drawing.Color.White;
			this.xrLabel16.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel16.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel16.ForeColor = System.Drawing.Color.White;
			this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(537.2916F, 95.41667F);
			this.xrLabel16.Name = "xrLabel16";
			this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel16.SizeF = new System.Drawing.SizeF(148.7502F, 20.20833F);
			this.xrLabel16.StylePriority.UseBackColor = false;
			this.xrLabel16.StylePriority.UseBorderColor = false;
			this.xrLabel16.StylePriority.UseBorders = false;
			this.xrLabel16.StylePriority.UseFont = false;
			this.xrLabel16.StylePriority.UseForeColor = false;
			this.xrLabel16.Text = "Person 1";
			// 
			// lblCount
			// 
			this.lblCount.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCount.LocationFloat = new DevExpress.Utils.PointFloat(633.7498F, 0F);
			this.lblCount.Name = "lblCount";
			this.lblCount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblCount.SizeF = new System.Drawing.SizeF(158.3334F, 25.08333F);
			this.lblCount.StylePriority.UseBackColor = false;
			this.lblCount.StylePriority.UseFont = false;
			this.lblCount.StylePriority.UseForeColor = false;
			this.lblCount.StylePriority.UseTextAlignment = false;
			this.lblCount.Text = "A";
			this.lblCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrLabel14
			// 
			this.xrLabel14.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(542.7081F, 0F);
			this.xrLabel14.Name = "xrLabel14";
			this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel14.SizeF = new System.Drawing.SizeF(76.04175F, 25.08333F);
			this.xrLabel14.StylePriority.UseBackColor = false;
			this.xrLabel14.StylePriority.UseFont = false;
			this.xrLabel14.StylePriority.UseForeColor = false;
			this.xrLabel14.StylePriority.UseTextAlignment = false;
			this.xrLabel14.Text = "Count:";
			this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblRotation
			// 
			this.lblRotation.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblRotation.LocationFloat = new DevExpress.Utils.PointFloat(831.6666F, 0F);
			this.lblRotation.Name = "lblRotation";
			this.lblRotation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblRotation.SizeF = new System.Drawing.SizeF(158.3334F, 25.08333F);
			this.lblRotation.StylePriority.UseBackColor = false;
			this.lblRotation.StylePriority.UseFont = false;
			this.lblRotation.StylePriority.UseForeColor = false;
			this.lblRotation.StylePriority.UseTextAlignment = false;
			this.lblRotation.Text = "Rotation: A";
			this.lblRotation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// xrLabel1
			// 
			this.xrLabel1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.ForeColor = System.Drawing.Color.Black;
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(76.04169F, 25.08333F);
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseForeColor = false;
			this.xrLabel1.StylePriority.UseTextAlignment = false;
			this.xrLabel1.Text = "Class:";
			this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrLabel12
			// 
			this.xrLabel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel12.BorderColor = System.Drawing.Color.White;
			this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel12.ForeColor = System.Drawing.Color.White;
			this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(428.7498F, 95.41998F);
			this.xrLabel12.Name = "xrLabel12";
			this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel12.SizeF = new System.Drawing.SizeF(108.5417F, 20.20833F);
			this.xrLabel12.StylePriority.UseBackColor = false;
			this.xrLabel12.StylePriority.UseBorderColor = false;
			this.xrLabel12.StylePriority.UseBorders = false;
			this.xrLabel12.StylePriority.UseFont = false;
			this.xrLabel12.StylePriority.UseForeColor = false;
			this.xrLabel12.Text = "Phone";
			// 
			// xrLabel9
			// 
			this.xrLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel9.BorderColor = System.Drawing.Color.White;
			this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel9.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel9.ForeColor = System.Drawing.Color.White;
			this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(257.2918F, 95.41667F);
			this.xrLabel9.Name = "xrLabel9";
			this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel9.SizeF = new System.Drawing.SizeF(171.4581F, 20.20833F);
			this.xrLabel9.StylePriority.UseBackColor = false;
			this.xrLabel9.StylePriority.UseBorderColor = false;
			this.xrLabel9.StylePriority.UseBorders = false;
			this.xrLabel9.StylePriority.UseFont = false;
			this.xrLabel9.StylePriority.UseForeColor = false;
			this.xrLabel9.Text = "Parent Name";
			// 
			// xrLabel6
			// 
			this.xrLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel6.BorderColor = System.Drawing.Color.White;
			this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel6.ForeColor = System.Drawing.Color.White;
			this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 95.41667F);
			this.xrLabel6.Name = "xrLabel6";
			this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel6.SizeF = new System.Drawing.SizeF(236.8751F, 20.20833F);
			this.xrLabel6.StylePriority.UseBackColor = false;
			this.xrLabel6.StylePriority.UseBorderColor = false;
			this.xrLabel6.StylePriority.UseBorders = false;
			this.xrLabel6.StylePriority.UseFont = false;
			this.xrLabel6.StylePriority.UseForeColor = false;
			this.xrLabel6.Text = "Student Name";
			// 
			// xrLabel4
			// 
			this.xrLabel4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(542.7082F, 40.625F);
			this.xrLabel4.Name = "xrLabel4";
			this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel4.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel4.StylePriority.UseFont = false;
			this.xrLabel4.Text = "Helper:";
			// 
			// xrLabel5
			// 
			this.xrLabel5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(542.7082F, 62.5F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.Text = "Helper:";
			// 
			// lblHelper1
			// 
			this.lblHelper1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHelper1.LocationFloat = new DevExpress.Utils.PointFloat(633.7499F, 40.625F);
			this.lblHelper1.Name = "lblHelper1";
			this.lblHelper1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHelper1.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblHelper1.StylePriority.UseFont = false;
			this.lblHelper1.Text = "Team Leader:";
			// 
			// lblHelper2
			// 
			this.lblHelper2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHelper2.LocationFloat = new DevExpress.Utils.PointFloat(633.7499F, 62.5F);
			this.lblHelper2.Name = "lblHelper2";
			this.lblHelper2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHelper2.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblHelper2.StylePriority.UseFont = false;
			this.lblHelper2.Text = "Team Leader:";
			// 
			// lblTeacher2
			// 
			this.lblTeacher2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTeacher2.LocationFloat = new DevExpress.Utils.PointFloat(101.0417F, 62.5F);
			this.lblTeacher2.Name = "lblTeacher2";
			this.lblTeacher2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTeacher2.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblTeacher2.StylePriority.UseFont = false;
			this.lblTeacher2.Text = "Team Leader:";
			// 
			// lblTeacher1
			// 
			this.lblTeacher1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTeacher1.LocationFloat = new DevExpress.Utils.PointFloat(101.0417F, 40.625F);
			this.lblTeacher1.Name = "lblTeacher1";
			this.lblTeacher1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTeacher1.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblTeacher1.StylePriority.UseFont = false;
			this.lblTeacher1.Text = "Team Leader:";
			// 
			// xrLabel3
			// 
			this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 62.5F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.Text = "Team Leader:";
			// 
			// xrLabel2
			// 
			this.xrLabel2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 40.625F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.Text = "Team Leader:";
			// 
			// lblClass
			// 
			this.lblClass.BackColor = System.Drawing.Color.Yellow;
			this.lblClass.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblClass.ForeColor = System.Drawing.Color.Red;
			this.lblClass.LocationFloat = new DevExpress.Utils.PointFloat(101.0417F, 0F);
			this.lblClass.Name = "lblClass";
			this.lblClass.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblClass.SizeF = new System.Drawing.SizeF(350.0001F, 25.08333F);
			this.lblClass.StylePriority.UseBackColor = false;
			this.lblClass.StylePriority.UseFont = false;
			this.lblClass.StylePriority.UseForeColor = false;
			this.lblClass.StylePriority.UseTextAlignment = false;
			this.lblClass.Text = "A";
			this.lblClass.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// gfClassCode
			// 
			this.gfClassCode.HeightF = 0F;
			this.gfClassCode.Name = "gfClassCode";
			this.gfClassCode.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
			this.PageFooter.HeightF = 34.0416F;
			this.PageFooter.Name = "PageFooter";
			// 
			// xrPageInfo2
			// 
			this.xrPageInfo2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo2.Format = "Printed on {0:MM/dd/yyyy} at {0:hh:mm:ss tt}";
			this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 11.0416F);
			this.xrPageInfo2.Name = "xrPageInfo2";
			this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
			this.xrPageInfo2.SizeF = new System.Drawing.SizeF(289.1666F, 23F);
			this.xrPageInfo2.StylePriority.UseFont = false;
			this.xrPageInfo2.StylePriority.UseTextAlignment = false;
			this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrPageInfo1
			// 
			this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo1.Format = "Page {0} of {1}";
			this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(811.2501F, 11.0416F);
			this.xrPageInfo1.Name = "xrPageInfo1";
			this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo1.SizeF = new System.Drawing.SizeF(178.7499F, 23F);
			this.xrPageInfo1.StylePriority.UseFont = false;
			this.xrPageInfo1.StylePriority.UseTextAlignment = false;
			this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// rptKidPickupByClass
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.ghClassCode,
            this.gfClassCode,
            this.PageFooter});
			this.Landscape = true;
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
			this.PageHeight = 850;
			this.PageWidth = 1100;
			this.Version = "17.1";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.XRPanel xrPanel2;
		private DevExpress.XtraReports.UI.XRLabel lblKidNum;
		private DevExpress.XtraReports.UI.XRLabel lblStudentName;
		private DevExpress.XtraReports.UI.XRLabel lblParentName;
		private DevExpress.XtraReports.UI.XRLabel lblMobilePhone;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
		private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.XRLabel rptTitle;
		private DevExpress.XtraReports.UI.XRPictureBox picHeader;
		private DevExpress.XtraReports.UI.GroupHeaderBand ghClassCode;
		private DevExpress.XtraReports.UI.XRLabel lblCount;
		private DevExpress.XtraReports.UI.XRLabel xrLabel14;
		private DevExpress.XtraReports.UI.XRLabel lblRotation;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
		private DevExpress.XtraReports.UI.XRLabel xrLabel12;
		private DevExpress.XtraReports.UI.XRLabel xrLabel9;
		private DevExpress.XtraReports.UI.XRLabel xrLabel6;
		private DevExpress.XtraReports.UI.XRLabel xrLabel4;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.UI.XRLabel lblHelper1;
		private DevExpress.XtraReports.UI.XRLabel lblHelper2;
		private DevExpress.XtraReports.UI.XRLabel lblTeacher2;
		private DevExpress.XtraReports.UI.XRLabel lblTeacher1;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRLabel lblClass;
		private DevExpress.XtraReports.UI.GroupFooterBand gfClassCode;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
		private DevExpress.XtraReports.UI.XRLabel xrLabel11;
		private DevExpress.XtraReports.UI.XRLabel xrLabel15;
		private DevExpress.XtraReports.UI.XRLabel xrLabel16;
		private DevExpress.XtraReports.UI.XRLabel lblPerson3;
		private DevExpress.XtraReports.UI.XRLabel lblPerson2;
		private DevExpress.XtraReports.UI.XRLabel lblPerson1;
	}
}
