﻿namespace VBS.Reports
{
	partial class rptClassRosterByStudent
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptClassRosterByStudent));
			this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
			this.picHeader = new DevExpress.XtraReports.UI.XRPictureBox();
			this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.lblStudentName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTShirt = new DevExpress.XtraReports.UI.XRLabel();
			this.lblClassGrade = new DevExpress.XtraReports.UI.XRLabel();
			this.lblPhone = new DevExpress.XtraReports.UI.XRLabel();
			this.lblChurch = new DevExpress.XtraReports.UI.XRLabel();
			this.lblGender = new DevExpress.XtraReports.UI.XRLabel();
			this.lblParentName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAllergy = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
			this.gfStudentName = new DevExpress.XtraReports.UI.GroupFooterBand();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.lblLastName = new DevExpress.XtraReports.UI.XRLabel();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
			this.ghStudentName = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// xrLabel9
			// 
			this.xrLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel9.BorderColor = System.Drawing.Color.White;
			this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel9.Dpi = 100F;
			this.xrLabel9.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel9.ForeColor = System.Drawing.Color.White;
			this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(508.3333F, 35.41667F);
			this.xrLabel9.Name = "xrLabel9";
			this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel9.SizeF = new System.Drawing.SizeF(159.375F, 20.20833F);
			this.xrLabel9.StylePriority.UseBackColor = false;
			this.xrLabel9.StylePriority.UseBorderColor = false;
			this.xrLabel9.StylePriority.UseBorders = false;
			this.xrLabel9.StylePriority.UseFont = false;
			this.xrLabel9.StylePriority.UseForeColor = false;
			this.xrLabel9.Text = "Parent Name";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.rptTitle,
            this.picHeader});
			this.PageHeader.Dpi = 100F;
			this.PageHeader.HeightF = 63.54167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// rptTitle
			// 
			this.rptTitle.Dpi = 100F;
			this.rptTitle.Font = new System.Drawing.Font("Verdana", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rptTitle.ForeColor = System.Drawing.Color.Blue;
			this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.rptTitle.Name = "rptTitle";
			this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.rptTitle.SizeF = new System.Drawing.SizeF(356.25F, 31.33333F);
			this.rptTitle.StylePriority.UseFont = false;
			this.rptTitle.StylePriority.UseForeColor = false;
			this.rptTitle.Text = "Class Roster (By Student)";
			// 
			// picHeader
			// 
			this.picHeader.Dpi = 100F;
			this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
			this.picHeader.LocationFloat = new DevExpress.Utils.PointFloat(619.7917F, 0F);
			this.picHeader.Name = "picHeader";
			this.picHeader.SizeF = new System.Drawing.SizeF(380.2083F, 56.25F);
			this.picHeader.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
			// 
			// xrLabel7
			// 
			this.xrLabel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel7.BorderColor = System.Drawing.Color.White;
			this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel7.Dpi = 100F;
			this.xrLabel7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel7.ForeColor = System.Drawing.Color.White;
			this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(227.0834F, 35.41667F);
			this.xrLabel7.Name = "xrLabel7";
			this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel7.SizeF = new System.Drawing.SizeF(215.2083F, 20.20833F);
			this.xrLabel7.StylePriority.UseBackColor = false;
			this.xrLabel7.StylePriority.UseBorderColor = false;
			this.xrLabel7.StylePriority.UseBorders = false;
			this.xrLabel7.StylePriority.UseFont = false;
			this.xrLabel7.StylePriority.UseForeColor = false;
			this.xrLabel7.Text = "Church";
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
			this.Detail.Dpi = 100F;
			this.Detail.HeightF = 31.45835F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
			// 
			// xrPanel1
			// 
			this.xrPanel1.BackColor = System.Drawing.Color.SkyBlue;
			this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblStudentName,
            this.lblTShirt,
            this.lblClassGrade,
            this.lblPhone,
            this.lblChurch,
            this.lblGender,
            this.lblParentName,
            this.lblAllergy});
			this.xrPanel1.Dpi = 100F;
			this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.xrPanel1.Name = "xrPanel1";
			this.xrPanel1.SizeF = new System.Drawing.SizeF(963.3333F, 31.45835F);
			this.xrPanel1.StylePriority.UseBackColor = false;
			// 
			// lblStudentName
			// 
			this.lblStudentName.Dpi = 100F;
			this.lblStudentName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStudentName.LocationFloat = new DevExpress.Utils.PointFloat(0.0001068115F, 0F);
			this.lblStudentName.Name = "lblStudentName";
			this.lblStudentName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblStudentName.SizeF = new System.Drawing.SizeF(206.6666F, 15F);
			this.lblStudentName.StylePriority.UseFont = false;
			this.lblStudentName.Text = "714-832-8664";
			// 
			// lblTShirt
			// 
			this.lblTShirt.Dpi = 100F;
			this.lblTShirt.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTShirt.LocationFloat = new DevExpress.Utils.PointFloat(803.75F, 0F);
			this.lblTShirt.Name = "lblTShirt";
			this.lblTShirt.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTShirt.SizeF = new System.Drawing.SizeF(63.95825F, 15F);
			this.lblTShirt.StylePriority.UseFont = false;
			this.lblTShirt.StylePriority.UseTextAlignment = false;
			this.lblTShirt.Text = "714-832-8664";
			this.lblTShirt.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblClassGrade
			// 
			this.lblClassGrade.Dpi = 100F;
			this.lblClassGrade.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblClassGrade.LocationFloat = new DevExpress.Utils.PointFloat(867.7083F, 0F);
			this.lblClassGrade.Name = "lblClassGrade";
			this.lblClassGrade.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblClassGrade.SizeF = new System.Drawing.SizeF(95.62524F, 15F);
			this.lblClassGrade.StylePriority.UseFont = false;
			this.lblClassGrade.Text = "714-832-8664";
			// 
			// lblPhone
			// 
			this.lblPhone.Dpi = 100F;
			this.lblPhone.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblPhone.LocationFloat = new DevExpress.Utils.PointFloat(647.2917F, 0F);
			this.lblPhone.Name = "lblPhone";
			this.lblPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblPhone.SizeF = new System.Drawing.SizeF(156.4581F, 15F);
			this.lblPhone.StylePriority.UseFont = false;
			this.lblPhone.Text = "714-832-8664";
			// 
			// lblChurch
			// 
			this.lblChurch.Dpi = 100F;
			this.lblChurch.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblChurch.LocationFloat = new DevExpress.Utils.PointFloat(206.6667F, 0F);
			this.lblChurch.Name = "lblChurch";
			this.lblChurch.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblChurch.SizeF = new System.Drawing.SizeF(215.2083F, 15F);
			this.lblChurch.StylePriority.UseFont = false;
			this.lblChurch.Text = "714-832-8664";
			// 
			// lblGender
			// 
			this.lblGender.Dpi = 100F;
			this.lblGender.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblGender.LocationFloat = new DevExpress.Utils.PointFloat(421.875F, 0F);
			this.lblGender.Name = "lblGender";
			this.lblGender.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblGender.SizeF = new System.Drawing.SizeF(66.04169F, 15F);
			this.lblGender.StylePriority.UseFont = false;
			this.lblGender.StylePriority.UseTextAlignment = false;
			this.lblGender.Text = "714-832-8664";
			this.lblGender.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblParentName
			// 
			this.lblParentName.Dpi = 100F;
			this.lblParentName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblParentName.LocationFloat = new DevExpress.Utils.PointFloat(487.9167F, 0F);
			this.lblParentName.Name = "lblParentName";
			this.lblParentName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblParentName.SizeF = new System.Drawing.SizeF(159.375F, 15F);
			this.lblParentName.StylePriority.UseFont = false;
			this.lblParentName.Text = "714-832-8664";
			// 
			// lblAllergy
			// 
			this.lblAllergy.Dpi = 100F;
			this.lblAllergy.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Italic);
			this.lblAllergy.ForeColor = System.Drawing.Color.Red;
			this.lblAllergy.LocationFloat = new DevExpress.Utils.PointFloat(22.29166F, 14.99999F);
			this.lblAllergy.Multiline = true;
			this.lblAllergy.Name = "lblAllergy";
			this.lblAllergy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAllergy.SizeF = new System.Drawing.SizeF(919.1666F, 15F);
			this.lblAllergy.StylePriority.UseFont = false;
			this.lblAllergy.StylePriority.UseForeColor = false;
			this.lblAllergy.Text = "714-832-8664";
			// 
			// xrLabel6
			// 
			this.xrLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel6.BorderColor = System.Drawing.Color.White;
			this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel6.Dpi = 100F;
			this.xrLabel6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel6.ForeColor = System.Drawing.Color.White;
			this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 35.41667F);
			this.xrLabel6.Name = "xrLabel6";
			this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel6.SizeF = new System.Drawing.SizeF(206.6667F, 20.20833F);
			this.xrLabel6.StylePriority.UseBackColor = false;
			this.xrLabel6.StylePriority.UseBorderColor = false;
			this.xrLabel6.StylePriority.UseBorders = false;
			this.xrLabel6.StylePriority.UseFont = false;
			this.xrLabel6.StylePriority.UseForeColor = false;
			this.xrLabel6.Text = "Student Name";
			// 
			// gfStudentName
			// 
			this.gfStudentName.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2});
			this.gfStudentName.Dpi = 100F;
			this.gfStudentName.HeightF = 25.08333F;
			this.gfStudentName.Name = "gfStudentName";
			// 
			// xrLabel2
			// 
			this.xrLabel2.Dpi = 100F;
			this.xrLabel2.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel2.ForeColor = System.Drawing.Color.Black;
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(20.41677F, 0F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(230.2083F, 25.08333F);
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.StylePriority.UseForeColor = false;
			this.xrLabel2.Text = " ";
			// 
			// xrLabel8
			// 
			this.xrLabel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel8.BorderColor = System.Drawing.Color.White;
			this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel8.Dpi = 100F;
			this.xrLabel8.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel8.ForeColor = System.Drawing.Color.White;
			this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(442.2917F, 35.41667F);
			this.xrLabel8.Name = "xrLabel8";
			this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel8.SizeF = new System.Drawing.SizeF(66.04172F, 20.20833F);
			this.xrLabel8.StylePriority.UseBackColor = false;
			this.xrLabel8.StylePriority.UseBorderColor = false;
			this.xrLabel8.StylePriority.UseBorders = false;
			this.xrLabel8.StylePriority.UseFont = false;
			this.xrLabel8.StylePriority.UseForeColor = false;
			this.xrLabel8.StylePriority.UseTextAlignment = false;
			this.xrLabel8.Text = "Gender";
			this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// BottomMargin
			// 
			this.BottomMargin.Dpi = 100F;
			this.BottomMargin.HeightF = 50F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// xrPageInfo1
			// 
			this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo1.Dpi = 100F;
			this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo1.Format = "Page {0} of {1}";
			this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(811.2501F, 9.999974F);
			this.xrPageInfo1.Name = "xrPageInfo1";
			this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo1.SizeF = new System.Drawing.SizeF(178.7499F, 23F);
			this.xrPageInfo1.StylePriority.UseFont = false;
			this.xrPageInfo1.StylePriority.UseTextAlignment = false;
			this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblLastName
			// 
			this.lblLastName.BackColor = System.Drawing.Color.Yellow;
			this.lblLastName.Dpi = 100F;
			this.lblLastName.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblLastName.ForeColor = System.Drawing.Color.Red;
			this.lblLastName.LocationFloat = new DevExpress.Utils.PointFloat(241.25F, 0F);
			this.lblLastName.Name = "lblLastName";
			this.lblLastName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblLastName.SizeF = new System.Drawing.SizeF(27.08331F, 25.08333F);
			this.lblLastName.StylePriority.UseBackColor = false;
			this.lblLastName.StylePriority.UseFont = false;
			this.lblLastName.StylePriority.UseForeColor = false;
			this.lblLastName.StylePriority.UseTextAlignment = false;
			this.lblLastName.Text = "A";
			this.lblLastName.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// TopMargin
			// 
			this.TopMargin.Dpi = 100F;
			this.TopMargin.HeightF = 50F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// xrLabel12
			// 
			this.xrLabel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel12.BorderColor = System.Drawing.Color.White;
			this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel12.Dpi = 100F;
			this.xrLabel12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel12.ForeColor = System.Drawing.Color.White;
			this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(888.1247F, 35.41667F);
			this.xrLabel12.Name = "xrLabel12";
			this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel12.SizeF = new System.Drawing.SizeF(95.62524F, 20.20833F);
			this.xrLabel12.StylePriority.UseBackColor = false;
			this.xrLabel12.StylePriority.UseBorderColor = false;
			this.xrLabel12.StylePriority.UseBorders = false;
			this.xrLabel12.StylePriority.UseFont = false;
			this.xrLabel12.StylePriority.UseForeColor = false;
			this.xrLabel12.Text = "Class/Grade";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Dpi = 100F;
			this.ReportHeader.HeightF = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// xrLabel13
			// 
			this.xrLabel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel13.BorderColor = System.Drawing.Color.White;
			this.xrLabel13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel13.Dpi = 100F;
			this.xrLabel13.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel13.ForeColor = System.Drawing.Color.White;
			this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(824.1666F, 35.41667F);
			this.xrLabel13.Name = "xrLabel13";
			this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel13.SizeF = new System.Drawing.SizeF(63.95831F, 20.20833F);
			this.xrLabel13.StylePriority.UseBackColor = false;
			this.xrLabel13.StylePriority.UseBorderColor = false;
			this.xrLabel13.StylePriority.UseBorders = false;
			this.xrLabel13.StylePriority.UseFont = false;
			this.xrLabel13.StylePriority.UseForeColor = false;
			this.xrLabel13.StylePriority.UseTextAlignment = false;
			this.xrLabel13.Text = "T-Shirt";
			this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
			this.PageFooter.Dpi = 100F;
			this.PageFooter.HeightF = 32.99997F;
			this.PageFooter.Name = "PageFooter";
			// 
			// xrPageInfo2
			// 
			this.xrPageInfo2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo2.Dpi = 100F;
			this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo2.Format = "Printed on {0:MM/dd/yyyy} at {0:hh:mm:ss tt}";
			this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 9.999974F);
			this.xrPageInfo2.Name = "xrPageInfo2";
			this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
			this.xrPageInfo2.SizeF = new System.Drawing.SizeF(289.1666F, 23F);
			this.xrPageInfo2.StylePriority.UseFont = false;
			this.xrPageInfo2.StylePriority.UseTextAlignment = false;
			this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrLabel11
			// 
			this.xrLabel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel11.BorderColor = System.Drawing.Color.White;
			this.xrLabel11.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel11.Dpi = 100F;
			this.xrLabel11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel11.ForeColor = System.Drawing.Color.White;
			this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(667.7083F, 35.41667F);
			this.xrLabel11.Name = "xrLabel11";
			this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel11.SizeF = new System.Drawing.SizeF(156.4582F, 20.20833F);
			this.xrLabel11.StylePriority.UseBackColor = false;
			this.xrLabel11.StylePriority.UseBorderColor = false;
			this.xrLabel11.StylePriority.UseBorders = false;
			this.xrLabel11.StylePriority.UseFont = false;
			this.xrLabel11.StylePriority.UseForeColor = false;
			this.xrLabel11.Text = "Phone";
			// 
			// ghStudentName
			// 
			this.ghStudentName.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel3,
            this.xrLabel1,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.lblLastName});
			this.ghStudentName.Dpi = 100F;
			this.ghStudentName.HeightF = 75.83332F;
			this.ghStudentName.KeepTogether = true;
			this.ghStudentName.Name = "ghStudentName";
			this.ghStudentName.RepeatEveryPage = true;
			// 
			// xrLabel3
			// 
			this.xrLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel3.Dpi = 100F;
			this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel3.ForeColor = System.Drawing.Color.White;
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(41.66687F, 55.62499F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(942.0832F, 20.20833F);
			this.xrLabel3.StylePriority.UseBackColor = false;
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.StylePriority.UseForeColor = false;
			this.xrLabel3.Text = "Allergy/Special Needs";
			// 
			// xrLabel1
			// 
			this.xrLabel1.Dpi = 100F;
			this.xrLabel1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.ForeColor = System.Drawing.Color.Black;
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(230.2083F, 25.08333F);
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseForeColor = false;
			this.xrLabel1.StylePriority.UseTextAlignment = false;
			this.xrLabel1.Text = "Last Name Starts With:";
			this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// rptClassRosterByStudent
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.ghStudentName,
            this.gfStudentName,
            this.PageFooter});
			this.Landscape = true;
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
			this.PageHeight = 850;
			this.PageWidth = 1100;
			this.Version = "16.1";
			this.ParametersRequestBeforeShow += new System.EventHandler<DevExpress.XtraReports.Parameters.ParametersRequestEventArgs>(this.rptClassRosterSinglePage_ParametersRequestBeforeShow);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.XRLabel xrLabel9;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.XRLabel rptTitle;
		private DevExpress.XtraReports.UI.XRPictureBox picHeader;
		private DevExpress.XtraReports.UI.XRLabel xrLabel7;
		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.XRPanel xrPanel1;
		private DevExpress.XtraReports.UI.XRLabel lblStudentName;
		private DevExpress.XtraReports.UI.XRLabel lblTShirt;
		private DevExpress.XtraReports.UI.XRLabel lblChurch;
		private DevExpress.XtraReports.UI.XRLabel lblGender;
		private DevExpress.XtraReports.UI.XRLabel lblParentName;
		private DevExpress.XtraReports.UI.XRLabel xrLabel6;
		private DevExpress.XtraReports.UI.GroupFooterBand gfStudentName;
		private DevExpress.XtraReports.UI.XRLabel xrLabel8;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
		private DevExpress.XtraReports.UI.XRLabel lblLastName;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.XRLabel xrLabel12;
		private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
		private DevExpress.XtraReports.UI.XRLabel xrLabel13;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.XRLabel xrLabel11;
		private DevExpress.XtraReports.UI.GroupHeaderBand ghStudentName;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
		private DevExpress.XtraReports.UI.XRLabel lblPhone;
		private DevExpress.XtraReports.UI.XRLabel lblClassGrade;
		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRLabel lblAllergy;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;

	}
}
