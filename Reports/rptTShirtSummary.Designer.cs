﻿namespace VBS.Reports
{
	partial class rptTShirtSummary
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptTShirtSummary));
			DevExpress.XtraReports.UI.XRSummary xrSummary1 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary2 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary3 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary4 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary5 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary6 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary7 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary8 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary9 = new DevExpress.XtraReports.UI.XRSummary();
			DevExpress.XtraReports.UI.XRSummary xrSummary10 = new DevExpress.XtraReports.UI.XRSummary();
			this.lblYouthS = new DevExpress.XtraReports.UI.XRLabel();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.lblYouthXS = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultXXL = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultXL = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultL = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYouthL = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultM = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultS = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTeam = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTotal = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYouthM = new DevExpress.XtraReports.UI.XRLabel();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.picHeader = new DevExpress.XtraReports.UI.XRPictureBox();
			this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
			this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
			this.lblYouthXSSum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultSSum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultMSum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultLSum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYouthLSum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultXLSum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAdultXXLSum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTotalSum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYouthMSum = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYouthSSum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblNoShirtSize = new DevExpress.XtraReports.UI.XRLabel();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// lblYouthS
			// 
			this.lblYouthS.BorderColor = System.Drawing.Color.Transparent;
			this.lblYouthS.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblYouthS.BorderWidth = 0F;
			this.lblYouthS.Dpi = 100F;
			this.lblYouthS.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYouthS.LocationFloat = new DevExpress.Utils.PointFloat(353.5001F, 0F);
			this.lblYouthS.Name = "lblYouthS";
			this.lblYouthS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYouthS.SizeF = new System.Drawing.SizeF(78.33337F, 15F);
			this.lblYouthS.StylePriority.UseBorderColor = false;
			this.lblYouthS.StylePriority.UseBorders = false;
			this.lblYouthS.StylePriority.UseBorderWidth = false;
			this.lblYouthS.StylePriority.UseFont = false;
			this.lblYouthS.StylePriority.UseTextAlignment = false;
			this.lblYouthS.Text = "714-832-8664";
			this.lblYouthS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// TopMargin
			// 
			this.TopMargin.Dpi = 100F;
			this.TopMargin.HeightF = 50F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
			this.Detail.Dpi = 100F;
			this.Detail.HeightF = 16.45836F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
			// 
			// xrPanel1
			// 
			this.xrPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(206)))), ((int)(((byte)(235)))));
			this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblYouthXS,
            this.lblAdultXXL,
            this.lblAdultXL,
            this.lblAdultL,
            this.lblYouthL,
            this.lblAdultM,
            this.lblAdultS,
            this.lblTeam,
            this.lblTotal,
            this.lblYouthS,
            this.lblYouthM});
			this.xrPanel1.Dpi = 100F;
			this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.xrPanel1.Name = "xrPanel1";
			this.xrPanel1.SizeF = new System.Drawing.SizeF(963.3333F, 16.45836F);
			this.xrPanel1.StylePriority.UseBackColor = false;
			// 
			// lblYouthXS
			// 
			this.lblYouthXS.BorderColor = System.Drawing.Color.Transparent;
			this.lblYouthXS.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblYouthXS.BorderWidth = 0F;
			this.lblYouthXS.Dpi = 100F;
			this.lblYouthXS.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYouthXS.LocationFloat = new DevExpress.Utils.PointFloat(275.1666F, 0F);
			this.lblYouthXS.Name = "lblYouthXS";
			this.lblYouthXS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYouthXS.SizeF = new System.Drawing.SizeF(78.33337F, 15F);
			this.lblYouthXS.StylePriority.UseBorderColor = false;
			this.lblYouthXS.StylePriority.UseBorders = false;
			this.lblYouthXS.StylePriority.UseBorderWidth = false;
			this.lblYouthXS.StylePriority.UseFont = false;
			this.lblYouthXS.StylePriority.UseTextAlignment = false;
			this.lblYouthXS.Text = "714-832-8664";
			this.lblYouthXS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblAdultXXL
			// 
			this.lblAdultXXL.BorderColor = System.Drawing.Color.Transparent;
			this.lblAdultXXL.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblAdultXXL.BorderWidth = 0F;
			this.lblAdultXXL.Dpi = 100F;
			this.lblAdultXXL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultXXL.LocationFloat = new DevExpress.Utils.PointFloat(822.0837F, 0F);
			this.lblAdultXXL.Name = "lblAdultXXL";
			this.lblAdultXXL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultXXL.SizeF = new System.Drawing.SizeF(61.66675F, 15F);
			this.lblAdultXXL.StylePriority.UseBorderColor = false;
			this.lblAdultXXL.StylePriority.UseBorders = false;
			this.lblAdultXXL.StylePriority.UseBorderWidth = false;
			this.lblAdultXXL.StylePriority.UseFont = false;
			this.lblAdultXXL.StylePriority.UseTextAlignment = false;
			this.lblAdultXXL.Text = "714-832-8664";
			this.lblAdultXXL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblAdultXL
			// 
			this.lblAdultXL.BorderColor = System.Drawing.Color.Transparent;
			this.lblAdultXL.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblAdultXL.BorderWidth = 0F;
			this.lblAdultXL.Dpi = 100F;
			this.lblAdultXL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultXL.LocationFloat = new DevExpress.Utils.PointFloat(761.4584F, 0F);
			this.lblAdultXL.Name = "lblAdultXL";
			this.lblAdultXL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultXL.SizeF = new System.Drawing.SizeF(60.62494F, 15F);
			this.lblAdultXL.StylePriority.UseBorderColor = false;
			this.lblAdultXL.StylePriority.UseBorders = false;
			this.lblAdultXL.StylePriority.UseBorderWidth = false;
			this.lblAdultXL.StylePriority.UseFont = false;
			this.lblAdultXL.StylePriority.UseTextAlignment = false;
			this.lblAdultXL.Text = "714-832-8664";
			this.lblAdultXL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblAdultL
			// 
			this.lblAdultL.BorderColor = System.Drawing.Color.Transparent;
			this.lblAdultL.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblAdultL.BorderWidth = 0F;
			this.lblAdultL.Dpi = 100F;
			this.lblAdultL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultL.LocationFloat = new DevExpress.Utils.PointFloat(705.0003F, 0F);
			this.lblAdultL.Name = "lblAdultL";
			this.lblAdultL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultL.SizeF = new System.Drawing.SizeF(56.25F, 15F);
			this.lblAdultL.StylePriority.UseBorderColor = false;
			this.lblAdultL.StylePriority.UseBorders = false;
			this.lblAdultL.StylePriority.UseBorderWidth = false;
			this.lblAdultL.StylePriority.UseFont = false;
			this.lblAdultL.StylePriority.UseTextAlignment = false;
			this.lblAdultL.Text = "714-832-8664";
			this.lblAdultL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblYouthL
			// 
			this.lblYouthL.BorderColor = System.Drawing.Color.Transparent;
			this.lblYouthL.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblYouthL.BorderWidth = 0F;
			this.lblYouthL.Dpi = 100F;
			this.lblYouthL.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYouthL.LocationFloat = new DevExpress.Utils.PointFloat(510.5834F, 0F);
			this.lblYouthL.Name = "lblYouthL";
			this.lblYouthL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYouthL.SizeF = new System.Drawing.SizeF(78.33331F, 15F);
			this.lblYouthL.StylePriority.UseBorderColor = false;
			this.lblYouthL.StylePriority.UseBorders = false;
			this.lblYouthL.StylePriority.UseBorderWidth = false;
			this.lblYouthL.StylePriority.UseFont = false;
			this.lblYouthL.StylePriority.UseTextAlignment = false;
			this.lblYouthL.Text = "714-832-8664";
			this.lblYouthL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblAdultM
			// 
			this.lblAdultM.BorderColor = System.Drawing.Color.Transparent;
			this.lblAdultM.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblAdultM.BorderWidth = 0F;
			this.lblAdultM.Dpi = 100F;
			this.lblAdultM.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultM.LocationFloat = new DevExpress.Utils.PointFloat(647.2919F, 0F);
			this.lblAdultM.Name = "lblAdultM";
			this.lblAdultM.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultM.SizeF = new System.Drawing.SizeF(57.49982F, 15F);
			this.lblAdultM.StylePriority.UseBorderColor = false;
			this.lblAdultM.StylePriority.UseBorders = false;
			this.lblAdultM.StylePriority.UseBorderWidth = false;
			this.lblAdultM.StylePriority.UseFont = false;
			this.lblAdultM.StylePriority.UseTextAlignment = false;
			this.lblAdultM.Text = "714-832-8664";
			this.lblAdultM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblAdultS
			// 
			this.lblAdultS.BorderColor = System.Drawing.Color.Transparent;
			this.lblAdultS.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblAdultS.BorderWidth = 0F;
			this.lblAdultS.Dpi = 100F;
			this.lblAdultS.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultS.LocationFloat = new DevExpress.Utils.PointFloat(588.9589F, 0F);
			this.lblAdultS.Name = "lblAdultS";
			this.lblAdultS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultS.SizeF = new System.Drawing.SizeF(58.33301F, 15F);
			this.lblAdultS.StylePriority.UseBorderColor = false;
			this.lblAdultS.StylePriority.UseBorders = false;
			this.lblAdultS.StylePriority.UseBorderWidth = false;
			this.lblAdultS.StylePriority.UseFont = false;
			this.lblAdultS.StylePriority.UseTextAlignment = false;
			this.lblAdultS.Text = "714-832-8664";
			this.lblAdultS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblTeam
			// 
			this.lblTeam.BorderColor = System.Drawing.Color.Transparent;
			this.lblTeam.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblTeam.BorderWidth = 0F;
			this.lblTeam.Dpi = 100F;
			this.lblTeam.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTeam.LocationFloat = new DevExpress.Utils.PointFloat(9.536743E-05F, 0F);
			this.lblTeam.Name = "lblTeam";
			this.lblTeam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTeam.SizeF = new System.Drawing.SizeF(275.1665F, 15F);
			this.lblTeam.StylePriority.UseBorderColor = false;
			this.lblTeam.StylePriority.UseBorders = false;
			this.lblTeam.StylePriority.UseBorderWidth = false;
			this.lblTeam.StylePriority.UseFont = false;
			this.lblTeam.StylePriority.UseTextAlignment = false;
			this.lblTeam.Text = "714-832-8664";
			this.lblTeam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblTotal
			// 
			this.lblTotal.BorderColor = System.Drawing.Color.Transparent;
			this.lblTotal.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblTotal.BorderWidth = 0F;
			this.lblTotal.Dpi = 100F;
			this.lblTotal.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTotal.LocationFloat = new DevExpress.Utils.PointFloat(883.75F, 0F);
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTotal.SizeF = new System.Drawing.SizeF(78.33319F, 15F);
			this.lblTotal.StylePriority.UseBorderColor = false;
			this.lblTotal.StylePriority.UseBorders = false;
			this.lblTotal.StylePriority.UseBorderWidth = false;
			this.lblTotal.StylePriority.UseFont = false;
			this.lblTotal.StylePriority.UseTextAlignment = false;
			this.lblTotal.Text = "714-832-8664";
			this.lblTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblYouthM
			// 
			this.lblYouthM.BorderColor = System.Drawing.Color.Transparent;
			this.lblYouthM.Borders = DevExpress.XtraPrinting.BorderSide.None;
			this.lblYouthM.BorderWidth = 0F;
			this.lblYouthM.Dpi = 100F;
			this.lblYouthM.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYouthM.LocationFloat = new DevExpress.Utils.PointFloat(431.8334F, 0F);
			this.lblYouthM.Name = "lblYouthM";
			this.lblYouthM.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYouthM.SizeF = new System.Drawing.SizeF(78.33331F, 15F);
			this.lblYouthM.StylePriority.UseBorderColor = false;
			this.lblYouthM.StylePriority.UseBorders = false;
			this.lblYouthM.StylePriority.UseBorderWidth = false;
			this.lblYouthM.StylePriority.UseFont = false;
			this.lblYouthM.StylePriority.UseTextAlignment = false;
			this.lblYouthM.Text = "714-832-8664";
			this.lblYouthM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Dpi = 100F;
			this.ReportHeader.HeightF = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// picHeader
			// 
			this.picHeader.Dpi = 100F;
			this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
			this.picHeader.LocationFloat = new DevExpress.Utils.PointFloat(619.7917F, 0F);
			this.picHeader.Name = "picHeader";
			this.picHeader.SizeF = new System.Drawing.SizeF(380.2083F, 56.25F);
			this.picHeader.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
			// 
			// xrPageInfo1
			// 
			this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo1.Dpi = 100F;
			this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo1.Format = "Page {0} of {1}";
			this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(811.2501F, 9.999974F);
			this.xrPageInfo1.Name = "xrPageInfo1";
			this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo1.SizeF = new System.Drawing.SizeF(178.7499F, 23F);
			this.xrPageInfo1.StylePriority.UseFont = false;
			this.xrPageInfo1.StylePriority.UseTextAlignment = false;
			this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel12,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.xrLabel7,
            this.xrLabel10,
            this.rptTitle,
            this.picHeader});
			this.PageHeader.Dpi = 100F;
			this.PageHeader.HeightF = 96.24999F;
			this.PageHeader.Name = "PageHeader";
			// 
			// xrLabel12
			// 
			this.xrLabel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel12.BorderColor = System.Drawing.Color.White;
			this.xrLabel12.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel12.Dpi = 100F;
			this.xrLabel12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel12.ForeColor = System.Drawing.Color.White;
			this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(295.5833F, 76.04166F);
			this.xrLabel12.Name = "xrLabel12";
			this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel12.SizeF = new System.Drawing.SizeF(78.33334F, 20.20833F);
			this.xrLabel12.StylePriority.UseBackColor = false;
			this.xrLabel12.StylePriority.UseBorderColor = false;
			this.xrLabel12.StylePriority.UseBorders = false;
			this.xrLabel12.StylePriority.UseFont = false;
			this.xrLabel12.StylePriority.UseForeColor = false;
			this.xrLabel12.StylePriority.UseTextAlignment = false;
			this.xrLabel12.Text = "Youth XS";
			this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel9
			// 
			this.xrLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel9.BorderColor = System.Drawing.Color.White;
			this.xrLabel9.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel9.Dpi = 100F;
			this.xrLabel9.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel9.ForeColor = System.Drawing.Color.White;
			this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(904.1666F, 76.04166F);
			this.xrLabel9.Name = "xrLabel9";
			this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel9.SizeF = new System.Drawing.SizeF(78.33331F, 20.20833F);
			this.xrLabel9.StylePriority.UseBackColor = false;
			this.xrLabel9.StylePriority.UseBorderColor = false;
			this.xrLabel9.StylePriority.UseBorders = false;
			this.xrLabel9.StylePriority.UseFont = false;
			this.xrLabel9.StylePriority.UseForeColor = false;
			this.xrLabel9.StylePriority.UseTextAlignment = false;
			this.xrLabel9.Text = "Total";
			this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel8
			// 
			this.xrLabel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel8.BorderColor = System.Drawing.Color.White;
			this.xrLabel8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel8.Dpi = 100F;
			this.xrLabel8.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel8.ForeColor = System.Drawing.Color.White;
			this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(842.5001F, 76.04166F);
			this.xrLabel8.Name = "xrLabel8";
			this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel8.SizeF = new System.Drawing.SizeF(61.66663F, 20.20833F);
			this.xrLabel8.StylePriority.UseBackColor = false;
			this.xrLabel8.StylePriority.UseBorderColor = false;
			this.xrLabel8.StylePriority.UseBorders = false;
			this.xrLabel8.StylePriority.UseFont = false;
			this.xrLabel8.StylePriority.UseForeColor = false;
			this.xrLabel8.StylePriority.UseTextAlignment = false;
			this.xrLabel8.Text = "Ad. XXL";
			this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel6
			// 
			this.xrLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel6.BorderColor = System.Drawing.Color.White;
			this.xrLabel6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel6.Dpi = 100F;
			this.xrLabel6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel6.ForeColor = System.Drawing.Color.White;
			this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(781.8751F, 76.04166F);
			this.xrLabel6.Name = "xrLabel6";
			this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel6.SizeF = new System.Drawing.SizeF(60.625F, 20.20833F);
			this.xrLabel6.StylePriority.UseBackColor = false;
			this.xrLabel6.StylePriority.UseBorderColor = false;
			this.xrLabel6.StylePriority.UseBorders = false;
			this.xrLabel6.StylePriority.UseFont = false;
			this.xrLabel6.StylePriority.UseForeColor = false;
			this.xrLabel6.StylePriority.UseTextAlignment = false;
			this.xrLabel6.Text = "Ad. XL";
			this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel5
			// 
			this.xrLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel5.BorderColor = System.Drawing.Color.White;
			this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel5.Dpi = 100F;
			this.xrLabel5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.ForeColor = System.Drawing.Color.White;
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(725.4167F, 76.04166F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(56.45837F, 20.20833F);
			this.xrLabel5.StylePriority.UseBackColor = false;
			this.xrLabel5.StylePriority.UseBorderColor = false;
			this.xrLabel5.StylePriority.UseBorders = false;
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.StylePriority.UseForeColor = false;
			this.xrLabel5.StylePriority.UseTextAlignment = false;
			this.xrLabel5.Text = "Ad. L";
			this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel4
			// 
			this.xrLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel4.BorderColor = System.Drawing.Color.White;
			this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel4.Dpi = 100F;
			this.xrLabel4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel4.ForeColor = System.Drawing.Color.White;
			this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(667.917F, 76.04166F);
			this.xrLabel4.Name = "xrLabel4";
			this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel4.SizeF = new System.Drawing.SizeF(57.49994F, 20.20833F);
			this.xrLabel4.StylePriority.UseBackColor = false;
			this.xrLabel4.StylePriority.UseBorderColor = false;
			this.xrLabel4.StylePriority.UseBorders = false;
			this.xrLabel4.StylePriority.UseFont = false;
			this.xrLabel4.StylePriority.UseForeColor = false;
			this.xrLabel4.StylePriority.UseTextAlignment = false;
			this.xrLabel4.Text = "Ad. M";
			this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel3
			// 
			this.xrLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel3.BorderColor = System.Drawing.Color.White;
			this.xrLabel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel3.Dpi = 100F;
			this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel3.ForeColor = System.Drawing.Color.White;
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(609.3755F, 76.04166F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(58.54144F, 20.20833F);
			this.xrLabel3.StylePriority.UseBackColor = false;
			this.xrLabel3.StylePriority.UseBorderColor = false;
			this.xrLabel3.StylePriority.UseBorders = false;
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.StylePriority.UseForeColor = false;
			this.xrLabel3.StylePriority.UseTextAlignment = false;
			this.xrLabel3.Text = "Ad. S";
			this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel2
			// 
			this.xrLabel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel2.BorderColor = System.Drawing.Color.White;
			this.xrLabel2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel2.Dpi = 100F;
			this.xrLabel2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel2.ForeColor = System.Drawing.Color.White;
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(530.5834F, 76.04166F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(78.33334F, 20.20833F);
			this.xrLabel2.StylePriority.UseBackColor = false;
			this.xrLabel2.StylePriority.UseBorderColor = false;
			this.xrLabel2.StylePriority.UseBorders = false;
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.StylePriority.UseForeColor = false;
			this.xrLabel2.StylePriority.UseTextAlignment = false;
			this.xrLabel2.Text = "Youth L";
			this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel1
			// 
			this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel1.BorderColor = System.Drawing.Color.White;
			this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel1.Dpi = 100F;
			this.xrLabel1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.ForeColor = System.Drawing.Color.White;
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(452.2499F, 76.04166F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(78.33334F, 20.20833F);
			this.xrLabel1.StylePriority.UseBackColor = false;
			this.xrLabel1.StylePriority.UseBorderColor = false;
			this.xrLabel1.StylePriority.UseBorders = false;
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseForeColor = false;
			this.xrLabel1.StylePriority.UseTextAlignment = false;
			this.xrLabel1.Text = "Youth M";
			this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel7
			// 
			this.xrLabel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel7.BorderColor = System.Drawing.Color.White;
			this.xrLabel7.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel7.Dpi = 100F;
			this.xrLabel7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel7.ForeColor = System.Drawing.Color.White;
			this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 76.04166F);
			this.xrLabel7.Name = "xrLabel7";
			this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel7.SizeF = new System.Drawing.SizeF(275.1666F, 20.20833F);
			this.xrLabel7.StylePriority.UseBackColor = false;
			this.xrLabel7.StylePriority.UseBorderColor = false;
			this.xrLabel7.StylePriority.UseBorders = false;
			this.xrLabel7.StylePriority.UseFont = false;
			this.xrLabel7.StylePriority.UseForeColor = false;
			this.xrLabel7.Text = "Class/Team";
			// 
			// xrLabel10
			// 
			this.xrLabel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel10.BorderColor = System.Drawing.Color.White;
			this.xrLabel10.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel10.Dpi = 100F;
			this.xrLabel10.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel10.ForeColor = System.Drawing.Color.White;
			this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(373.9167F, 76.04166F);
			this.xrLabel10.Name = "xrLabel10";
			this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel10.SizeF = new System.Drawing.SizeF(78.33334F, 20.20833F);
			this.xrLabel10.StylePriority.UseBackColor = false;
			this.xrLabel10.StylePriority.UseBorderColor = false;
			this.xrLabel10.StylePriority.UseBorders = false;
			this.xrLabel10.StylePriority.UseFont = false;
			this.xrLabel10.StylePriority.UseForeColor = false;
			this.xrLabel10.StylePriority.UseTextAlignment = false;
			this.xrLabel10.Text = "Youth S";
			this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// rptTitle
			// 
			this.rptTitle.Dpi = 100F;
			this.rptTitle.Font = new System.Drawing.Font("Verdana", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rptTitle.ForeColor = System.Drawing.Color.Blue;
			this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.rptTitle.Name = "rptTitle";
			this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.rptTitle.SizeF = new System.Drawing.SizeF(356.25F, 31.33333F);
			this.rptTitle.StylePriority.UseFont = false;
			this.rptTitle.StylePriority.UseForeColor = false;
			this.rptTitle.Text = "T-Shirt Summary";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
			this.PageFooter.Dpi = 100F;
			this.PageFooter.HeightF = 33.00001F;
			this.PageFooter.Name = "PageFooter";
			// 
			// xrPageInfo2
			// 
			this.xrPageInfo2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo2.Dpi = 100F;
			this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo2.Format = "Printed on {0:MM/dd/yyyy} at {0:hh:mm:ss tt}";
			this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
			this.xrPageInfo2.Name = "xrPageInfo2";
			this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
			this.xrPageInfo2.SizeF = new System.Drawing.SizeF(289.1666F, 23F);
			this.xrPageInfo2.StylePriority.UseFont = false;
			this.xrPageInfo2.StylePriority.UseTextAlignment = false;
			this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// BottomMargin
			// 
			this.BottomMargin.Dpi = 100F;
			this.BottomMargin.HeightF = 50F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNoShirtSize,
            this.lblYouthXSSum,
            this.lblAdultSSum,
            this.lblAdultMSum,
            this.lblAdultLSum,
            this.lblYouthLSum,
            this.lblAdultXLSum,
            this.lblAdultXXLSum,
            this.lblTotalSum,
            this.lblYouthMSum,
            this.xrLabel11,
            this.lblYouthSSum});
			this.ReportFooter.Dpi = 100F;
			this.ReportFooter.HeightF = 60.41667F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ReportFooter_BeforePrint);
			this.ReportFooter.AfterPrint += new System.EventHandler(this.ReportFooter_AfterPrint);
			// 
			// lblYouthXSSum
			// 
			this.lblYouthXSSum.Dpi = 100F;
			this.lblYouthXSSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYouthXSSum.LocationFloat = new DevExpress.Utils.PointFloat(297.25F, 45.41667F);
			this.lblYouthXSSum.Name = "lblYouthXSSum";
			this.lblYouthXSSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYouthXSSum.SizeF = new System.Drawing.SizeF(76.66656F, 15F);
			this.lblYouthXSSum.StylePriority.UseFont = false;
			this.lblYouthXSSum.StylePriority.UseTextAlignment = false;
			xrSummary1.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblYouthXSSum.Summary = xrSummary1;
			this.lblYouthXSSum.Text = "714-832-8664";
			this.lblYouthXSSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// lblAdultSSum
			// 
			this.lblAdultSSum.Dpi = 100F;
			this.lblAdultSSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultSSum.LocationFloat = new DevExpress.Utils.PointFloat(609.3755F, 45.41667F);
			this.lblAdultSSum.Name = "lblAdultSSum";
			this.lblAdultSSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultSSum.SizeF = new System.Drawing.SizeF(58.33307F, 15F);
			this.lblAdultSSum.StylePriority.UseFont = false;
			this.lblAdultSSum.StylePriority.UseTextAlignment = false;
			xrSummary2.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblAdultSSum.Summary = xrSummary2;
			this.lblAdultSSum.Text = "714-832-8664";
			this.lblAdultSSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// lblAdultMSum
			// 
			this.lblAdultMSum.Dpi = 100F;
			this.lblAdultMSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultMSum.LocationFloat = new DevExpress.Utils.PointFloat(667.7086F, 45.41667F);
			this.lblAdultMSum.Name = "lblAdultMSum";
			this.lblAdultMSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultMSum.SizeF = new System.Drawing.SizeF(57.5F, 15F);
			this.lblAdultMSum.StylePriority.UseFont = false;
			this.lblAdultMSum.StylePriority.UseTextAlignment = false;
			xrSummary3.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblAdultMSum.Summary = xrSummary3;
			this.lblAdultMSum.Text = "714-832-8664";
			this.lblAdultMSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// lblAdultLSum
			// 
			this.lblAdultLSum.Dpi = 100F;
			this.lblAdultLSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultLSum.LocationFloat = new DevExpress.Utils.PointFloat(725.2086F, 45.41667F);
			this.lblAdultLSum.Name = "lblAdultLSum";
			this.lblAdultLSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultLSum.SizeF = new System.Drawing.SizeF(56.45844F, 15F);
			this.lblAdultLSum.StylePriority.UseFont = false;
			this.lblAdultLSum.StylePriority.UseTextAlignment = false;
			xrSummary4.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblAdultLSum.Summary = xrSummary4;
			this.lblAdultLSum.Text = "714-832-8664";
			this.lblAdultLSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// lblYouthLSum
			// 
			this.lblYouthLSum.Dpi = 100F;
			this.lblYouthLSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYouthLSum.LocationFloat = new DevExpress.Utils.PointFloat(530.9999F, 45.41667F);
			this.lblYouthLSum.Name = "lblYouthLSum";
			this.lblYouthLSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYouthLSum.SizeF = new System.Drawing.SizeF(78.33337F, 15F);
			this.lblYouthLSum.StylePriority.UseFont = false;
			this.lblYouthLSum.StylePriority.UseTextAlignment = false;
			xrSummary5.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblYouthLSum.Summary = xrSummary5;
			this.lblYouthLSum.Text = "714-832-8664";
			this.lblYouthLSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// lblAdultXLSum
			// 
			this.lblAdultXLSum.Dpi = 100F;
			this.lblAdultXLSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultXLSum.LocationFloat = new DevExpress.Utils.PointFloat(781.8751F, 45.41667F);
			this.lblAdultXLSum.Name = "lblAdultXLSum";
			this.lblAdultXLSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultXLSum.SizeF = new System.Drawing.SizeF(60.625F, 15F);
			this.lblAdultXLSum.StylePriority.UseFont = false;
			this.lblAdultXLSum.StylePriority.UseTextAlignment = false;
			xrSummary6.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblAdultXLSum.Summary = xrSummary6;
			this.lblAdultXLSum.Text = "714-832-8664";
			this.lblAdultXLSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// lblAdultXXLSum
			// 
			this.lblAdultXXLSum.Dpi = 100F;
			this.lblAdultXXLSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAdultXXLSum.LocationFloat = new DevExpress.Utils.PointFloat(842.5003F, 45.41667F);
			this.lblAdultXXLSum.Name = "lblAdultXXLSum";
			this.lblAdultXXLSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAdultXXLSum.SizeF = new System.Drawing.SizeF(61.66675F, 15F);
			this.lblAdultXXLSum.StylePriority.UseFont = false;
			this.lblAdultXXLSum.StylePriority.UseTextAlignment = false;
			xrSummary7.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblAdultXXLSum.Summary = xrSummary7;
			this.lblAdultXXLSum.Text = "714-832-8664";
			this.lblAdultXXLSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// lblTotalSum
			// 
			this.lblTotalSum.Dpi = 100F;
			this.lblTotalSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTotalSum.LocationFloat = new DevExpress.Utils.PointFloat(904.1666F, 45.41667F);
			this.lblTotalSum.Name = "lblTotalSum";
			this.lblTotalSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTotalSum.SizeF = new System.Drawing.SizeF(78.33337F, 15F);
			this.lblTotalSum.StylePriority.UseFont = false;
			this.lblTotalSum.StylePriority.UseTextAlignment = false;
			xrSummary8.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblTotalSum.Summary = xrSummary8;
			this.lblTotalSum.Text = "714-832-8664";
			this.lblTotalSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// lblYouthMSum
			// 
			this.lblYouthMSum.Dpi = 100F;
			this.lblYouthMSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYouthMSum.LocationFloat = new DevExpress.Utils.PointFloat(452.2499F, 45.41667F);
			this.lblYouthMSum.Name = "lblYouthMSum";
			this.lblYouthMSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYouthMSum.SizeF = new System.Drawing.SizeF(78.33334F, 15F);
			this.lblYouthMSum.StylePriority.UseFont = false;
			this.lblYouthMSum.StylePriority.UseTextAlignment = false;
			xrSummary9.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblYouthMSum.Summary = xrSummary9;
			this.lblYouthMSum.Text = "714-832-8664";
			this.lblYouthMSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// xrLabel11
			// 
			this.xrLabel11.Dpi = 100F;
			this.xrLabel11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(22.08354F, 45.41667F);
			this.xrLabel11.Name = "xrLabel11";
			this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel11.SizeF = new System.Drawing.SizeF(275.1665F, 15F);
			this.xrLabel11.StylePriority.UseFont = false;
			this.xrLabel11.Text = "Total:";
			// 
			// lblYouthSSum
			// 
			this.lblYouthSSum.Dpi = 100F;
			this.lblYouthSSum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYouthSSum.LocationFloat = new DevExpress.Utils.PointFloat(375.5834F, 45.41667F);
			this.lblYouthSSum.Name = "lblYouthSSum";
			this.lblYouthSSum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYouthSSum.SizeF = new System.Drawing.SizeF(76.66656F, 15F);
			this.lblYouthSSum.StylePriority.UseFont = false;
			this.lblYouthSSum.StylePriority.UseTextAlignment = false;
			xrSummary10.Running = DevExpress.XtraReports.UI.SummaryRunning.Report;
			this.lblYouthSSum.Summary = xrSummary10;
			this.lblYouthSSum.Text = "714-832-8664";
			this.lblYouthSSum.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// lblNoShirtSize
			// 
			this.lblNoShirtSize.Dpi = 100F;
			this.lblNoShirtSize.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Italic);
			this.lblNoShirtSize.ForeColor = System.Drawing.Color.Red;
			this.lblNoShirtSize.LocationFloat = new DevExpress.Utils.PointFloat(55.20833F, 10F);
			this.lblNoShirtSize.Name = "lblNoShirtSize";
			this.lblNoShirtSize.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblNoShirtSize.SizeF = new System.Drawing.SizeF(872.0415F, 15F);
			this.lblNoShirtSize.StylePriority.UseFont = false;
			this.lblNoShirtSize.StylePriority.UseForeColor = false;
			this.lblNoShirtSize.Text = "Total:";
			// 
			// rptTShirtSummary
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.PageFooter,
            this.ReportFooter});
			this.Landscape = true;
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
			this.PageHeight = 850;
			this.PageWidth = 1100;
			this.Version = "16.1";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.XRLabel lblYouthS;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.XRPanel xrPanel1;
		private DevExpress.XtraReports.UI.XRLabel lblTeam;
		private DevExpress.XtraReports.UI.XRLabel lblTotal;
		private DevExpress.XtraReports.UI.XRLabel lblYouthM;
		private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
		private DevExpress.XtraReports.UI.XRPictureBox picHeader;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.XRLabel xrLabel7;
		private DevExpress.XtraReports.UI.XRLabel xrLabel10;
		private DevExpress.XtraReports.UI.XRLabel rptTitle;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
		private DevExpress.XtraReports.UI.XRLabel xrLabel9;
		private DevExpress.XtraReports.UI.XRLabel xrLabel8;
		private DevExpress.XtraReports.UI.XRLabel xrLabel6;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.UI.XRLabel xrLabel4;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
		private DevExpress.XtraReports.UI.XRLabel lblAdultXXL;
		private DevExpress.XtraReports.UI.XRLabel lblAdultXL;
		private DevExpress.XtraReports.UI.XRLabel lblAdultL;
		private DevExpress.XtraReports.UI.XRLabel lblYouthL;
		private DevExpress.XtraReports.UI.XRLabel lblAdultM;
		private DevExpress.XtraReports.UI.XRLabel lblAdultS;
		private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
		private DevExpress.XtraReports.UI.XRLabel lblYouthSSum;
		private DevExpress.XtraReports.UI.XRLabel xrLabel11;
		private DevExpress.XtraReports.UI.XRLabel lblAdultSSum;
		private DevExpress.XtraReports.UI.XRLabel lblAdultMSum;
		private DevExpress.XtraReports.UI.XRLabel lblAdultLSum;
		private DevExpress.XtraReports.UI.XRLabel lblYouthLSum;
		private DevExpress.XtraReports.UI.XRLabel lblAdultXLSum;
		private DevExpress.XtraReports.UI.XRLabel lblAdultXXLSum;
		private DevExpress.XtraReports.UI.XRLabel lblTotalSum;
		private DevExpress.XtraReports.UI.XRLabel lblYouthMSum;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
		private DevExpress.XtraReports.UI.XRLabel lblYouthXS;
		private DevExpress.XtraReports.UI.XRLabel xrLabel12;
		private DevExpress.XtraReports.UI.XRLabel lblYouthXSSum;
		private DevExpress.XtraReports.UI.XRLabel lblNoShirtSize;
	}
}
