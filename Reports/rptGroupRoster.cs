﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Web;


namespace VBS.Reports
{
	public partial class rptGroupRoster : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptGroupRoster()
		{
			InitializeComponent();

			// Set the bindings
			lblLastName.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_GroupName);
			lblLastName.DataBindings.Add("Bookmark", null, Search_VBS_Volunteer.FN_GridCustom_1);
			//lblLastName.DataBindings.Add("NavigateUrl", null, Search_VBS_Volunteer.FN_EmailHyperlink);

			lblAdultCount.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_GridCustom_2);
			lblUnder18Count.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_GridCustom_3);

			// Detail Fields
			lblStudentName.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_VolunteerNameFormatted);
			lblContactPhone.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_HomePhone);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_MobilePhone);
			lblEmail.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_Email);
			lblEmail.DataBindings.Add("NavigateUrl", null, Search_VBS_Volunteer.FN_EmailHyperlink);
			chkTeamCoord.DataBindings.Add("Checked", null, Search_VBS_Volunteer.FN_TeamCoordinator);
			chkAdult.DataBindings.Add("Checked", null, Search_VBS_Volunteer.FN_IsAdult);
			//chkJuniorHelper.DataBindings.Add("Checked", null, Search_VBS_Volunteer.FN_TeamCoordinator);
			lblTShirt.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_ShirtSize);
			lblLimitedWork.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_HelpDOWHTMLFormattedReport);

			// Bind the groupheader
			ghGroup.GroupFields.Add(new GroupField(Search_VBS_Volunteer.FN_GroupName, XRColumnSortOrder.Ascending));
			ghGroup.GroupFields.Add(new GroupField(Search_VBS_Volunteer.FN_GridCustom_2, XRColumnSortOrder.Ascending));
			ghGroup.GroupFields.Add(new GroupField(Search_VBS_Volunteer.FN_GridCustom_3, XRColumnSortOrder.Ascending));
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			pnlColor.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			//// Set the height depending on Days of the week
			////16.45836
			lblLimitedWork.Visible = true;
			//if (lblLimitedWork.Text.ToLower().Contains("all"))
			if (GetCurrentColumnValue(Search_VBS_Volunteer.FN_HelpDOW).ToString().ToLower().Contains("all"))
			{
				lblLimitedWork.Visible = false;
				pnlColor.HeightF = 16.45836F;
				Detail.HeightF = 16.45836F;
			}

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_Volunteer.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_Volunteer.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblEmail.Font = new Font(lblEmail.Font, FontStyle.Underline);
				lblEmail.ForeColor = Color.Blue;
			}
			else
			{
				lblEmail.Font = new Font(lblEmail.Font, FontStyle.Regular);
				lblEmail.ForeColor = Color.Black;
			}

			// Set if they're not an adult
			chkJuniorHelper.Checked = !(bool)(GetCurrentColumnValue(Search_VBS_Volunteer.FN_IsAdult));
		}

		private void rptClassRosterSinglePage_ParametersRequestBeforeShow(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
		{
			//foreach (ParameterInfo info in e.ParametersInformation)
			//{
			//	if (info.Parameter.Name.ToLower() == "ClassCodeParam".ToLower())
			//	{
			//		ASPxGridLookup lookUpEdit = new ASPxGridLookup();
			//		//LookUpEdit lookUpEdit = new LookUpEdit();
			//		//lookUpEdit.Properties.DataSource = dataSet.Categories;
			//		lookUpEdit.Properties.DisplayMember = "CategoryName";
			//		lookUpEdit.Properties.ValueMember = "CategoryID";
			//		lookUpEdit.Properties.Columns.Add(new LookUpColumnInfo("CategoryName", 0, "Category Name"));
			//		lookUpEdit.Properties.NullText = "<Select Category>";

			//		info.Editor = lookUpEdit;
			//		info.Parameter.Value = DBNull.Value;
			//	}
			//}
		}

		private void Detail_AfterPrint(object sender, EventArgs e)
		{
			// Set the height depending on Days of the week
			//16.45836
			//lblLimitedWork.Visible = true;
			//if (lblLimitedWork.Text.ToLower().Contains("all"))
			//{
			//	lblLimitedWork.Visible = false;
			//	pnlColor.HeightF = 16.45836F;
			//	Detail.HeightF = 16.45836F;
			//}
		}
	}
}
