﻿<%@ Page Title="Student Export (Excel)" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExcelExtract.aspx.cs" Inherits="VBS.Reports.ExcelExtract" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Student Export (Excel)</h1>
				<h3>Use the screen below to view class assignments. To select fields that appear, right click on any column header in 
					the grid, and choose "Show/Hide hidden field list".  Drag and drop columns into the grid.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script type="text/javascript">
		var colName;
		function OnItemClick(s, e) {
			if (e.item.name == 'HideColumn') {
				grid.PerformCallback(colName);
				colName = null;
			}
			else {
				if (grid.IsCustomizationWindowVisible())
					grid.HideCustomizationWindow();
				else
					grid.ShowCustomizationWindow();
			}
		}

		function OnContextMenu(s, e) {
			if (e.objectType == 'header') {
				colName = s.GetColumn(e.index).fieldName;
				headerMenu.GetItemByName('HideColumn').SetEnabled((colName == null ? false : true));
				headerMenu.ShowAtPos(ASPxClientUtils.GetEventX(e.htmlEvent), ASPxClientUtils.GetEventY(e.htmlEvent));
			}
		}
    </script>
	<table>
		<tr>
			<td width="1%"><dx:ASPxButton ID="btnXlsxExport" runat="server" Text="Export to XLSX (Excel 2007 and up)" UseSubmitBehavior="False"
                    OnClick="btnXlsxExport_Click" /></td>
			<td width="1%"><dx:ASPxButton ID="btnXlsExport" runat="server" Text="Export to XLS (Excel 97/2003)" UseSubmitBehavior="False"
                    OnClick="btnXlsExport_Click" /></td>
			<td width="98%" nowrap>&nbsp;</td>
		</tr>
	</table>
	<dx:ASPxGridView ID="grid" ClientInstanceName="grid" runat="server" 
		Width="100%" SettingsEditing-Mode="Batch" OnDataBinding="grid_DataBinding" OnCustomCallback="grid_CustomCallback"
		SettingsBehavior-FilterRowMode="OnClick" SettingsPager-PageSize="30" OnCommandButtonInitialize="grid_CommandButtonInitialize"
		SettingsBehavior-AllowSort="false" Settings-UseFixedTableLayout="true">
		<Columns>
			<dx:GridViewDataColumn FieldName="FullNameLastFirst" VisibleIndex="1" /> 
			<dx:GridViewDataColumn FieldName="Grade" VisibleIndex="2" /> 
			<dx:GridViewDataColumn FieldName="Friend1FullNameFirstLast" VisibleIndex="3" /> 
			<dx:GridViewDataColumn FieldName="Friend2FullNameFirstLast" VisibleIndex="4" /> 
			<dx:GridViewDataColumn FieldName="Gender" VisibleIndex="5" /> 
			<dx:GridViewDataColumn FieldName="ClassCode" VisibleIndex="6" /> 
			


			<dx:GridViewDataColumn FieldName="ChurchAttended" Visible="false" VisibleIndex="102" />
			<dx:GridViewDataColumn FieldName="City" Visible="false" VisibleIndex="103" />
			<dx:GridViewDataColumn FieldName="Comments" Visible="false" VisibleIndex="105" />
			<dx:GridViewDataColumn FieldName="ConfirmationNum" Visible="false" VisibleIndex="106" />
			<dx:GridViewDataColumn FieldName="DateDOBChild" Visible="false" VisibleIndex="108" />
			<dx:GridViewDataColumn FieldName="DateModified" Visible="false" VisibleIndex="109" />
			<dx:GridViewDataColumn FieldName="DateSubmitted" Visible="false" VisibleIndex="110" />
			<dx:GridViewDataColumn FieldName="DonateMoneyToScholarship" Visible="false" VisibleIndex="112" />
			<dx:GridViewDataColumn FieldName="Email" Visible="false" VisibleIndex="113"><DataItemTemplate>
				<dx:ASPxHyperLink ForeColor="Blue" ID="ASPxHyperLinkTest" 
					runat="server" Text='<%#Eval("Email") %>' NavigateUrl='<%#String.Format("mailto:{0}",Eval("Email"))%>' />
			</DataItemTemplate></dx:GridViewDataColumn>
			<dx:GridViewDataColumn FieldName="FirstName" Visible="false" VisibleIndex="114" />
			<dx:GridViewDataColumn FieldName="FoodAllergies" Visible="false" VisibleIndex="115" />
			<dx:GridViewDataColumn FieldName="FormTotal" Visible="false" VisibleIndex="116" />
			<dx:GridViewDataColumn FieldName="Friend1First" Visible="false" VisibleIndex="117" />
			<dx:GridViewDataColumn FieldName="Friend1Last" Visible="false" VisibleIndex="118" />
			<dx:GridViewDataColumn FieldName="Friend2First" Visible="false" VisibleIndex="119" />
			<dx:GridViewDataColumn FieldName="Friend2Last" Visible="false" VisibleIndex="120" />
			<dx:GridViewDataColumn FieldName="GradeInFall" Visible="false" VisibleIndex="123" />
			<dx:GridViewDataColumn FieldName="HomePhone" Visible="false" VisibleIndex="124" />
			<dx:GridViewDataColumn FieldName="Inactive" Visible="false" VisibleIndex="125" />
			<dx:GridViewDataColumn FieldName="IndividualID" Visible="false" VisibleIndex="126" />
			<dx:GridViewDataColumn FieldName="LastName" Visible="false" VisibleIndex="127" />
			<dx:GridViewDataColumn FieldName="Left1LastName" Visible="false" VisibleIndex="128" />
			<dx:GridViewDataColumn FieldName="Length" Visible="false" VisibleIndex="129" />
			<dx:GridViewDataColumn FieldName="LinkGUID" Visible="false" VisibleIndex="130" />
			<dx:GridViewDataColumn FieldName="MedicalReleaseForm" Visible="false" VisibleIndex="131" />
			<dx:GridViewDataColumn FieldName="MobilePhone" Visible="false" VisibleIndex="132" />
			<dx:GridViewDataColumn FieldName="ParentFirst" Visible="false" VisibleIndex="133" />
			<dx:GridViewDataColumn FieldName="ParentLast" Visible="false" VisibleIndex="134" />
			<dx:GridViewDataColumn FieldName="PaymentStatus" Visible="false" VisibleIndex="135" />
			<dx:GridViewDataColumn FieldName="PaymentType" Visible="false" VisibleIndex="136" />
			<dx:GridViewDataColumn FieldName="ProfileMatch" Visible="false" VisibleIndex="137" />
			<dx:GridViewDataColumn FieldName="RawGUID" Visible="false" VisibleIndex="138" />
			<dx:GridViewDataColumn FieldName="ResponseID" Visible="false" VisibleIndex="139" />
			<dx:GridViewDataColumn FieldName="Rotation" Visible="false" VisibleIndex="140" />
			<dx:GridViewDataColumn FieldName="School" Visible="false" VisibleIndex="141" />
			<dx:GridViewDataColumn FieldName="ShirtSize" Visible="false" VisibleIndex="142" />
			<dx:GridViewDataColumn FieldName="SpecialNeeds" Visible="false" VisibleIndex="143" />
			<dx:GridViewDataColumn FieldName="State" Visible="false" VisibleIndex="144" />
			<dx:GridViewDataColumn FieldName="Street" Visible="false" VisibleIndex="145" />
			<dx:GridViewDataColumn FieldName="Type" Visible="false" VisibleIndex="146" />
			<dx:GridViewDataColumn FieldName="UpdatedByUserID" Visible="false" VisibleIndex="147" />
			<dx:GridViewDataColumn FieldName="WillingToVolunteer" Visible="false" VisibleIndex="148" />
			<dx:GridViewDataColumn FieldName="Zip" Visible="false" VisibleIndex="149" />


			<dx:GridViewDataColumn FieldName="FullNameLastFirstUpper" Visible="false" VisibleIndex="160" /> 
			<dx:GridViewDataColumn FieldName="FullNameFirstLast" Visible="false" VisibleIndex="161" /> 
			<dx:GridViewDataColumn FieldName="StudentNameFormatted" Visible="false" VisibleIndex="162" /> 
			<dx:GridViewDataColumn FieldName="StudentNameWithFriends" Visible="false" VisibleIndex="163" /> 
			<dx:GridViewDataColumn FieldName="ParentNameFormatted" Visible="false" VisibleIndex="164" /> 
			<dx:GridViewDataColumn FieldName="ClassCodeWithDesc" Visible="false" VisibleIndex="165" /> 
			<dx:GridViewDataColumn FieldName="Friend1FullNameLastFirst" Visible="false" VisibleIndex="166" /> 
			<dx:GridViewDataColumn FieldName="Friend1FullNameLastFirstUpper" Visible="false" VisibleIndex="167" /> 
			<dx:GridViewDataColumn FieldName="Friend2FullNameLastFirst" Visible="false" VisibleIndex="169" /> 
			<dx:GridViewDataColumn FieldName="Friend2FullNameLastFirstUpper" Visible="false" VisibleIndex="170" /> 
			<dx:GridViewDataColumn FieldName="AllergyAndSpecialNeeds" Visible="false" VisibleIndex="172" /> 
        </Columns>
		<Settings ShowFilterRow="true" ShowFilterRowMenu="true" />
		<SettingsCustomizationWindow Enabled="True" />
		<SettingsPopup>
			<CustomizationWindow Width="250" Height="400" />
		</SettingsPopup>
        <ClientSideEvents ContextMenu="OnContextMenu" />
	</dx:ASPxGridView>
	<dx:ASPxGridViewExporter ID="gridExport" runat="server" GridViewID="grid"></dx:ASPxGridViewExporter>
	<dx:ASPxPopupMenu ID="headerMenu" runat="server" ClientInstanceName="headerMenu">
		<Items>
			<dx:MenuItem Text="Hide column" Name="HideColumn">
			</dx:MenuItem>
			<dx:MenuItem Text="Show/Hide hidden field list" Name="ShowHideList">
			</dx:MenuItem>
		</Items>
		<ClientSideEvents ItemClick="OnItemClick"/>
	</dx:ASPxPopupMenu>
</asp:Content>