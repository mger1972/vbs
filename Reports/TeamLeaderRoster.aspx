﻿<%@ Page Title="Team Leader Roster" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TeamLeaderRoster.aspx.cs" Inherits="VBS.Reports.TeacherRoster" %>

<%@ Register Assembly="DevExpress.XtraReports.v17.1.Web, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
				<h3>A list of all team leaders and their current assignments.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script language="javascript">
		$(document).ready(function () {
			$("#MainContent_documentViewer_Splitter_0").css("background-color", "#FFFFFF");
			$("#MainContent_documentViewer_Splitter_0").css("border", "1px solid black");
			$("#MainContent_documentViewer_Splitter_1i1i0").css("border", "0px solid black");
		})

		function documentViewer_BeginCallback(s, e) {
			s.AdjustControl();
			var width = s.GetSplitter().GetWidth() - 70;
			document.getElementById("hfReportViewerWidth").value = width;

		}

		function documentViewer_ToolbarItemClick(s, e) {
			if (e.item.name === "Refresh") {
				s.Refresh();
			}
		}
	 </script>
    <dx:ASPxCheckBox ID="chkShowStudents" ClientInstanceName="chkShowStudents" AutoPostBack="true" Checked="true" runat="server" 
        OnCheckedChanged="chkShowStudents_CheckedChanged"></dx:ASPxCheckBox>
    <dx:ASPxLabel ID="lblShowStudents" runat="server" Text="Show students on report?"></dx:ASPxLabel>
	<input id="hfReportViewerWidth" name="ReportViewerWidth" type="hidden" />
	<dx:ASPxDocumentViewer Height="600px" ID="documentViewer" runat="server" EnableViewState="false" 
		DocumentViewerInternal=""
		ToolbarMode="Ribbon" StylesSplitter-SidePaneWidth="60" StylesDocumentMap-NodeText-Font-Size="X-Small" OnInit="documentViewer_Init">
		<ClientSideEvents BeginCallback="documentViewer_BeginCallback" ToolbarItemClick="documentViewer_ToolbarItemClick" />
            <SettingsReportViewer EnableReportMargins="true" />
            <ToolbarItems>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarButton Name="Refresh" 
                    ToolTip="Refresh" />
            </ToolbarItems>       
	</dx:ASPxDocumentViewer>
</asp:Content>