﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class TShirtDetail : System.Web.UI.Page
	{
		protected TShirtDetailReportType _type = TShirtDetailReportType.OrderByName;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			//Search_VBS_TShirtListCollection detail = new Search_VBS_TShirtListCollection(string.Empty);
			//detail.Sort(Search_VBS_TShirtList.FN_FullNameLastFirstUpper);

			//// Add some unique text in there for the class
			//foreach (Search_VBS_TShirtList item in detail)
			//{
			//	if (!String.IsNullOrEmpty(item.GroupGUID))
			//	{
			//		item.GridCustom_5 = String.Format("Team: {0}", item.GroupName);
			//	}
			//	else if (!String.IsNullOrEmpty(item.ClassCode))
			//	{
			//		item.GridCustom_5 = String.Format("Class: {0} ({1})", item.ClassCode, item.Grade);
			//	}
			//}

			//rptTShirtDetail rpt = new rptTShirtDetail();
			//rpt.DataSource = detail;
			//rpt.DisplayName = "T-Shirt Detail";
			//rpt.CreateDocument();
			//documentViewer.Report = rpt;
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Figure out which type of report this is
			_type = TShirtDetailReportType.OrderByName;		// Set the default
			foreach (string key in Request.QueryString.AllKeys)
			{
				if (key.ToLower().Equals("type"))
				{
					// Get the value
					string val = Request.QueryString[key];
					if (val.ToLower().Contains("class")) { _type = TShirtDetailReportType.OrderByClass; }
					break;
				}
			}

			Search_VBS_TShirtListCollection detail = new Search_VBS_TShirtListCollection(string.Empty);
			//detail.Sort(Search_VBS_TShirtList.FN_FullNameLastFirstUpper);

			// Add the kids T-shirts as well
			Search_VBS_VolunteerChildCollection kidShirts = new
				Search_VBS_VolunteerChildCollection("sChildShirtSize IS NOT NULL AND LTRIM(RTRIM(sChildShirtSize)) <> ''");
			foreach (Search_VBS_VolunteerChild ch in kidShirts)
			{
				Search_VBS_TShirtList item = new VBS.Search_VBS_TShirtList();
				item.RecGUID = System.Guid.NewGuid().ToString();
				item.RawGUID = ch.DetailGUID;
				item.ResponseID = ch.ResponseID;
				item.FirstName = ch.ChildFirstName;
				item.LastName = ch.ChildLastName;
				item.Size = ch.ChildShirtSize;
				item.ClassCode = "Pre-School";
				item.GroupName = "Pre-School";
				item.Length = 99;
				detail.Add(item);
			}

			// Add some unique text in there for the class
			foreach (Search_VBS_TShirtList item in detail)
			{
				if (_type == TShirtDetailReportType.OrderByName &&
					!String.IsNullOrEmpty(item.LastNameStartsWith))
				{
					item.GridCustom_7 = ((int)item.LastNameStartsWith.ToCharArray()[0]).ToString();
				}
				if (!String.IsNullOrEmpty(item.GroupGUID))
				{
					item.GridCustom_5 = String.Format("Team: {0}", item.GroupName);
					if (_type == TShirtDetailReportType.OrderByClass)
					{
						item.GridCustom_7 = "D";
					}
				}
				else if (!String.IsNullOrEmpty(item.ClassCode))
				{
					item.GridCustom_5 = String.Format("Class: {0} ({1})", item.ClassCode, item.Grade);
					if (_type == TShirtDetailReportType.OrderByClass)
					{
						item.GridCustom_7 = "C";
					}
				}
				else if (item.Length == 99)
				{
					// This one is a kid's shirt
					item.GridCustom_5 = String.Format("Location: {0}", item.ClassCode);
					if (_type == TShirtDetailReportType.OrderByClass)
					{
						item.GridCustom_7 = "A";
					}
				}
				else  
				{
					item.GridCustom_5 = "< Unassigned >";
					if (_type == TShirtDetailReportType.OrderByClass)
					{
						item.GridCustom_7 = "B";
					}
				}
			}
			if (_type == TShirtDetailReportType.OrderByName)
			{
				detail.Sort(Search_VBS_TShirtList.FN_FullNameLastFirstUpper);
			}
			else
			{
				detail.Sort(Search_VBS_TShirtList.FN_GridCustom_7 + ", " + 
					Search_VBS_TShirtList.FN_Length + ", " +
					Search_VBS_TShirtList.FN_GridCustom_5 + ", " + 
					Search_VBS_TShirtList.FN_FullNameLastFirstUpper);
			}

			string displayName = string.Empty, reportTitle = string.Empty;
			switch (_type)
			{
				case TShirtDetailReportType.OrderByName:
					displayName = "T-Shirts (Name)";
					reportTitle = "Ordered By Name";
					break;
				case TShirtDetailReportType.OrderByClass:
					displayName = "T-Shirts (Class)";
					reportTitle = "Ordered By Class";

					break;
			}
			rptTShirtDetail rpt = new rptTShirtDetail(reportTitle);
			rpt.DataSource = detail;
			rpt.DisplayName = "T-Shirt Detail";

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.96f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}

	public enum TShirtDetailReportType : int
	{
		OrderByName = 0,
		OrderByClass,
	}
}