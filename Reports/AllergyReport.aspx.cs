﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class AllergyReport : System.Web.UI.Page
	{
		protected AllergyReportType _type = AllergyReportType.Alphabetical;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Figure out which type of report this is
			_type = AllergyReportType.Alphabetical;      // Set the default
			foreach (string key in Request.QueryString.AllKeys)
			{
				if (key.ToLower().Equals("type"))
				{
					// Get the value
					string val = Request.QueryString[key];
					if (val.ToLower().Contains("date")) { _type = AllergyReportType.ByDate; }
					break;
				}
			}

			Search_VBS_AllergiesCollection allergies = new Search_VBS_AllergiesCollection(string.Empty);
			allergies.Sort(Search_VBS_Allergies.FN_FullNameLastFirstUpper);
			if (_type == AllergyReportType.ByDate) { allergies.Sort(Search_VBS_Allergies.FN_DateSubmitted); }

			Dictionary<string, int> formIDs = Utils.FormIDs;
			
			// Set up the names
			foreach (Search_VBS_Allergies allergy in allergies)
			{
				allergy.GridCustom_7 = allergy.LastName + ", " + allergy.FirstName;
				allergy.GridCustom_9 = (!String.IsNullOrEmpty(allergy.ParentLast) ? allergy.ParentLast + ", " + allergy.ParentFirst : string.Empty);
				allergy.GridCustom_8 = "Rotation: " + (!String.IsNullOrEmpty(allergy.Rotation) ? allergy.Rotation : "< Not Assigned >");

				// Build out the link for the page to go back to myvbc
				allergy.GridCustom_5 = String.Format("https://voyagers.ccbchurch.com/form_response.php?response_id={0}&id={1}&redirect=%2Fform_response_list.php%3Fid%3D{1}", 
					allergy.ResponseID, 
					formIDs[allergy.SheetType]);
				allergy.GridCustom_6 = "_blank";
			}

			rptAllergyReport rpt = new rptAllergyReport();

			rpt.DataSource = allergies;
			rpt.DisplayName = "Allergies";
			
			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.98f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}

	public enum AllergyReportType : int
	{
		Alphabetical = 0,
		ByDate,
	}
}