﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class ChurchListing : System.Web.UI.Page
	{
		protected ChurchSchoolReportType _type = ChurchSchoolReportType.Church;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Figure out which type of report this is
			_type = ChurchSchoolReportType.Church;      // Set the default
			foreach (string key in Request.QueryString.AllKeys)
			{
				if (key.ToLower().Equals("type"))
				{
					// Get the value
					string val = Request.QueryString[key];
					if (val.ToLower().StartsWith("s")) { _type = ChurchSchoolReportType.School; }
					break;
				}
			}

			if (_type == ChurchSchoolReportType.Church)
			{
				// Get the collection for the master
				Search_VBS_ChurchListingCollection churchCollection = new Search_VBS_ChurchListingCollection(string.Empty);
				churchCollection.Sort(Search_VBS_ChurchListing.FN_Count + " DESC, " + Search_VBS_ChurchListing.FN_ChurchAttended);

				// Take off values for the subreport
				while (churchCollection.Count > 20)
				{
					churchCollection.RemoveAt(churchCollection.Count - 1);
				}

				// Put in the percentages
				int totalCount = 0;
				foreach (Search_VBS_ChurchListing item in churchCollection)
				{
					totalCount += (item.Count.HasValue ? item.Count.Value : 0);
				}
				foreach (Search_VBS_ChurchListing item in churchCollection)
				{
					if (item.Count.HasValue)
					{
						item.GridCustom_4 = ((item.Count.Value / (decimal)totalCount) * 100).ToString("##0.0") + "%";
					}
				}

				Search_VBS_ChurchListingCollection churchCollectionChart = new Search_VBS_ChurchListingCollection(string.Empty);
				churchCollectionChart.Sort(Search_VBS_ChurchListing.FN_Count + " DESC, " + Search_VBS_ChurchListing.FN_ChurchAttended);

				while (churchCollectionChart.Count > 20)
				{
					churchCollectionChart.RemoveAt(churchCollectionChart.Count - 1);
				}

				rptChurchListing rpt = new rptChurchListing();
				rpt.DataSource = churchCollectionChart;
				rpt.ChurchCollectionDetail = churchCollection;
				rpt.DisplayName = "Top 20 Churches";

				int reportViewerWidth;
				if (Request["ReportViewerWidth"] != null &&
					Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
				{
					float reportWidth = rpt.PageWidth * 0.98f;
					float scaleFactor = reportViewerWidth / reportWidth;

					Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
						Convert.ToInt32(rpt.Margins.Right * scaleFactor),
						Convert.ToInt32(rpt.Margins.Top * scaleFactor),
						Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
					rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
					rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
					rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
					rpt.Margins = newMargins;
					rpt.CreateDocument();

					rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
				}

				documentViewer.Report = rpt;
			}
			else
			{
				// Get the collection for the master
				Search_VBS_SchoolListingCollection schoolCollection = new Search_VBS_SchoolListingCollection(string.Empty);
				schoolCollection.Sort(Search_VBS_SchoolListing.FN_Count + " DESC, " + Search_VBS_SchoolListing.FN_School);

				// Take off values for the subreport
				while (schoolCollection.Count > 20)
				{
					schoolCollection.RemoveAt(schoolCollection.Count - 1);
				}

				// Put in the percentages
				int totalCount = 0;
				foreach (Search_VBS_SchoolListing item in schoolCollection)
				{
					totalCount += (item.Count.HasValue ? item.Count.Value : 0);
				}
				foreach (Search_VBS_SchoolListing item in schoolCollection)
				{
					if (item.Count.HasValue)
					{
						item.GridCustom_4 = ((item.Count.Value / (decimal)totalCount) * 100).ToString("##0.0") + "%";
					}
				}

				Search_VBS_SchoolListingCollection schoolCollectionChart = new Search_VBS_SchoolListingCollection(string.Empty);
				schoolCollectionChart.Sort(Search_VBS_SchoolListing.FN_Count + " DESC, " + Search_VBS_SchoolListing.FN_School);

				while (schoolCollectionChart.Count > 20)
				{
					schoolCollectionChart.RemoveAt(schoolCollectionChart.Count - 1);
				}

				rptSchoolListing rpt = new rptSchoolListing();
				rpt.DataSource = schoolCollectionChart;
				rpt.SchoolCollectionDetail = schoolCollection;
				rpt.DisplayName = "Top 20 Schools";

				int reportViewerWidth;
				if (Request["ReportViewerWidth"] != null &&
					Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
				{
					float reportWidth = rpt.PageWidth * 0.98f;
					float scaleFactor = reportViewerWidth / reportWidth;

					Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
						Convert.ToInt32(rpt.Margins.Right * scaleFactor),
						Convert.ToInt32(rpt.Margins.Top * scaleFactor),
						Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
					rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
					rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
					rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
					rpt.Margins = newMargins;
					rpt.CreateDocument();

					rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
				}

				documentViewer.Report = rpt;
			}
		}
	}

	public enum ChurchSchoolReportType : int
	{
		Church = 0,
		School,
	}
}