﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class TShirt_SignIn : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Make a DataTable that we can use to populate the report
			DataTable dt = new DataTable();
			dt.Columns.Add("sLineNum", typeof(System.String));
			dt.Columns.Add("sGroup", typeof(System.String));
			dt.Columns.Add("sName", typeof(System.String));
			dt.Columns.Add("sSize", typeof(System.String));

			for (int i = 0; i < 20; i++)
			{
				dt.Rows.Add(new object[]
				{
					String.Format("{0}.", (i + 1)),
					DBNull.Value,
					DBNull.Value,
					DBNull.Value,
				});
			}

			rptTShirt_SignIn rpt = new rptTShirt_SignIn();

			rpt.DataSource = dt;
			rpt.DisplayName = "T-Shirt Sign In Sheet";

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.96f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}
}