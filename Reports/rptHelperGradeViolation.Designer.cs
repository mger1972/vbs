﻿namespace VBS.Reports
{
	partial class rptHelperGradeViolation
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptHelperGradeViolation));
			this.picHeader = new DevExpress.XtraReports.UI.XRPictureBox();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.gfGroup = new DevExpress.XtraReports.UI.GroupFooterBand();
			this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblHelperGrade = new DevExpress.XtraReports.UI.XRLabel();
			this.lblAssignedTo = new DevExpress.XtraReports.UI.XRLabel();
			this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.lblHighestGrade = new DevExpress.XtraReports.UI.XRLabel();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.ghGroup = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// picHeader
			// 
			this.picHeader.Dpi = 100F;
			this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
			this.picHeader.LocationFloat = new DevExpress.Utils.PointFloat(369.7917F, 0F);
			this.picHeader.Name = "picHeader";
			this.picHeader.SizeF = new System.Drawing.SizeF(380.2083F, 56.25F);
			this.picHeader.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Dpi = 100F;
			this.ReportHeader.HeightF = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// xrLabel5
			// 
			this.xrLabel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel5.BorderColor = System.Drawing.Color.White;
			this.xrLabel5.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel5.Dpi = 100F;
			this.xrLabel5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.ForeColor = System.Drawing.Color.White;
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(498.5417F, 10.00001F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(117.0834F, 20.20833F);
			this.xrLabel5.StylePriority.UseBackColor = false;
			this.xrLabel5.StylePriority.UseBorderColor = false;
			this.xrLabel5.StylePriority.UseBorders = false;
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.StylePriority.UseForeColor = false;
			this.xrLabel5.StylePriority.UseTextAlignment = false;
			this.xrLabel5.Text = "Highest Grade";
			this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel3
			// 
			this.xrLabel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel3.BorderColor = System.Drawing.Color.White;
			this.xrLabel3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel3.Dpi = 100F;
			this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel3.ForeColor = System.Drawing.Color.White;
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(20.41661F, 10F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(206.6667F, 20.20833F);
			this.xrLabel3.StylePriority.UseBackColor = false;
			this.xrLabel3.StylePriority.UseBorderColor = false;
			this.xrLabel3.StylePriority.UseBorders = false;
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.StylePriority.UseForeColor = false;
			this.xrLabel3.Text = "Name";
			// 
			// gfGroup
			// 
			this.gfGroup.Dpi = 100F;
			this.gfGroup.HeightF = 25.08333F;
			this.gfGroup.Name = "gfGroup";
			this.gfGroup.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
			// 
			// xrPageInfo2
			// 
			this.xrPageInfo2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo2.Dpi = 100F;
			this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo2.Format = "Printed on {0:MM/dd/yyyy} at {0:hh:mm:ss tt}";
			this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
			this.xrPageInfo2.Name = "xrPageInfo2";
			this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
			this.xrPageInfo2.SizeF = new System.Drawing.SizeF(289.1666F, 23F);
			this.xrPageInfo2.StylePriority.UseFont = false;
			this.xrPageInfo2.StylePriority.UseTextAlignment = false;
			this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrLabel4
			// 
			this.xrLabel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel4.BorderColor = System.Drawing.Color.White;
			this.xrLabel4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel4.Dpi = 100F;
			this.xrLabel4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel4.ForeColor = System.Drawing.Color.White;
			this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(243.7501F, 10.00001F);
			this.xrLabel4.Name = "xrLabel4";
			this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel4.SizeF = new System.Drawing.SizeF(112.4999F, 20.20833F);
			this.xrLabel4.StylePriority.UseBackColor = false;
			this.xrLabel4.StylePriority.UseBorderColor = false;
			this.xrLabel4.StylePriority.UseBorders = false;
			this.xrLabel4.StylePriority.UseFont = false;
			this.xrLabel4.StylePriority.UseForeColor = false;
			this.xrLabel4.StylePriority.UseTextAlignment = false;
			this.xrLabel4.Text = "Helper Grade";
			this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblName
			// 
			this.lblName.Dpi = 100F;
			this.lblName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblName.LocationFloat = new DevExpress.Utils.PointFloat(0.0001068115F, 0F);
			this.lblName.Name = "lblName";
			this.lblName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblName.SizeF = new System.Drawing.SizeF(206.6666F, 15F);
			this.lblName.StylePriority.UseFont = false;
			this.lblName.Text = "714-832-8664";
			// 
			// lblHelperGrade
			// 
			this.lblHelperGrade.Dpi = 100F;
			this.lblHelperGrade.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHelperGrade.LocationFloat = new DevExpress.Utils.PointFloat(223.3334F, 0F);
			this.lblHelperGrade.Name = "lblHelperGrade";
			this.lblHelperGrade.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHelperGrade.SizeF = new System.Drawing.SizeF(112.4999F, 15F);
			this.lblHelperGrade.StylePriority.UseFont = false;
			this.lblHelperGrade.StylePriority.UseTextAlignment = false;
			this.lblHelperGrade.Text = "714-832-8664";
			this.lblHelperGrade.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblAssignedTo
			// 
			this.lblAssignedTo.Dpi = 100F;
			this.lblAssignedTo.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblAssignedTo.LocationFloat = new DevExpress.Utils.PointFloat(349.375F, 0F);
			this.lblAssignedTo.Name = "lblAssignedTo";
			this.lblAssignedTo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblAssignedTo.SizeF = new System.Drawing.SizeF(112.4999F, 15F);
			this.lblAssignedTo.StylePriority.UseFont = false;
			this.lblAssignedTo.StylePriority.UseTextAlignment = false;
			this.lblAssignedTo.Text = "714-832-8664";
			this.lblAssignedTo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// rptTitle
			// 
			this.rptTitle.Dpi = 100F;
			this.rptTitle.Font = new System.Drawing.Font("Verdana", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rptTitle.ForeColor = System.Drawing.Color.Blue;
			this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.rptTitle.Name = "rptTitle";
			this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.rptTitle.SizeF = new System.Drawing.SizeF(356.25F, 31.33333F);
			this.rptTitle.StylePriority.UseFont = false;
			this.rptTitle.StylePriority.UseForeColor = false;
			this.rptTitle.Text = "Jr. Helper Grade Violation";
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
			this.Detail.Dpi = 100F;
			this.Detail.HeightF = 16.45836F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
			// 
			// xrPanel1
			// 
			this.xrPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(206)))), ((int)(((byte)(235)))));
			this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblHighestGrade,
            this.lblName,
            this.lblHelperGrade,
            this.lblAssignedTo});
			this.xrPanel1.Dpi = 100F;
			this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.xrPanel1.Name = "xrPanel1";
			this.xrPanel1.SizeF = new System.Drawing.SizeF(595.2084F, 16.45836F);
			this.xrPanel1.StylePriority.UseBackColor = false;
			// 
			// lblHighestGrade
			// 
			this.lblHighestGrade.Dpi = 100F;
			this.lblHighestGrade.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHighestGrade.LocationFloat = new DevExpress.Utils.PointFloat(478.1251F, 0F);
			this.lblHighestGrade.Name = "lblHighestGrade";
			this.lblHighestGrade.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHighestGrade.SizeF = new System.Drawing.SizeF(117.0834F, 15F);
			this.lblHighestGrade.StylePriority.UseFont = false;
			this.lblHighestGrade.StylePriority.UseTextAlignment = false;
			this.lblHighestGrade.Text = "714-832-8664";
			this.lblHighestGrade.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.rptTitle,
            this.picHeader});
			this.PageHeader.Dpi = 100F;
			this.PageHeader.HeightF = 63.54168F;
			this.PageHeader.Name = "PageHeader";
			// 
			// xrPageInfo1
			// 
			this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo1.Dpi = 100F;
			this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo1.Format = "Page {0} of {1}";
			this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(547.2921F, 10.00001F);
			this.xrPageInfo1.Name = "xrPageInfo1";
			this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo1.SizeF = new System.Drawing.SizeF(178.7499F, 23F);
			this.xrPageInfo1.StylePriority.UseFont = false;
			this.xrPageInfo1.StylePriority.UseTextAlignment = false;
			this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
			this.PageFooter.Dpi = 100F;
			this.PageFooter.HeightF = 33.00001F;
			this.PageFooter.Name = "PageFooter";
			// 
			// TopMargin
			// 
			this.TopMargin.Dpi = 100F;
			this.TopMargin.HeightF = 50F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// ghGroup
			// 
			this.ghGroup.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrLabel4,
            this.xrLabel5,
            this.xrLabel3});
			this.ghGroup.Dpi = 100F;
			this.ghGroup.HeightF = 30.20834F;
			this.ghGroup.Name = "ghGroup";
			this.ghGroup.RepeatEveryPage = true;
			// 
			// xrLabel1
			// 
			this.xrLabel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel1.BorderColor = System.Drawing.Color.White;
			this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel1.Dpi = 100F;
			this.xrLabel1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.ForeColor = System.Drawing.Color.White;
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(369.7917F, 10.00001F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(112.4999F, 20.20833F);
			this.xrLabel1.StylePriority.UseBackColor = false;
			this.xrLabel1.StylePriority.UseBorderColor = false;
			this.xrLabel1.StylePriority.UseBorders = false;
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseForeColor = false;
			this.xrLabel1.StylePriority.UseTextAlignment = false;
			this.xrLabel1.Text = "Assigned To";
			this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// BottomMargin
			// 
			this.BottomMargin.Dpi = 100F;
			this.BottomMargin.HeightF = 50F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// rptHelperGradeViolation
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.PageFooter,
            this.ghGroup,
            this.gfGroup});
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
			this.Version = "16.1";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.XRPictureBox picHeader;
		private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.GroupFooterBand gfGroup;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
		private DevExpress.XtraReports.UI.XRLabel xrLabel4;
		private DevExpress.XtraReports.UI.XRLabel lblName;
		private DevExpress.XtraReports.UI.XRLabel lblHelperGrade;
		private DevExpress.XtraReports.UI.XRLabel lblAssignedTo;
		private DevExpress.XtraReports.UI.XRLabel rptTitle;
		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.XRPanel xrPanel1;
		private DevExpress.XtraReports.UI.XRLabel lblHighestGrade;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.GroupHeaderBand ghGroup;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
	}
}
