﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraCharts;

namespace VBS.Reports
{
	public partial class rptSchoolListing : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;
		private Search_VBS_SchoolListingCollection _schoolCollectionDetail = new Search_VBS_SchoolListingCollection();

		public rptSchoolListing()
		{
			InitializeComponent();

			// Set the bindings
			//lblLastName.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_ClassCodeWithDesc);
			//lblLastName.DataBindings.Add("Bookmark", null, Search_VBS_Volunteer.FN_GridCustom_1);

			//lblRotation.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_Rotation);

			//// Detail Fields
			//lblStudentName.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_VolunteerNameFormatted);
			//lblEMail.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_Email);
			//chkJuniorHelper.DataBindings.Add("Checked", null, Search_VBS_Volunteer.FN_JuniorHelper);
			//chkTeacher.DataBindings.Add("Checked", null, Search_VBS_Volunteer.FN_Teacher);
			//lblContactPhone.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_ContactPhone);
			//lblTShirt.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_ShirtSize);

			// Bind the groupheader
			//ghClass.GroupFields.Add(new GroupField(Search_VBS_Volunteer.FN_Length, XRColumnSortOrder.Ascending));
			//ghClass.GroupFields.Add(new GroupField(Search_VBS_Volunteer.FN_ClassCode, XRColumnSortOrder.Ascending));
		}

		/// <summary>
		/// The collection of school detail records
		/// </summary>
		public Search_VBS_SchoolListingCollection SchoolCollectionDetail
		{
			get { return _schoolCollectionDetail; }
			set { _schoolCollectionDetail = value; }
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			//xrPanel1.BackColor = _detailBackColor;
			//_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);
		}

		private void PageHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			// Set up the chart object
			Series s = chart.Series[0];
			s.DataSource = this.DataSource;
			s.ArgumentScaleType = ScaleType.Auto;
			s.ArgumentDataMember = "School";
			s.ValueScaleType = ScaleType.Numerical;
			s.ValueDataMembers.AddRange(new string[] { "Count" });

			// Set up the subreport
			subrpt.ReportSource = new rptSchoolDetail();
			subrpt.ReportSource.DataSource = _schoolCollectionDetail;
		}

		private void chart_CustomDrawSeriesPoint(object sender, CustomDrawSeriesPointEventArgs e)
		{
			e.LegendText = e.SeriesPoint.Argument;
		}
	}
}
