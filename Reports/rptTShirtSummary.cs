﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptTShirtSummary : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		private string _noShirtSizeMessage = string.Empty;
		public string NoShirtSizeMessage
		{
			get { return _noShirtSizeMessage; }
			set { _noShirtSizeMessage = value; }
		}

		public rptTShirtSummary()
		{
			InitializeComponent();

			// Set the bindings
			lblTeam.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_GridCustom_1);
			//lblTeam.DataBindings.Add("Bookmark", null, Search_VBS_Volunteer.FN_GridCustom_1);

			// Detail Fields
			lblYouthXS.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_YouthXS);
			lblYouthS.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_YouthS);
			lblYouthM.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_YouthM);
			lblYouthL.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_YouthL);

			lblAdultS.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultS);
			lblAdultM.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultM);
			lblAdultL.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultL);
			lblAdultXL.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultXL);
			lblAdultXXL.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultXXL);

			lblTotal.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_Total);

			// Summary fields
			lblYouthXSSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_YouthXS);
			lblYouthSSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_YouthS);
			lblYouthMSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_YouthM);
			lblYouthLSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_YouthL);

			lblAdultSSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultS);
			lblAdultMSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultM);
			lblAdultLSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultL);
			lblAdultXLSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultXL);
			lblAdultXXLSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_AdultXXL);

			lblTotalSum.DataBindings.Add("Text", null, Search_VBS_TShirtSummary.FN_Total);

			lblNoShirtSize.Visible = !String.IsNullOrEmpty(_noShirtSizeMessage);

			// Bind the groupheader
			//ghClass.GroupFields.Add(new GroupField(Search_VBS_Volunteer.FN_Length, XRColumnSortOrder.Ascending));
			//ghClass.GroupFields.Add(new GroupField(Search_VBS_Volunteer.FN_ClassCode, XRColumnSortOrder.Ascending));
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);
		}

		private void ReportFooter_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{

		}

		private void ReportFooter_AfterPrint(object sender, EventArgs e)
		{
			lblNoShirtSize.Text = _noShirtSizeMessage;
		}
	}
}
