﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.Parameters;

namespace VBS.Reports
{
	public partial class TestClass : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			DataTable dt = new DataTable();
			dt.Columns.Add("Field1", typeof(System.String));
			dt.Columns.Add("FieldInt", typeof(System.Int32));

			for (int i = 0; i < 10; i++)
			{
				dt.Rows.Add(new object[] {
					"Value " + (i + 1).ToString(),
					(i + 1),
				});
			}

			rptClassRosterByClass rpt = new rptClassRosterByClass();

			DevExpress.XtraReports.Parameters.Parameter param = new DevExpress.XtraReports.Parameters.Parameter();
			param.Name = "FieldVal";

			// Set up the type
			param.Type = typeof(System.Int32);
			param.Value = 1;
			param.Description = "Field Value:";
			param.Visible = true;

			rpt.Parameters.Add(param);

			// Specify the report's filter string.
			rpt.FilterString = "[FieldInt] = [Parameters.FieldVal]";

			// Force the report creation without previously 
			// requesting the parameter value from end-users.
			rpt.RequestParameters = false;

			rpt.DataSource = dt;
			rpt.DisplayName = "Class Roster";
			documentViewer.Report = rpt;
		}
	}
}