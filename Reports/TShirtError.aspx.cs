﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
    public partial class TShirtError : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login.aspx", true);
                return;
            }
            else
            {
                Session["UserName"] = HttpContext.Current.User.Identity.Name;
            }

            // Tell the parent who we belong to
            ((SiteMaster)this.Master).SetChildPageInstance(this);

            //Search_VBS_AllergiesCollection allergies = new Search_VBS_AllergiesCollection(string.Empty);
            //allergies.Sort(Search_VBS_Allergies.FN_FullNameLastFirstUpper);

            //// Set up the names
            //foreach (Search_VBS_Allergies allergy in allergies)
            //{
            //	allergy.GridCustom_7 = allergy.LastName + ", " + allergy.FirstName;
            //	allergy.GridCustom_9 = (!String.IsNullOrEmpty(allergy.ParentLast) ? allergy.ParentLast + ", " + allergy.ParentFirst : string.Empty);
            //}

            //rptAllergyReport rpt = new rptAllergyReport();

            //rpt.DataSource = allergies;
            //rpt.DisplayName = "Allergies";
            //rpt.CreateDocument();
            //documentViewer.Report = rpt;
        }

        protected void documentViewer_Init(object sender, EventArgs e)
        {
            ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

            Search_VBS_TShirtErrorCollection tShirtErrors = new Search_VBS_TShirtErrorCollection(string.Empty);
            tShirtErrors.Sort(Search_VBS_TShirtError.FN_Reason + ", " + Search_VBS_TShirtError.FN_LastName + ", " + Search_VBS_TShirtError.FN_FirstName);

			Dictionary<string, int> formIDs = Utils.FormIDs;
			
			// Set up the names
			foreach (Search_VBS_TShirtError err in tShirtErrors)
            {
                //allergy.GridCustom_7 = allergy.LastName + ", " + allergy.FirstName;
                //allergy.GridCustom_9 = (!String.IsNullOrEmpty(allergy.ParentLast) ? allergy.ParentLast + ", " + allergy.ParentFirst : string.Empty);
                //allergy.GridCustom_8 = "Rotation: " + (!String.IsNullOrEmpty(allergy.Rotation) ? allergy.Rotation : "< Not Assigned >");

                // Build out the link for the page to go back to myvbc
                err.GridCustom_5 = String.Format("https://voyagers.ccbchurch.com/form_response.php?response_id={0}&id={1}&redirect=%2Fform_response_list.php%3Fid%3D{1}",
                    err.ResponseID,
                    formIDs[err.SheetType]);
                err.GridCustom_6 = "_blank";

                // Set the type depending on the reason
                err.GridCustom_7 = "More Than One Shirt";
                if (err.Reason.ToLower().StartsWith("nothelping")) { err.GridCustom_7 = "Not Helping All Week"; }
                else if (err.Reason.ToLower().StartsWith("nursery")) { err.GridCustom_7 = "Nursery / Walker Kid's Shirts"; }
                else if (err.Reason.ToLower().StartsWith("noshirt")) { err.GridCustom_7 = "No Shirt Size"; }

				// Set the sheet type
				if (err.SheetType.ToLower().Contains("under18")) { err.GridCustom_8 = "Volunteer - Under 18"; }
                else if (err.SheetType.ToLower().Contains("adult")) { err.GridCustom_8 = "Volunteer - Adult"; }
                else if (err.SheetType.ToLower().Contains("alt")) { err.GridCustom_8 = "Alt. Pay"; }
                else { err.GridCustom_8 = "Regular/Base Pay"; }
            }

            rptTShirtError rpt = new rptTShirtError();

            rpt.DataSource = tShirtErrors;
            rpt.DisplayName = "T-Shirt Errors";

            int reportViewerWidth;
            if (Request["ReportViewerWidth"] != null &&
                Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
            {
                float reportWidth = rpt.PageWidth * 0.98f;
                float scaleFactor = reportViewerWidth / reportWidth;

                Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Right * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Top * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
                rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
                rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
                rpt.Margins = newMargins;
                rpt.CreateDocument();

                rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
            }

            documentViewer.Report = rpt;
        }
    }
}