﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptClassSignIn2 : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptClassSignIn2()
		{
			InitializeComponent();

			// Set the bindings
			lblTeacher1.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_3);
			lblTeacher2.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_4);
			lblHelper1.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_5);
			lblHelper2.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_6);

			lblClass.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ClassCodeWithDesc);
			lblClass.DataBindings.Add("Bookmark", null, Search_VBS_ClassAssignment.FN_ClassCodeWithDesc);

			lblRotation.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_9);
			lblCount.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_2);
			lblKidNum.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_7);

			// Put in all the sign in rows
			//lblMonIn.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);
			//lblMonOut.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);
			//lblTueIn.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);
			//lblTueOut.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);
			//lblWedIn.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);
			//lblWedOut.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);
			//lblThuIn.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);
			//lblThuOut.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);
			//lblFriIn.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);
			//lblFriOut.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_0);

			//lblAllergy.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds);

			//lblGrade.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_9);

			// Detail Fields
			lblStudentName.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_StudentNameFormatted);
			lblGender.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_Gender);
			//lblParentName.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ParentNameFormatted);
			//lblParentName.DataBindings.Add("NavigateUrl", null, Search_VBS_ClassAssignment.FN_EmailHyperlink);
			//lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_MobilePhone);
			//lblTShirt.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ShirtSize);

			// Bind the groupheader
			ghClassCode.GroupFields.Add(new GroupField(Search_VBS_ClassAssignment.FN_Length, XRColumnSortOrder.Ascending));
			ghClassCode.GroupFields.Add(new GroupField(Search_VBS_ClassAssignment.FN_ClassCode, XRColumnSortOrder.Ascending));
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel2.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_EmailHyperlink).ToString() : string.Empty);
			//if (!String.IsNullOrEmpty(emailHyperlink))
			//{
			//	lblParentName.Font = new Font(lblParentName.Font, FontStyle.Underline);
			//	lblParentName.ForeColor = Color.Blue;
			//}
			//else
			//{
			//	lblParentName.Font = new Font(lblParentName.Font, FontStyle.Regular);
			//	lblParentName.ForeColor = Color.Black;
			//}
		}
	}
}
