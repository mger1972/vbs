﻿namespace VBS.Reports
{
	partial class rptClassRosterSinglePage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptClassRosterSinglePage));
			this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTeacher2 = new DevExpress.XtraReports.UI.XRLabel();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblHomePhone = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
			this.gfClassCode = new DevExpress.XtraReports.UI.GroupFooterBand();
			this.lblStudentName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblParentName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblGrade = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblGender = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTShirt = new DevExpress.XtraReports.UI.XRLabel();
			this.lblSchool = new DevExpress.XtraReports.UI.XRLabel();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.lblClass = new DevExpress.XtraReports.UI.XRLabel();
			this.lblWorkPhone = new DevExpress.XtraReports.UI.XRLabel();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.lblHelper2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblHelper1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
			this.picHeader = new DevExpress.XtraReports.UI.XRPictureBox();
			this.lblTeacher1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
			this.ghClassCode = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.lblContactPhone = new DevExpress.XtraReports.UI.XRLabel();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// xrLabel9
			// 
			this.xrLabel9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel9.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel9.ForeColor = System.Drawing.Color.White;
			this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(432.2917F, 95.41667F);
			this.xrLabel9.Name = "xrLabel9";
			this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel9.SizeF = new System.Drawing.SizeF(159.375F, 20.20833F);
			this.xrLabel9.StylePriority.UseBackColor = false;
			this.xrLabel9.StylePriority.UseFont = false;
			this.xrLabel9.StylePriority.UseForeColor = false;
			this.xrLabel9.Text = "Parent Name";
			// 
			// lblTeacher2
			// 
			this.lblTeacher2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTeacher2.LocationFloat = new DevExpress.Utils.PointFloat(101.0417F, 62.5F);
			this.lblTeacher2.Name = "lblTeacher2";
			this.lblTeacher2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTeacher2.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblTeacher2.StylePriority.UseFont = false;
			this.lblTeacher2.Text = "Teacher:";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.rptTitle,
            this.picHeader});
			this.PageHeader.HeightF = 63.54167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// rptTitle
			// 
			this.rptTitle.Font = new System.Drawing.Font("Verdana", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rptTitle.ForeColor = System.Drawing.Color.Blue;
			this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.rptTitle.Name = "rptTitle";
			this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.rptTitle.SizeF = new System.Drawing.SizeF(356.25F, 31.33333F);
			this.rptTitle.StylePriority.UseFont = false;
			this.rptTitle.StylePriority.UseForeColor = false;
			this.rptTitle.Text = "Class Roster - Single Page";
			// 
			// xrLabel3
			// 
			this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 62.5F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.Text = "Teacher:";
			// 
			// lblHomePhone
			// 
			this.lblHomePhone.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHomePhone.LocationFloat = new DevExpress.Utils.PointFloat(571.2501F, 0F);
			this.lblHomePhone.Name = "lblHomePhone";
			this.lblHomePhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHomePhone.SizeF = new System.Drawing.SizeF(111.0417F, 15F);
			this.lblHomePhone.StylePriority.UseFont = false;
			this.lblHomePhone.Text = "714-832-8664";
			// 
			// xrLabel7
			// 
			this.xrLabel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel7.ForeColor = System.Drawing.Color.White;
			this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(227.0833F, 95.41667F);
			this.xrLabel7.Name = "xrLabel7";
			this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel7.SizeF = new System.Drawing.SizeF(139.1666F, 20.20833F);
			this.xrLabel7.StylePriority.UseBackColor = false;
			this.xrLabel7.StylePriority.UseFont = false;
			this.xrLabel7.StylePriority.UseForeColor = false;
			this.xrLabel7.Text = "School";
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
			this.Detail.HeightF = 16.45836F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// xrLabel6
			// 
			this.xrLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel6.ForeColor = System.Drawing.Color.White;
			this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 95.41667F);
			this.xrLabel6.Name = "xrLabel6";
			this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel6.SizeF = new System.Drawing.SizeF(206.6667F, 20.20833F);
			this.xrLabel6.StylePriority.UseBackColor = false;
			this.xrLabel6.StylePriority.UseFont = false;
			this.xrLabel6.StylePriority.UseForeColor = false;
			this.xrLabel6.Text = "Student Name";
			// 
			// gfClassCode
			// 
			this.gfClassCode.HeightF = 0F;
			this.gfClassCode.Name = "gfClassCode";
			this.gfClassCode.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
			// 
			// lblStudentName
			// 
			this.lblStudentName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStudentName.LocationFloat = new DevExpress.Utils.PointFloat(0.0001068115F, 0F);
			this.lblStudentName.Name = "lblStudentName";
			this.lblStudentName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblStudentName.SizeF = new System.Drawing.SizeF(206.6666F, 15F);
			this.lblStudentName.StylePriority.UseFont = false;
			this.lblStudentName.Text = "714-832-8664";
			// 
			// lblParentName
			// 
			this.lblParentName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblParentName.LocationFloat = new DevExpress.Utils.PointFloat(411.8751F, 0F);
			this.lblParentName.Name = "lblParentName";
			this.lblParentName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblParentName.SizeF = new System.Drawing.SizeF(159.375F, 15F);
			this.lblParentName.StylePriority.UseFont = false;
			this.lblParentName.Text = "714-832-8664";
			// 
			// lblGrade
			// 
			this.lblGrade.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblGrade.ForeColor = System.Drawing.Color.Black;
			this.lblGrade.LocationFloat = new DevExpress.Utils.PointFloat(633.7499F, 0F);
			this.lblGrade.Name = "lblGrade";
			this.lblGrade.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblGrade.SizeF = new System.Drawing.SizeF(350.0001F, 25.08333F);
			this.lblGrade.StylePriority.UseFont = false;
			this.lblGrade.StylePriority.UseForeColor = false;
			this.lblGrade.StylePriority.UseTextAlignment = false;
			this.lblGrade.Text = "Class";
			this.lblGrade.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
			// 
			// xrLabel5
			// 
			this.xrLabel5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(542.7082F, 62.5F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.Text = "Helper:";
			// 
			// xrLabel8
			// 
			this.xrLabel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel8.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel8.ForeColor = System.Drawing.Color.White;
			this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(366.25F, 95.41667F);
			this.xrLabel8.Name = "xrLabel8";
			this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel8.SizeF = new System.Drawing.SizeF(66.04172F, 20.20833F);
			this.xrLabel8.StylePriority.UseBackColor = false;
			this.xrLabel8.StylePriority.UseFont = false;
			this.xrLabel8.StylePriority.UseForeColor = false;
			this.xrLabel8.StylePriority.UseTextAlignment = false;
			this.xrLabel8.Text = "Gender";
			this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel4
			// 
			this.xrLabel4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(542.7082F, 40.625F);
			this.xrLabel4.Name = "xrLabel4";
			this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel4.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel4.StylePriority.UseFont = false;
			this.xrLabel4.Text = "Helper:";
			// 
			// lblGender
			// 
			this.lblGender.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblGender.LocationFloat = new DevExpress.Utils.PointFloat(345.8334F, 0F);
			this.lblGender.Name = "lblGender";
			this.lblGender.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblGender.SizeF = new System.Drawing.SizeF(66.04169F, 15F);
			this.lblGender.StylePriority.UseFont = false;
			this.lblGender.StylePriority.UseTextAlignment = false;
			this.lblGender.Text = "714-832-8664";
			this.lblGender.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblTShirt
			// 
			this.lblTShirt.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTShirt.LocationFloat = new DevExpress.Utils.PointFloat(899.3751F, 0F);
			this.lblTShirt.Name = "lblTShirt";
			this.lblTShirt.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTShirt.SizeF = new System.Drawing.SizeF(63.95825F, 15F);
			this.lblTShirt.StylePriority.UseFont = false;
			this.lblTShirt.Text = "714-832-8664";
			// 
			// lblSchool
			// 
			this.lblSchool.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblSchool.LocationFloat = new DevExpress.Utils.PointFloat(206.6667F, 0F);
			this.lblSchool.Name = "lblSchool";
			this.lblSchool.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblSchool.SizeF = new System.Drawing.SizeF(139.1666F, 15F);
			this.lblSchool.StylePriority.UseFont = false;
			this.lblSchool.Text = "714-832-8664";
			// 
			// BottomMargin
			// 
			this.BottomMargin.HeightF = 50F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// xrPageInfo1
			// 
			this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo1.Format = "Page {0} of {1}";
			this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(811.2501F, 9.999974F);
			this.xrPageInfo1.Name = "xrPageInfo1";
			this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo1.SizeF = new System.Drawing.SizeF(178.7499F, 23F);
			this.xrPageInfo1.StylePriority.UseFont = false;
			this.xrPageInfo1.StylePriority.UseTextAlignment = false;
			this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblClass
			// 
			this.lblClass.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblClass.ForeColor = System.Drawing.Color.Black;
			this.lblClass.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
			this.lblClass.Name = "lblClass";
			this.lblClass.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblClass.SizeF = new System.Drawing.SizeF(356.25F, 25.08333F);
			this.lblClass.StylePriority.UseFont = false;
			this.lblClass.StylePriority.UseForeColor = false;
			this.lblClass.Text = "Class";
			// 
			// lblWorkPhone
			// 
			this.lblWorkPhone.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblWorkPhone.LocationFloat = new DevExpress.Utils.PointFloat(682.2918F, 0F);
			this.lblWorkPhone.Name = "lblWorkPhone";
			this.lblWorkPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblWorkPhone.SizeF = new System.Drawing.SizeF(108.5417F, 15F);
			this.lblWorkPhone.StylePriority.UseFont = false;
			this.lblWorkPhone.Text = "714-832-8664";
			// 
			// TopMargin
			// 
			this.TopMargin.HeightF = 50F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// lblHelper2
			// 
			this.lblHelper2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHelper2.LocationFloat = new DevExpress.Utils.PointFloat(633.7499F, 62.5F);
			this.lblHelper2.Name = "lblHelper2";
			this.lblHelper2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHelper2.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblHelper2.StylePriority.UseFont = false;
			this.lblHelper2.Text = "Teacher:";
			// 
			// lblHelper1
			// 
			this.lblHelper1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHelper1.LocationFloat = new DevExpress.Utils.PointFloat(633.7499F, 40.625F);
			this.lblHelper1.Name = "lblHelper1";
			this.lblHelper1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHelper1.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblHelper1.StylePriority.UseFont = false;
			this.lblHelper1.Text = "Teacher:";
			// 
			// xrLabel12
			// 
			this.xrLabel12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel12.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel12.ForeColor = System.Drawing.Color.White;
			this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(702.7084F, 95.41667F);
			this.xrLabel12.Name = "xrLabel12";
			this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel12.SizeF = new System.Drawing.SizeF(108.5417F, 20.20833F);
			this.xrLabel12.StylePriority.UseBackColor = false;
			this.xrLabel12.StylePriority.UseFont = false;
			this.xrLabel12.StylePriority.UseForeColor = false;
			this.xrLabel12.Text = "Work Phone";
			// 
			// xrLabel10
			// 
			this.xrLabel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel10.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel10.ForeColor = System.Drawing.Color.White;
			this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(591.6667F, 95.41667F);
			this.xrLabel10.Name = "xrLabel10";
			this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel10.SizeF = new System.Drawing.SizeF(111.0417F, 20.20833F);
			this.xrLabel10.StylePriority.UseBackColor = false;
			this.xrLabel10.StylePriority.UseFont = false;
			this.xrLabel10.StylePriority.UseForeColor = false;
			this.xrLabel10.Text = "Home Phone";
			// 
			// ReportHeader
			// 
			this.ReportHeader.HeightF = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// xrLabel2
			// 
			this.xrLabel2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 40.625F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.Text = "Teacher:";
			// 
			// xrLabel13
			// 
			this.xrLabel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel13.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel13.ForeColor = System.Drawing.Color.White;
			this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(919.7917F, 95.41667F);
			this.xrLabel13.Name = "xrLabel13";
			this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel13.SizeF = new System.Drawing.SizeF(63.95831F, 20.20833F);
			this.xrLabel13.StylePriority.UseBackColor = false;
			this.xrLabel13.StylePriority.UseFont = false;
			this.xrLabel13.StylePriority.UseForeColor = false;
			this.xrLabel13.Text = "T-Shirt";
			// 
			// picHeader
			// 
			this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
			this.picHeader.LocationFloat = new DevExpress.Utils.PointFloat(619.7917F, 0F);
			this.picHeader.Name = "picHeader";
			this.picHeader.SizeF = new System.Drawing.SizeF(380.2083F, 56.25F);
			this.picHeader.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize;
			// 
			// lblTeacher1
			// 
			this.lblTeacher1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTeacher1.LocationFloat = new DevExpress.Utils.PointFloat(101.0417F, 40.625F);
			this.lblTeacher1.Name = "lblTeacher1";
			this.lblTeacher1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTeacher1.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblTeacher1.StylePriority.UseFont = false;
			this.lblTeacher1.Text = "Teacher:";
			// 
			// xrPanel1
			// 
			this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblStudentName,
            this.lblTShirt,
            this.lblHomePhone,
            this.lblWorkPhone,
            this.lblContactPhone,
            this.lblSchool,
            this.lblGender,
            this.lblParentName});
			this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.xrPanel1.Name = "xrPanel1";
			this.xrPanel1.SizeF = new System.Drawing.SizeF(963.3333F, 16.45836F);
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
			this.PageFooter.HeightF = 32.99997F;
			this.PageFooter.Name = "PageFooter";
			// 
			// xrLabel11
			// 
			this.xrLabel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel11.ForeColor = System.Drawing.Color.White;
			this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(811.2501F, 95.41667F);
			this.xrLabel11.Name = "xrLabel11";
			this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel11.SizeF = new System.Drawing.SizeF(108.5417F, 20.20833F);
			this.xrLabel11.StylePriority.UseBackColor = false;
			this.xrLabel11.StylePriority.UseFont = false;
			this.xrLabel11.StylePriority.UseForeColor = false;
			this.xrLabel11.Text = "Contact Phone";
			// 
			// ghClassCode
			// 
			this.ghClassCode.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel10,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel4,
            this.xrLabel5,
            this.lblHelper1,
            this.lblHelper2,
            this.lblTeacher2,
            this.lblTeacher1,
            this.xrLabel3,
            this.xrLabel2,
            this.lblGrade,
            this.lblClass});
			this.ghClassCode.HeightF = 115.625F;
			this.ghClassCode.KeepTogether = true;
			this.ghClassCode.Name = "ghClassCode";
			// 
			// lblContactPhone
			// 
			this.lblContactPhone.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblContactPhone.LocationFloat = new DevExpress.Utils.PointFloat(790.8336F, 0F);
			this.lblContactPhone.Name = "lblContactPhone";
			this.lblContactPhone.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblContactPhone.SizeF = new System.Drawing.SizeF(108.5417F, 15F);
			this.lblContactPhone.StylePriority.UseFont = false;
			this.lblContactPhone.Text = "714-832-8664";
			// 
			// rptClassRosterSinglePage
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.ghClassCode,
            this.gfClassCode,
            this.PageFooter});
			this.Landscape = true;
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
			this.PageHeight = 850;
			this.PageWidth = 1100;
			this.Version = "14.1";
			this.ParametersRequestBeforeShow += new System.EventHandler<DevExpress.XtraReports.Parameters.ParametersRequestEventArgs>(this.rptClassRosterSinglePage_ParametersRequestBeforeShow);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.XRLabel xrLabel9;
		private DevExpress.XtraReports.UI.XRLabel lblTeacher2;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.XRLabel rptTitle;
		private DevExpress.XtraReports.UI.XRPictureBox picHeader;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.XRLabel lblHomePhone;
		private DevExpress.XtraReports.UI.XRLabel xrLabel7;
		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.XRPanel xrPanel1;
		private DevExpress.XtraReports.UI.XRLabel lblStudentName;
		private DevExpress.XtraReports.UI.XRLabel lblTShirt;
		private DevExpress.XtraReports.UI.XRLabel lblWorkPhone;
		private DevExpress.XtraReports.UI.XRLabel lblContactPhone;
		private DevExpress.XtraReports.UI.XRLabel lblSchool;
		private DevExpress.XtraReports.UI.XRLabel lblGender;
		private DevExpress.XtraReports.UI.XRLabel lblParentName;
		private DevExpress.XtraReports.UI.XRLabel xrLabel6;
		private DevExpress.XtraReports.UI.GroupFooterBand gfClassCode;
		private DevExpress.XtraReports.UI.XRLabel lblGrade;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.UI.XRLabel xrLabel8;
		private DevExpress.XtraReports.UI.XRLabel xrLabel4;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
		private DevExpress.XtraReports.UI.XRLabel lblClass;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.XRLabel lblHelper2;
		private DevExpress.XtraReports.UI.XRLabel lblHelper1;
		private DevExpress.XtraReports.UI.XRLabel xrLabel12;
		private DevExpress.XtraReports.UI.XRLabel xrLabel10;
		private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRLabel xrLabel13;
		private DevExpress.XtraReports.UI.XRLabel lblTeacher1;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.XRLabel xrLabel11;
		private DevExpress.XtraReports.UI.GroupHeaderBand ghClassCode;

	}
}
