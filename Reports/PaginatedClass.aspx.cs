﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.Parameters;

namespace VBS.Reports
{
	public partial class PaginatedClass : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			Search_VBS_ClassAssignmentCollection assignments = new Search_VBS_ClassAssignmentCollection("iLength IS NOT NULL");
			assignments.Sort(Search_VBS_ClassAssignment.FN_Length + ", " +
				Search_VBS_ClassAssignment.FN_ClassCode + ", " +
				Search_VBS_ClassAssignment.FN_Teacher + " DESC, " +
				Search_VBS_ClassAssignment.FN_JuniorHelper + " DESC, " +
				Search_VBS_ClassAssignment.FN_Student + " DESC, " +
				Search_VBS_ClassAssignment.FN_LastName + ", " +
				Search_VBS_ClassAssignment.FN_FirstName);

			string lastValue = string.Empty;
			string teacher1 = string.Empty, teacher2 = string.Empty;
			string helper1 = string.Empty, helper2 = string.Empty;
			int counter = 1;
			foreach (Search_VBS_ClassAssignment a in assignments)
			{
				if (lastValue.ToLower() != a.ClassCode.ToLower()) 
				{ 
					counter = 1;
					teacher1 = string.Empty;
					teacher2 = string.Empty;
					helper1 = string.Empty;
					helper2 = string.Empty;
					lastValue = a.ClassCode;
				}

				if (String.IsNullOrEmpty(teacher1) && a.Teacher) { teacher1 = a.FirstName + " " + a.LastName; }
				else if (String.IsNullOrEmpty(teacher2) && a.Teacher) { teacher2 = a.FirstName + " " + a.LastName; }
				else if (String.IsNullOrEmpty(helper1) && a.JuniorHelper) { helper1 = a.FirstName + " " + a.LastName; }
				else if (String.IsNullOrEmpty(helper2) && a.JuniorHelper) { helper2 = a.FirstName + " " + a.LastName; }
				else if (a.Student)
				{
					a.GridCustom_3 = teacher1;
					a.GridCustom_4 = teacher2;
					a.GridCustom_5 = helper1;
					a.GridCustom_6 = helper2;
				}

				a.GridCustom_0 = a.FirstName + " " + a.LastName;
				a.GridCustom_1 = a.ParentFirst + " " + a.ParentLast;
				a.GridCustom_7 = "Class: " + a.ClassCode;
				a.GridCustom_8 = counter.ToString() + ".";
				a.GridCustom_9 = (a.Grade.Length > 3 ? a.Grade : a.Grade + " Grade");
				counter += 1;
			}

			// Remove the ones that are teacher or helper
			for (int i = assignments.Count - 1; i >= 0; i--)
			{
				if (assignments[i].Teacher ||
					assignments[i].JuniorHelper)
				{
					assignments.RemoveAt(i);
				}
			}

			//DataTable dt = new DataTable();
			//dt.Columns.Add("Field1", typeof(System.String));
			//dt.Columns.Add("FieldInt", typeof(System.Int32));

			//for (int i = 0; i < 10; i++)
			//{
			//	dt.Rows.Add(new object[] {
			//		"Value " + (i + 1).ToString(),
			//		(i + 1),
			//	});
			//}

			rptClassRosterSinglePage rpt = new rptClassRosterSinglePage();

			//DevExpress.XtraReports.Parameters.Parameter param = new DevExpress.XtraReports.Parameters.Parameter();
			//param.Name = "ClassCodeParam";

			//// Set up the type
			//param.Type = typeof(System.Int32);
			//param.Value = 1;
			//param.Description = "Field Value:";
			//param.Visible = true;

			//rpt.Parameters.Add(param);

			//// Specify the report's filter string.
			//rpt.FilterString = "[FieldInt] = [Parameters.FieldVal]";

			//// Force the report creation without previously 
			//// requesting the parameter value from end-users.
			//rpt.RequestParameters = false;

			rpt.DataSource = assignments;
			rpt.DisplayName = "Class Roster";
			rpt.CreateDocument();
			//rpt.PrintingSystem.Document.ScaleFactor = .7F;
			documentViewer.Report = rpt;
		}
	}
}