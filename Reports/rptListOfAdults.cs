﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptListOfAdults : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptListOfAdults()
		{
			InitializeComponent();
		}

		public rptListOfAdults(DateTime? dtFrom, int totalCount)
		{
			InitializeComponent();

			// Detail Fields
			lblStudentName.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_VolunteerNameFormatted);
			lblStudentName.DataBindings.Add("NavigateUrl", null, Search_VBS_Volunteer.FN_EmailHyperlink);

			lblContactPhone.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_HomePhone);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_MobilePhone);
			lblEmail.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_Email);
			lblDateSubmitted.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_DateSubmitted);
			lblDateSubmitted.DataBindings[0].FormatString = "{0:MM/dd/yyyy hh:mm tt}";

			lblRegisteredBetween.Text = string.Empty;
			if (dtFrom.HasValue)
			{
				lblRegisteredBetween.Text = "Registered After " + dtFrom.Value.ToString("MM/dd/yyyy hh:mm tt");
			}

			lblTotalCount.Text = String.Format("Total Count: {0:###,##0}", totalCount);
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_Volunteer.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_Volunteer.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblStudentName.Font = new Font(lblStudentName.Font, FontStyle.Underline);
				lblStudentName.ForeColor = Color.Blue;
			}
			else
			{
				lblStudentName.Font = new Font(lblStudentName.Font, FontStyle.Regular);
				lblStudentName.ForeColor = Color.Black;
			}
		}
	}
}
