﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
    public partial class rptNonVoyagersVolunteers : DevExpress.XtraReports.UI.XtraReport
    {
        private Color _detailBackColor = Color.White;

		public rptNonVoyagersVolunteers()
		{
			InitializeComponent();
		}


		public rptNonVoyagersVolunteers(int totalCount)
        {
            InitializeComponent();

            // Set the bindings
            lblName.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_FullNameLastFirst);
            lblName.DataBindings.Add("NavigateUrl", null, Search_VBS_Volunteer.FN_EmailHyperlink);

            lblEMail.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_Email);
            lblEMail.DataBindings.Add("NavigateUrl", null, Search_VBS_Volunteer.FN_EmailHyperlink);
            lblType.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_SheetType);

            lblHomePhone.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_HomePhone);
            lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_MobilePhone);

            chkAdult.DataBindings.Add("Checked", null, Search_VBS_Volunteer.FN_IsAdult);

            lblClass.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_ClassCode);
            lblGroup.DataBindings.Add("Text", null, Search_VBS_Volunteer.FN_GroupName);

			lblTotalCount.Text = String.Format("Total Count: {0:###,##0}", totalCount);
		}

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrPanel1.BackColor = _detailBackColor;
            _detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

            //chkMedRelForm.Visible = (GetCurrentColumnValue(Search_VBS_MedicalComparison.FN_MedicalReleaseForm) != null);

            string emailHyperlink = (GetCurrentColumnValue(Search_VBS_Volunteer.FN_EmailHyperlink) != null ?
                GetCurrentColumnValue(Search_VBS_Volunteer.FN_EmailHyperlink).ToString() : string.Empty);
            if (!String.IsNullOrEmpty(emailHyperlink))
            {
                lblEMail.Font = new Font(lblHomePhone.Font, FontStyle.Underline);
                lblEMail.ForeColor = Color.Blue;
                lblName.Font = new Font(lblHomePhone.Font, FontStyle.Underline);
                lblName.ForeColor = Color.Blue;
            }
            else
            {
                lblEMail.Font = new Font(lblHomePhone.Font, FontStyle.Regular);
                lblEMail.ForeColor = Color.Black;
                lblName.Font = new Font(lblHomePhone.Font, FontStyle.Regular);
                lblName.ForeColor = Color.Black;
            }
        }
    }
}
