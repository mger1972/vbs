﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
    public partial class MedicalCheckSend : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login.aspx", true);
                return;
            }
            else
            {
                Session["UserName"] = HttpContext.Current.User.Identity.Name;
            }

            // Tell the parent who we belong to
            ((SiteMaster)this.Master).SetChildPageInstance(this);

            //// Load the content into the HTML window
            //StringBuilder sb = new StringBuilder();

            ////sb.AppendLine("<html>");
            ////sb.AppendLine("<head>");
            ////sb.AppendLine("<title>Background Check Required</title>");
            ////sb.AppendLine("</head>");

            ////sb.AppendLine("<body>");
            //sb.Append("<p><font=\"Calibri, Arial, Times New Roman\">Dear &lt;&lt;firstName&gt;&gt; &lt;&lt;lastName&gt;&gt;:</font></p>");
            //sb.Append("<p><font=\"Calibri, Arial, Times New Roman\">Thank you for volunteering for Sonrise Island at Voyagers Bible Church.  We’re excited to have you on board for this year!</font></p>");
            //sb.Append("<p><font=\"Calibri, Arial, Times New Roman\">We’ve reviewed our files and noticed we don’t have a recent background check on file for you.  Please contact Kim Coyle @ Voyagers (<a href=\"mailto:kcoyle@voyagers.org\">kcoyle@voyagers.org</a>) to obtain a background check form.  </font></p>");
            //sb.Append("<p><font=\"Calibri, Arial, Times New Roman\">If you have any questions or feel you’ve received this message in error, please don’t hesitate to contact me.</font></p>");
            //sb.Append("<p><font=\"Calibri, Arial, Times New Roman\">Blessings, <br />Jennifer Gerlach</font></p>");
            ////sb.AppendLine("</body>");

            ////sb.AppendLine("</html>");

            ////htmlEditor.Html = sb.ToString();		// Set the text
            //int test = 1;
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            // Send out an email
            //Utils.SendEMailAsJen("Mark Gerlach", "mark@gerlach5.com", "test", "testing the email", string.Empty, Session["UserName"].ToString());
        }
    }
}