﻿namespace VBS.Reports
{
	partial class rptClassSignIn2
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptClassSignIn2));
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.ghClassCode = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.gfClassCode = new DevExpress.XtraReports.UI.GroupFooterBand();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.xrPanel2 = new DevExpress.XtraReports.UI.XRPanel();
			this.lblWedIn = new DevExpress.XtraReports.UI.XRLabel();
			this.lblMonOut = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTueIn = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTueOut = new DevExpress.XtraReports.UI.XRLabel();
			this.lblMonIn = new DevExpress.XtraReports.UI.XRLabel();
			this.lblKidNum = new DevExpress.XtraReports.UI.XRLabel();
			this.lblStudentName = new DevExpress.XtraReports.UI.XRLabel();
			this.lblGender = new DevExpress.XtraReports.UI.XRLabel();
			this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
			this.picHeader = new DevExpress.XtraReports.UI.XRPictureBox();
			this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblCount = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblRotation = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblHelper1 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblHelper2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTeacher2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblTeacher1 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblClass = new DevExpress.XtraReports.UI.XRLabel();
			this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.lblFriOut = new DevExpress.XtraReports.UI.XRLabel();
			this.lblThuIn = new DevExpress.XtraReports.UI.XRLabel();
			this.lblThuOut = new DevExpress.XtraReports.UI.XRLabel();
			this.lblFriIn = new DevExpress.XtraReports.UI.XRLabel();
			this.lblWedOut = new DevExpress.XtraReports.UI.XRLabel();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel2});
			this.Detail.HeightF = 30.41668F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
			// 
			// TopMargin
			// 
			this.TopMargin.HeightF = 50F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// BottomMargin
			// 
			this.BottomMargin.HeightF = 50F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// ReportHeader
			// 
			this.ReportHeader.HeightF = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.rptTitle,
            this.picHeader});
			this.PageHeader.HeightF = 63.54167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// ghClassCode
			// 
			this.ghClassCode.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel7,
            this.xrLabel11,
            this.xrLabel15,
            this.xrLabel16,
            this.xrLabel17,
            this.lblCount,
            this.xrLabel14,
            this.lblRotation,
            this.xrLabel1,
            this.xrLabel8,
            this.xrLabel6,
            this.xrLabel4,
            this.xrLabel5,
            this.lblHelper1,
            this.lblHelper2,
            this.lblTeacher2,
            this.lblTeacher1,
            this.xrLabel3,
            this.xrLabel2,
            this.lblClass});
			this.ghClassCode.HeightF = 115.6267F;
			this.ghClassCode.KeepTogether = true;
			this.ghClassCode.Name = "ghClassCode";
			// 
			// gfClassCode
			// 
			this.gfClassCode.HeightF = 0F;
			this.gfClassCode.Name = "gfClassCode";
			this.gfClassCode.PageBreak = DevExpress.XtraReports.UI.PageBreak.AfterBand;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
			this.PageFooter.HeightF = 32.99997F;
			this.PageFooter.Name = "PageFooter";
			// 
			// xrPanel2
			// 
			this.xrPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(135)))), ((int)(((byte)(206)))), ((int)(((byte)(235)))));
			this.xrPanel2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblFriOut,
            this.lblThuIn,
            this.lblThuOut,
            this.lblFriIn,
            this.lblWedOut,
            this.lblWedIn,
            this.lblMonOut,
            this.lblTueIn,
            this.lblTueOut,
            this.lblMonIn,
            this.lblKidNum,
            this.lblStudentName,
            this.lblGender});
			this.xrPanel2.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.xrPanel2.Name = "xrPanel2";
			this.xrPanel2.SizeF = new System.Drawing.SizeF(963.3333F, 30.41668F);
			this.xrPanel2.StylePriority.UseBackColor = false;
			// 
			// lblWedIn
			// 
			this.lblWedIn.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblWedIn.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblWedIn.CanGrow = false;
			this.lblWedIn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblWedIn.LocationFloat = new DevExpress.Utils.PointFloat(567.0769F, 0F);
			this.lblWedIn.Name = "lblWedIn";
			this.lblWedIn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblWedIn.SizeF = new System.Drawing.SizeF(66.03992F, 30.41668F);
			this.lblWedIn.StylePriority.UseBorders = false;
			this.lblWedIn.StylePriority.UseFont = false;
			this.lblWedIn.StylePriority.UseTextAlignment = false;
			this.lblWedIn.Text = " ";
			this.lblWedIn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// lblMonOut
			// 
			this.lblMonOut.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblMonOut.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblMonOut.CanGrow = false;
			this.lblMonOut.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMonOut.LocationFloat = new DevExpress.Utils.PointFloat(368.9568F, 0F);
			this.lblMonOut.Name = "lblMonOut";
			this.lblMonOut.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblMonOut.SizeF = new System.Drawing.SizeF(66.04001F, 30.41668F);
			this.lblMonOut.StylePriority.UseBorders = false;
			this.lblMonOut.StylePriority.UseFont = false;
			this.lblMonOut.StylePriority.UseTextAlignment = false;
			this.lblMonOut.Text = " ";
			this.lblMonOut.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// lblTueIn
			// 
			this.lblTueIn.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblTueIn.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblTueIn.CanGrow = false;
			this.lblTueIn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTueIn.LocationFloat = new DevExpress.Utils.PointFloat(434.9968F, 0F);
			this.lblTueIn.Name = "lblTueIn";
			this.lblTueIn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTueIn.SizeF = new System.Drawing.SizeF(66.03998F, 30.41668F);
			this.lblTueIn.StylePriority.UseBorders = false;
			this.lblTueIn.StylePriority.UseFont = false;
			this.lblTueIn.StylePriority.UseTextAlignment = false;
			this.lblTueIn.Text = " ";
			this.lblTueIn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// lblTueOut
			// 
			this.lblTueOut.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblTueOut.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblTueOut.CanGrow = false;
			this.lblTueOut.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTueOut.LocationFloat = new DevExpress.Utils.PointFloat(501.0369F, 0F);
			this.lblTueOut.Name = "lblTueOut";
			this.lblTueOut.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTueOut.SizeF = new System.Drawing.SizeF(66.04004F, 30.41668F);
			this.lblTueOut.StylePriority.UseBorders = false;
			this.lblTueOut.StylePriority.UseFont = false;
			this.lblTueOut.StylePriority.UseTextAlignment = false;
			this.lblTueOut.Text = " ";
			this.lblTueOut.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// lblMonIn
			// 
			this.lblMonIn.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblMonIn.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblMonIn.CanGrow = false;
			this.lblMonIn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMonIn.LocationFloat = new DevExpress.Utils.PointFloat(302.9168F, 0F);
			this.lblMonIn.Name = "lblMonIn";
			this.lblMonIn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblMonIn.SizeF = new System.Drawing.SizeF(66.04001F, 30.41668F);
			this.lblMonIn.StylePriority.UseBorders = false;
			this.lblMonIn.StylePriority.UseFont = false;
			this.lblMonIn.StylePriority.UseTextAlignment = false;
			this.lblMonIn.Text = " ";
			this.lblMonIn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// lblKidNum
			// 
			this.lblKidNum.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblKidNum.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.lblKidNum.Name = "lblKidNum";
			this.lblKidNum.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblKidNum.SizeF = new System.Drawing.SizeF(30.20852F, 30.41668F);
			this.lblKidNum.StylePriority.UseFont = false;
			this.lblKidNum.Text = "714-832-8664";
			// 
			// lblStudentName
			// 
			this.lblStudentName.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblStudentName.LocationFloat = new DevExpress.Utils.PointFloat(30.20852F, 0F);
			this.lblStudentName.Name = "lblStudentName";
			this.lblStudentName.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblStudentName.SizeF = new System.Drawing.SizeF(206.6666F, 30.41668F);
			this.lblStudentName.StylePriority.UseFont = false;
			this.lblStudentName.Text = "714-832-8664";
			// 
			// lblGender
			// 
			this.lblGender.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblGender.LocationFloat = new DevExpress.Utils.PointFloat(236.8751F, 0F);
			this.lblGender.Name = "lblGender";
			this.lblGender.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblGender.SizeF = new System.Drawing.SizeF(66.04166F, 30.41668F);
			this.lblGender.StylePriority.UseFont = false;
			this.lblGender.StylePriority.UseTextAlignment = false;
			this.lblGender.Text = "714-832-8664";
			this.lblGender.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// rptTitle
			// 
			this.rptTitle.Font = new System.Drawing.Font("Verdana", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rptTitle.ForeColor = System.Drawing.Color.Blue;
			this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.rptTitle.Name = "rptTitle";
			this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.rptTitle.SizeF = new System.Drawing.SizeF(356.25F, 31.33333F);
			this.rptTitle.StylePriority.UseFont = false;
			this.rptTitle.StylePriority.UseForeColor = false;
			this.rptTitle.Text = "Classroom Sign-In Sheets";
			// 
			// picHeader
			// 
			this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
			this.picHeader.LocationFloat = new DevExpress.Utils.PointFloat(619.7917F, 0F);
			this.picHeader.Name = "picHeader";
			this.picHeader.SizeF = new System.Drawing.SizeF(380.2083F, 56.25F);
			this.picHeader.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
			// 
			// xrLabel7
			// 
			this.xrLabel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel7.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel7.ForeColor = System.Drawing.Color.White;
			this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(323.3335F, 95.415F);
			this.xrLabel7.Name = "xrLabel7";
			this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel7.SizeF = new System.Drawing.SizeF(132.08F, 20.21F);
			this.xrLabel7.StylePriority.UseBackColor = false;
			this.xrLabel7.StylePriority.UseFont = false;
			this.xrLabel7.StylePriority.UseForeColor = false;
			this.xrLabel7.StylePriority.UseTextAlignment = false;
			this.xrLabel7.Text = "Mon.";
			this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// xrLabel11
			// 
			this.xrLabel11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel11.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel11.ForeColor = System.Drawing.Color.White;
			this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(455.4135F, 95.41499F);
			this.xrLabel11.Name = "xrLabel11";
			this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel11.SizeF = new System.Drawing.SizeF(132.08F, 20.21F);
			this.xrLabel11.StylePriority.UseBackColor = false;
			this.xrLabel11.StylePriority.UseFont = false;
			this.xrLabel11.StylePriority.UseForeColor = false;
			this.xrLabel11.StylePriority.UseTextAlignment = false;
			this.xrLabel11.Text = "Tue.";
			this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// xrLabel15
			// 
			this.xrLabel15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel15.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel15.ForeColor = System.Drawing.Color.White;
			this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(587.4935F, 95.41499F);
			this.xrLabel15.Name = "xrLabel15";
			this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel15.SizeF = new System.Drawing.SizeF(132.08F, 20.21F);
			this.xrLabel15.StylePriority.UseBackColor = false;
			this.xrLabel15.StylePriority.UseFont = false;
			this.xrLabel15.StylePriority.UseForeColor = false;
			this.xrLabel15.StylePriority.UseTextAlignment = false;
			this.xrLabel15.Text = "Wed.";
			this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// xrLabel16
			// 
			this.xrLabel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel16.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel16.ForeColor = System.Drawing.Color.White;
			this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(719.5735F, 95.41499F);
			this.xrLabel16.Name = "xrLabel16";
			this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel16.SizeF = new System.Drawing.SizeF(132.08F, 20.21F);
			this.xrLabel16.StylePriority.UseBackColor = false;
			this.xrLabel16.StylePriority.UseFont = false;
			this.xrLabel16.StylePriority.UseForeColor = false;
			this.xrLabel16.StylePriority.UseTextAlignment = false;
			this.xrLabel16.Text = "Thu.";
			this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// xrLabel17
			// 
			this.xrLabel17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel17.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel17.ForeColor = System.Drawing.Color.White;
			this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(851.6536F, 95.41667F);
			this.xrLabel17.Name = "xrLabel17";
			this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel17.SizeF = new System.Drawing.SizeF(132.08F, 20.21F);
			this.xrLabel17.StylePriority.UseBackColor = false;
			this.xrLabel17.StylePriority.UseFont = false;
			this.xrLabel17.StylePriority.UseForeColor = false;
			this.xrLabel17.StylePriority.UseTextAlignment = false;
			this.xrLabel17.Text = "Fri.";
			this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// lblCount
			// 
			this.lblCount.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCount.LocationFloat = new DevExpress.Utils.PointFloat(633.7498F, 0F);
			this.lblCount.Name = "lblCount";
			this.lblCount.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblCount.SizeF = new System.Drawing.SizeF(158.3334F, 25.08333F);
			this.lblCount.StylePriority.UseBackColor = false;
			this.lblCount.StylePriority.UseFont = false;
			this.lblCount.StylePriority.UseForeColor = false;
			this.lblCount.StylePriority.UseTextAlignment = false;
			this.lblCount.Text = "A";
			this.lblCount.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrLabel14
			// 
			this.xrLabel14.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(542.7081F, 0F);
			this.xrLabel14.Name = "xrLabel14";
			this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel14.SizeF = new System.Drawing.SizeF(76.04175F, 25.08333F);
			this.xrLabel14.StylePriority.UseBackColor = false;
			this.xrLabel14.StylePriority.UseFont = false;
			this.xrLabel14.StylePriority.UseForeColor = false;
			this.xrLabel14.StylePriority.UseTextAlignment = false;
			this.xrLabel14.Text = "Count:";
			this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblRotation
			// 
			this.lblRotation.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblRotation.LocationFloat = new DevExpress.Utils.PointFloat(831.6666F, 0F);
			this.lblRotation.Name = "lblRotation";
			this.lblRotation.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblRotation.SizeF = new System.Drawing.SizeF(158.3334F, 25.08333F);
			this.lblRotation.StylePriority.UseBackColor = false;
			this.lblRotation.StylePriority.UseFont = false;
			this.lblRotation.StylePriority.UseForeColor = false;
			this.lblRotation.StylePriority.UseTextAlignment = false;
			this.lblRotation.Text = "Rotation: A";
			this.lblRotation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// xrLabel1
			// 
			this.xrLabel1.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel1.ForeColor = System.Drawing.Color.Black;
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(76.04169F, 25.08333F);
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseForeColor = false;
			this.xrLabel1.StylePriority.UseTextAlignment = false;
			this.xrLabel1.Text = "Class:";
			this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrLabel8
			// 
			this.xrLabel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel8.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel8.ForeColor = System.Drawing.Color.White;
			this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(257.2918F, 95.41667F);
			this.xrLabel8.Name = "xrLabel8";
			this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel8.SizeF = new System.Drawing.SizeF(66.04172F, 20.20833F);
			this.xrLabel8.StylePriority.UseBackColor = false;
			this.xrLabel8.StylePriority.UseFont = false;
			this.xrLabel8.StylePriority.UseForeColor = false;
			this.xrLabel8.StylePriority.UseTextAlignment = false;
			this.xrLabel8.Text = "Gender";
			this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// xrLabel6
			// 
			this.xrLabel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(128)))));
			this.xrLabel6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel6.ForeColor = System.Drawing.Color.White;
			this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 95.41667F);
			this.xrLabel6.Name = "xrLabel6";
			this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel6.SizeF = new System.Drawing.SizeF(236.8751F, 20.20833F);
			this.xrLabel6.StylePriority.UseBackColor = false;
			this.xrLabel6.StylePriority.UseFont = false;
			this.xrLabel6.StylePriority.UseForeColor = false;
			this.xrLabel6.Text = "Student Name";
			// 
			// xrLabel4
			// 
			this.xrLabel4.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(542.7082F, 40.625F);
			this.xrLabel4.Name = "xrLabel4";
			this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel4.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel4.StylePriority.UseFont = false;
			this.xrLabel4.Text = "Helper:";
			// 
			// xrLabel5
			// 
			this.xrLabel5.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(542.7082F, 62.5F);
			this.xrLabel5.Name = "xrLabel5";
			this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel5.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel5.StylePriority.UseFont = false;
			this.xrLabel5.Text = "Helper:";
			// 
			// lblHelper1
			// 
			this.lblHelper1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHelper1.LocationFloat = new DevExpress.Utils.PointFloat(633.7499F, 40.625F);
			this.lblHelper1.Name = "lblHelper1";
			this.lblHelper1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHelper1.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblHelper1.StylePriority.UseFont = false;
			this.lblHelper1.Text = "Team Leader:";
			// 
			// lblHelper2
			// 
			this.lblHelper2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblHelper2.LocationFloat = new DevExpress.Utils.PointFloat(633.7499F, 62.5F);
			this.lblHelper2.Name = "lblHelper2";
			this.lblHelper2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblHelper2.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblHelper2.StylePriority.UseFont = false;
			this.lblHelper2.Text = "Team Leader:";
			// 
			// lblTeacher2
			// 
			this.lblTeacher2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTeacher2.LocationFloat = new DevExpress.Utils.PointFloat(101.0417F, 62.5F);
			this.lblTeacher2.Name = "lblTeacher2";
			this.lblTeacher2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTeacher2.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblTeacher2.StylePriority.UseFont = false;
			this.lblTeacher2.Text = "Team Leader:";
			// 
			// lblTeacher1
			// 
			this.lblTeacher1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTeacher1.LocationFloat = new DevExpress.Utils.PointFloat(101.0417F, 40.625F);
			this.lblTeacher1.Name = "lblTeacher1";
			this.lblTeacher1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblTeacher1.SizeF = new System.Drawing.SizeF(350.0001F, 15F);
			this.lblTeacher1.StylePriority.UseFont = false;
			this.lblTeacher1.Text = "Team Leader:";
			// 
			// xrLabel3
			// 
			this.xrLabel3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 62.5F);
			this.xrLabel3.Name = "xrLabel3";
			this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel3.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel3.StylePriority.UseFont = false;
			this.xrLabel3.Text = "Team Leader:";
			// 
			// xrLabel2
			// 
			this.xrLabel2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 40.625F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(76.0417F, 15F);
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.Text = "Team Leader:";
			// 
			// lblClass
			// 
			this.lblClass.BackColor = System.Drawing.Color.Yellow;
			this.lblClass.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblClass.ForeColor = System.Drawing.Color.Red;
			this.lblClass.LocationFloat = new DevExpress.Utils.PointFloat(101.0417F, 0F);
			this.lblClass.Name = "lblClass";
			this.lblClass.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblClass.SizeF = new System.Drawing.SizeF(350.0001F, 25.08333F);
			this.lblClass.StylePriority.UseBackColor = false;
			this.lblClass.StylePriority.UseFont = false;
			this.lblClass.StylePriority.UseForeColor = false;
			this.lblClass.StylePriority.UseTextAlignment = false;
			this.lblClass.Text = "A";
			this.lblClass.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrPageInfo2
			// 
			this.xrPageInfo2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo2.Format = "Printed on {0:MM/dd/yyyy} at {0:hh:mm:ss tt}";
			this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 9.999974F);
			this.xrPageInfo2.Name = "xrPageInfo2";
			this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
			this.xrPageInfo2.SizeF = new System.Drawing.SizeF(289.1666F, 23F);
			this.xrPageInfo2.StylePriority.UseFont = false;
			this.xrPageInfo2.StylePriority.UseTextAlignment = false;
			this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// xrPageInfo1
			// 
			this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo1.Format = "Page {0} of {1}";
			this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(811.2501F, 9.999974F);
			this.xrPageInfo1.Name = "xrPageInfo1";
			this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo1.SizeF = new System.Drawing.SizeF(178.7499F, 23F);
			this.xrPageInfo1.StylePriority.UseFont = false;
			this.xrPageInfo1.StylePriority.UseTextAlignment = false;
			this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// lblFriOut
			// 
			this.lblFriOut.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblFriOut.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblFriOut.CanGrow = false;
			this.lblFriOut.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFriOut.LocationFloat = new DevExpress.Utils.PointFloat(897.2933F, 0F);
			this.lblFriOut.Name = "lblFriOut";
			this.lblFriOut.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblFriOut.SizeF = new System.Drawing.SizeF(66.03998F, 30.41668F);
			this.lblFriOut.StylePriority.UseBorders = false;
			this.lblFriOut.StylePriority.UseFont = false;
			this.lblFriOut.StylePriority.UseTextAlignment = false;
			this.lblFriOut.Text = " ";
			this.lblFriOut.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// lblThuIn
			// 
			this.lblThuIn.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblThuIn.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblThuIn.CanGrow = false;
			this.lblThuIn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblThuIn.LocationFloat = new DevExpress.Utils.PointFloat(699.1732F, 0F);
			this.lblThuIn.Name = "lblThuIn";
			this.lblThuIn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblThuIn.SizeF = new System.Drawing.SizeF(66.03998F, 30.41668F);
			this.lblThuIn.StylePriority.UseBorders = false;
			this.lblThuIn.StylePriority.UseFont = false;
			this.lblThuIn.StylePriority.UseTextAlignment = false;
			this.lblThuIn.Text = " ";
			this.lblThuIn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// lblThuOut
			// 
			this.lblThuOut.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblThuOut.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblThuOut.CanGrow = false;
			this.lblThuOut.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblThuOut.LocationFloat = new DevExpress.Utils.PointFloat(765.2133F, 0F);
			this.lblThuOut.Name = "lblThuOut";
			this.lblThuOut.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblThuOut.SizeF = new System.Drawing.SizeF(66.03998F, 30.41668F);
			this.lblThuOut.StylePriority.UseBorders = false;
			this.lblThuOut.StylePriority.UseFont = false;
			this.lblThuOut.StylePriority.UseTextAlignment = false;
			this.lblThuOut.Text = " ";
			this.lblThuOut.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// lblFriIn
			// 
			this.lblFriIn.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblFriIn.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblFriIn.CanGrow = false;
			this.lblFriIn.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblFriIn.LocationFloat = new DevExpress.Utils.PointFloat(831.2533F, 0F);
			this.lblFriIn.Name = "lblFriIn";
			this.lblFriIn.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblFriIn.SizeF = new System.Drawing.SizeF(66.03998F, 30.41668F);
			this.lblFriIn.StylePriority.UseBorders = false;
			this.lblFriIn.StylePriority.UseFont = false;
			this.lblFriIn.StylePriority.UseTextAlignment = false;
			this.lblFriIn.Text = " ";
			this.lblFriIn.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// lblWedOut
			// 
			this.lblWedOut.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
			this.lblWedOut.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblWedOut.CanGrow = false;
			this.lblWedOut.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblWedOut.LocationFloat = new DevExpress.Utils.PointFloat(633.1332F, 0F);
			this.lblWedOut.Name = "lblWedOut";
			this.lblWedOut.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblWedOut.SizeF = new System.Drawing.SizeF(66.03998F, 30.41668F);
			this.lblWedOut.StylePriority.UseBorders = false;
			this.lblWedOut.StylePriority.UseFont = false;
			this.lblWedOut.StylePriority.UseTextAlignment = false;
			this.lblWedOut.Text = " ";
			this.lblWedOut.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomCenter;
			// 
			// rptClassSignIn2
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.ghClassCode,
            this.gfClassCode,
            this.PageFooter});
			this.Landscape = true;
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
			this.PageHeight = 850;
			this.PageWidth = 1100;
			this.Version = "17.1";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}

		#endregion

		private DevExpress.XtraReports.UI.DetailBand Detail;
		private DevExpress.XtraReports.UI.XRPanel xrPanel2;
		private DevExpress.XtraReports.UI.XRLabel lblWedIn;
		private DevExpress.XtraReports.UI.XRLabel lblMonOut;
		private DevExpress.XtraReports.UI.XRLabel lblTueIn;
		private DevExpress.XtraReports.UI.XRLabel lblTueOut;
		private DevExpress.XtraReports.UI.XRLabel lblMonIn;
		private DevExpress.XtraReports.UI.XRLabel lblKidNum;
		private DevExpress.XtraReports.UI.XRLabel lblStudentName;
		private DevExpress.XtraReports.UI.XRLabel lblGender;
		private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
		private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
		private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
		private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
		private DevExpress.XtraReports.UI.XRLabel rptTitle;
		private DevExpress.XtraReports.UI.XRPictureBox picHeader;
		private DevExpress.XtraReports.UI.GroupHeaderBand ghClassCode;
		private DevExpress.XtraReports.UI.XRLabel xrLabel7;
		private DevExpress.XtraReports.UI.XRLabel xrLabel11;
		private DevExpress.XtraReports.UI.XRLabel xrLabel15;
		private DevExpress.XtraReports.UI.XRLabel xrLabel16;
		private DevExpress.XtraReports.UI.XRLabel xrLabel17;
		private DevExpress.XtraReports.UI.XRLabel lblCount;
		private DevExpress.XtraReports.UI.XRLabel xrLabel14;
		private DevExpress.XtraReports.UI.XRLabel lblRotation;
		private DevExpress.XtraReports.UI.XRLabel xrLabel1;
		private DevExpress.XtraReports.UI.XRLabel xrLabel8;
		private DevExpress.XtraReports.UI.XRLabel xrLabel6;
		private DevExpress.XtraReports.UI.XRLabel xrLabel4;
		private DevExpress.XtraReports.UI.XRLabel xrLabel5;
		private DevExpress.XtraReports.UI.XRLabel lblHelper1;
		private DevExpress.XtraReports.UI.XRLabel lblHelper2;
		private DevExpress.XtraReports.UI.XRLabel lblTeacher2;
		private DevExpress.XtraReports.UI.XRLabel lblTeacher1;
		private DevExpress.XtraReports.UI.XRLabel xrLabel3;
		private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRLabel lblClass;
		private DevExpress.XtraReports.UI.GroupFooterBand gfClassCode;
		private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
		private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
		private DevExpress.XtraReports.UI.XRLabel lblFriOut;
		private DevExpress.XtraReports.UI.XRLabel lblThuIn;
		private DevExpress.XtraReports.UI.XRLabel lblThuOut;
		private DevExpress.XtraReports.UI.XRLabel lblFriIn;
		private DevExpress.XtraReports.UI.XRLabel lblWedOut;
	}
}
