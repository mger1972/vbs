﻿<%@ Page Title="Email Listing" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EmailListing.aspx.cs" Inherits="VBS.Reports.EmailListing" %>

<%@ Register Assembly="DevExpress.XtraReports.v17.1.Web, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
				<h3>A list of email addresses for teams, groups, and students.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script type="text/javascript">
		function OnConfirm(s, e) {
			if (myButtonClick == true) {
				e.cancel = true;
				myButtonClick = false;
			}

		}
		var myButtonClick = false;
		function OnClick(s, e) {
			myButtonClick = true;
			pnlLoading.Show();
		}
	</script>
	<table id="selectionTable" width="100%" align="center" border="0">
		<tr>
			<td width="80%">
				<table width="100%">
					<tr>
						<td width="1%" valign="middle">Class:&nbsp;</td>
						<td valign="top"><dx:ASPxComboBox runat="server" ID="cboType" ClientInstanceName="cboType" DropDownStyle="DropDownList"
							width="300px" DropDownWidth="300px" OnDataBinding="cboType_DataBinding" AutoPostBack="true"
							ValueField="RawType" TextField="TypeText"
							OnSelectedIndexChanged="cboType_SelectedIndexChanged" ValueType="System.String" 
							EnableCallbackMode="true" TextFormatString="{0}">
							<Columns>
								<dx:ListBoxColumn FieldName="TypeText" Width="150px" />
							</Columns>
							<ClientSideEvents SelectedIndexChanged="function(s, e) { pnlLoading.Text = 'Loading Records...'; pnlLoading.Show(); }" />
						</dx:ASPxComboBox></td>
						<td width="20%" align="right" nowrap>
							<% if (!String.IsNullOrEmpty(MessageText.Text)) { %>
							<span class="validation-summary-errors"><asp:Literal runat="server" ID="MessageText" Text="" />&nbsp; &nbsp;</span>
							<% } %>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table id="bottomPanel" width="100%" align="center" border="0">
		<tr>
			<td colspan="2"><hr /></td>
		</tr>
		<tr>
			<td colspan="2">
				<table style="padding:0px;border-spacing:0px;width:100%;border:none;" align="center">
					<tr>
						<td><% =_sbList.ToString() %></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<dx:ASPxLoadingPanel ID="pnlLoading" runat="server" ContainerElementID="bottomPanel" ClientInstanceName="pnlLoading" Modal="true"></dx:ASPxLoadingPanel>
</asp:Content>