﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class ListOfAdults : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			if (dtpFrom.Date < DateTime.Parse("1/1/2015"))
			{
				dtpFrom.Date = DateTime.Parse("1/1/2016");		// Set the date
				ResetReport(null);
			}
			else
			{
				ResetReport(dtpFrom.Date);
			}
		}

		/// <summary>
		/// Reset the report
		/// </summary>
		protected void ResetReport(DateTime? dateTime)
		{
			Search_VBS_VolunteerCollection volunteers = new Search_VBS_VolunteerCollection("bIsAdult <> 0" + 
			(dateTime.HasValue ? " AND dtSubmitted >= '" + dateTime.Value.ToString("MM/dd/yyyy hh:mm:ss tt") + "'" : string.Empty));
			volunteers.Sort(Search_VBS_Volunteer.FN_DateSubmitted + " DESC");

			// Set up the names
			//foreach (Search_VBS_Allergies allergy in allergies)
			//{
			//	allergy.GridCustom_7 = allergy.LastName + ", " + allergy.FirstName;
			//	allergy.GridCustom_9 = (!String.IsNullOrEmpty(allergy.ParentLast) ? allergy.ParentLast + ", " + allergy.ParentFirst : string.Empty);
			//}

			rptListOfAdults rpt = new rptListOfAdults(dateTime.HasValue ? dateTime.Value : (DateTime?)null, volunteers.Count);

			rpt.DataSource = volunteers;
			rpt.DisplayName = "List of Adults";
			//rpt.CreateDocument();

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.98f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}

		protected void dtpFrom_DateChanged(object sender, EventArgs e)
		{
			// When the date changes, update the report
			//ResetReport(dtpFrom.Date);
		}
	}
}