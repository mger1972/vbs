﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Web;

namespace VBS.Reports
{
	public partial class rptVolunteerDOW_Area : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptVolunteerDOW_Area()
		{
			InitializeComponent();

			// Summary fields
			lblClassSummary.DataBindings.Add("Text", null, Search_VBS_VolunterDOW.FN_ClassCode);
			lblClassSummary.DataBindings.Add("Bookmark", null, Search_VBS_VolunterDOW.FN_ClassCode);

			// Detail Fields
			lblClass.DataBindings.Add("Text", null, Search_VBS_VolunterDOW.FN_ClassCode);
			lblGrade.DataBindings.Add("Text", null, Search_VBS_VolunterDOW.FN_Grade);
			lblName.DataBindings.Add("Text", null, Search_VBS_VolunterDOW.FN_FullNameLastFirst);
			lblLineNum.DataBindings.Add("Text", null, Search_VBS_VolunterDOW.FN_GridCustom_2);
			//lblFirstName.DataBindings.Add("Text", null, Search_VBS_VolunterDOW.FN_FirstName);

			chkJrHelper.DataBindings.Add("Checked", null, Search_VBS_VolunterDOW.FN_JuniorHelper);
			chkTeamLeader.DataBindings.Add("Checked", null, Search_VBS_VolunterDOW.FN_Teacher);

			// Bind the groupheader
			ghArea.GroupFields.Add(new GroupField(Search_VBS_VolunterDOW.FN_ClassCode, XRColumnSortOrder.Ascending));
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			// Check out the dow and we'll figure out how to color the boxes
			string dow = (GetCurrentColumnValue(Search_VBS_VolunterDOW.FN_HelpDOW) != null ?
				GetCurrentColumnValue(Search_VBS_VolunterDOW.FN_HelpDOW).ToString() : string.Empty);
			if (!dow.ToLower().Contains("all") && !dow.ToLower().Contains("mon")) { lblMon.BackColor = Color.DarkGray; } else { lblMon.BackColor = Color.Transparent; }
			if (!dow.ToLower().Contains("all") && !dow.ToLower().Contains("tue")) { lblTue.BackColor = Color.DarkGray; } else { lblTue.BackColor = Color.Transparent; }
			if (!dow.ToLower().Contains("all") && !dow.ToLower().Contains("wed")) { lblWed.BackColor = Color.DarkGray; } else { lblWed.BackColor = Color.Transparent; }
			if (!dow.ToLower().Contains("all") && !dow.ToLower().Contains("thu")) { lblThu.BackColor = Color.DarkGray; } else { lblThu.BackColor = Color.Transparent; }
			if (!dow.ToLower().Contains("all") && !dow.ToLower().Contains("fri")) { lblFri.BackColor = Color.DarkGray; } else { lblFri.BackColor = Color.Transparent; }
		}
	}
}
