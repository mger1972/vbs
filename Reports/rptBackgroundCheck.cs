﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptBackgroundCheck : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptBackgroundCheck()
		{
			InitializeComponent();
		}

		public rptBackgroundCheck(int totalCount)
		{
			InitializeComponent();

			// Detail Fields
			lblName.DataBindings.Add("Text", null, Search_VBS_BackgroundCheck.FN_BackgroundVolunteerNameFormatted);
			lblName.DataBindings.Add("NavigateUrl", null, Search_VBS_BackgroundCheck.FN_EmailHyperlink);

			lblContactPhone.DataBindings.Add("Text", null, Search_VBS_BackgroundCheck.FN_HomePhone);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_BackgroundCheck.FN_MobilePhone);
			lblEmail.DataBindings.Add("Text", null, Search_VBS_BackgroundCheck.FN_Email);
			lblDateSubmitted.DataBindings.Add("Text", null, Search_VBS_BackgroundCheck.FN_DateSubmitted);
			lblDateSubmitted.DataBindings[0].FormatString = "{0:MM/dd/yyyy hh:mm tt}";

			lblTotalCount.Text = String.Format("Total Count: {0:###,##0}", totalCount);
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			pnlColor.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_BackgroundCheck.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_BackgroundCheck.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblName.Font = new Font(lblName.Font, FontStyle.Underline);
				lblName.ForeColor = Color.Blue;
			}
			else
			{
				lblName.Font = new Font(lblName.Font, FontStyle.Regular);
				lblName.ForeColor = Color.Black;
			}
		}
	}
}
