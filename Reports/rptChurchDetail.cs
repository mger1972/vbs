﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptChurchDetail : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptChurchDetail()
		{
			InitializeComponent();

			// Set the bindings
			lblChurch.DataBindings.Add("Text", null, Search_VBS_ChurchListing.FN_ChurchAttended);
			lblCount.DataBindings.Add("Text", null, Search_VBS_ChurchListing.FN_Count);
			lblPercentage.DataBindings.Add("Text", null, Search_VBS_ChurchListing.FN_GridCustom_4);
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);
		}

	}
}
