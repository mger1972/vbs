﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Web;


namespace VBS.Reports
{
	public partial class rptClassRosterByStudent : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptClassRosterByStudent()
		{
			InitializeComponent();

			// Set the bindings
			lblLastName.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_Left1LastName);
			lblLastName.DataBindings.Add("Bookmark", null, Search_VBS_ClassAssignment.FN_Left1LastName);

			// Detail Fields
			lblStudentName.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_StudentNameFormatted);
			lblChurch.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ChurchAttended);
			lblGender.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_Gender);
			lblParentName.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ParentNameFormatted);
			lblParentName.DataBindings.Add("NavigateUrl", null, Search_VBS_ClassAssignment.FN_EmailHyperlink);
			lblClassGrade.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_GridCustom_5);
			lblPhone.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_MobilePhone);
			//lblPhone.DataBindings.Add("NavigateUrl", null, Search_VBS_ClassAssignment.FN_EmailHyperlink);
			lblTShirt.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ShirtSize);

			lblAllergy.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds);

			// Bind the groupheader
			ghStudentName.GroupFields.Add(new GroupField(Search_VBS_ClassAssignment.FN_Left1LastName, XRColumnSortOrder.Ascending));
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblParentName.Font = new Font(lblParentName.Font, FontStyle.Underline);
				lblParentName.ForeColor = Color.Blue;
				//lblPhone.Font = new Font(lblParentName.Font, FontStyle.Underline);
				//lblPhone.ForeColor = Color.Blue;
			}
			else
			{
				lblParentName.Font = new Font(lblParentName.Font, FontStyle.Regular);
				lblParentName.ForeColor = Color.Black;
				//lblPhone.Font = new Font(lblParentName.Font, FontStyle.Regular);
				//lblPhone.ForeColor = Color.Black;
			}

			//// Set the height depending on allergies
			////16.45836
			lblAllergy.Visible = true;
			if (String.IsNullOrEmpty(GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds) != null ? 
                GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds).ToString() : string.Empty))
			{
				lblAllergy.Visible = false;
				xrPanel1.HeightF = 16.45836F;
				Detail.HeightF = 16.45836F;
			}
		}

		private void rptClassRosterSinglePage_ParametersRequestBeforeShow(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
		{
			//foreach (ParameterInfo info in e.ParametersInformation)
			//{
			//	if (info.Parameter.Name.ToLower() == "ClassCodeParam".ToLower())
			//	{
			//		ASPxGridLookup lookUpEdit = new ASPxGridLookup();
			//		//LookUpEdit lookUpEdit = new LookUpEdit();
			//		//lookUpEdit.Properties.DataSource = dataSet.Categories;
			//		lookUpEdit.Properties.DisplayMember = "CategoryName";
			//		lookUpEdit.Properties.ValueMember = "CategoryID";
			//		lookUpEdit.Properties.Columns.Add(new LookUpColumnInfo("CategoryName", 0, "Category Name"));
			//		lookUpEdit.Properties.NullText = "<Select Category>";

			//		info.Editor = lookUpEdit;
			//		info.Parameter.Value = DBNull.Value;
			//	}
			//}
		}
	}
}
