﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class TeamLeaderListing : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			//// Get the collection for the list
			//Search_VBS_TeamLeaderListCollection leaderList = new Search_VBS_TeamLeaderListCollection(string.Empty);
			////leaderList.Sort(Search_VBS_TeamLeaderList.FN_IsAdult + " DESC, " + Search_VBS_TeamLeaderList.FN_VolunteerNameFormatted);

			//// Go through the list and put in descriptive text about the person
			//foreach (Search_VBS_TeamLeaderList item in leaderList)
			//{
			//	item.GridCustom_5 = (item.IsAdult.HasValue && item.IsAdult.Value ? "Adult Volunteer" : "Under 18 - Volunteer");
			//	item.GridCustom_4 = item.HelpInAreas.Replace(",", ", ");
			//}

			//rptTeamLeaderListing rpt = new rptTeamLeaderListing();
			//rpt.DataSource = leaderList;
			//rpt.DisplayName = "Team Leader Listing";
			//rpt.CreateDocument();
			//documentViewer.Report = rpt;
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Get the collection for the list
			Search_VBS_TeamLeaderListCollection leaderList = new Search_VBS_TeamLeaderListCollection(string.Empty);
			//leaderList.Sort(Search_VBS_TeamLeaderList.FN_IsAdult + " DESC, " + Search_VBS_TeamLeaderList.FN_VolunteerNameFormatted);

			// Go through the list and put in descriptive text about the person
			foreach (Search_VBS_TeamLeaderList item in leaderList)
			{
				item.GridCustom_5 = (item.IsAdult.HasValue && item.IsAdult.Value ? "Adult Volunteer" : "Under 18 - Volunteer");
				item.GridCustom_4 = item.HelpInAreas.Replace(",", ", ");
			}

			rptTeamLeaderListing rpt = new rptTeamLeaderListing();
			rpt.DataSource = leaderList;
			rpt.DisplayName = "Team Leader Listing";

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 1.28f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}
}