﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class SpecialNeedsReport : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			//Search_VBS_AllergiesCollection allergies = new Search_VBS_AllergiesCollection(string.Empty);
			//allergies.Sort(Search_VBS_Allergies.FN_FullNameLastFirstUpper);

			//// Set up the names
			//foreach (Search_VBS_Allergies allergy in allergies)
			//{
			//	allergy.GridCustom_7 = allergy.LastName + ", " + allergy.FirstName;
			//	allergy.GridCustom_9 = (!String.IsNullOrEmpty(allergy.ParentLast) ? allergy.ParentLast + ", " + allergy.ParentFirst : string.Empty);
			//}

			//rptAllergyReport rpt = new rptAllergyReport();

			//rpt.DataSource = allergies;
			//rpt.DisplayName = "Allergies";
			//rpt.CreateDocument();
			//documentViewer.Report = rpt;
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			Search_VBS_SpecialNeedsCollection specialNeeds = new Search_VBS_SpecialNeedsCollection(string.Empty);
			specialNeeds.Sort(Search_VBS_SpecialNeeds.FN_FullNameLastFirstUpper);

			// Set up the names
			foreach (Search_VBS_SpecialNeeds need in specialNeeds)
			{
				need.GridCustom_7 = need.LastName + ", " + need.FirstName;
				need.GridCustom_9 = (!String.IsNullOrEmpty(need.ParentLast) ? need.ParentLast + ", " + need.ParentFirst : string.Empty);
			}

			rptSpecialNeedsListing rpt = new rptSpecialNeedsListing();

			rpt.DataSource = specialNeeds;
			rpt.DisplayName = "Special Needs";

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.98f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}
	}
}