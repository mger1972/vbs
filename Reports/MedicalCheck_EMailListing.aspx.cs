﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class MedicalCheck_EMailListing : System.Web.UI.Page
	{
		protected StringBuilder _sbList = new StringBuilder();
		protected string _html = string.Empty, _htmlNoLinks = string.Empty;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Get a copy of the HTML we're sending
			_htmlNoLinks = _html = Session["MedicalCheckHTML"].ToString();

			while (_htmlNoLinks.ToLower().Contains("</a>"))
			{ 
				int startLinkPos = _htmlNoLinks.ToLower().IndexOf("<a ");
				int endLinkPos = _htmlNoLinks.ToLower().IndexOf(">", startLinkPos + 1);
				_htmlNoLinks = _htmlNoLinks.Substring(0, startLinkPos) +
					_htmlNoLinks.Substring(endLinkPos + 1);

				startLinkPos = _htmlNoLinks.ToLower().IndexOf("</a>");
				endLinkPos = startLinkPos + 4;
				_htmlNoLinks = _htmlNoLinks.Substring(0, startLinkPos) +
					_htmlNoLinks.Substring(endLinkPos);
			}

			// Build out the list of emails for this group
			_sbList = new StringBuilder(BuildEmailList());
		}

		private string BuildEmailList()
		{
			StringBuilder sb = new StringBuilder(), sbEmail = new StringBuilder();
			string rtv = string.Empty;
			string userName = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
			if (String.IsNullOrEmpty(userName))
			{
				return "Can't send email: User not logged in.";
			}

			Search_VBS_MedicalCheckCollection summary = new Search_VBS_MedicalCheckCollection("bMedicalReleaseForm = 0");
			summary.Sort(Search_VBS_MedicalCheck.FN_LastName + ", " +
				Search_VBS_MedicalCheck.FN_FirstName);
			List<string> existingEmails = new List<string>();
			
			foreach (Search_VBS_MedicalCheck item in summary)
			{
				if (!existingEmails.Contains(item.Email.ToLower()))
				{
					bool isEmail = Regex.IsMatch(item.Email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
					if (isEmail)
					{
						sb.Append("<br />&nbsp;&nbsp;&nbsp;\"" + item.FirstName + " " + item.LastName + "\" (<a href=\"mailto:" + item.Email + "\">" + item.Email + "</a>)");
						if (sbEmail.ToString().Length > 0) { sbEmail.Append(";"); }
						sbEmail.Append(item.Email);
						existingEmails.Add(item.Email.ToLower());
					}
				}
			}

			if (sbEmail.ToString().Length > 2000)
			{
				// The email string is too long to generate
				sbEmail = new StringBuilder("The <em>automated</em> email string is too long to generate. Highlight and copy the emails below to send an email.<br />&nbsp;<br />" +
					"<b>PLEASE PASTE THE EMAIL ADDRESSES AS A BCC (BLIND CARBON COPY) SO PEOPLE CAN'T SEE OTHER PEOPLE'S ADDRESSES.</b>");
				rtv = sbEmail.ToString() + "<br>&nbsp;<br>" + sb.ToString().Replace(" (", "<").Replace(")", ">");
			}
			else if (sb.ToString().Length > 0)
			{
				rtv = "<a href=\"mailto:" + userName + "&bcc=" + sbEmail.ToString() +
					"&subject=Sonrise Island - Medical Release Form&body=" +
					HttpUtility.UrlEncode(_html) + 
					//_html + 
					">Click here to send an email to: </a><br />";
				string test = "&nbsp;" + sb.ToString();
			}
			if (String.IsNullOrEmpty(rtv))
			{
				rtv = "No records/emails were found for this group.";
			}

			return rtv;		// Return the string of emails
		}
	}
}