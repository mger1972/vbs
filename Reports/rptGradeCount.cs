﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptGradeCount : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		private int _totalCount = 0;
		public int TotalCount
		{
			get { return _totalCount; }
			set { _totalCount = value; }
		}

		public rptGradeCount()
		{
			InitializeComponent();

			// Detail Fields
			lblGrade.DataBindings.Add("Text", null, Search_VBS_GradeCount.FN_GradeInFall);
			lblCount.DataBindings.Add("Text", null, Search_VBS_GradeCount.FN_Count);

			lblTotalSum.DataBindings.Add("Text", null, Search_VBS_GradeCount.FN_Count);
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);
		}
	}
}
