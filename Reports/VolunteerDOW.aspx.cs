﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.Web;
using DevExpress.XtraReports.UI;
using System.Drawing.Printing;

namespace VBS.Reports
{
	public partial class VolunteerDOW : System.Web.UI.Page
	{
		protected VolunteerDOWReportType _type = VolunteerDOWReportType.Classes;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Figure out which type of report this is
			_type = VolunteerDOWReportType.Classes;      // Set the default
			foreach (string key in Request.QueryString.AllKeys)
			{
				if (key.ToLower().Equals("type"))
				{
					// Get the value
					string val = Request.QueryString[key];
					if (val.ToLower().StartsWith("area")) { _type = VolunteerDOWReportType.Areas; }
					else if (val.ToLower().StartsWith("all")) { _type = VolunteerDOWReportType.All; }
					break;
				}
			}

			Search_VBS_VolunterDOWCollection dowCollection = new Search_VBS_VolunterDOWCollection("iSortOrder = 1");    // Default
			Search_VBS_VolunterDOWCollection dowAreaCollection = new Search_VBS_VolunterDOWCollection("iSortOrder = 2"); 
			if (_type == VolunteerDOWReportType.All) { dowCollection = new Search_VBS_VolunterDOWCollection("iSortOrder < 2"); }

			dowCollection.Sort(Search_VBS_VolunterDOW.FN_SortOrder + ", " +
				Search_VBS_VolunterDOW.FN_Length + ", " +
				Search_VBS_VolunterDOW.FN_ClassCode + ", " +
				Search_VBS_VolunterDOW.FN_Teacher + " DESC, " +
				Search_VBS_VolunterDOW.FN_LastName + ", " + 
				Search_VBS_VolunterDOW.FN_FirstName);
			dowAreaCollection.Sort(Search_VBS_VolunterDOW.FN_SortOrder + ", " +
				Search_VBS_VolunterDOW.FN_Length + ", " +
				Search_VBS_VolunterDOW.FN_ClassCode + ", " +
				Search_VBS_VolunterDOW.FN_Teacher + " DESC, " +
				Search_VBS_VolunterDOW.FN_LastName + ", " +
				Search_VBS_VolunterDOW.FN_FirstName);

			// Run the basic one first
			rptVolunteerDOW rpt = new rptVolunteerDOW();

			rpt.DataSource = dowCollection;
			rpt.DisplayName = "Volunteer - Sign in Sheet";

			// Put in the line numbers
			int lineNum = 1;
			foreach (Search_VBS_VolunterDOW item in dowCollection)
			{
				item.GridCustom_2 = lineNum.ToString("###,##0") + ".";
				lineNum++;
			}

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.98f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			// Run the secondary
			rptVolunteerDOW_Area rptArea = new rptVolunteerDOW_Area();

			rptArea.DataSource = dowAreaCollection;
			rptArea.DisplayName = "Volunteer - Sign in Sheet";

			lineNum = 1;
			string lastEntity = string.Empty;
			foreach (Search_VBS_VolunterDOW item in dowAreaCollection)
			{
				if (!lastEntity.ToLower().Equals(item.ClassCode.ToLower()))
				{
					lineNum = 1;
					lastEntity = item.ClassCode;
				}
				item.GridCustom_2 = lineNum.ToString("###,##0") + ".";
				lineNum++;
			}

			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rptArea.PageWidth * 0.98f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rptArea.Margins.Left * scaleFactor),
					Convert.ToInt32(rptArea.Margins.Right * scaleFactor),
					Convert.ToInt32(rptArea.Margins.Top * scaleFactor),
					Convert.ToInt32(rptArea.Margins.Bottom * scaleFactor));
				rptArea.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rptArea.PageWidth = (int)(rptArea.PageWidth * scaleFactor);
				rptArea.PageHeight = (int)(rptArea.PageHeight * scaleFactor);
				rptArea.Margins = newMargins;
				rptArea.CreateDocument();

				rptArea.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			if (_type == VolunteerDOWReportType.Classes)
			{
				documentViewer.Report = rpt;
			}
			else if (_type == VolunteerDOWReportType.Areas)
			{
				documentViewer.Report = rptArea;
			}
			else if (_type == VolunteerDOWReportType.All)
			{
				// Add this report to the base
				rpt.Pages.AddRange(rptArea.Pages);

				// Continiue the page numbering
				rpt.PrintingSystem.ContinuousPageNumbering = true;

				documentViewer.Report = rpt;
			}
		}
	}

	public enum VolunteerDOWReportType : int
	{
		Classes = 0,
		All,
		Areas,
	}
}