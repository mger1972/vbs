﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HTMLEditorPage.aspx.cs" Inherits="VBS.Reports.HTMLEditorPage" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" align="center">
		<tr>
			<td colspan="2">
				<dx:ASPxHtmlEditor ID="htmlEditor" runat="server" Width="100%" Height="340px" OnHtmlCorrecting="htmlEditor_HtmlCorrecting"></dx:ASPxHtmlEditor>
			</td>
		</tr>
		<tr>
			<td align="left" width="50%">
				<dx:ASPxCheckBox ID="chkSendCopyToMyself" Text="Send a copy of the email to me." Checked="true" runat="server"></dx:ASPxCheckBox>
			</td>
			<td align="right">
				<dx:ASPxButton ID="btnSubmit" runat="server" Text="Send Email" OnClick="btnSubmit_Click"></dx:ASPxButton>
			</td>
		</tr>
	</table>
    </form>
</body>
</html>
