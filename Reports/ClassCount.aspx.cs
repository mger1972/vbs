﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.Web;
using DevExpress.XtraReports.UI;
using System.Drawing.Printing;

namespace VBS.Reports
{
	public partial class ClassCount : System.Web.UI.Page
	{
		protected ClassCountReportType _type = ClassCountReportType.GradeCount;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			//Search_VBS_GradeCountCollection gradeCounts = new Search_VBS_GradeCountCollection(string.Empty);
			//gradeCounts.Sort(Search_VBS_GradeCount.FN_SortOrder + ", " + Search_VBS_GradeCount.FN_GradeInFall);

			//rptClassCount rpt = new rptClassCount();

			//rpt.DataSource = gradeCounts;
			//rpt.DisplayName = "Class Count";
			//rpt.CreateDocument();
			//documentViewer.Report = rpt;
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Figure out which type of report this is
			_type = ClassCountReportType.GradeCount;      // Set the default
			foreach (string key in Request.QueryString.AllKeys)
			{
				if (key.ToLower().Equals("type"))
				{
					// Get the value
					string val = Request.QueryString[key];
					if (val.ToLower().StartsWith("c")) { _type = ClassCountReportType.ClassCount; }
					//else if (val.ToLower().StartsWith("w")) { _type = NurseryReportType.Walker; }
					break;
				}
			}

			if (_type == ClassCountReportType.GradeCount)
			{
				// Run the grade count report
				Search_VBS_GradeCountCollection gradeCounts = new Search_VBS_GradeCountCollection(string.Empty);
				gradeCounts.Sort(Search_VBS_GradeCount.FN_SortOrder + ", " + Search_VBS_GradeCount.FN_GradeInFall);

				rptGradeCount rpt = new rptGradeCount();

				rpt.DataSource = gradeCounts;
				rpt.DisplayName = "Grade Count";

				int reportViewerWidth;
				if (Request["ReportViewerWidth"] != null &&
					Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
				{
					float reportWidth = rpt.PageWidth * 0.98f;
					float scaleFactor = reportViewerWidth / reportWidth;

					Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
						Convert.ToInt32(rpt.Margins.Right * scaleFactor),
						Convert.ToInt32(rpt.Margins.Top * scaleFactor),
						Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
					rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
					rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
					rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
					rpt.Margins = newMargins;
					rpt.CreateDocument();

					rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
				}

				documentViewer.Report = rpt;
			}
			else if (_type == ClassCountReportType.ClassCount)
			{
				// Run the class count report
				Search_VBS_ClassCodeCollection classCounts = new Search_VBS_ClassCodeCollection(string.Empty);
				classCounts.Sort(Search_VBS_ClassCode.FN_Length + ", " + Search_VBS_ClassCode.FN_ClassCode);

				//int lineNum = 1;
				//foreach (Search_VBS_ClassCode item in classCounts)
				//{
				//	item.GridCustom_4 = classCounts.SumOfTeacherCount.ToString("###,###,##0");
				//	item.GridCustom_5 = classCounts.SumOfJuniorHelperCount.ToString("###,###,##0");
				//	item.GridCustom_6 = classCounts.SumOfStudentCount.ToString("###,###,##0");

				//	item.GridCustom_8 = lineNum.ToString("###,##0") + ".";
				//	lineNum++;
				//}

				rptClassCount rpt = new rptClassCount();

				rpt.DataSource = classCounts;
				rpt.DisplayName = "Class Count";

				int reportViewerWidth;
				if (Request["ReportViewerWidth"] != null &&
					Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
				{
					float reportWidth = rpt.PageWidth * 0.98f;
					float scaleFactor = reportViewerWidth / reportWidth;

					Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
						Convert.ToInt32(rpt.Margins.Right * scaleFactor),
						Convert.ToInt32(rpt.Margins.Top * scaleFactor),
						Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
					rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
					rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
					rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
					rpt.Margins = newMargins;
					rpt.CreateDocument();

					rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
				}

				documentViewer.Report = rpt;
			}
		}
	}

	public enum ClassCountReportType : int
	{
		ClassCount = 0,
		GradeCount,
	}
}