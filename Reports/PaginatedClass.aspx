﻿<%@ Page Title="Paginated Class Report" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PaginatedClass.aspx.cs" Inherits="VBS.Reports.PaginatedClass" %>

<%@ Register Assembly="DevExpress.XtraReports.v14.2.Web, Version=14.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v14.2, Version=14.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>



<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Paginated Class Report</h1><br /><h5>A paginated class roster, split by classes.</h5>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script language="javascript">
		$(document).ready(function () {
			$("#MainContent_documentViewer_Splitter_0").css("background-color", "#FFFFFF");
			$("#MainContent_documentViewer_Splitter_0").css("border", "1px solid black");
			//$("#MainContent_documentViewer_Splitter_1i1_CC").css("background-color", "#FF0000");
			$("#MainContent_documentViewer_Splitter_1i1i0").css("border", "0px solid black");
			//$("#MainContent_documentViewer_Splitter_1i1").css("border", "3px solid black");		// Outside border
			//$("#MainContent_documentViewer_Splitter_1i1").css("border", "3px solid black");
			//$("#MainContent_documentViewer_Splitter_1i1i0_CC").css("style", "height:1px;width:1px;");
			})
	 </script>
	<dx:ASPxDocumentViewer Height="600px" ID="documentViewer" runat="server" EnableViewState="false" ToolbarMode="Ribbon" StylesSplitter-SidePaneWidth="60"></dx:ASPxDocumentViewer>
</asp:Content>


