﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraReports.Web;
using DevExpress.XtraReports.UI;
using System.Drawing.Printing;

namespace VBS.Reports
{
	public partial class HelperGradeViolation : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			if (Session["ShowAllViolation"] == null) { Session["ShowAllViolation"] = false; }
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			bool studentsChecked = true;
			if (Session["ShowAllViolation"] != null) { studentsChecked = (bool)Session["ShowAllViolation"]; }

			Search_VBS_GradeHelperViolationCollection violations = new Search_VBS_GradeHelperViolationCollection(string.Empty);
			if (!studentsChecked)
			{
				violations = new Search_VBS_GradeHelperViolationCollection("iGradeDiff IS NOT NULL AND iGradeDiff < 5");
			}
			violations.Sort(Search_VBS_GradeHelperViolation.FN_LastName + ", " + Search_VBS_GradeHelperViolation.FN_FirstName);

			rptHelperGradeViolation rpt = new rptHelperGradeViolation();
			
			rpt.DataSource = violations;
			rpt.DisplayName = "Jr. Helper Grade Violation";

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.96f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			documentViewer.Report = rpt;
		}

		protected void chkShowAll_CheckedChanged(object sender, EventArgs e)
		{
			// When the checkbox changes, reset the data
			Session["ShowAllViolation"] = chkShowAll.Checked;
		}
	}
}