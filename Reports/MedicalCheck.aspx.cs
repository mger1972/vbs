﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing.Printing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
    public partial class MedicalCheck : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login.aspx", true);
                return;
            }
            else
            {
                Session["UserName"] = HttpContext.Current.User.Identity.Name;
            }

            // Tell the parent who we belong to
            ((SiteMaster)this.Master).SetChildPageInstance(this);
        }

        protected void documentViewer_Init(object sender, EventArgs e)
        {
            ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

            Search_VBS_MedicalCheckCollection summary = new Search_VBS_MedicalCheckCollection("bMedicalReleaseForm = 0");
            summary.Sort(Search_VBS_MedicalCheck.FN_LastName + ", " +
                Search_VBS_MedicalCheck.FN_FirstName);

            rptMedicalCheck rpt = new rptMedicalCheck(summary.Count);
            rpt.DataSource = summary;
            rpt.DisplayName = "Medical Release Form Check";

            int reportViewerWidth;
            if (Request["ReportViewerWidth"] != null &&
                Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
            {
                float reportWidth = rpt.PageWidth * 0.98f;
                float scaleFactor = reportViewerWidth / reportWidth;

                Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Right * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Top * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
                rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
                rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
                rpt.Margins = newMargins;
                rpt.CreateDocument();

                rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
            }

            documentViewer.Report = rpt;
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            // Send out an email
            //Utils.SendEMailAsJen("Mark Gerlach", "mgerlach@shultzsteel.com", "test", "testing the email", string.Empty, Session["UserName"].ToString());
            Response.Redirect("MedicalCheckSend.aspx");
        }
    }
}