﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptAllergyReport : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptAllergyReport()
		{
			InitializeComponent();

			// Set the bindings
			lblClass.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_ClassCode);
			lblGrade.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_GradeInFall);

			lblStudentName.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_GridCustom_7);
			lblStudentName.DataBindings.Add("NavigateUrl", null, Search_VBS_Allergies.FN_GridCustom_5);
			lblStudentName.DataBindings.Add("Target", null, Search_VBS_Allergies.FN_GridCustom_6);

			lblTeacherNames.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_Teachers);
			lblParentName.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_GridCustom_9);
			lblParentName.DataBindings.Add("NavigateUrl", null, Search_VBS_Allergies.FN_EmailHyperlink);

			lblDateSubmitted.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_DateSubmitted);

			lblEMail.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_Email);
			lblEMail.DataBindings.Add("NavigateUrl", null, Search_VBS_Allergies.FN_EmailHyperlink);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_MobilePhone);

			lblAllergy.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_AllergyAndSpecialNeeds);
			lblRotation.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_GridCustom_8);

			//lblClass.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_GridCustom_7);
			//lblClass.DataBindings.Add("Bookmark", null, Search_VBS_Allergies.FN_GridCustom_9);

			//lblGrade.DataBindings.Add("Text", null, Search_VBS_Allergies.FN_GridCustom_9);
		}

		private void ghClassCode_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_Allergies.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_Allergies.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblParentName.Font = new Font(lblParentName.Font, FontStyle.Underline);
				lblParentName.ForeColor = Color.Blue;
				lblEMail.Font = new Font(lblParentName.Font, FontStyle.Underline);
				lblEMail.ForeColor = Color.Blue;
			}
			else
			{
				lblParentName.Font = new Font(lblParentName.Font, FontStyle.Regular);
				lblParentName.ForeColor = Color.Black;
				lblEMail.Font = new Font(lblParentName.Font, FontStyle.Regular);
				lblEMail.ForeColor = Color.Black;
			}

			string responseIDHyperlink = (GetCurrentColumnValue(Search_VBS_Allergies.FN_GridCustom_5) != null ?
				GetCurrentColumnValue(Search_VBS_Allergies.FN_GridCustom_5).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(responseIDHyperlink))
			{
				lblStudentName.Font = new Font(lblStudentName.Font, FontStyle.Underline);
				lblStudentName.ForeColor = Color.Blue;
			}
			else
			{
				lblStudentName.Font = new Font(lblStudentName.Font, FontStyle.Regular);
				lblStudentName.ForeColor = Color.Black;
			}
		}
	}
}
