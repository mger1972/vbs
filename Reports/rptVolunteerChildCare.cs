﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptVolunteerChildCare : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptVolunteerChildCare()
		{
			InitializeComponent();

			// Set the bindings
			lblName.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_VolunteerNameFormatted);
			lblName.DataBindings.Add("Bookmark", null, Search_VBS_VolunteerChild.FN_VolunteerNameFormatted);
			lblName.DataBindings.Add("NavigateUrl", null, Search_VBS_VolunteerChild.FN_EmailHyperlink);

			lblContactPhone.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_HomePhone);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_MobilePhone);
			lblEmail.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_Email);
			lblCurrentAssignment.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_Assignment);

			// Detail Fields
			lblChildName.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_ChildNameFirstLast);
			lblChildsAge.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_AgeOfChild);
			chkPottyTrained.DataBindings.Add("Checked", null, Search_VBS_VolunteerChild.FN_PottyTrained);
			lblAllergies.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_Allergies);

			// Bind the groupheader
			ghGroup.GroupFields.Add(new GroupField(Search_VBS_VolunteerChild.FN_LastName, XRColumnSortOrder.Ascending));
			ghGroup.GroupFields.Add(new GroupField(Search_VBS_VolunteerChild.FN_FirstName, XRColumnSortOrder.Ascending));
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			pnlDetail.BackColor = _detailBackColor;
			//_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);
		}

		private void ghGroup_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);
			pnlGroup.BackColor = _detailBackColor;

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_VolunteerChild.FN_EmailHyperlink) != null ? 
				GetCurrentColumnValue(Search_VBS_VolunteerChild.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblName.Font    = new Font(lblName.Font, FontStyle.Underline);
				lblName.ForeColor = Color.Blue;
			}
			else
			{
				lblName.Font = new Font(lblName.Font, FontStyle.Regular);
				lblName.ForeColor = Color.Black;
			}
		}
	}
}
