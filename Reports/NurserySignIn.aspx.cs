﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class NurserySignIn : System.Web.UI.Page
	{
		protected NurseryReportType _type = NurseryReportType.Nursery;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);
		}

		protected void documentViewer_Init(object sender, EventArgs e)
		{
			ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

			// Figure out which type of report this is
			_type = NurseryReportType.Nursery;      // Set the default
			foreach (string key in Request.QueryString.AllKeys)
			{
				if (key.ToLower().Equals("type"))
				{
					// Get the value
					string val = Request.QueryString[key];
					if (val.ToLower().StartsWith("p")) { _type = NurseryReportType.Preschool; }
					else if (val.ToLower().StartsWith("w")) { _type = NurseryReportType.Walker; }
					break;
				}
			}

			Dictionary<string, int> formIDs = Utils.FormIDs;

			Search_VBS_SignInSheetCollection children = new Search_VBS_SignInSheetCollection("sCurrentAssignment LIKE '" +
				_type.ToString().Substring(0, 1) + "%'"); ;
			children.Sort(Search_VBS_SignInSheet.FN_ChildNameLastFirst);

			// Add the line numbers
			int lineNum = 1;
			foreach (Search_VBS_SignInSheet item in children)
			{
				item.GridCustom_2 = lineNum.ToString("###,##0") + ".";
				lineNum++;
			}

			// Set up the names
			//foreach (Search_VBS_SignInSheet child in children)
			//{
			//	child.GridCustom_7 = "Rotation: " + (!String.IsNullOrEmpty(child.Rotation) ? child.Rotation : "< Not Assigned >");

			//	// Build out the link for the page to go back to myvbc
			//	child.GridCustom_5 = String.Format("https://voyagers.ccbchurch.com/form_response.php?response_id={0}&id={1}&redirect=%2Fform_response_list.php%3Fid%3D{1}",
			//		child.ResponseID,
			//		formIDs[child.SheetType]);
			//	child.GridCustom_6 = "_blank";
			//}

			string displayName = string.Empty, reportTitle = string.Empty;
			switch (_type)
			{
				case NurseryReportType.Nursery:
					displayName = "Nursery";
					reportTitle = "Nursery Roster";
					break;
				case NurseryReportType.Walker:
					displayName = "Walker";
					reportTitle = "Walker Roster";
					break;
				case NurseryReportType.Preschool:
					displayName = "Pre-school";
					reportTitle = "Pre-school Roster";
					break;
			}

			DateTime dtReport = DateTime.Parse("6/12/2017");
			rptSignInSheet rpt = new rptSignInSheet(reportTitle, dtReport);

			rpt.DataSource = children;
			rpt.DisplayName = displayName;

			int reportViewerWidth;
			if (Request["ReportViewerWidth"] != null &&
				Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
			{
				float reportWidth = rpt.PageWidth * 0.98f;
				float scaleFactor = reportViewerWidth / reportWidth;

				Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
					Convert.ToInt32(rpt.Margins.Right * scaleFactor),
					Convert.ToInt32(rpt.Margins.Top * scaleFactor),
					Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
				rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
				rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
				rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
				rpt.Margins = newMargins;
				rpt.CreateDocument();

				rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
			}

			// Run it four more times for the remaining days of the week
			for (int i = 1; i <= 4; i++)
			{
				dtReport = dtReport.AddDays(1);
				rptSignInSheet rptSub = new rptSignInSheet(reportTitle, dtReport);

				rptSub.DataSource = children;
				rptSub.DisplayName = displayName;

				if (Request["ReportViewerWidth"] != null &&
					Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
				{
					float reportWidth = rptSub.PageWidth * 0.98f;
					float scaleFactor = reportViewerWidth / reportWidth;

					Margins newMargins = new Margins(Convert.ToInt32(rptSub.Margins.Left * scaleFactor),
						Convert.ToInt32(rptSub.Margins.Right * scaleFactor),
						Convert.ToInt32(rptSub.Margins.Top * scaleFactor),
						Convert.ToInt32(rptSub.Margins.Bottom * scaleFactor));
					rptSub.PaperKind = System.Drawing.Printing.PaperKind.Custom;
					rptSub.PageWidth = (int)(rptSub.PageWidth * scaleFactor);
					rptSub.PageHeight = (int)(rptSub.PageHeight * scaleFactor);
					rptSub.Margins = newMargins;
					rptSub.CreateDocument();

					rptSub.PrintingSystem.Document.ScaleFactor = scaleFactor;
				}

				// Add this report to the base
				rpt.Pages.AddRange(rptSub.Pages);

				// Continiue the page numbering
				rpt.PrintingSystem.ContinuousPageNumbering = true;
			}

			documentViewer.Report = rpt;
		}
	}
}