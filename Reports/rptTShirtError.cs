﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
    public partial class rptTShirtError : DevExpress.XtraReports.UI.XtraReport
    {
        private Color _detailBackColor = Color.White;
		private int _runningCount = 0;

        public rptTShirtError()
        {
            InitializeComponent();

            // Set the bindings
            lblType.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_GridCustom_8);
            lblName.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_FullNameLastFirst);
            lblName.DataBindings.Add("NavigateUrl", null, Search_VBS_TShirtError.FN_GridCustom_5);
            lblName.DataBindings.Add("Target", null, Search_VBS_TShirtError.FN_GridCustom_6);

            lblEmail.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_Email);
            lblEmail.DataBindings.Add("NavigateUrl", null, Search_VBS_TShirtError.FN_EmailHyperlink);
            lblShirtSize.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_ShirtSize);

            lblComment.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_CommentExtended);
            //lblEmail.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_GridCustom_9);
            //lblEmail.DataBindings.Add("NavigateUrl", null, Search_VBS_TShirtError.FN_EmailHyperlink);

            //lblShirtSize.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_HomePhone);
            //lblComment.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_MobilePhone);

            //lblAllergy.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_AllergyAndSpecialNeeds);
            //lblRotation.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_GridCustom_8);

            lblReason.DataBindings.Add("Text", null, Search_VBS_TShirtError.FN_GridCustom_7);

            // Bind the group header
            ghReason.GroupFields.Add(new GroupField(Search_VBS_TShirtError.FN_GridCustom_7, XRColumnSortOrder.Ascending));
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
			// Increment the count
			_runningCount++;

            xrPanel1.BackColor = _detailBackColor;
            _detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

            string emailHyperlink = (GetCurrentColumnValue(Search_VBS_TShirtError.FN_EmailHyperlink) != null ?
                GetCurrentColumnValue(Search_VBS_TShirtError.FN_EmailHyperlink).ToString() : string.Empty);
            if (!String.IsNullOrEmpty(emailHyperlink))
            {
                lblEmail.Font = new Font(lblEmail.Font, FontStyle.Underline);
                lblEmail.ForeColor = Color.Blue;
            }
            else
            {
                lblEmail.Font = new Font(lblEmail.Font, FontStyle.Regular);
                lblEmail.ForeColor = Color.Black;
            }

            string responseIDHyperlink = (GetCurrentColumnValue(Search_VBS_TShirtError.FN_GridCustom_5) != null ?
                GetCurrentColumnValue(Search_VBS_TShirtError.FN_GridCustom_5).ToString() : string.Empty);
            if (!String.IsNullOrEmpty(responseIDHyperlink))
            {
                lblName.Font = new Font(lblName.Font, FontStyle.Underline);
                lblName.ForeColor = Color.Blue;
            }
            else
            {
                lblName.Font = new Font(lblName.Font, FontStyle.Regular);
                lblName.ForeColor = Color.Black;
            }
        }

		private void gfReason_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			lblTotalCount.Text = String.Format("Total Count: {0:###,##0}", _runningCount);
			_runningCount = 0;		// Reset the count
		}
	}
}
