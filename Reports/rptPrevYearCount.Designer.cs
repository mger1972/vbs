﻿namespace VBS.Reports
{
    partial class rptPrevYearCount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPrevYearCount));
			this.lblSubType = new DevExpress.XtraReports.UI.XRLabel();
			this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYear1Value = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYear2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYear0Value = new DevExpress.XtraReports.UI.XRLabel();
			this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
			this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.xrPageInfo2 = new DevExpress.XtraReports.UI.XRPageInfo();
			this.picHeader = new DevExpress.XtraReports.UI.XRPictureBox();
			this.Detail = new DevExpress.XtraReports.UI.DetailBand();
			this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
			this.lblYear2Value = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYear0 = new DevExpress.XtraReports.UI.XRLabel();
			this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
			this.lblYear1 = new DevExpress.XtraReports.UI.XRLabel();
			this.rptTitle = new DevExpress.XtraReports.UI.XRLabel();
			this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
			this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
			this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
			this.ghSortOrder = new DevExpress.XtraReports.UI.GroupHeaderBand();
			this.lblType = new DevExpress.XtraReports.UI.XRLabel();
			this.gfSortOrder = new DevExpress.XtraReports.UI.GroupFooterBand();
			this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYear3 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYear4 = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYear3Value = new DevExpress.XtraReports.UI.XRLabel();
			this.lblYear4Value = new DevExpress.XtraReports.UI.XRLabel();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// lblSubType
			// 
			this.lblSubType.Font = new System.Drawing.Font("Verdana", 10F);
			this.lblSubType.LocationFloat = new DevExpress.Utils.PointFloat(79.37444F, 0F);
			this.lblSubType.Name = "lblSubType";
			this.lblSubType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblSubType.SizeF = new System.Drawing.SizeF(278.125F, 20F);
			this.lblSubType.StylePriority.UseFont = false;
			this.lblSubType.StylePriority.UseTextAlignment = false;
			this.lblSubType.Text = "714-832-8664";
			this.lblSubType.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// xrLabel1
			// 
			this.xrLabel1.BackColor = System.Drawing.Color.White;
			this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.xrLabel1.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
			this.xrLabel1.ForeColor = System.Drawing.Color.Navy;
			this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(99.79115F, 75.83338F);
			this.xrLabel1.Name = "xrLabel1";
			this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel1.SizeF = new System.Drawing.SizeF(278.125F, 23.33333F);
			this.xrLabel1.StylePriority.UseBackColor = false;
			this.xrLabel1.StylePriority.UseBorders = false;
			this.xrLabel1.StylePriority.UseFont = false;
			this.xrLabel1.StylePriority.UseForeColor = false;
			this.xrLabel1.StylePriority.UseTextAlignment = false;
			this.xrLabel1.Text = "Type";
			this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// lblYear1Value
			// 
			this.lblYear1Value.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYear1Value.LocationFloat = new DevExpress.Utils.PointFloat(525.0001F, 0F);
			this.lblYear1Value.Name = "lblYear1Value";
			this.lblYear1Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear1Value.SizeF = new System.Drawing.SizeF(108.5417F, 15F);
			this.lblYear1Value.StylePriority.UseFont = false;
			this.lblYear1Value.StylePriority.UseTextAlignment = false;
			this.lblYear1Value.Text = "714-832-8664";
			this.lblYear1Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblYear2
			// 
			this.lblYear2.BackColor = System.Drawing.Color.White;
			this.lblYear2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblYear2.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
			this.lblYear2.ForeColor = System.Drawing.Color.Navy;
			this.lblYear2.LocationFloat = new DevExpress.Utils.PointFloat(653.9584F, 75.83338F);
			this.lblYear2.Name = "lblYear2";
			this.lblYear2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear2.SizeF = new System.Drawing.SizeF(111.0416F, 23.33333F);
			this.lblYear2.StylePriority.UseBackColor = false;
			this.lblYear2.StylePriority.UseBorders = false;
			this.lblYear2.StylePriority.UseFont = false;
			this.lblYear2.StylePriority.UseForeColor = false;
			this.lblYear2.StylePriority.UseTextAlignment = false;
			this.lblYear2.Text = "Potty Trained";
			this.lblYear2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// lblYear0Value
			// 
			this.lblYear0Value.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYear0Value.LocationFloat = new DevExpress.Utils.PointFloat(416.4578F, 0F);
			this.lblYear0Value.Name = "lblYear0Value";
			this.lblYear0Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear0Value.SizeF = new System.Drawing.SizeF(108.5417F, 15F);
			this.lblYear0Value.StylePriority.UseFont = false;
			this.lblYear0Value.StylePriority.UseTextAlignment = false;
			this.lblYear0Value.Text = "714-832-8664";
			this.lblYear0Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// ReportHeader
			// 
			this.ReportHeader.HeightF = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// xrPageInfo1
			// 
			this.xrPageInfo1.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo1.Format = "Page {0} of {1}";
			this.xrPageInfo1.LocationFloat = new DevExpress.Utils.PointFloat(811.2501F, 10.00001F);
			this.xrPageInfo1.Name = "xrPageInfo1";
			this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo1.SizeF = new System.Drawing.SizeF(178.7499F, 23F);
			this.xrPageInfo1.StylePriority.UseFont = false;
			this.xrPageInfo1.StylePriority.UseTextAlignment = false;
			this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
			// 
			// xrPageInfo2
			// 
			this.xrPageInfo2.AnchorVertical = DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom;
			this.xrPageInfo2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.xrPageInfo2.Format = "Printed on {0:MM/dd/yyyy} at {0:hh:mm:ss tt}";
			this.xrPageInfo2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 10.00001F);
			this.xrPageInfo2.Name = "xrPageInfo2";
			this.xrPageInfo2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrPageInfo2.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime;
			this.xrPageInfo2.SizeF = new System.Drawing.SizeF(289.1666F, 23F);
			this.xrPageInfo2.StylePriority.UseFont = false;
			this.xrPageInfo2.StylePriority.UseTextAlignment = false;
			this.xrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
			// 
			// picHeader
			// 
			this.picHeader.Image = ((System.Drawing.Image)(resources.GetObject("picHeader.Image")));
			this.picHeader.LocationFloat = new DevExpress.Utils.PointFloat(619.7917F, 0F);
			this.picHeader.Name = "picHeader";
			this.picHeader.SizeF = new System.Drawing.SizeF(380.2083F, 56.25F);
			this.picHeader.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1});
			this.Detail.HeightF = 20.625F;
			this.Detail.Name = "Detail";
			this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			this.Detail.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.Detail_BeforePrint);
			// 
			// xrPanel1
			// 
			this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblYear4Value,
            this.lblYear3Value,
            this.lblYear2Value,
            this.lblYear0Value,
            this.lblYear1Value,
            this.lblSubType});
			this.xrPanel1.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.xrPanel1.Name = "xrPanel1";
			this.xrPanel1.SizeF = new System.Drawing.SizeF(966.6665F, 20.625F);
			// 
			// lblYear2Value
			// 
			this.lblYear2Value.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYear2Value.LocationFloat = new DevExpress.Utils.PointFloat(633.5417F, 0F);
			this.lblYear2Value.Name = "lblYear2Value";
			this.lblYear2Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear2Value.SizeF = new System.Drawing.SizeF(111.0415F, 15F);
			this.lblYear2Value.StylePriority.UseFont = false;
			this.lblYear2Value.StylePriority.UseTextAlignment = false;
			this.lblYear2Value.Text = "714-832-8664";
			this.lblYear2Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblYear0
			// 
			this.lblYear0.BackColor = System.Drawing.Color.White;
			this.lblYear0.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblYear0.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
			this.lblYear0.ForeColor = System.Drawing.Color.Navy;
			this.lblYear0.LocationFloat = new DevExpress.Utils.PointFloat(436.8745F, 75.83338F);
			this.lblYear0.Name = "lblYear0";
			this.lblYear0.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear0.SizeF = new System.Drawing.SizeF(108.5417F, 23.33333F);
			this.lblYear0.StylePriority.UseBackColor = false;
			this.lblYear0.StylePriority.UseBorders = false;
			this.lblYear0.StylePriority.UseFont = false;
			this.lblYear0.StylePriority.UseForeColor = false;
			this.lblYear0.StylePriority.UseTextAlignment = false;
			this.lblYear0.Text = "Home Phone";
			this.lblYear0.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblYear4,
            this.lblYear3,
            this.lblYear0,
            this.xrLabel1,
            this.lblYear2,
            this.lblYear1,
            this.rptTitle,
            this.picHeader});
			this.PageHeader.HeightF = 99.16671F;
			this.PageHeader.Name = "PageHeader";
			// 
			// lblYear1
			// 
			this.lblYear1.BackColor = System.Drawing.Color.White;
			this.lblYear1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblYear1.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
			this.lblYear1.ForeColor = System.Drawing.Color.Navy;
			this.lblYear1.LocationFloat = new DevExpress.Utils.PointFloat(545.4167F, 75.83338F);
			this.lblYear1.Name = "lblYear1";
			this.lblYear1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear1.SizeF = new System.Drawing.SizeF(108.5417F, 23.33333F);
			this.lblYear1.StylePriority.UseBackColor = false;
			this.lblYear1.StylePriority.UseBorders = false;
			this.lblYear1.StylePriority.UseFont = false;
			this.lblYear1.StylePriority.UseForeColor = false;
			this.lblYear1.StylePriority.UseTextAlignment = false;
			this.lblYear1.Text = "Mobile Phone";
			this.lblYear1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// rptTitle
			// 
			this.rptTitle.Font = new System.Drawing.Font("Verdana", 16F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.rptTitle.ForeColor = System.Drawing.Color.Blue;
			this.rptTitle.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
			this.rptTitle.Name = "rptTitle";
			this.rptTitle.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.rptTitle.SizeF = new System.Drawing.SizeF(470.8333F, 31.33333F);
			this.rptTitle.StylePriority.UseFont = false;
			this.rptTitle.StylePriority.UseForeColor = false;
			this.rptTitle.Text = "Previous Year Counts";
			// 
			// BottomMargin
			// 
			this.BottomMargin.HeightF = 50F;
			this.BottomMargin.Name = "BottomMargin";
			this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo2,
            this.xrPageInfo1});
			this.PageFooter.HeightF = 33.00001F;
			this.PageFooter.Name = "PageFooter";
			// 
			// TopMargin
			// 
			this.TopMargin.HeightF = 50F;
			this.TopMargin.Name = "TopMargin";
			this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
			this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// ghSortOrder
			// 
			this.ghSortOrder.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblType});
			this.ghSortOrder.HeightF = 22.29167F;
			this.ghSortOrder.KeepTogether = true;
			this.ghSortOrder.Name = "ghSortOrder";
			this.ghSortOrder.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.ghSortOrder_BeforePrint);
			// 
			// lblType
			// 
			this.lblType.BackColor = System.Drawing.Color.Navy;
			this.lblType.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
			this.lblType.ForeColor = System.Drawing.Color.White;
			this.lblType.LocationFloat = new DevExpress.Utils.PointFloat(20.41667F, 0F);
			this.lblType.Name = "lblType";
			this.lblType.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblType.SizeF = new System.Drawing.SizeF(966.6665F, 22.29167F);
			this.lblType.StylePriority.UseBackColor = false;
			this.lblType.StylePriority.UseFont = false;
			this.lblType.StylePriority.UseForeColor = false;
			this.lblType.Text = "Kindergarten";
			// 
			// gfSortOrder
			// 
			this.gfSortOrder.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2});
			this.gfSortOrder.HeightF = 30.00001F;
			this.gfSortOrder.Name = "gfSortOrder";
			// 
			// xrLabel2
			// 
			this.xrLabel2.Font = new System.Drawing.Font("Verdana", 10F);
			this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(375.8335F, 10.00001F);
			this.xrLabel2.Name = "xrLabel2";
			this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.xrLabel2.SizeF = new System.Drawing.SizeF(278.125F, 20F);
			this.xrLabel2.StylePriority.UseFont = false;
			this.xrLabel2.StylePriority.UseTextAlignment = false;
			this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
			// 
			// lblYear3
			// 
			this.lblYear3.BackColor = System.Drawing.Color.White;
			this.lblYear3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblYear3.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
			this.lblYear3.ForeColor = System.Drawing.Color.Navy;
			this.lblYear3.LocationFloat = new DevExpress.Utils.PointFloat(765F, 75.83338F);
			this.lblYear3.Name = "lblYear3";
			this.lblYear3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear3.SizeF = new System.Drawing.SizeF(111.0416F, 23.33333F);
			this.lblYear3.StylePriority.UseBackColor = false;
			this.lblYear3.StylePriority.UseBorders = false;
			this.lblYear3.StylePriority.UseFont = false;
			this.lblYear3.StylePriority.UseForeColor = false;
			this.lblYear3.StylePriority.UseTextAlignment = false;
			this.lblYear3.Text = "Potty Trained";
			this.lblYear3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// lblYear4
			// 
			this.lblYear4.BackColor = System.Drawing.Color.White;
			this.lblYear4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top) 
            | DevExpress.XtraPrinting.BorderSide.Right) 
            | DevExpress.XtraPrinting.BorderSide.Bottom)));
			this.lblYear4.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
			this.lblYear4.ForeColor = System.Drawing.Color.Navy;
			this.lblYear4.LocationFloat = new DevExpress.Utils.PointFloat(876.0416F, 75.83338F);
			this.lblYear4.Name = "lblYear4";
			this.lblYear4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear4.SizeF = new System.Drawing.SizeF(111.0416F, 23.33333F);
			this.lblYear4.StylePriority.UseBackColor = false;
			this.lblYear4.StylePriority.UseBorders = false;
			this.lblYear4.StylePriority.UseFont = false;
			this.lblYear4.StylePriority.UseForeColor = false;
			this.lblYear4.StylePriority.UseTextAlignment = false;
			this.lblYear4.Text = "Potty Trained";
			this.lblYear4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
			// 
			// lblYear3Value
			// 
			this.lblYear3Value.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYear3Value.LocationFloat = new DevExpress.Utils.PointFloat(744.5834F, 0F);
			this.lblYear3Value.Name = "lblYear3Value";
			this.lblYear3Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear3Value.SizeF = new System.Drawing.SizeF(111.0415F, 15F);
			this.lblYear3Value.StylePriority.UseFont = false;
			this.lblYear3Value.StylePriority.UseTextAlignment = false;
			this.lblYear3Value.Text = "714-832-8664";
			this.lblYear3Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// lblYear4Value
			// 
			this.lblYear4Value.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblYear4Value.LocationFloat = new DevExpress.Utils.PointFloat(855.6249F, 0F);
			this.lblYear4Value.Name = "lblYear4Value";
			this.lblYear4Value.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
			this.lblYear4Value.SizeF = new System.Drawing.SizeF(111.0415F, 15F);
			this.lblYear4Value.StylePriority.UseFont = false;
			this.lblYear4Value.StylePriority.UseTextAlignment = false;
			this.lblYear4Value.Text = "714-832-8664";
			this.lblYear4Value.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
			// 
			// rptPrevYearCount
			// 
			this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.PageHeader,
            this.PageFooter,
            this.ghSortOrder,
            this.gfSortOrder});
			this.Landscape = true;
			this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
			this.PageHeight = 850;
			this.PageWidth = 1100;
			this.Version = "17.1";
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRLabel lblSubType;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblYear1Value;
        private DevExpress.XtraReports.UI.XRLabel lblYear2;
        private DevExpress.XtraReports.UI.XRLabel lblYear0Value;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo2;
        private DevExpress.XtraReports.UI.XRPictureBox picHeader;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRLabel lblYear2Value;
        private DevExpress.XtraReports.UI.XRLabel lblYear0;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel lblYear1;
        private DevExpress.XtraReports.UI.XRLabel rptTitle;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.GroupHeaderBand ghSortOrder;
        private DevExpress.XtraReports.UI.XRLabel lblType;
        private DevExpress.XtraReports.UI.GroupFooterBand gfSortOrder;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
		private DevExpress.XtraReports.UI.XRLabel lblYear4Value;
		private DevExpress.XtraReports.UI.XRLabel lblYear3Value;
		private DevExpress.XtraReports.UI.XRLabel lblYear4;
		private DevExpress.XtraReports.UI.XRLabel lblYear3;
	}
}
