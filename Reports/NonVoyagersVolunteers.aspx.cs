﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
    public partial class NonVoyagersVolunteers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login.aspx", true);
                return;
            }
            else
            {
                Session["UserName"] = HttpContext.Current.User.Identity.Name;
            }

            // Tell the parent who we belong to
            ((SiteMaster)this.Master).SetChildPageInstance(this);
        }

        protected void documentViewer_Init(object sender, EventArgs e)
        {
            ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

            Search_VBS_VolunteerCollection vol = new Search_VBS_VolunteerCollection("bRegVoyagersAttendee = 0");
            vol.Sort(Search_VBS_Volunteer.FN_FullNameLastFirstUpper);

            rptNonVoyagersVolunteers rpt = new rptNonVoyagersVolunteers(vol.Count);

            rpt.DataSource = vol;
            rpt.DisplayName = "Non-Voyagers Volunteers";

            int reportViewerWidth;
            if (Request["ReportViewerWidth"] != null &&
                Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
            {
                float reportWidth = rpt.PageWidth * 0.98f;
                float scaleFactor = reportViewerWidth / reportWidth;

                Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Right * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Top * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
                rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
                rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
                rpt.Margins = newMargins;
                rpt.CreateDocument();

                rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
            }

            documentViewer.Report = rpt;
        }
    }
}