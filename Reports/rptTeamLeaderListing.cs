﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptTeamLeaderListing : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptTeamLeaderListing()
		{
			InitializeComponent();

			// Set the bindings
			lblAdult.DataBindings.Add("Text", null, Search_VBS_TeamLeaderList.FN_GridCustom_5);
			lblAdult.DataBindings.Add("Bookmark", null, Search_VBS_TeamLeaderList.FN_GridCustom_5);
			//lblLastName.DataBindings.Add("NavigateUrl", null, Search_VBS_TeamLeaderList.FN_EmailHyperlink);

			// Detail Fields
			lblStudentName.DataBindings.Add("Text", null, Search_VBS_TeamLeaderList.FN_VolunteerNameFormatted);
			
			lblContactPhone.DataBindings.Add("Text", null, Search_VBS_TeamLeaderList.FN_HomePhone);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_TeamLeaderList.FN_MobilePhone);
			lblEmail.DataBindings.Add("Text", null, Search_VBS_TeamLeaderList.FN_Email);
			lblEmail.DataBindings.Add("NavigateUrl", null, Search_VBS_TeamLeaderList.FN_EmailHyperlink);
			lblRequest.DataBindings.Add("Text", null, Search_VBS_TeamLeaderList.FN_GridCustom_4);

			// Bind the groupheader
			ghGroup.GroupFields.Add(new GroupField(Search_VBS_TeamLeaderList.FN_IsAdult, XRColumnSortOrder.Descending));

			this.Detail.SortFields.Add(new GroupField(Search_VBS_TeamLeaderList.FN_LastName, XRColumnSortOrder.Ascending));
			this.Detail.SortFields.Add(new GroupField(Search_VBS_TeamLeaderList.FN_FirstName, XRColumnSortOrder.Ascending));
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			
			pnlColor.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_TeamLeaderList.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_TeamLeaderList.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblStudentName.Font = new Font(lblStudentName.Font, FontStyle.Underline);
				lblStudentName.ForeColor = Color.Blue;
			}
			else
			{
				lblStudentName.Font = new Font(lblStudentName.Font, FontStyle.Regular);
				lblStudentName.ForeColor = Color.Black;
			}
		}
	}
}
