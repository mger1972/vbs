﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace VBS.Reports
{
    public partial class rptPrevYearCount : DevExpress.XtraReports.UI.XtraReport
    {
        private Color _detailBackColor = Color.AliceBlue;

        public rptPrevYearCount()
        {
            InitializeComponent();
        }

        public rptPrevYearCount(DataTable dt)
        {
            InitializeComponent();

            // Set the bindings
            lblType.DataBindings.Add("Text", null, "Desc");
            lblSubType.DataBindings.Add("Text", null, "sExtDesc");

            lblYear0Value.DataBindings.Add("Text", null, dt.Columns[4].ColumnName);
            lblYear1Value.DataBindings.Add("Text", null, dt.Columns[5].ColumnName);
            lblYear2Value.DataBindings.Add("Text", null, dt.Columns[6].ColumnName);
            lblYear3Value.DataBindings.Add("Text", null, dt.Columns[7].ColumnName);
            lblYear4Value.DataBindings.Add("Text", null, dt.Columns[8].ColumnName);

            // Set the labels
            lblYear0.Text = dt.Columns[4].ColumnName.Substring(6);
            lblYear1.Text = dt.Columns[5].ColumnName.Substring(6);
            lblYear2.Text = dt.Columns[6].ColumnName.Substring(6);
            lblYear3.Text = dt.Columns[7].ColumnName.Substring(6);
            lblYear4.Text = dt.Columns[8].ColumnName.Substring(6);

            // Bind the group header
            ghSortOrder.GroupFields.Add(new GroupField("iSortOrder", XRColumnSortOrder.Ascending));
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrPanel1.BackColor = _detailBackColor;
            _detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

            //string emailHyperlink = (GetCurrentColumnValue(Search_VBS_TShirtError.FN_EmailHyperlink) != null ?
            //    GetCurrentColumnValue(Search_VBS_TShirtError.FN_EmailHyperlink).ToString() : string.Empty);
            //if (!String.IsNullOrEmpty(emailHyperlink))
            //{
            //    lblEmail.Font = new Font(lblEmail.Font, FontStyle.Underline);
            //    lblEmail.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lblEmail.Font = new Font(lblEmail.Font, FontStyle.Regular);
            //    lblEmail.ForeColor = Color.Black;
            //}

            //string responseIDHyperlink = (GetCurrentColumnValue(Search_VBS_TShirtError.FN_GridCustom_5) != null ?
            //    GetCurrentColumnValue(Search_VBS_TShirtError.FN_GridCustom_5).ToString() : string.Empty);
            //if (!String.IsNullOrEmpty(responseIDHyperlink))
            //{
            //    lblName.Font = new Font(lblName.Font, FontStyle.Underline);
            //    lblName.ForeColor = Color.Blue;
            //}
            //else
            //{
            //    lblName.Font = new Font(lblName.Font, FontStyle.Regular);
            //    lblName.ForeColor = Color.Black;
            //}
        }

        private void ghSortOrder_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            _detailBackColor = Color.AliceBlue;
        }
    }
}
