﻿using DevExpress.XtraReports.Web;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
    public partial class MedicalComparison : System.Web.UI.Page
    {
        protected MedicalComparisonReportType _type = MedicalComparisonReportType.Matched;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login.aspx", true);
                return;
            }
            else
            {
                Session["UserName"] = HttpContext.Current.User.Identity.Name;
            }

            // Tell the parent who we belong to
            ((SiteMaster)this.Master).SetChildPageInstance(this);

            // Set the val type 
            lblDesc.Text = "This list includes all records where the name is matched between a registration (reg. or alt. pay) in MyVBC and a MyVBC medical release. The checkbox shows if the \"Medical Release Form\" box is checked in MyVBC.";
            foreach (string key in Request.QueryString.AllKeys)
            {
                if (key.ToLower().Equals("type"))
                {
                    // Get the value
                    string val = Request.QueryString[key];
                    if (val.ToLower().Contains("medicalnotchecked")) { lblDesc.Text = "This list includes all records where the name is matched between a registration and an entered medical form, but the \"Medical Release Form\" box isn't checked in MyVBC."; }
                    else if (val.ToLower().Contains("studentnomed")) { lblDesc.Text = "This list includes records where the MyVBC registration doesn't match any medical on file.  The checkbox shows if the \"Medical Release Form\" box is checked in MyVBC."; }
                    else if (val.ToLower().Contains("medicalnostu")) { lblDesc.Text = "This list includes medical records where a registration for Sonrise Island hasn't been created yet."; }
                    break;
                }
            }
        }

        protected void documentViewer_Init(object sender, EventArgs e)
        {
            ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

            // Figure out which type of report this is
            _type = MedicalComparisonReportType.Matched;      // Set the default
            foreach (string key in Request.QueryString.AllKeys)
            {
                if (key.ToLower().Equals("type"))
                {
                    // Get the value
                    string val = Request.QueryString[key];
                    if (val.ToLower().Contains("medicalnotchecked")) { _type = MedicalComparisonReportType.MatchedMedicalNotChecked; }
                    else if (val.ToLower().Contains("studentnomed")) { _type = MedicalComparisonReportType.StudentNoMedical; }
                    else if (val.ToLower().Contains("medicalnostu")) { _type = MedicalComparisonReportType.MedicalNoStudent; }
                    break;
                }
            }

            Search_VBS_MedicalComparisonCollection coll = null;

            //Search_VBS_VolunteerChildCollection children = new Search_VBS_VolunteerChildCollection("sCurrentAssignment LIKE '" +
            //    _type.ToString().Substring(0, 1) + "%'"); ;
            //children.Sort(Search_VBS_VolunteerChild.FN_ChildNameLastFirst);

            //// Set up the names
            //foreach (Search_VBS_VolunteerChild child in children)
            //{
            //    child.GridCustom_7 = "Rotation: " + (!String.IsNullOrEmpty(child.Rotation) ? child.Rotation : "< Not Assigned >");

            //    // Build out the link for the page to go back to myvbc
            //    child.GridCustom_5 = String.Format("https://voyagers.ccbchurch.com/form_response.php?response_id={0}&id={1}&redirect=%2Fform_response_list.php%3Fid%3D{1}",
            //        child.ResponseID,
            //        formIDs[child.SheetType]);
            //    child.GridCustom_6 = "_blank";
            //}

            string displayName = string.Empty, reportTitle = string.Empty;
            switch (_type)
            {
                case MedicalComparisonReportType.Matched:
                    displayName = "Matched";
                    reportTitle = "Matched Records";
                    coll = new Search_VBS_MedicalComparisonCollection("sStatus = 'Matching'");
                    break;
                case MedicalComparisonReportType.MatchedMedicalNotChecked:
                    displayName = "Matched - Medical Not Checked";
                    reportTitle = "Matched - Medical Not Checked";
                    coll = new Search_VBS_MedicalComparisonCollection("sStatus = 'Matching' AND bMedicalReleaseForm = 0");
                    break;
                case MedicalComparisonReportType.StudentNoMedical:
                    displayName = "Students without a Medical Release";
                    reportTitle = "Students without a Medical Release";
                    coll = new Search_VBS_MedicalComparisonCollection("sStatus = 'StudentNoMedical'");
                    break;
                case MedicalComparisonReportType.MedicalNoStudent:
                    displayName = "Medical Records with No Student";
                    reportTitle = "Medical Records with No Student";
                    coll = new Search_VBS_MedicalComparisonCollection("sStatus = 'MedicalNoStudent'");
                    break;
            }

			// Update the link
			foreach (Search_VBS_MedicalComparison child in coll)
			{
				//if (child.FormID.HasValue)
				//{
				//child.GridCustom_5 = String.Format("https://voyagers.ccbchurch.com/form_response.php?response_id={0}&id={1}&redirect=%2Fform_response_list.php%3Fid%3D{1}",
				//	child.ResponseID,
				//	child.FormID);
				//child.GridCustom_5 = String.Format("https://voyagers.ccbchurch.com/form_response.php?response_id={0}&id={1}&redirect=%2Fform_response_list.php%3Fid%3D{1}",
				//	child.ChildResponseID,
				//	"604");		// 2017 form
				child.GridCustom_5 = String.Format("https://voyagers.ccbchurch.com/form_response.php?response_id={0}&id={1}&redirect=%2Fform_response_list.php%3Fid%3D{1}",
					child.ChildResponseID,
					Utils.FormIDs["Base"]);
					//"708");
				child.GridCustom_6 = "_blank";
				//}
			}

            rptMedicalComparison rpt = new rptMedicalComparison(reportTitle, coll.Count);

            // Sort the collection
            //coll.Sort(Search_VBS_MedicalComparison.FN_StatusOrder + ", " + 
            //    Search_VBS_MedicalComparison.FN_Type + ", " +
            //    Search_VBS_MedicalComparison.FN_LastName + ", " +
            //    Search_VBS_MedicalComparison.FN_FirstName);   // Old way

            coll.Sort(Search_VBS_MedicalComparison.FN_StatusOrder + ", " +
                Search_VBS_MedicalComparison.FN_LastName + ", " +
                Search_VBS_MedicalComparison.FN_FirstName + ", " +
                Search_VBS_MedicalComparison.FN_Type);

            rpt.DataSource = coll;
            rpt.DisplayName = displayName;

            int reportViewerWidth;
            if (Request["ReportViewerWidth"] != null &&
                Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
            {
                float reportWidth = rpt.PageWidth * 0.98f;
                float scaleFactor = reportViewerWidth / reportWidth;

                Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Right * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Top * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
                rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
                rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
                rpt.Margins = newMargins;
                rpt.CreateDocument();

                rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
            }

            documentViewer.Report = rpt;
        }
    }

    public enum MedicalComparisonReportType : int
    {
        Matched = 0,
        MatchedMedicalNotChecked,
        StudentNoMedical,
        MedicalNoStudent,
    }
}