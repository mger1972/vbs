﻿using DevExpress.XtraReports.Web;
using System.Drawing.Printing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
    public partial class TShirtMailingLabels : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("/Account/Login.aspx", true);
                return;
            }
            else
            {
                Session["UserName"] = HttpContext.Current.User.Identity.Name;
            }

            // Tell the parent who we belong to
            ((SiteMaster)this.Master).SetChildPageInstance(this);

            if (Session["ShowStudents"] == null) { Session["ShowStudents"] = true; }
        }

        protected void documentViewer_Init(object sender, EventArgs e)
        {
            ASPxDocumentViewer documentViewer = sender as ASPxDocumentViewer;

            Search_VBS_TShirtListCollection shirts = new Search_VBS_TShirtListCollection(string.Empty);
            
			// Add the kids T-shirts as well
			Search_VBS_VolunteerChildCollection kidShirts = new 
				Search_VBS_VolunteerChildCollection("sChildShirtSize IS NOT NULL AND LTRIM(RTRIM(sChildShirtSize)) <> ''");
			foreach (Search_VBS_VolunteerChild ch in kidShirts)
			{
				Search_VBS_TShirtList item = new VBS.Search_VBS_TShirtList();
				item.RecGUID = System.Guid.NewGuid().ToString();
				item.RawGUID = ch.DetailGUID;
				item.ResponseID = ch.ResponseID;
				item.FirstName = ch.ChildFirstName;
				item.LastName = ch.ChildLastName;
				item.Size = ch.ChildShirtSize;
				item.ClassCode = "Pre-School";
				item.GroupName = "Pre-School";
				item.Length = 99;
				shirts.Add(item);
			}

			shirts.Sort(Search_VBS_TShirtList.FN_ShirtSort + ", " +
				Search_VBS_TShirtList.FN_Length + ", " +
				Search_VBS_TShirtList.FN_ClassCode + ", " +
				Search_VBS_TShirtList.FN_GroupName + ", " +
				Search_VBS_TShirtList.FN_LastName + ", " +
				Search_VBS_TShirtList.FN_FirstName);

			rptTShirtMailingLabels rpt = new rptTShirtMailingLabels();

            rpt.DataSource = shirts;
            rpt.DisplayName = "T-Shirt Mailing Labels";

            int reportViewerWidth;
            if (Request["ReportViewerWidth"] != null &&
                Int32.TryParse((string)Request["ReportViewerWidth"], out reportViewerWidth))
            {
                float reportWidth = rpt.PageWidth * 0.98f;
                float scaleFactor = reportViewerWidth / reportWidth;

                Margins newMargins = new Margins(Convert.ToInt32(rpt.Margins.Left * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Right * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Top * scaleFactor),
                    Convert.ToInt32(rpt.Margins.Bottom * scaleFactor));
                rpt.PaperKind = System.Drawing.Printing.PaperKind.Custom;
                rpt.PageWidth = (int)(rpt.PageWidth * scaleFactor);
                rpt.PageHeight = (int)(rpt.PageHeight * scaleFactor);
                rpt.Margins = newMargins;
                rpt.CreateDocument();

                rpt.PrintingSystem.Document.ScaleFactor = scaleFactor;
            }

            documentViewer.Report = rpt;
        }
    }
}