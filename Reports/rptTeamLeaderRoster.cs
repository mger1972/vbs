﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using DevExpress.XtraReports.Parameters;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.Web;


namespace VBS.Reports
{
	public partial class rptTeamLeaderRoster : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptTeamLeaderRoster()
		{
			InitializeComponent();

			// Set the bindings
			lblLastName.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ClassCodeWithDesc);
			lblLastName.DataBindings.Add("Bookmark", null, Search_VBS_ClassAssignment.FN_GridCustom_1);
			//lblLastName.DataBindings.Add("NavigateUrl", null, Search_VBS_ClassAssignment.FN_EmailHyperlink);

			lblRotation.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_Rotation);
			lblTeamColor.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ClassColor);

			// Detail Fields
			lblStudentName.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_VolunteerNameFormatted);
			lblEMail.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_Email);
			lblEMail.DataBindings.Add("NavigateUrl", null, Search_VBS_ClassAssignment.FN_EmailHyperlink);
			chkJuniorHelper.DataBindings.Add("Checked", null, Search_VBS_ClassAssignment.FN_JuniorHelper);
			chkTeacher.DataBindings.Add("Checked", null, Search_VBS_ClassAssignment.FN_Teacher);
			lblContactPhone.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_HomePhone);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_MobilePhone);
			lblTShirt.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_ShirtSize);

			lblAllergy.DataBindings.Add("Text", null, Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds);

			// Bind the groupheader
			ghClass.GroupFields.Add(new GroupField(Search_VBS_ClassAssignment.FN_Length, XRColumnSortOrder.Ascending));
			ghClass.GroupFields.Add(new GroupField(Search_VBS_ClassAssignment.FN_ClassCode, XRColumnSortOrder.Ascending));
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblEMail.Font = new Font(lblEMail.Font, FontStyle.Underline);
				lblEMail.ForeColor = Color.Blue;
			}
			else
			{
				lblEMail.Font = new Font(lblEMail.Font, FontStyle.Regular);
				lblEMail.ForeColor = Color.Black;
			}

			//// Set the height depending on allergies
			////16.45836
			lblAllergy.Visible = true;
			if (String.IsNullOrEmpty(GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds) != null ? 
                GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_AllergyAndSpecialNeeds).ToString() : string.Empty))
			{
				lblAllergy.Visible = false;
				xrPanel1.HeightF = 16.45836F;
				Detail.HeightF = 16.45836F;
			}
		}

		private void rptClassRosterSinglePage_ParametersRequestBeforeShow(object sender, DevExpress.XtraReports.Parameters.ParametersRequestEventArgs e)
		{
			//foreach (ParameterInfo info in e.ParametersInformation)
			//{
			//	if (info.Parameter.Name.ToLower() == "ClassCodeParam".ToLower())
			//	{
			//		ASPxGridLookup lookUpEdit = new ASPxGridLookup();
			//		//LookUpEdit lookUpEdit = new LookUpEdit();
			//		//lookUpEdit.Properties.DataSource = dataSet.Categories;
			//		lookUpEdit.Properties.DisplayMember = "CategoryName";
			//		lookUpEdit.Properties.ValueMember = "CategoryID";
			//		lookUpEdit.Properties.Columns.Add(new LookUpColumnInfo("CategoryName", 0, "Category Name"));
			//		lookUpEdit.Properties.NullText = "<Select Category>";

			//		info.Editor = lookUpEdit;
			//		info.Parameter.Value = DBNull.Value;
			//	}
			//}
		}

		private void ghClass_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			string teamColor = (GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_ClassColor) != null ?
				GetCurrentColumnValue(Search_VBS_ClassAssignment.FN_ClassColor).ToString() : string.Empty);
			if (teamColor.ToLower().Contains("red"))
			{
				lblTeamColor.ForeColor = Color.Red;
			}
			else if (teamColor.ToLower().Contains("blue"))
			{
				lblTeamColor.ForeColor = Color.Blue;
			}
			else
			{
				lblTeamColor.ForeColor = Color.Black;
			}
		}
	}
}
