﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
    public partial class rptTShirtMailingLabels : DevExpress.XtraReports.UI.XtraReport
    {
        public rptTShirtMailingLabels()
        {
            InitializeComponent();

            lblName.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_FullNameLastFirst);
            lblClassCode.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_ClassCode);
            lblGroup.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_GroupName);
            lblShirtSize.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_Size);
            lblTeacher.DataBindings.Add("Text", null, Search_VBS_TShirtList.FN_TeacherNames);
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            // As each one prints, find out if we need to turn anything off
            int sortOrder = (GetCurrentColumnValue(Search_VBS_TShirtList.FN_ShirtSort) != null ?
                (int)GetCurrentColumnValue(Search_VBS_TShirtList.FN_ShirtSort) : -1);
            lblTeacher.Visible =
                lblTch.Visible =
                false;
            lblGroup.Visible = false;
            lblClassCode.Visible = true;
            lblCC.Text = "Class Code:";
            //lblClassCode.Text = (GetCurrentColumnValue(Search_VBS_TShirtList.FN_GroupName) != null ?
            //    GetCurrentColumnValue(Search_VBS_TShirtList.FN_GroupName).ToString() : string.Empty);
            switch (sortOrder)
            {
                case 0:
                    lblName.ForeColor = Color.Red;
                    break;
                case 1:
                    lblName.ForeColor = Color.DarkGreen;
                    break;
                case 2:
                    lblName.ForeColor = Color.Blue;
                    lblTeacher.Visible =
                        lblTch.Visible = true;
                    break;
                case -1:
                default:
                    lblName.ForeColor = Color.Black;
                    lblCC.Text = "Group:";
                    lblGroup.Visible = true;
                    lblClassCode.Visible = false;
                    //lblClassCode.Text = (GetCurrentColumnValue(Search_VBS_TShirtList.FN_GroupName) != null ?
                    //    GetCurrentColumnValue(Search_VBS_TShirtList.FN_GroupName).ToString() : string.Empty);
                    break;
            }
        }
    }
}
