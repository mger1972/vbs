﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptSignInSheet : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;
		private int _lineNum = 1;

		public rptSignInSheet()
		{
			InitializeComponent();
		}

		public rptSignInSheet(string reportTitle, DateTime dow)
		{
			InitializeComponent();

			// Set the title
			rptTitle.Text = reportTitle;
			lblDOW.Text = dow.ToString("dddd");

			// Set the bindings
			lblChildsName.DataBindings.Add("Text", null, Search_VBS_SignInSheet.FN_ChildNameLastFirst);
			lblParentsName.DataBindings.Add("Text", null, Search_VBS_SignInSheet.FN_FullNameLastFirst);
			lblAssignment.DataBindings.Add("Text", null, Search_VBS_SignInSheet.FN_Assignment);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_SignInSheet.FN_MobilePhone);
			lblDays.DataBindings.Add("Text", null, Search_VBS_SignInSheet.FN_HelpDOW);

			lblLineNum.DataBindings.Add("Text", null, Search_VBS_SignInSheet.FN_GridCustom_2);

			//lblChildsName.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_ChildNameLastFirst);
			//lblChildsName.DataBindings.Add("NavigateUrl", null, Search_VBS_VolunteerChild.FN_GridCustom_5);
			//lblChildsName.DataBindings.Add("Target", null, Search_VBS_VolunteerChild.FN_GridCustom_6);

			//lblAge.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_AgeOfChild);
			//lblParentsName.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_FullNameLastFirst);
			//lblAssignment.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_Assignment);

			//lblAllergies.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_Allergies);
			//lblEmail.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_Email);
			//lblEmail.DataBindings.Add("NavigateUrl", null, Search_VBS_VolunteerChild.FN_EmailHyperlink);

			//lblHomePhone.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_HomePhone);
			//lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_MobilePhone);

			//lblRotation.DataBindings.Add("Text", null, Search_VBS_VolunteerChild.FN_GridCustom_7);

			//chkPottyTrained.DataBindings.Add("Checked", null, Search_VBS_VolunteerChild.FN_PottyTrained);
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			//lblLineNum.Text = _lineNum.ToString("##0") + ".";
			//_lineNum++;

			//chkPottyTrained.Visible = (GetCurrentColumnValue(Search_VBS_VolunteerChild.FN_DetailGUID) != null);

			//string emailHyperlink = (GetCurrentColumnValue(Search_VBS_VolunteerChild.FN_EmailHyperlink) != null ?
			//	GetCurrentColumnValue(Search_VBS_VolunteerChild.FN_EmailHyperlink).ToString() : string.Empty);
			//if (!String.IsNullOrEmpty(emailHyperlink))
			//{
			//	lblEmail.Font = new Font(lblAssignment.Font, FontStyle.Underline);
			//	lblEmail.ForeColor = Color.Blue;
			//}
			//else
			//{
			//	lblEmail.Font = new Font(lblAssignment.Font, FontStyle.Regular);
			//	lblEmail.ForeColor = Color.Black;
			//}

			//string responseIDHyperlink = (GetCurrentColumnValue(Search_VBS_VolunteerChild.FN_GridCustom_5) != null ?
			//	GetCurrentColumnValue(Search_VBS_VolunteerChild.FN_GridCustom_5).ToString() : string.Empty);
			//if (!String.IsNullOrEmpty(responseIDHyperlink))
			//{
			//	lblChildsName.Font = new Font(lblChildsName.Font, FontStyle.Underline);
			//	lblChildsName.ForeColor = Color.Blue;
			//}
			//else
			//{
			//	lblChildsName.Font = new Font(lblChildsName.Font, FontStyle.Regular);
			//	lblChildsName.ForeColor = Color.Black;
			//}
		}
	}
}
