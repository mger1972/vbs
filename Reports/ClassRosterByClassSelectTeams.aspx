﻿<%@ Page Title="Class Roster - Send Email" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClassRosterByClassSelectTeams.aspx.cs" Inherits="VBS.Reports.ClassRosterByClassSelectTeams" %>

<%@ Register Assembly="DevExpress.Web.ASPxRichEdit.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRichEdit" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.XtraReports.v17.1.Web, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
	<section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
				<h3>Send an email to the team leaders - Select teams.</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<script language="javascript">
		$(document).ready(function () {
			$("#MainContent_documentViewer_Splitter_0").css("background-color", "#FFFFFF");
			$("#MainContent_documentViewer_Splitter_0").css("border", "1px solid black");
			$("#MainContent_documentViewer_Splitter_1i1i0").css("border", "0px solid black");
		})
		function onCheckedChanged(s, e) {
			alert('hi');
            setTimeout(function () { gridList.batchEditApi.EndEdit(); }, 0);  
        }
	 </script>
	<table width="100%" align="center" border="1">
		<tr>
			<td>
				<dx:ASPxButton ID="btnCheckUncheckAll" runat="server" OnClick="btnCheckUncheckAll_Click" Text="Check/Uncheck All" 
					AutoPostBack="true"></dx:ASPxButton>
			</td>
			<td align="right">&nbsp;<% if (!String.IsNullOrEmpty(MessageText.Text)) { %>
				<span class="validation-summary-errors"><asp:Literal runat="server" ID="MessageText" Text="" />&nbsp; &nbsp;</span>
				<% } %></td>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<dx:ASPxGridView ID="gridList" ClientInstanceName="gridList" runat="server" Width="100%" 
					OnBatchUpdate="gridList_BatchUpdate" OnCommandButtonInitialize="gridList_CommandButtonInitialize"
					Paddings-PaddingLeft="10px" Paddings-PaddingRight="10px" Paddings-PaddingTop="0px" Paddings-PaddingBottom="0px"
					EnableViewState="false">
					<Border BorderWidth="0px" />
					<ClientSideEvents BatchEditConfirmShowing="function(s, e) { e.cancel = true; }" 
						BeginCallback="function(s, e) {
							if (e.command == 'UPDATEEDIT') {
								pnlLoading.Text = 'Saving Changes';
								pnlLoading.Show();
								isUpdateEdit = true;
							}
						}"
						EndCallback="function(s, e) {
							if (isUpdateEdit) {
								isUpdateEdit = false;
							}
							pnlLoading.Hide();
						}" />
					<Columns>
						<dx:GridViewDataCheckColumn FieldName="CheckedInGrid" Width="2px" Caption="Select"
							HeaderStyle-HorizontalAlign="Center" VisibleIndex="2">
							 <DataItemTemplate>
								<dx:ASPxCheckBox ID="chk" runat="server" Value='<%# Eval("CheckedInGrid") %>' OnInit="chk_Init">
								</dx:ASPxCheckBox>
							</DataItemTemplate>
						</dx:GridViewDataCheckColumn>
						<dx:GridViewDataComboBoxColumn FieldName="ClassCode" Caption="Class" VisibleIndex="3"></dx:GridViewDataComboBoxColumn>
						<dx:GridViewDataComboBoxColumn FieldName="Count" Caption="Child Count" VisibleIndex="4"></dx:GridViewDataComboBoxColumn>
					</Columns>
					<Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowTitlePanel="false" />
					<SettingsBehavior FilterRowMode="OnClick" AllowSort="false" />
					<SettingsEditing Mode="Batch" />
					<SettingsPager PageSize="50" Visible="false" />
					<Styles>
						<InlineEditCell>
							<Border BorderStyle="None" />
						</InlineEditCell>
					</Styles>
				</dx:ASPxGridView>
				<dx:ASPxCallback ID="cb" runat="server" ClientInstanceName="cb" OnCallback="cb_Callback">
				</dx:ASPxCallback>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="right"><dx:ASPxButton ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Send Emails" AutoPostBack="true">
			</dx:ASPxButton></td>
		</tr>
	</table>

</asp:Content>