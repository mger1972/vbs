﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Reports
{
	public partial class HTMLEditorPage : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			// Load the content into the HTML window
			if (String.IsNullOrEmpty(htmlEditor.Html))
			{
				StringBuilder sb = new StringBuilder();

				//sb.AppendLine("<html>");
				//sb.AppendLine("<head>");
				//sb.AppendLine("<title>Background Check Required</title>");
				//sb.AppendLine("</head>");

				//sb.AppendLine("<body>");
				//sb.Append("<p><font face=\"Arial, Times New Roman\">Dear &lt;&lt;firstName&gt;&gt; &lt;&lt;lastName&gt;&gt;:</font></p>");
				sb.Append("<p><font face=\"Arial, Times New Roman\">Thank you for volunteering for Sonrise Island at Voyagers Bible Church.  " + 
					"We’re excited to have you on board for this year!</font></p>");
				sb.Append("<p><font face=\"Arial, Times New Roman\">We’ve reviewed our files and noticed we don’t have a recent background check " +
					"on file for you.  Please <a href=\"http://www.sonriseislandvbs.com/Reports/BackgroundCheckRev10_13.pdf\">click here</a> to download the " + 
					"background check form.  If you have questions about the background check form, process, or feel you have received this message " + 
					"in error, please contact the Voyagers secretary.</font></p>");
				//sb.Append("<p><font face=\"Arial, Times New Roman\">If you have any questions or feel you’ve received this message in error, " + 
				//	"please don’t hesitate to contact me.</font></p>");
				//sb.Append("<p><font face=\"Arial, Times New Roman\">Blessings, <br />Jennifer Gerlach</font></p>");
				sb.Append("<p><font face=\"Arial, Times New Roman\">Blessings,</font></p>");
				sb.Append("<p><font face=\"Arial, Times New Roman\">Sonrise Island - Registration Coordinator</font></p>");
				//sb.AppendLine("</body>");

				//sb.Append("<font face=\"Tahoma, Arial, Times New Roman\">");
				//sb.Append("Dear &lt;&lt;firstName&gt;&gt; &lt;&lt;lastName&gt;&gt;:");
				//sb.Append("Thank you for volunteering for Sonrise Island at Voyagers Bible Church.  We’re excited to have you on board for this year!");
				//sb.Append("We’ve reviewed our files and noticed we don’t have a recent background check on file for you.  Please contact Kim Coyle @ Voyagers (<a href=\"mailto:kcoyle@voyagers.org\">kcoyle@voyagers.org</a>) to obtain a background check form.  ");
				//sb.Append("If you have any questions or feel you’ve received this message in error, please don’t hesitate to contact me.");
				//sb.Append("Blessings, <br />Jennifer Gerlach");
				//sb.Append("</font>");
				//sb.AppendLine("</html>");

				htmlEditor.Html = sb.ToString();		// Set the text
			}
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			// Send out an email
			// Get all the people we have to send this out for
			Search_VBS_BackgroundCheckCollection coll = new Search_VBS_BackgroundCheckCollection("bCurrentBackgroundCheck = 0");
			if (System.Environment.MachineName.ToLower().Contains("vp-is-"))
			{
				coll = new Search_VBS_BackgroundCheckCollection("bCurrentBackgroundCheck = 0 AND sEmail LIKE 'mgerlach%'");
			}
			coll.Sort(Search_VBS_BackgroundCheck.FN_LastName + ", " + Search_VBS_BackgroundCheck.FN_FirstName);

			#region Old Code - one at a time
			//foreach (Search_VBS_BackgroundCheck check in coll)
			//{
			//	// Change the mail from the control
			//	string convertedHTML = htmlEditor.Html;
			//	//convertedHTML = convertedHTML.Replace("&lt;&lt;firstName&gt;&gt;", check.FirstName);
			//	//convertedHTML = convertedHTML.Replace("&lt;&lt;lastName&gt;&gt;", check.LastName);

			//	// Start the email
			//	List<MailAddress> toList = new List<MailAddress>(new MailAddress[] {
			//		new MailAddress(check.Email, check.FirstName + " " + check.LastName),
			//	});
			//	List<MailAddress> ccList = new List<MailAddress>();
			//	if (chkSendCopyToMyself.Checked)
			//	{
			//		ccList.Add(new MailAddress("jen@gerlach5.com", "Jennifer Gerlach"));
			//	}
			//	Utils.SendEMailAsJen(toList, ccList, new List<MailAddress>(), "Sonrise Island Background Check", convertedHTML, string.Empty, Session["UserName"].ToString());
			//}
			#endregion Old Code - one at a time


			// Change the mail from the control
			string convertedHTML = htmlEditor.Html;
			//convertedHTML = convertedHTML.Replace("&lt;&lt;firstName&gt;&gt;", check.FirstName);
			//convertedHTML = convertedHTML.Replace("&lt;&lt;lastName&gt;&gt;", check.LastName);

			// Start the email
			List<MailAddress> ccList = new List<MailAddress>();
			List<MailAddress> bccList = new List<MailAddress>();
			List<MailAddress> toList = new List<MailAddress>();
			List<string> existingEMails = new List<string>();

			// Send a copy to Jen
			toList.Add(new MailAddress("jen@gerlach5.com", "Jennifer Gerlach"));

			if (chkSendCopyToMyself.Checked)
			{
				bccList.Add(new MailAddress(Session["UserName"].ToString(), Session["UserName"].ToString()));
			}
			foreach (Search_VBS_BackgroundCheck check in coll)
			{
				if (!existingEMails.Contains(check.Email.ToLower()))
				{
					existingEMails.Add(check.Email.ToLower());
					bccList.Add(new MailAddress(check.Email, check.FirstName + " " + check.LastName));
				}
			}
			Utils.SendEMailAsJen(toList, ccList, bccList, "Sonrise Island Background Check", convertedHTML, string.Empty, Session["UserName"].ToString());

			// Send the second confirm email to Jen and I
			toList.Clear();
			ccList.Clear();
			bccList.Clear();

			StringBuilder sb = new StringBuilder();
			sb.AppendLine("The following people were included in this Background Check Email:<br />&nbsp;<br />");
			foreach (Search_VBS_BackgroundCheck check in coll)
			{
				sb.AppendLine(check.FirstName + " " + check.LastName + " (" + check.Email + ")<br />");
			}

			// Send it
			toList.Add(new MailAddress("jen@gerlach5.com", "Jennifer Gerlach"));
			toList.Add(new MailAddress("mark@gerlach5.com", "Mark Gerlach"));
			Utils.SendEMailAsJen(toList, ccList, bccList, "Sonrise Island Background Check Confirmation", sb.ToString(), string.Empty, Session["UserName"].ToString());

			Response.Write("<script>top.location='BackgroundCheck.aspx';parent.location='BackgroundCheck.aspx';</script>");
			//Response.Redirect("BackgroundCheck.aspx");		// Redirect the user
		}

		protected void htmlEditor_HtmlCorrecting(object sender, DevExpress.Web.ASPxHtmlEditor.HtmlCorrectingEventArgs e)
		{
			e.Handled = true;
		}
	}
}