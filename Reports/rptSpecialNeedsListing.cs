﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace VBS.Reports
{
	public partial class rptSpecialNeedsListing : DevExpress.XtraReports.UI.XtraReport
	{
		private Color _detailBackColor = Color.White;

		public rptSpecialNeedsListing()
		{
			InitializeComponent();

			// Set the bindings
			lblClass.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_ClassCode);
			lblGrade.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_GradeInFall);

			lblStudentName.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_GridCustom_7);
			lblTeacherNames.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_Teachers);
			lblParentName.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_GridCustom_9);
			lblParentName.DataBindings.Add("NavigateUrl", null, Search_VBS_SpecialNeeds.FN_EmailHyperlink);

			lblHomePhone.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_HomePhone);
			lblMobilePhone.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_MobilePhone);

			lblAllergy.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_SpecialNeeds);
			lblRotation.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_Rotation);

			//lblClass.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_GridCustom_7);
			//lblClass.DataBindings.Add("Bookmark", null, Search_VBS_SpecialNeeds.FN_GridCustom_9);

			//lblGrade.DataBindings.Add("Text", null, Search_VBS_SpecialNeeds.FN_GridCustom_9);
		}

		private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
		{
			xrPanel1.BackColor = _detailBackColor;
			_detailBackColor = (_detailBackColor == Color.White ? Color.AliceBlue : Color.White);

			string emailHyperlink = (GetCurrentColumnValue(Search_VBS_SpecialNeeds.FN_EmailHyperlink) != null ?
				GetCurrentColumnValue(Search_VBS_SpecialNeeds.FN_EmailHyperlink).ToString() : string.Empty);
			if (!String.IsNullOrEmpty(emailHyperlink))
			{
				lblParentName.Font = new Font(lblParentName.Font, FontStyle.Underline);
				lblParentName.ForeColor = Color.Blue;
			}
			else
			{
				lblParentName.Font = new Font(lblParentName.Font, FontStyle.Regular);
				lblParentName.ForeColor = Color.Black;
			}
		}
	}
}
