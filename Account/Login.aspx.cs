﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS.Account
{
	public partial class Login : Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//RegisterHyperLink.NavigateUrl = "Register.aspx";
			//OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];

			//loginControl.FailureText = "failure.";
			//loginControl.FailureTextStyle.ForeColor = Color.Red;
			var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
			//if (!String.IsNullOrEmpty(returnUrl))
			//{
			//	RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
			//}
			//loginControl.FailureAction = LoginFailureAction.Refresh;
			//loginControl.FailureText = "Failure";
			//loginControl.FailureTextStyle.ForeColor = Color.Red;
		}

		bool IsValidEmail(string strIn)
		{
			// Return true if strIn is in valid e-mail format.
			return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
		}

		protected void Unnamed_LoggingIn(object sender, LoginCancelEventArgs e)
		{
			if (!IsValidEmail(loginControl.UserName))
			{
				//loginControl.InstructionText = "You must enter a valid e-mail address.";
				LoginFailureText.Text = "You must enter a valid e-mail address.";
				e.Cancel = true;
			}
			else if (loginControl.UserName.ToLower().Contains("select") ||
				loginControl.Password.ToLower().Contains("select"))
			{
				//loginControl.InstructionText = "Stop trying to hack our SQL.";
				LoginFailureText.Text = "Stop trying to hack our SQL.";
				e.Cancel = true;
			}
			else
			{
				loginControl.InstructionText = String.Empty;

				// Check the user name and password against the database
				VBSUserCollection users = new VBSUserCollection("sUserName = '" + loginControl.UserName.Replace("'", "''") + "'");
				if (users.Count == 0)
				{
					// The user doesn't exist
					//loginControl.InstructionText = "The user name/email you entered doesn't exist.";
					LoginFailureText.Text = "The user name/email you entered doesn't exist.";
					e.Cancel = true;
				}
				else if (users[0].Inactive)
				{
					//loginControl.InstructionText = "The user entered is inactive.";
					LoginFailureText.Text = "The user entered is inactive.";
					e.Cancel = true;
				}
				else
				{
					// Check the password
					if (loginControl.Password == users[0].Password)
					{
						// Authenticate the user
						Session["UserName"] = users[0].UserName;
					}
					else
					{
						//loginControl.FailureText = "The password you entered is incorrect.";
						//loginControl.FailureAction = LoginFailureAction.Refresh;
						LoginFailureText.Text = "The password you entered is incorrect.";
						e.Cancel = true;
					}
				}
			}
		}

		protected void loginControl_Authenticate(object sender, AuthenticateEventArgs e)
		{
			e.Authenticated = true;
		}

		protected void loginControl_LoggedIn(object sender, EventArgs e)
		{
			Response.Redirect("/Default.aspx");
		}

		protected void loginControl_LoginError(object sender, EventArgs e)
		{
			loginControl.TitleText = "LoginError";
		}
	}
}