﻿using SmarterASPNetTestProject;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using VBS.classes;

namespace VBS
{
	/**************************************
	 * 
	 * Had to set the debug="true" part of this line <compilation debug="true" targetFramework="4.5"> to get the UpdatePanel to work
	 * 
	 * ***********************************/

	public partial class Download : System.Web.UI.Page
	{
        private static StringBuilder _processString;

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			//else
			//{
			//	Session["UserName"] = HttpContext.Current.User.Identity.Name;
			//}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			if (Utils.LAST_YEARS_SITE)
			{
				//DateTime.Now.AddYears(-1).Year.ToString("yyyy");
				cbProcess.Enabled = false;
				btnProcessDownload.Enabled = false;
			}

			// Set the meta refresh tag
			HtmlMeta meta = new HtmlMeta();
			meta.Name = "refreshTag";
			meta.HttpEquiv = "refresh";

			// Randomize the code so it goes between 8 minutes and 12 minutes (every 5 seconds)
			Random r = new Random();
			int diff = (60 * 8) + (r.Next(0, 48) * 5);
			meta.Content = diff.ToString();
			MetaPlaceHolder.Controls.Add(meta);

            if (!IsPostBack &&
                !IsCallback)
            {
                _processString = new StringBuilder();       // Reset the progress
            }

			//// Download the files
			//// Login to the system
			//string loginAddress = "https://voyagers.ccbchurch.com/login.php";
			//StringBuilder sb = new StringBuilder();
			//sb.Append("ax=" + HttpUtility.UrlEncode("login"));
			//sb.Append("&" + "form[login]=" + HttpUtility.UrlEncode("jen@efgtech.com"));
			//sb.Append("&" + "form[password]=" + HttpUtility.UrlEncode("stretch1"));

			//VBS_CookieAwareWebClient client = new VBS_CookieAwareWebClient();
			//client.Login(loginAddress, sb.ToString());

			//// Download the files
			// Get them from the Utils class (FormIDs)

			//// Download the first file
			//bool firstPass = true;
			//ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			//DateTime start = DateTime.Now.AddSeconds(-1);
			//foreach (KeyValuePair<string, int> kvp in fileIDs)
			//{
			//	string remoteUri = "https://voyagers.ccbchurch.com/output/form_responses.php?form_id=" + kvp.Value.ToString();
			//	string serverPath = System.AppDomain.CurrentDomain.BaseDirectory;
			//	string fileName = Path.Combine(serverPath, @"vbs_dl\dl_" + kvp.Key + "_" + kvp.Value.ToString() + ".xlsx");
			//	if (File.Exists(fileName)) { File.Delete(fileName); }
			//	client.DownloadFile(remoteUri, fileName);

			//	// Check to see if the new file exists and parse it if it does
			//	if (File.Exists(fileName))
			//	{
			//		ImportType type = ImportType.Base;
			//		if (kvp.Key.ToLower().Contains("alt")) { type = ImportType.AltPay; }
			//		else if (kvp.Key.ToLower().Contains("under18")) { type = ImportType.VolunteerUnder18; }
			//		else if (kvp.Key.ToLower().Contains("adult")) { type = ImportType.VolunteerAdult; }
			//		errors.AddRange(ImportFiles.Import(type, fileName));

			//		VBSLog log = new VBSLog();
			//		log.LogGUID = System.Guid.NewGuid().ToString();
			//		start = start.AddMilliseconds(100);
			//		log.DateLogged = start;
			//		log.Information = true;
			//		log.Description = "Start import: " + type.ToString();
			//		log.AddedByUserID =
			//			log.UpdatedByUserID =
			//			"<SystemImport>";
			//		errors.AddRange(log.AddUpdate());

			//		_sb.AppendLine("<b>" + type.ToString() + "</b><br />");
			//		int errorCount = 0;
			//		VBSLogCollection logCollectionUpdate = new VBSLogCollection();
			//		foreach (ClassGenException ex in errors)
			//		{
			//			VBSLog item = new VBSLog();
			//			item.LogGUID = System.Guid.NewGuid().ToString();
			//			start = start.AddMilliseconds(100);
			//			item.DateLogged = start;
			//			if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Warning) { item.Warning = true; }
			//			if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Critical) { item.Error = true; }
			//			if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Information) { item.Information = true; }
			//			item.Description = ex.Message;
			//			item.AddedByUserID =
			//				item.UpdatedByUserID =
			//				"<SystemImport>";
			//			logCollectionUpdate.Add(item);

			//			_sb.AppendLine("&nbsp; &nbsp;" + ex.DescriptionWithException + "<br />");
			//			errorCount++;
			//		}
			//		if (logCollectionUpdate.Count > 0)
			//		{
			//			errors.AddRange(logCollectionUpdate.AddUpdateAll());
			//		}
			//		errors.Clear();
			//	}
			//	_sb.AppendLine("<br>");
			//	firstPass = false;
			//}

			//_sb.AppendLine("<br>&nbsp;");

			// Go out and pull the last 100 messages from the log so we can see what happened
			//_sbLog = new StringBuilder();
			//VBSLogCollection logCollection = new VBSLogCollection("sAddedByUserID = '<SystemImport>' AND dtLogged > DATEADD(dd, -14, GETDATE())");
			//logCollection.Sort(VBSLog.FN_DateLogged + " DESC");
			//int logCount = 0;
			//foreach (VBSLog item in logCollection)
			//{
			//	_sbLog.AppendLine(String.Format("{0}: {1}<br>", item.DateLogged.ToString("MM/dd/yyyy hh:mm:ss tt"), item.Description));
			//	logCount++;
			//	if (logCount > 250) { break; }
			//}
			//_sbLog.AppendLine("<br>&nbsp;");
		}

		//protected void tmrBase_Tick(object sender, EventArgs e)
		//{
  //          // Do the magic, then disable the timer
  //          lblBase.Text = DownloadData(ImportType.Base);
  //          tmrBase.Enabled = false;
  //      }

		//protected void tmrAltPay_Tick(object sender, EventArgs e)
		//{
  //          // Do the magic, then disable the timer
  //          lblAltPay.Text = DownloadData(ImportType.AltPay);
  //          tmrAltPay.Enabled = false;
  //      }

		//protected void tmrVolunteerAdult_Tick(object sender, EventArgs e)
		//{
  //          // Do the magic, then disable the timer
  //          lblVolunteerAdult.Text = DownloadData(ImportType.VolunteerAdult);
  //          tmrVolunteerAdult.Enabled = false;
  //      }

		//protected void tmrVolunteerUnder18_Tick(object sender, EventArgs e)
		//{
		//	// Do the magic, then disable the timer
		//	lblVolunteerUnder18.Text = DownloadData(ImportType.VolunteerUnder18);
		//	tmrVolunteerUnder18.Enabled = false;
		//}

		//protected void tmrLog_Tick(object sender, EventArgs e)
		//{
		//	// Do the magic, then disable the timer
		//	lblLog.Text = GetLogInfo();
		//	tmrLog.Enabled = false;
		//}

		/// <summary>
		/// Get the log information from the database
		/// </summary>
		/// <returns>The formatted string to post to the page</returns>
		private string GetLogInfo()
		{
			StringBuilder sbLog = new StringBuilder();
			Search_VBS_LogCollection logCollection = new Search_VBS_LogCollection(string.Empty);
			//sbLog.AppendLine("<b>Log</b><br />");
			int logCount = 0;
			foreach (Search_VBS_Log item in logCollection)
			{
				sbLog.AppendLine(String.Format("&nbsp; &nbsp;{0}: {1}<br>", item.DateLogged.ToString("MM/dd/yyyy hh:mm:ss tt"), item.Description));
				logCount++;
				if (logCount > 250) { break; }
			}
			sbLog.AppendLine("<br>&nbsp;");

			return sbLog.ToString();		// Return the string
		}

		/// <summary>
		/// Download the data from the server
		/// </summary>
		///// <param name="type">The type to download from the server</param>
		private string DownloadData()
		{
            _processString = new StringBuilder();

            // Download the files
            // Login to the system
            string loginAddress = "https://voyagers.ccbchurch.com/login.php";
			StringBuilder sb = new StringBuilder();
			sb.Append("ax=" + HttpUtility.UrlEncode("login"));
			sb.Append("&" + "form[login]=" + HttpUtility.UrlEncode("jen@efgtech.com"));
			sb.Append("&" + "form[password]=" + HttpUtility.UrlEncode("stretch1"));

            _processString.Insert(0, "Grabbing Download Files..." + "<br />");

			VBS_CookieAwareWebClient client = new VBS_CookieAwareWebClient();
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
			client.Login(loginAddress, sb.ToString());

			// Download the files
			sb = new StringBuilder();
			Dictionary<string, int> fileIDs = Utils.FormIDs;
            Dictionary<string, string> filesDownloaded = new Dictionary<string, string>();

			// Go through and download the files
			string fileName = string.Empty, serverPath = string.Empty;
            foreach (KeyValuePair<string, int> kvp in fileIDs)
            {
				// Bypass the medical release form download
				// This was done because the medical release forms are a live time download on other forms
				// But it turns out this is needed in 2017 - commenting it out
				//if (kvp.Key.ToLower().StartsWith("Medical".ToLower()))
				//{
				//	serverPath = System.AppDomain.CurrentDomain.BaseDirectory;
				//	fileName = Path.Combine(serverPath, @"vbs_dl\dl_" + kvp.Key + "_" + kvp.Value.ToString() + ".xlsx");
				//	filesDownloaded.Add(kvp.Key, fileName);
				//	continue;       // Hit the next value
				//}

				_processString.Insert(0, "Downloading file: " + kvp.Key + "<br />");
                string remoteUri = "https://voyagers.ccbchurch.com/output/form_responses.php?form_id=" + kvp.Value.ToString();
                serverPath = System.AppDomain.CurrentDomain.BaseDirectory;
                fileName = Path.Combine(serverPath, @"vbs_dl\dl_" + kvp.Key + "_" + kvp.Value.ToString() + ".xlsx");
                if (File.Exists(fileName)) { File.Delete(fileName); }
                client.DownloadFile(remoteUri, fileName);
                filesDownloaded.Add(kvp.Key, fileName);
                System.Threading.Thread.Sleep(500);
            }

			// Delete the medicals no matter what
			// Begin Delete
			VBSDataCollection collDelete = null;
			VBSClassLinkCollection collClassLinks = null;
			VBSResponseToGroupCollection collResponseToGroups = null;
			//VBSVolunteerChildCollection collVolunteerChildren = null;

			collDelete = new VBSDataCollection("sSheetType = 'MedicalReleaseForm'");
			if (collDelete.Count > 0)
			{
				collClassLinks = new VBSClassLinkCollection("iResponseID IN (" + Utils.JoinString(collDelete.GetDistinctFromCollection(VBSDataField.ResponseID), ", ") + ")");
				collResponseToGroups = new VBSResponseToGroupCollection("iResponseID IN (" + Utils.JoinString(collDelete.GetDistinctFromCollection(VBSDataField.ResponseID), ", ") + ")");
				//collVolunteerChildren = new VBSVolunteerChildCollection("sRawGUID IN ('" + Utils.JoinString(collDelete.GetDistinctFromCollection(VBSDataField.RawGUID), ", ") + "')");

				//foreach (VBSVolunteerChild child in collVolunteerChildren) { child.Delete(); }
				foreach (VBSResponseToGroup response in collResponseToGroups) { response.Delete(); }
				foreach (VBSClassLink link in collClassLinks) { link.Delete(); }
				foreach (VBSData dataItem in collDelete) { dataItem.Delete(); }
				//collVolunteerChildren.AddUpdateAll();
				collResponseToGroups.AddUpdateAll();
				collClassLinks.AddUpdateAll();
				collDelete.AddUpdateAll();
			}
			// End Delete

			// Process the files
			//bool firstPass = true;
			ClassGenExceptionCollection errors = new ClassGenExceptionCollection();
			DateTime start = DateTime.Now.AddSeconds(-1);
			foreach (KeyValuePair<string, string> kvp in filesDownloaded)
			{
				//string remoteUri = "https://voyagers.ccbchurch.com/output/form_responses.php?form_id=" + kvp.Value.ToString();
				//string serverPath = System.AppDomain.CurrentDomain.BaseDirectory;
				//string fileName = Path.Combine(serverPath, @"vbs_dl\dl_" + kvp.Key + "_" + kvp.Value.ToString() + ".xlsx");
				//if (File.Exists(fileName)) { File.Delete(fileName); }
				//client.DownloadFile(remoteUri, fileName);
                //continue;

				// Check to see if the new file exists and parse it if it does
				if (File.Exists(kvp.Value))
				{
                    _processString.Insert(0, "Processing file: " + kvp.Value + "<br />");
                    System.Threading.Thread.Sleep(500);

                    ImportType type = ImportType.Base;
					if (kvp.Key.ToLower().Contains("alt")) { type = ImportType.AltPay; }
					else if (kvp.Key.ToLower().Contains("under18")) { type = ImportType.VolunteerUnder18; }
					else if (kvp.Key.ToLower().Contains("adult")) { type = ImportType.VolunteerAdult; }
                    else if (kvp.Key.ToLower().Contains("medical"))
					{
						type = ImportType.MedicalReleaseFormCurrent;
						if (kvp.Key.ToLower().Equals("MedicalReleaseFormPrevYear".ToLower()))
						{
							type = ImportType.MedicalReleaseFormPrevYear;
						}
					}
                    errors.AddRange(ImportFiles.Import(type, kvp.Value));

                    // Add the dump messages to the string
                    foreach (ClassGenException exMsg in errors)
                    {
                        _processString.Insert(0, exMsg.Message + "<br />");
                    }

                    VBSLog log = new VBSLog();
					log.LogGUID = System.Guid.NewGuid().ToString();
					start = start.AddMilliseconds(100);
					log.DateLogged = start;
					log.Information = true;
					log.Description = "Start import: " + type.ToString();
					log.AddedByUserID =
						log.UpdatedByUserID =
						"<SystemImport>";
					errors.AddRange(log.AddUpdate());

					//sb.AppendLine("<b>" + type.ToString() + "</b><br />");
					int errorCount = 0;
					VBSLogCollection logCollectionUpdate = new VBSLogCollection();
					foreach (ClassGenException ex in errors)
					{
						VBSLog item = new VBSLog();
						item.LogGUID = System.Guid.NewGuid().ToString();
						start = start.AddMilliseconds(100);
						item.DateLogged = start;
						if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Warning) { item.Warning = true; }
						if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Critical) { item.Error = true; }
						if (ex.ClassGenExceptionIconType == ClassGenExceptionIconType.Information) { item.Information = true; }
						item.Description = ex.Message;
						item.AddedByUserID =
							item.UpdatedByUserID =
							"<SystemImport>";
						logCollectionUpdate.Add(item);

						sb.AppendLine("&nbsp; &nbsp;" + ex.DescriptionWithException + "<br />");
						errorCount++;
					}
					if (logCollectionUpdate.Count > 0)
					{
						errors.AddRange(logCollectionUpdate.AddUpdateAll());
					}
					errors.Clear();
				}
				//sb.AppendLine("<br>");
				//firstPass = false;
			}

            // Run some updates on the table values (school and church)
            //DBHelper.UpdateDBValuesAfterImport();

			//sb.AppendLine("<br>&nbsp;");

			return sb.ToString();
		}

        protected void cbProcess_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
            // Start the download
            DownloadData();

            System.Threading.Thread.Sleep(1500);        // Wait to get the information
        }

        protected void cbGetStatus_Callback(object source, DevExpress.Web.CallbackEventArgs e)
        {
			if (_processString == null) { return; }
            e.Result = _processString.ToString();
        }
    }
}