﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS
{
	public partial class SiteMaster : MasterPage
	{
		private const string AntiXsrfTokenKey = "__AntiXsrfToken";
		private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
		private string _antiXsrfTokenValue;
		protected string _currentpageName = string.Empty;

		protected void Page_Init(object sender, EventArgs e)
		{
			// The code below helps to protect against XSRF attacks
			var requestCookie = Request.Cookies[AntiXsrfTokenKey];
			Guid requestCookieGuidValue;
			if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
			{
				// Use the Anti-XSRF token from the cookie
				_antiXsrfTokenValue = requestCookie.Value;
				Page.ViewStateUserKey = _antiXsrfTokenValue;
			}
			else
			{
				// Generate a new Anti-XSRF token and save to the cookie
				_antiXsrfTokenValue = Guid.NewGuid().ToString("N");
				Page.ViewStateUserKey = _antiXsrfTokenValue;

				var responseCookie = new HttpCookie(AntiXsrfTokenKey)
				{
					HttpOnly = true,
					Value = _antiXsrfTokenValue
				};
				if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
				{
					responseCookie.Secure = true;
				}
				Response.Cookies.Set(responseCookie);
			}

			// Check to see if they're in last year's site
			string requestedDomain = HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
			if (requestedDomain.ToLower().StartsWith("ly.") ||
				requestedDomain.ToLower().StartsWith("lastyear."))
			{
				Utils.LAST_YEARS_SITE = true;
			}
			else
			{
				Utils.LAST_YEARS_SITE = false;
			}

			// Redirect the page if this is a phone
			if (Request.Browser["IsMobile"] == "True" &&
				Session["MobileFullSite"] == null &&
				HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/iOS/iOS_Default2.aspx", true);
			}

			Page.PreLoad += master_Page_PreLoad;
		}

		protected void master_Page_PreLoad(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				// Set Anti-XSRF token
				ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
				ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
			}
			else
			{
				// Validate the Anti-XSRF token
				if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
					|| (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
				{
					throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
				}
			}
		}

		/// <summary>
		/// Sets the child page's instance
		/// </summary>
		/// <param name="page">The instance of the child page</param>
		public void SetChildPageInstance(Page page)
		{
			_currentpageName = page.GetType().FullName.Replace("ASP.", "").Replace("_", ".");
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			
		}
	}
}