﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VBS
{
	public partial class Stats : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//tmrCounts.Enabled = true;
			//tmrLog.Enabled = true;

			if (!HttpContext.Current.User.Identity.IsAuthenticated)
			{
				Response.Redirect("/Account/Login.aspx", true);
				return;
			}
			else
			{
				Session["UserName"] = HttpContext.Current.User.Identity.Name;
			}

			// Tell the parent who we belong to
			((SiteMaster)this.Master).SetChildPageInstance(this);

			// Update the counts
			lblCounts.Text = GetCountInfo();
			lblLog.Text = GetLogInfo();
		}

		//protected void tmrLog_Tick(object sender, EventArgs e)
		//{
		//	// Do the magic, then disable the timer
		//	lblLog.Text = GetLogInfo();
		//	tmrLog.Enabled = false;
		//}

		//protected void tmrCounts_Tick(object sender, EventArgs e)
		//{
		//	// Do the magic, then disable the timer
		//	lblCounts.Text = GetCountInfo();
		//	tmrCounts.Enabled = false;
		//}

		/// <summary>
		/// Get the log information from the database
		/// </summary>
		/// <returns>The formatted string to post to the page</returns>
		private string GetLogInfo()
		{
			StringBuilder sbLog = new StringBuilder();
			Search_VBS_LogCollection logCollection = new Search_VBS_LogCollection(string.Empty);
			logCollection.Sort(Search_VBS_Log.FN_DateLogged + " DESC");
			//sbLog.AppendLine("<b>Log</b><br />");
			int logCount = 0;
			foreach (Search_VBS_Log item in logCollection)
			{
				sbLog.AppendLine(String.Format("&nbsp; &nbsp;{0}: {1}<br>", item.DateLogged.ToString("MM/dd/yyyy hh:mm:ss tt"), item.Description));
				logCount++;
				if (logCount > 250) { break; }
			}

			return sbLog.ToString();		// Return the string
		}

		/// <summary>
		/// Get the count information from the database
		/// </summary>
		/// <returns>The formatted string to post to the page</returns>
		private string GetCountInfo()
		{
			StringBuilder sbCount = new StringBuilder();
			Search_VBS_RawDataCountCollection countCollection = new Search_VBS_RawDataCountCollection(string.Empty);
			countCollection.Sort(Search_VBS_RawDataCount.FN_SortOrder);
			int itemTotal = 0;
			foreach (Search_VBS_RawDataCount item in countCollection)
			{
				sbCount.AppendLine(String.Format("&nbsp; &nbsp;{0}: {1}<br>", item.SheetType, item.Count));
				itemTotal += (item.Count.HasValue ? item.Count.Value : 0);
			}
			sbCount.AppendLine(String.Format("&nbsp; &nbsp;<font color=\"#FF0000\">{0}: {1}</font><br>", "Total Count", itemTotal));

			return sbCount.ToString();		// Return the string
		}
	}
}