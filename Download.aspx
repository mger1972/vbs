﻿<%@ Page Title="Download Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Download.aspx.cs" Inherits="VBS.Download" EnableSessionState="False" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content runat="server" ID="HeadContent" ContentPlaceHolderID="HeadContent">
	<asp:PlaceHolder id="MetaPlaceHolder" runat="server" />
</asp:Content>
<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>Sonrise Island VBS</h1>
                <h3>Click the button to download the files and process them...</h3>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
	<table width="100%" align="center">
		<tr>
            <td>
                <dx:ASPxCallback ID="cbProcess" runat="server" ClientInstanceName="cbProcess" OnCallback="cbProcess_Callback">
                    <ClientSideEvents CallbackComplete="function(s, e) {
                        btnProcessDownload.SetEnabled(true);
                        downloadTimer.SetEnabled(false);
                        //cbGetStatus.PerformCallback();
                        lblStatus.SetText('Download and Import complete.<br />' + lblStatus.GetText());
                     }" />
                </dx:ASPxCallback>
                <dx:ASPxButton ID="btnProcessDownload" runat="server" Text="Process Download" AutoPostBack="false" ClientInstanceName="btnProcessDownload">
                    <ClientSideEvents Click="function(s, e) {
                        s.SetEnabled(false);
	                    lblStatus.SetText('');
	                    //lblStatus.SetText('Process completion: 0% ');
	                    lblStatus.SetClientVisible(true);
	                    downloadTimer.SetEnabled(true);
                        cbProcess.PerformCallback();
                    }" />
                </dx:ASPxButton>
                <dx:ASPxCallback ID="cbGetStatus" runat="server" ClientInstanceName="cbGetStatus" OnCallback="cbGetStatus_Callback">
                    <ClientSideEvents CallbackComplete="function(s, e) {
                        var labelText = lblStatus.GetText();
						//alert(e.result);
                        //var alreadyComplete = (labelText.substring(0, 19) == 'Download and Import');
                        //alert(alreadyComplete);
                        //if (!alreadyComplete) {
	                    //if (!String.IsNullOrEmpty(e.result))
						//{
							lblStatus.SetText(e.result);
	                    //}
						//}
                    }" />
                </dx:ASPxCallback>
                <dx:ASPxTimer ID="downloadTimer" runat="server" ClientInstanceName="downloadTimer" Enabled="false" Interval="1000">
                    <ClientSideEvents Tick="function(s, e) {
                        //alert('hi');
	                    cbGetStatus.PerformCallback();
                    }" />
                </dx:ASPxTimer>
            </td>
		</tr>
        <tr>
            <td>
                <dx:ASPxLabel ID="lblStatus" runat="server" Text="" ClientVisible="false" ClientInstanceName="lblStatus"></dx:ASPxLabel>
            </td>
        </tr>
        <%--<tr>
			<asp:UpdatePanel ID="pnlBase" runat="server">
				<ContentTemplate>
					<asp:Timer ID="tmrBase" runat="server" Interval="2000" OnTick="tmrBase_Tick" />
					&nbsp;<br /><b>Base</b><br /><asp:Label ID="lblBase" runat="server">&nbsp; &nbsp;Loading...</asp:Label>
				</ContentTemplate>
			</asp:UpdatePanel>
		</tr>
		<tr>
			<asp:UpdatePanel ID="pnlAltPay" runat="server">
				<ContentTemplate>
					<asp:Timer ID="tmrAltPay" runat="server" Interval="2000" OnTick="tmrAltPay_Tick" />
					&nbsp;<br /><b>Alt Pay</b><br /><asp:Label ID="lblAltPay" runat="server">&nbsp; &nbsp;Loading...</asp:Label>
				</ContentTemplate>
			</asp:UpdatePanel>
		</tr>
		<tr>
			<asp:UpdatePanel ID="pnlVolunteerAdult" runat="server">
				<ContentTemplate>
					<asp:Timer ID="tmrVolunteerAdult" runat="server" Interval="2000" OnTick="tmrVolunteerAdult_Tick" />
					&nbsp;<br /><b>Volunteer Adult</b><br /><asp:Label ID="lblVolunteerAdult" runat="server">&nbsp; &nbsp;Loading...</asp:Label>
				</ContentTemplate>
			</asp:UpdatePanel>
		</tr>
		<tr>
			<asp:UpdatePanel ID="pnlVolunteerUnder18" runat="server">
				<ContentTemplate>
					<asp:Timer ID="tmrVolunteerUnder18" runat="server" Interval="2000" OnTick="tmrVolunteerUnder18_Tick" />
					&nbsp;<br /><b>Volunteer - Under 18</b><br /><asp:Label ID="lblVolunteerUnder18" runat="server">&nbsp; &nbsp;Loading...</asp:Label>
				</ContentTemplate>
			</asp:UpdatePanel>
		</tr>
		<tr>
			<asp:UpdatePanel ID="pnlLog" runat="server">
				<ContentTemplate>
					<asp:Timer ID="tmrLog" runat="server" Interval="2000" OnTick="tmrLog_Tick" />
					&nbsp;<br /><b>Log</b><br /><asp:Label ID="lblLog" runat="server">&nbsp; &nbsp;Loading...</asp:Label>
				</ContentTemplate>
			</asp:UpdatePanel>
		</tr>--%>
	</table>
</asp:Content>

